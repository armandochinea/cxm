﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Maps.Web;
using System.Web.Script.Serialization;
using CXM.Module.BusinessObjects.CxmDataModelCode;

namespace CXM.Module.Web.Controllers
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class ShipmentDropsMapViewController : ViewController
	{
		public ShipmentDropsMapViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.

			WebMapsListEditor mapsListEditor = ((ListView)View).Editor as WebMapsListEditor;
			if (mapsListEditor != null)
			{
				mapsListEditor.MapViewer.MapSettings.Provider = MapProvider.Google;
				//mapsListEditor.MapViewer.MapSettings.BingApiKey = "AlP6bnPs2k3Q-jijC3NA1JnSMIDuPQYZz63GRNDMM0tmqI-wwgOudng1fPAIcSFb";
				//mapsListEditor.MapViewer.MapSettings.GoogleApiKey = "AIzaSyCWUqG1NteTJRNfb_FkZpO-cPjzdv6M0Zc";
				mapsListEditor.MapViewer.MapSettings.RouteSettings.Enabled = true;
				mapsListEditor.MapViewer.ClientSideEvents.Customize = GetCustomizeScript();
			}
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();
		}
		private string GetCustomizeScript()
		{

			if (((ListView)View).CollectionSource.GetCount() == 0)
			{
				return String.Empty;
			}

			var jsSerializer = new JavaScriptSerializer();

			ShipmentDrop[] markersArray = new ShipmentDrop[((ListView)View).CollectionSource.GetCount()];
			((ListView)View).CollectionSource.List.CopyTo(markersArray, 0);

			var colorsList = markersArray.Select(d =>
			{
				if (1 == 0) //d.Shipment.NextDrop != null && d.Id == d.Shipment.NextDrop.Id)
				{
					return "FFB700";
				}
				if (d.DropStatus == ShipmentDropStatus.Canceled)
				{
					return "716F6F";
				}
				return d.DropStatus == ShipmentDropStatus.InRoute ? "FF3D00" : "05B76E";
			}).ToList();

			string routes = jsSerializer.Serialize(GetRoutes(markersArray));
			string markerColors = jsSerializer.Serialize(colorsList);

			//var truck = markersArray.First().Shipment.Truck;
			//var color = truck.Color.Name.Substring(2).ToUpper();


			return string.Format(
				@"function(sender, map) {{
                    map.on('ready', function(e) {{
                        map.option('markerIconSrc', 'http://js.devexpress.com/Demos/RealtorApp/images/map-marker.png');   
                        map.option('routes',{0});
                        var markers = map.option('markers'); 
                        var markerColors = {1};
                        markers.forEach( function( item, idx) {{
                            var color = markerColors[idx];
                            item.iconSrc = 'http://www.googlemapsmarkers.com/v1/' + ( idx + 1 ).toString() + '/' + color + '/FFFFFF/FFFFFF/';
                        }});
                    }});
                }}", routes, markerColors);

		}
		private List<Object> GetRoutes(ShipmentDrop[] drops)
		{
			List<Object> routes = new List<object>();
			List<Object> locations = new List<object>();
			List<Object> startLocations = new List<object>();

			var markers = drops.OrderBy(d => d.DeliveryOrder).ToList();

			if (markers.Count == 0)
			{
				return routes;
			}

			IMapsMarker firstMarker = (IMapsMarker)markers[0];
			//var truck = drops.First().Shipment.Truck;

			//
			var truckColor = "#FF0000"; //+ truck.Color.Name.ToLower().Substring(2);
			//
			//startLocations.Add(new { lat = truck.DepartureLatitude, lng = truck.DepartureLongitude });
			startLocations.Add(new { lat = firstMarker.Latitude, lng = firstMarker.Longitude });
			routes.Add(new { color = truckColor, locations = startLocations });
			//
			foreach (IMapsMarker marker in markers)
			{
				locations.Add(new { lat = marker.Latitude, lng = marker.Longitude });
			}
			routes.Add(new { color = truckColor, locations = locations });

			return routes;
		}
	}
}
