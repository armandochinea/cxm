﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
namespace CXM.Module.BusinessObjects.CXM
{

	public partial class Actions : XPLiteObject
	{
		public struct ActionIDStruct
		{
			[Association(@"ActionsReferencesIssues")]
			[Persistent("IssueID")]
			public Issues IssueID { get; set; }
			[Persistent("ActivityDate")]
			public DateTime ActivityDate { get; set; }
		}
		[Key, Persistent]
		public ActionIDStruct ActionID;
		string fUserID;
		public string UserID
		{
			get { return fUserID; }
			set { SetPropertyValue<string>("UserID", ref fUserID, value); }
		}
		string fDescription;
		[Size(SizeAttribute.Unlimited)]
		public string Description
		{
			get { return fDescription; }
			set { SetPropertyValue<string>("Description", ref fDescription, value); }
		}
	}

}
