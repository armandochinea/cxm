﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.ExpressApp;
using System.Linq;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.DC;
using static CXM.Module.Enums;

namespace CXM.Module.BusinessObjects.CXM
{
	public partial class SalesOrder
	{
		public SalesOrder(Session session) : base(session) { }
		public override void AfterConstruction() {
			base.AfterConstruction();
			OrderDate = DateTime.Now;
			ShipDate = OrderDate.AddDays(1);
			Comments = string.Empty;
			TransStatus = Trans_Status.New;
			TransType = TransTypes.NormalOrder;
			CreatedBy = SecuritySystem.CurrentUserName;
			HasErrorMessages = false;
			HasMessages = false;
			OrderPO = string.Empty;
			ServerPricingStatus = 0;
            CollectionReference = string.Empty;
			PrePaid = false;
			ContactEmail = string.Empty;
		}

		protected override void OnChanged(string propertyName, object oldValue, object newValue)
		{
			base.OnChanged(propertyName, oldValue, newValue);

			if (propertyName == "Customer")
			{
				if (newValue != null && newValue != oldValue)
				{
					WhsID = (newValue as Customers).WhsID;
				}
			}
		}
		protected override void OnSaving()
		{
			base.OnSaving();
			
		}

		public String OrderNumber
		{
			get {
				return this.ID.ToString().PadLeft(8, '0');
			}
		}

		public Decimal Amount
		{
			get
			{
				return this.SalesLines.Sum(l => l.ExtPrice);
			}
		}

		private Trans_Status fTransStatus;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public Trans_Status TransStatus
		{
			get
			{
				return fTransStatus;
			}
			set
			{
				SetPropertyValue<Trans_Status>("TransStatus", ref fTransStatus, value);
			}
		}

		private TransTypes fTransType;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public TransTypes TransType
		{
			get
			{
				return fTransType;
			}
			set
			{
				SetPropertyValue<TransTypes>("TransType", ref fTransType, value);
			}
		}

		private string fCreatedBy;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public string CreatedBy
		{
			get
			{
				return fCreatedBy;
			}
			set
			{
				SetPropertyValue<string>("CreatedBy", ref fCreatedBy, value);
			}
		}

		private Route fRoute;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false), DataSourceProperty("Routes")]
		public Route Route
		{
			get
			{
				return fRoute;
			}
			set
			{
				SetPropertyValue<Route>("Route", ref fRoute, value);
			}
		}

		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false), Delayed]
		XPCollection<Route_Customer> RouteCustomer
		{
			get
			{
				CriteriaOperator op = XPQuery<Route_Customer>.TransformExpression(Session, r => r.Customer == this.Customer && r.Customer.Active == true);
				var routeCustomer = new XPCollection<Route_Customer>(Session, op);
				return routeCustomer;
			}
		}

		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false), Delayed]
		XPCollection<Route> AllRoutes
		{
			get
			{
				return new XPCollection<Route>(Session);
			}
		}

		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false), Delayed]
		List<Route> Routes
		{
			get
			{
				List<Route> routes = new List<Route>();

				if (RouteCustomer.Count == 0)
				{
					foreach (var route in AllRoutes)
					{
						routes.Add(route);
					}
				}
				else
				{
					foreach (var route in RouteCustomer)
					{
						routes.Add(route.Route);
					}
				}

				return routes;
			}
		}

		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public Decimal TotalSalesTax
		{
			get
			{
				return this.SalesLines.Sum(l => l.SalesTax);
			}
		}

		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public Decimal OrderTotalAmount
		{
			get
			{
				return this.Amount + this.TotalSalesTax;
			}
		}

		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public Decimal TotalDiscount
		{
			get
			{
				Decimal tDiscount = 0;

				foreach (var salesline in this.SalesLines)
				{
					tDiscount += (Decimal)(salesline.Qty * salesline.Discount);
				}
				return tDiscount;
			}
		}

		private bool fHasErrorMessages;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public bool HasErrorMessages
		{
			get
			{
				return fHasErrorMessages;
			}
			set
			{
				SetPropertyValue<bool>("HasErrorMessages", ref fHasErrorMessages, value);
			}
		}

		private bool fHasMessages;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public bool HasMessages
		{
			get
			{
				return fHasMessages;
			}
			set
			{
				SetPropertyValue<bool>("HasMessages", ref fHasMessages, value);
			}
		}

		private int fServerPricingStatus;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public int ServerPricingStatus
		{
			get
			{
				return fServerPricingStatus;
			}
			set
			{
				SetPropertyValue<int>("ServerPricingStatus", ref fServerPricingStatus, value);
			}
		}

		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public string AmountDesc => Amount.ToString("$#,##0.00");

		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public string TotalSalesTaxDesc => TotalSalesTax.ToString("$#,##0.00");

		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public string OrderTotalAmountDesc => OrderTotalAmount.ToString("$#,##0.00");

		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public string TotalDiscountDesc => TotalDiscount.ToString("$#,##0.00");

        private string fCollectionReference;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string CollectionReference
        {
            get { return fCollectionReference; }
            set { SetPropertyValue<string>("CollectionReference", ref fCollectionReference, value); }
        }

		private string fContactEmail;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public string ContactEmail
		{
			get { return fContactEmail; }
			set { SetPropertyValue<string>("ContactEmail", ref fContactEmail, value); }
		}

		private bool fPrePaid;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public bool PrePaid
		{
			get { return fPrePaid; }
			set { SetPropertyValue<bool>("PrePaid", ref fPrePaid, value); }
		}
	}
}
