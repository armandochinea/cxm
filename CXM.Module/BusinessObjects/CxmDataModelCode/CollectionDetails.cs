﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using static CXM.Module.Enums;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using DevExpress.Persistent.Base;
using System.Linq;

namespace CXM.Module.BusinessObjects.CXM
{

    public partial class CollectionDetails
    {
        public CollectionDetails(Session session) : base(session) { }
        public override void AfterConstruction() {
            base.AfterConstruction();

            this.PaymentType = PaymentTypes.ACH;
            this.AccountType = AccountTypes.Checking;
            this.RoutingNumber = string.Empty;
            this.AccountNumber = string.Empty;
            this.CardholderName = string.Empty;
            this.CardExpYear = DateTime.Now.Year + 1;
            this.OrderReference = string.Empty;
        }

        PaymentTypes fPaymentType;
        [ImmediatePostData]
        public PaymentTypes PaymentType
        {
            get { return fPaymentType; }
            set { SetPropertyValue<PaymentTypes>("PaymentType", ref fPaymentType, value); }
        }

        string fRoutingNumber;
        public string RoutingNumber
        {
            get { return fRoutingNumber; }
            set { SetPropertyValue<string>("RoutingNumber", ref fRoutingNumber, value); }
        }

        string fAccountNumber;
        public string AccountNumber
        {
            get { return fAccountNumber; }
            set { SetPropertyValue<string>("AccountNumber", ref fAccountNumber, value); }
        }

        AccountTypes fAccountType;
        [ImmediatePostData]
        public AccountTypes AccountType
        {
            get { return fAccountType; }
            set { SetPropertyValue<AccountTypes>("AccountType", ref fAccountType, value); }
        }

        string fCardNumber;
        public string CardNumber
        {
            get { return fCardNumber; }
            set { SetPropertyValue<string>("CardNumber", ref fCardNumber, value); }
        }

        Enums.Months fCardExpMonth;
        [ImmediatePostData]
        public Enums.Months CardExpMonth
        {
            get { return fCardExpMonth; }
            set { SetPropertyValue<Enums.Months>("CardExpMonth", ref fCardExpMonth, value); }
        }

        int fCardExpYear;
        [ImmediatePostData]
        public int CardExpYear
        {
            get { return fCardExpYear; }
            set { SetPropertyValue<int>("CardExpYear", ref fCardExpYear, value); }
        }

        string fCardholderName;
        public string CardholderName
        {
            get { return fCardholderName; }
            set { SetPropertyValue<string>("CardholderName", ref fCardholderName, value); }
        }

        public string CardExpDesc
        {
            get
            {
                if (CardExpMonth == Months.UnSet)
                {
                    return string.Empty;
                }
                return CardExpMonth.ToString() + " " + CardExpYear.ToString();
            }
        }

        public string PaymentAmountDesc => PaymentAmount.ToString("$#,##0.00");

        public List<Account> SavedAccounts
        {
            get { return Session.Query<Account>().Where(r => r.Customer.CustomerID == this.Collection.Customer.CustomerID && r.IsActive == true).ToList(); }
        }

        private Account fSavedAccount;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false), DataSourceProperty("SavedAccounts"), ImmediatePostData]
        public Account SavedAccount
        {
            get { return fSavedAccount; }
            set { SetPropertyValue<Account>("SavedAccount", ref fSavedAccount, value); }
        }

        private string fOrderReference;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string OrderReference
        {
            get { return fOrderReference; }
            set { SetPropertyValue<string>("OrderReference", ref fOrderReference, value); }
        }

		private string fCVVNumber;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public string CVVNumber
		{
			get { return fCVVNumber; }
			set { SetPropertyValue<string>("CVVNumber", ref fCVVNumber, value); }
		}

		protected override void OnChanged(string propertyName, object oldValue, object newValue)
        {
            base.OnChanged(propertyName, oldValue, newValue);

            if (propertyName == "PaymentType")
            {
                if ((PaymentTypes)newValue == PaymentTypes.CreditCard)
                {
                    if (AccountType == AccountTypes.Checking || AccountType == AccountTypes.None)
                    {
                        AccountType = AccountTypes.Visa;
                    }
                } else if ((PaymentTypes)newValue == PaymentTypes.ACH)
                {
                    AccountType = AccountTypes.Checking;
                }
            }
            if (propertyName == "CardExpYear")
            {
                if ((int)newValue != -1 && (int)newValue < DateTime.Now.Year)
                {
                    CardExpYear = DateTime.Now.Year;

                    if (CardExpMonth < (Enums.Months)DateTime.Now.Month)
                    {
                        CardExpMonth = Enums.Months.UnSet;
                    }
                }
            }
            if (propertyName == "CardExpMonth")
            {
                if (CardExpYear == DateTime.Now.Year && (int)newValue < DateTime.Now.Month)
                {
                    CardExpMonth = (Enums.Months)DateTime.Now.Month;
                }
            }
            if (propertyName == "AccountType")
            {
                if ((AccountTypes)newValue == AccountTypes.None || (PaymentType == PaymentTypes.CreditCard && (AccountTypes)newValue == AccountTypes.Checking) || (PaymentType == PaymentTypes.ACH && (AccountTypes)newValue != AccountTypes.Checking))
                {
                    AccountType = (AccountTypes)oldValue;
                }
            }
        }
    }
}
