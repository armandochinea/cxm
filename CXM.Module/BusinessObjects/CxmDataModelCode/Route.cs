﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Persistent.Base;

namespace CXM.Module.BusinessObjects.CXM
{
	[DefaultProperty("RouteDescription")]
	public partial class Route
	{
		public Route(Session session) : base(session) { }
		public override void AfterConstruction() { base.AfterConstruction(); }

		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public string RouteDescription => RouteNo + " - " + Name ?? String.Empty;

		private string fDivCode;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public string DivCode
		{
			get { return fDivCode; }
			set { SetPropertyValue<string>("DivCode", ref fDivCode, value); }
		}

		private decimal fOrderAmountMinimum;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public decimal OrderAmountMinimum
		{
			get { return fOrderAmountMinimum; }
			set { SetPropertyValue<decimal>("OrderAmountMinimum", ref fOrderAmountMinimum, value); }
		}
	}
}
