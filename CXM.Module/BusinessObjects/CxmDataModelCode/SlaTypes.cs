﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Persistent.Base;

namespace CXM.Module.BusinessObjects.CXM
{
    [DefaultProperty("Type")]
    public partial class SlaTypes
    {
        public SlaTypes(Session session) : base(session) { }

        public override void AfterConstruction() {
            base.AfterConstruction();
        }
    }
}