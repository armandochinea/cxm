﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Persistent.Base;

namespace CXM.Module.BusinessObjects.CXM
{
    [ImageName("BO_Invoice"), DefaultProperty("InvoiceID")]
    public partial class Invoice
    {
        public Invoice(Session session) : base(session) { }
        public override void AfterConstruction() { base.AfterConstruction(); }

		public string ShipTo => String.Format("{0} {1} {2}, {3} {4}", CustomerID.Address1, CustomerID.Address2, CustomerID.City, CustomerID.State, CustomerID.ZipCode);

		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public string AgingDesc
		{
			get
			{
				if (this.CurrentDays <= 30)
				{
					return "30 days";
				}
				else if (this.CurrentDays > 30 && this.CurrentDays <= 45)
				{
					return "31 to 45 days";
				}
				else if (this.CurrentDays > 46 && this.CurrentDays <= 60)
				{
					return "46 to 60 days";
				}
				else if (this.CurrentDays > 60 && this.CurrentDays <= 90)
				{
					return "61 to 90 days";
				}
				else if (this.CurrentDays > 90 && this.CurrentDays <= 120)
				{
					return "91 to 120 days";
				}
				else if (this.CurrentDays > 120)
				{
					return "Over 120 days";
				}
				else
				{
					return string.Empty;
				}
			}
		}
    }

}
