﻿using CXM.Module.BusinessObjects.CXM;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CXM.Module.BusinessObjects.CxmDataModelCode
{
	[Persistent("vwShipmentLine")]
	public class ShipmentLine : XPLiteObject
	{
		public ShipmentLine(Session session) : base(session) { }

		[Key]
		[VisibleInListView(false), VisibleInDetailView(false)]
		public string ID { get; set; }
		[VisibleInListView(false), VisibleInDetailView(false)]
		public long ShipmentDrop { get; set; }
		[VisibleInListView(false), VisibleInDetailView(false)]
        public long Product { get; set; }

        public long ShipmentNumber { get; set; }
        public string LineId { get; set; }

        public string Code { get; set; }
		public string ProductErpID { get; set; }
		public string Description { get; set; }
		public double Qty { get; set; }
	}
}
