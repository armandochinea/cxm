﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Persistent.Base;
using System.Linq;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using DevExpress.ExpressApp;
using static CXM.Module.Enums;

namespace CXM.Module.BusinessObjects.CXM
{
    public partial class CustomerPlan
	{
		public CustomerPlan(Session session) : base(session) { }
		public override void AfterConstruction()
		{
			base.AfterConstruction();
			Active = true;
		}

		FrequencyType fFrequency;
		[ImmediatePostData]
		public FrequencyType Frequency
		{
			get { return fFrequency; }
			set { SetPropertyValue<FrequencyType>("Frequency", ref fFrequency, value); }
		}

		CxmUser fAgent;
		[VisibleInDetailView(false), VisibleInListView(false), DataSourceProperty("Agents")]
		public CxmUser Agent
		{
			get { return fAgent; }
			set { SetPropertyValue<CxmUser>("Agent", ref fAgent, value); }
		}

		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false), Delayed]
		List<CxmUser> Agents
		{
			get
			{
				List<CxmUser> agents = new List<CxmUser>();

				PermissionPolicyRole multiAgentRole = Session.Query<PermissionPolicyRole>().Where(r => r.Name == "MultiAgent").FirstOrDefault();
				if (multiAgentRole != null)
				{
					foreach (PermissionPolicyUser mUser in multiAgentRole.Users)
					{
						agents.Add(Session.Query<CxmUser>().Where(u => u.Oid == mUser.Oid).FirstOrDefault());
					}
				}

				agents.AddRange(Session.Query<CxmUser>().Where(r => r.Supervisor == SecuritySystem.CurrentUserId).ToList());

				return agents;
			}
		}

		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public string PlanDescription
		{
			get
			{
				var description = "";
				
				if (Frequency == FrequencyType.Weekly)
				{
					description += "Wk";
				}
				else if (Frequency == FrequencyType.BiWeekly)
				{
					description += "BW";

					if (Even)
					{
						description += " ( Even )";
					}
					if (Odd)
					{
						description += " ( Odd )";
					}
				}
				else if (Frequency == FrequencyType.Monthly)
				{
					description += "MO";

					description += " (";
					if (First) description += " 1 ";
					if (Second) description += " 2 ";
					if (Third) description += " 3 ";
					if (Fourth) description += " 4 ";
					description += ")";
				}

				if (Monday) description += " Mon";
				if (Tuesday) description += " Tue";
				if (Wednesday) description += " Wed";
				if (Thursday) description += " Thu";
				if (Friday) description += " Fri";
				if (Saturday) description += " Sat";

				description.Trim();

				if (description.Length == 0) description = "Not Defined";

				return description;
			}
		}

		private bool fBefore9;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public bool Before9
		{
			get
			{
				return fBefore9;
			}
			set
			{
				SetPropertyValue<bool>("Before9", ref fBefore9, value);
			}
		}

		private bool fBetween9and12;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public bool Between9and12
		{
			get
			{
				return fBetween9and12;
			}
			set
			{
				SetPropertyValue<bool>("Between9and12", ref fBetween9and12, value);
			}
		}

		private bool fBetween12and3;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public bool Between12and3
		{
			get
			{
				return fBetween12and3;
			}
			set
			{
				SetPropertyValue<bool>("Between12and3", ref fBetween12and3, value);
			}
		}

		private bool fAfter3;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public bool After3
		{
			get
			{
				return fAfter3;
			}
			set
			{
				SetPropertyValue<bool>("After3", ref fAfter3, value);
			}
		}

		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public string CallTimeFrame
		{
			get
			{
				var retval = "";

				if (Before9)
				{
					retval = "Before 9:00 am";
				}

				if (Between9and12)
				{
					retval += retval.Length > 0 ? ", 9:00 AM - 12:00 PM" : "9:00 AM - 12:00 PM";
				}

				if (Between12and3)
				{
					retval += retval.Length > 0 ? ", 12:00 PM - 3:00 PM" : "12:00 PM - 3:00 PM";
				}

				if (After3)
				{
					retval += retval.Length > 0 ? ", After 3:00 PM" : "After 3:00 PM";
				}

				return retval.Length > 0 ? retval : "N/A";
			}
		}

		private DateTime fLastUpdated;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public DateTime LastUpdated
		{
			get { return fLastUpdated; }
			set { SetPropertyValue<DateTime>("LastUpdated", ref fLastUpdated, value); }
		}

		protected override void OnChanged(string propertyName, object oldValue, object newValue)
		{
			base.OnChanged(propertyName, oldValue, newValue);

			if (propertyName == "Frequency")
			{
				if ((FrequencyType)newValue == FrequencyType.Weekly)
				{
					Even = false;
					Odd = false;
					First = false;
					Second = false;
					Third = false;
					Fourth = false;
				}
				else if ((FrequencyType)newValue == FrequencyType.BiWeekly)
				{
					First = false;
					Second = false;
					Third = false;
					Fourth = false;
				}
				else if ((FrequencyType)newValue == FrequencyType.Monthly)
				{
					Even = false;
					Odd = false;
				}
			}

			LastUpdated = DateTime.Now;
		}
	}
}
