﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using DevExpress.ExpressApp;

namespace CXM.Module.BusinessObjects.CXM
{

    public partial class Actions
    {
        public Actions(Session session) : base(session) { }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

			UserID = SecuritySystem.CurrentUserName;
			ActionID.ActivityDate = DateTime.Now;
        }

		public string UserFullName
		{
			get
			{
				var name = "";
				if (UserID != null)
				{
					name = Session.Query<CxmUser>().Where(r => r.UserName == UserID).FirstOrDefault().FullName;
				}
				return name;
			}
		}
    }
}