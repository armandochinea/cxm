﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Persistent.Base;
using System.Linq;
using DevExpress.Xpo.DB;
using DevExpress.ExpressApp;
//using CXM.Module.BusinessObjects.BOHelper;

namespace CXM.Module.BusinessObjects.CXM
{
    [DefaultProperty("Description"), ImageName("BO_Customer")]
    public partial class Customers
    {
        public Customers(Session session) : base(session) { }

        public override void AfterConstruction() { base.AfterConstruction(); }

        #region Properties
        public string Description => string.Format("{0} - {1}", CustomerNo, Name);

        [VisibleInDetailView(false), VisibleInListView(false)]
        public string FullAddress => string.Format("{0} {1} {2}, {3} {4}", Address1, Address2, City, State, ZipCode);

        // Customer's issues properties
        [VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public string IsActive => Active ? "YES" : "NO";

        [VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public string IsPORequired => PORequired ? "YES" : "NO";

        [VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public string CreditLimitDescription => CreditLimit.ToString(@"$#,##0.00");

		private Boolean fUseSalesTax;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public Boolean UseSalesTax
		{
			get
			{
				return fUseSalesTax;
			}
			set
			{
				SetPropertyValue<Boolean>("UseSalesTax", ref fUseSalesTax, value);
			}
		}

		private string fCertMunicipal;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public string CertMunicipal
		{
			get
			{
				return fCertMunicipal;
			}
			set
			{
				SetPropertyValue<string>("CertMunicipal", ref fCertMunicipal, value);
			}
		}

		private string fCertEstatal;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public string CertEstatal
		{
			get
			{
				return fCertEstatal;
			}
			set
			{
				SetPropertyValue<string>("CertEstatal", ref fCertEstatal, value);
			}
		}

		private DateTime fCertMunicipalExpDate;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public DateTime CertMunicipalExpDate
		{
			get
			{
				return fCertMunicipalExpDate;
			}
			set
			{
				SetPropertyValue<DateTime>("CertMunicipalExpDate", ref fCertMunicipalExpDate, value);
			}
		}

		private float fStateTaxPercent;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public float StateTaxPercent
		{
			get
			{
				return fStateTaxPercent;
			}
			set
			{
				SetPropertyValue<float>("StateTaxPercent", ref fStateTaxPercent, value);
			}
		}

		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public string PlanDays
		{
			get
			{
				var customerPlan = Session.Query<CustomerPlan>().Where(r => r.Customer.CustomerID == CustomerID).FirstOrDefault();

				if (customerPlan != null)
				{
					return customerPlan.PlanDescription;
				}

				return "Not Defined";
			}
		}

		private string tNotes;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false), Size(SizeAttribute.Unlimited)]
		public string Notes
		{
			get { return tNotes; }
			set { SetPropertyValue<string>("Notes", ref tNotes, value); }
		}

		private int tPaymentTermsDays;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public int PaymentTermsDays
		{
			get { return tPaymentTermsDays; }
			set { SetPropertyValue<int>("PaymentTermsDescription", ref tPaymentTermsDays, value); }
		}

        private bool fHasVaultAccount;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public bool HasVaultAccount
        {
            get { return fHasVaultAccount; }
            set { SetPropertyValue<bool>("HasVaultAccount", ref fHasVaultAccount, value); }
        }

        #region Invoice Info
        // Customer's invoices properties
        [VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
        public int ParamInvoiceCount
        {
            get
            {
                int paramInvoiceCount;
                int.TryParse(Session.Query<GlobalParameters>().Where(r => r.Name == "INVOICE_COUNT").FirstOrDefault()?.Value, out paramInvoiceCount);
                return paramInvoiceCount;
            }
        }

        [VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false), Delayed]
		public XPCollection<Invoice> LastInvoices
		{
			get
			{
				CriteriaOperator op = XPQuery<Invoice>.TransformExpression(Invoices.Session, r => r.CustomerID.CustomerID == CustomerID);
				var list = new XPCollection<Invoice>(Invoices.Session, op);
				list.Sorting.Add(new SortProperty("InvoiceDate", DevExpress.Xpo.DB.SortingDirection.Descending));
				list.TopReturnedObjects = ParamInvoiceCount;
				return list;
			}
		}

		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
        public int ParamPercProductsForOrder
        {
            get
            {
                int paramPercProductsForOrder;
                int.TryParse(Session.Query<GlobalParameters>().Where(r => r.Name == "PERC_PRODUCTS_FOR_ORDER").FirstOrDefault()?.Value, out paramPercProductsForOrder);
                return paramPercProductsForOrder;
            }
        }
        
		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
        public decimal LastInvoicesAmount
        {
            get
            {
                decimal lastInvoicesAmount = 0;
                foreach (Invoice invoice in LastInvoices)
                {
                    lastInvoicesAmount += invoice.Amount;
                }
                return lastInvoicesAmount;
            }
        }

        [VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
        public string LastInvoicesAmountDescription
        {
            get
            {
                return string.Format("{0} in last {1} invoices.", LastInvoicesAmount.ToString(@"$#,##0.00"), CustomerInvoiceCount.ToString());
            }

        }

        [VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
        public int CustomerInvoiceCount
        {
            get
            {
                int invoiceCount = ParamInvoiceCount;
                if (LastInvoices.Count < invoiceCount)
                {
                    invoiceCount = LastInvoices.Count;
                }
                return invoiceCount;
            }

        }

		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false), Delayed]
		public XPCollection<Invoice> OpenInvoices
		{
			get
			{
				CriteriaOperator op = XPQuery<Invoice>.TransformExpression(Invoices.Session, r => r.CustomerID.CustomerID == CustomerID && r.OpenAmount != 0);
				var list = new XPCollection<Invoice>(Invoices.Session, op);
				list.Sorting.Add(new SortProperty("InvoiceDate", DevExpress.Xpo.DB.SortingDirection.Descending));
				return list;
			}
		}

        [VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
        public decimal OpenBalance
        {
            get
            {
                decimal openBalance = 0;
                foreach (Invoice invoice in OpenInvoices)
                {
                    openBalance += invoice.OpenAmount;
                }
                return openBalance;
            }
        }

        [VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
        public string OpenBalanceDescription => OpenBalance.ToString(@"$#,##0.00");

        [VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false), Delayed]
        public XPCollection<Shipment> Shipments
        {
            get
            {
                Session mSession = this.Session;
                CriteriaOperator op = XPQuery<Shipment>.TransformExpression(mSession, r => r.Customer.CustomerID == CustomerID);
                var customerShipments = new XPCollection<Shipment>(mSession, op);
                return customerShipments;
            }
        }

        [VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false), Delayed]
		public XPCollection<Invoice> DueInvoices
        {
            get
            {
				CriteriaOperator op = XPQuery<Invoice>.TransformExpression(Invoices.Session, r => r.CustomerID.CustomerID == CustomerID && r.OpenAmount != 0 && r.CurrentDays >= PaymentTermsDays);
                var list = new XPCollection<Invoice>(Invoices.Session, op);
                list.Sorting.Add(new SortProperty("InvoiceDate", DevExpress.Xpo.DB.SortingDirection.Descending));
                return list;
            }
        }

        [VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public decimal DueBalance
        {
            get
            {
				decimal dueBalance = 0;
				foreach (Invoice invoice in DueInvoices)
				{
					dueBalance += invoice.OpenAmount;
				}
				return dueBalance;
            }
        }

        [VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public string DueBalanceDescription => DueBalance.ToString(@"$#,##0.00");
		#endregion

		#region Issues
		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false), Delayed]
        public XPCollection<Issues> LastIssues
        {
            get
            {
                Session mSession = this.Session;
                CriteriaOperator op = XPQuery<Issues>.TransformExpression(mSession, r => r.CustomerID.CustomerID == CustomerID);
                var list = new XPCollection<Issues>(mSession, op);
                list.Sorting.Add(new SortProperty("IssueID", SortingDirection.Descending));
                return list;
            }
        }

        [VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
        public Issues LastIssue
        {
            get
            {
                return LastIssues.FirstOrDefault();
            }
        }

		// Customer's issues properties
        [VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public int TotalIssuesThisYear => Session.Query<Issues>().Where(r => r.CustomerID.CustomerID == CustomerID && r.IssueDate.Year == DateTime.Now.Year).Count();

        [VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public int TotalOpenIssuesThisYear => Session.Query<Issues>().Where(r => r.CustomerID.CustomerID == CustomerID && r.IssueDate.Year == DateTime.Now.Year && r.Status.Description == "OPEN").Count();

        [VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public int TotalResolvedIssuesThisYear => Session.Query<Issues>().Where(r => r.CustomerID.CustomerID == CustomerID && r.IssueDate.Year == DateTime.Now.Year && r.Status.Description == "RESOLVED").Count();

        [VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public int TotalCanceledIssuesThisYear => Session.Query<Issues>().Where(r => r.CustomerID.CustomerID == CustomerID && r.IssueDate.Year == DateTime.Now.Year && r.Status.Description == "CANCELED").Count();
		#endregion

		#region Order Status
		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false), Delayed]
		public XPCollection<OrderStatus> OrderStatus
        {
            get
            {
				Session mSession = this.Session;
				CriteriaOperator op = XPQuery<OrderStatus>.TransformExpression(mSession, r => r.Route == SecuritySystem.CurrentUserName && r.Customer == CustomerNo);
				var list = new XPCollection<OrderStatus>(mSession, op);
				list.Sorting.Add(new SortProperty("OrderDate", SortingDirection.Ascending));
				return list;
            }
        }
		#endregion

		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false), Delayed]
		public XPCollection<CustomerProducts> CustomerProducts
        {
            get
            {
				Session mSession = this.Session;
				CriteriaOperator op = XPQuery<CustomerProducts>.TransformExpression(mSession, r => r.CustomerID == CustomerID);
				var list = new XPCollection<CustomerProducts>(mSession, op);
				list.Sorting.Add(new SortProperty("Qty", SortingDirection.Descending));
				return list;
            }
        }

		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false), Delayed]
		public XPCollection<CustomerSpecialProducts> SpecialProducts
		{
			get
			{
				CriteriaOperator op = XPQuery<CustomerSpecialProducts>.TransformExpression(Session, r => r.Customer == this);
				var specialProducts = new XPCollection<CustomerSpecialProducts>(Session, op);
				return specialProducts;
			}
		}
		#endregion
	}
}
