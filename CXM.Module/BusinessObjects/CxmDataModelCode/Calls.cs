﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using System.Linq;
using DevExpress.ExpressApp.DC;

namespace CXM.Module.BusinessObjects.CXM
{
	public partial class Calls
    {
		public enum CallCategory
		{
			Complete = 0,
			Incomplete = 1,
			[XafDisplayName("No Answer")]
			NoAnswer = 2,
			Rejected = 3,
			Rescheduled = 4,
			[XafDisplayName("Off-Call Transactions")]
			OffCallTransactions = 5
		}

		public enum CallStatus
		{
			New = 0,
			InProgress = 1,
			Finished = 9
		}

        public Calls(Session session) : base(session) { }

        public override void AfterConstruction() {
            base.AfterConstruction();

			ID = Guid.NewGuid().ToString();
			Outgoing = true;
			Planned = true;
			StartDateTime = DateTime.Now.Date;
			EndDateTime = StartDateTime.Date;
			Attempted = false;
			CallNumber = string.Empty;
			Status = CallStatus.New;
        }

		/*##################################################################*/
		#region Properties
		string fContactName;
		[Size(200)]
		public string ContactName
		{
			get { return fContactName; }
			set { SetPropertyValue<string>("ContactName", ref fContactName, value); }
		}

		Customers fCustomerID;
		[ImmediatePostData]
		public Customers CustomerID
		{
			get { return fCustomerID; }
			set { SetPropertyValue<Customers>("CustomerID", ref fCustomerID, value); }
		}

		string fCustomerNumber;
		[VisibleInListView(false), VisibleInDetailView(false)]
		public string CustomerNumber
		{
			get { return fCustomerNumber; }
			set { SetPropertyValue<string>("CustomerNumber", ref fCustomerNumber, value); }
		}

		string fCalledPhoneNumber;
		[ImmediatePostData]
		public string CalledPhoneNumber
		{
			get { return fCalledPhoneNumber; }
			set { SetPropertyValue<string>("CalledPhoneNumber", ref fCalledPhoneNumber, value); }
		}

		Contacts fContact;
		[VisibleInListView(false), ImmediatePostData]
		public Contacts Contact
		{
			get { return fContact; }
			set
			{
				SetPropertyValue<Contacts>("Contact", ref fContact, value);
			}
		}

		CallCategory fCategory;
		[Persistent("Category")]
		public CallCategory Category
		{
			get
			{
				return fCategory;
			}
			set
			{
				SetPropertyValue<CallCategory>("Category", ref fCategory, value);
			}
		}

		CallStatus fStatus;
		[Persistent("Status"), VisibleInListView(false), VisibleInDetailView(false)]
		public CallStatus Status
		{
			get
			{
				return fStatus;
			}
			set
			{
				SetPropertyValue<CallStatus>("Status", ref fStatus, value);
			}
		}

		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public string CustomerName => CustomerID?.Name;

		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public double CallDuration => EndDateTime.Subtract(StartDateTime).TotalSeconds;

		[VisibleInListView(false), VisibleInLookupListView(false)]
		public string CallDurationDesc
		{
			get
			{
				var format = @"m\:ss";
				if (CallDuration >= 3600) { format = @"h\:m" + format; }
				return String.Format("{0} seconds ({1})", ((int)CallDuration).ToString("#,###"), TimeSpan.FromSeconds(CallDuration).ToString(format));
			}
		}

		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public string CustomerTitleForCall => string.Format("{0} | {1} | {2} | {3}", CustomerID?.CustomerNo, CustomerID?.Name, CustomerID?.FullAddress, CustomerID?.Phone);

		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public string ContactTitleForCall => string.Format("{0} | {1} | {2}", Contact?.Phone, Contact?.Name, Contact?.Title);

		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public int? TotalIssuesThisYear => CustomerID?.TotalIssuesThisYear;

		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public int? TotalOpenIssuesThisYear => CustomerID?.TotalOpenIssuesThisYear;

		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public int? TotalResolvedIssuesThisYear => CustomerID?.TotalResolvedIssuesThisYear;

		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public int? TotalCanceledIssuesThisYear => CustomerID?.TotalCanceledIssuesThisYear;

		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public string LastInvoicesAmountDescription => CustomerID?.LastInvoicesAmountDescription;

		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public string OpenBalanceDescription => CustomerID?.OpenBalanceDescription;

		//%%%%
		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false), Delayed]
		//public XPCollection<Issues> LastIssues => CustomerID?.LastIssues;
		public XPCollection<Issues> LastIssues => CustomerID?.Issues;

		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false), Delayed]
		public XPCollection<Invoice> LastInvoices => CustomerID?.LastInvoices;

		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false), Delayed]
		public XPCollection<Shipment> Shipments => CustomerID?.Shipments;

		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false), DevExpress.Xpo.Aggregated, Delayed]
		public XPCollection<Issues> Issues => CustomerID?.Issues;

		//%%%%
		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false), Delayed]
		public XPCollection<CustomerProducts> CustomerProducts => CustomerID?.CustomerProducts;

		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public string Type { get { return Outgoing ? "OUT" : "INC"; } }

		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public string OutgoingConfirmation { get { return string.Format("Do you wish to call {0} at {1}?", CustomerName, CalledPhoneNumber); } }

        [VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
        public string SalesRoute
        {
            get
            {
				if (this.CustomerID == null) return string.Empty;

                List<Route_Customer> routes = Session.Query<Route_Customer>().Where(r => r.Customer.CustomerID == this.CustomerID.CustomerID && r.Active == true).ToList();
                if (routes == null || routes.Count == 0) return string.Empty;
				
                return string.Format("{0} - {1}", routes[0].Route.RouteNo, routes[0].Route.Name);
            }
        }

        #endregion

        protected override void OnChanged(string propertyName, object oldValue, object newValue)
		{
			base.OnChanged(propertyName, oldValue, newValue);
			if (propertyName == "Contact")
			{
				ContactName = Contact?.Name;
				CustomerID = Contact?.CustomerID;
                if (Contact?.Phone.Length < 10 || string.IsNullOrWhiteSpace(Contact?.Phone))
                {
                    CalledPhoneNumber = CustomerID?.Phone;
                } else
                {
                    CalledPhoneNumber = Contact?.Phone;
                }
				CustomerNumber = CustomerID?.CustomerNo;
			}
			if (propertyName == "CustomerID")
			{
				CalledPhoneNumber = CustomerID?.Phone;
				CustomerNumber = CustomerID?.CustomerNo;
			}
		}
	}
}