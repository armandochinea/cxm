﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Persistent.Base;
using System.Linq;
using DevExpress.ExpressApp;

namespace CXM.Module.BusinessObjects.CXM
{
    public partial class Issues
    {
		public Issues(Session session) : base(session) { }

        public override void AfterConstruction() {
            base.AfterConstruction();

			IssueDate = DateTime.Now;
			LastUpdate = DateTime.Now;
			UserID = SecuritySystem.CurrentUserName;
			Status = Session.FindObject<IssueStatus>(new BinaryOperator("Description", "OPEN"));
			RequestedBy = string.Empty;
			ContactName = string.Empty;
			ContactPhone = string.Empty;
			Description = string.Empty;
		}

		[VisibleInListView(false), VisibleInDetailView(false)]
		public string CustomerDescription => String.Format("{0} - {1}", CustomerID?.CustomerNo, CustomerID?.Name);

		public string UserFullName
		{
			get
			{
				var name = "";
				if (UserID != null)
				{
					name = Session.Query<CxmUser>().Where(r => r.UserName == UserID).FirstOrDefault().FullName;
				}
				return name;
			}
		}

		IssueStatus fStatus;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false), DataSourceProperty("StatusList")]
		public IssueStatus Status
		{
			get { return fStatus; }
			set { SetPropertyValue<IssueStatus>("Status", ref fStatus, value); }
		}

		private bool fObjectChanged; 
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false), NonPersistent]
		public bool ObjectChanged
		{
			get { return fObjectChanged; }
			set { fObjectChanged = value; }
		}

		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public List<IssueStatus> StatusList
		{
			get	{ return Session.Query<IssueStatus>().ToList(); }
		}

		protected override void OnChanged(string propertyName, object oldValue, object newValue)
		{
			base.OnChanged(propertyName, oldValue, newValue);

			ObjectChanged = true;

			if (propertyName == "Reason")
			{
				Assignee = Reason?.DefaultAssignee;
			}
		}

		[VisibleInListView(false), VisibleInDetailView(false)]
		public string ResponseDesc
		{
			get
			{
				var sla = "N/A";
				if (Reason?.Response != null)
				{
					sla = Reason.Response.SlaDesc;
				}
				return string.Format("Response: {0}", sla);
			}
		}

		[VisibleInListView(false), VisibleInDetailView(false)]
		public string ResolutionDesc
		{
			get
			{
				var sla = "N/A";
				if (Reason?.Response != null)
				{
					sla = Reason.Resolution.SlaDesc;
				}
				return string.Format("Resolution: {0}", sla);
			}
		}
	}
}