﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Persistent.Base;

namespace CXM.Module.BusinessObjects.CXM
{
    [ImageName("BO_Contact")]
    public partial class Contacts
    {
		//public string Description => String.Format("{0} / {1} / {2}", Name, Phone, CustomerID.Name); 
        public Contacts(Session session) : base(session) { }
        public override void AfterConstruction() { base.AfterConstruction(); }

		private string fEmail;
		[Size(100)]
		public string Email
		{
			get { return fEmail; }
			set { SetPropertyValue<string>("Email", ref fEmail, value); }
		}
	}
}
