﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CXM.Module.BusinessObjects.CXM
{
	[DefaultProperty("FullName")]
    public partial class CxmUser : PermissionPolicyUser
    {
        public CxmUser(Session session) : base(session) { }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            Email = "N/A";
        }

        private string fFullName;
        public string FullName
        {
            get { return fFullName; }
            set { SetPropertyValue<string>("FullName", ref fFullName, value); }
        }

        private Int16 fNextCallNumber;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public Int16 NextCallNumber
        {
            get { return fNextCallNumber; }
            set { SetPropertyValue<Int16>("NextCallNumber", ref fNextCallNumber, value); }
        }

        CxmUser fSupervisor;
        [VisibleInDetailView(false), VisibleInListView(false)]
        public CxmUser Supervisor
        {
            get { return fSupervisor; }
            set { SetPropertyValue<CxmUser>("Supervisor", ref fSupervisor, value); }
        }

        private string fEmail;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false), DefaultValue("N/A")]
        public string Email
        {
            get { return fEmail; }
            set { SetPropertyValue<string>("Email", ref fEmail, value); }
        }

        private bool fSendEmail;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false), DefaultValue("N/A")]
        public bool SendEmail
        {
            get { return fSendEmail; }
            set { SetPropertyValue<bool>("SendEmail", ref fSendEmail, value); }
        }
		
        private bool fSendSMS;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false), DefaultValue("N/A")]
        public bool SendSMS
        {
            get { return fSendSMS; }
            set { SetPropertyValue<bool>("SendSMS", ref fSendSMS, value); }
        }

        private bool fSendWhatsapp;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false), DefaultValue("N/A")]
        public bool SendWhatsapp
        {
            get { return fSendWhatsapp; }
            set { SetPropertyValue<bool>("SendWhatsapp", ref fSendWhatsapp, value); }
        }

        private string fPhoneNumber;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false), DefaultValue("N/A")]
        public string PhoneNumber
        {
            get { return fPhoneNumber; }
            set { SetPropertyValue<string>("PhoneNumber", ref fPhoneNumber, value); }
        }

        [NonPersistent, VisibleInDetailView(false), VisibleInListView(false)]
        public string PhoneSMSGateWay
        {
            get
            {
                if (this.PhoneNumber != null && this.Carrier != null)
                {
                    return System.Text.RegularExpressions.Regex.Replace(this.PhoneNumber, "[^0-9]", "") + this.Carrier.CarrierGateway ?? string.Empty;
                }
                return string.Empty;
            }
        }

        private CarrierList fCarrier;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false), DefaultValue("N/A")]
        public CarrierList Carrier
        {
            get { return fCarrier; }
            set { SetPropertyValue<CarrierList>("Carrier", ref fCarrier, value); }
        }

		CxmUser fMultiAgent;
		[VisibleInDetailView(false), VisibleInListView(false)]
		public CxmUser MultiAgent
		{
			get { return fMultiAgent; }
			set { SetPropertyValue<CxmUser>("MultiAgent", ref fMultiAgent, value); }
		}

		protected override void OnChanged(string propertyName, object oldValue, object newValue)
        {
            base.OnChanged(propertyName, oldValue, newValue);

            if (propertyName == "Email")
            {
                if (newValue == null || newValue.ToString().Trim() == string.Empty)
                {
                    Email = "N/A";
                }
            }
        }
    }
}
