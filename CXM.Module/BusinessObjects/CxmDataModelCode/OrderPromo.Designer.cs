﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
namespace CXM.Module.BusinessObjects.CXM
{

	public partial class OrderPromo : XPLiteObject
	{
		string fPromoID;
		[Key]
		public string PromoID
		{
			get { return fPromoID; }
			set { SetPropertyValue<string>("PromoID", ref fPromoID, value); }
		}
		long fCompID;
		public long CompID
		{
			get { return fCompID; }
			set { SetPropertyValue<long>("CompID", ref fCompID, value); }
		}
		Route fRoute;
		[Association(@"OrderPromoReferencesRoute")]
		public Route Route
		{
			get { return fRoute; }
			set { SetPropertyValue<Route>("Route", ref fRoute, value); }
		}
		string fPromoCode;
		public string PromoCode
		{
			get { return fPromoCode; }
			set { SetPropertyValue<string>("PromoCode", ref fPromoCode, value); }
		}
		SalesOrder fOrder;
		[Association(@"OrderPromoReferencesSalesOrder")]
		public SalesOrder Order
		{
			get { return fOrder; }
			set { SetPropertyValue<SalesOrder>("Order", ref fOrder, value); }
		}
		Product fProduct;
		public Product Product
		{
			get { return fProduct; }
			set { SetPropertyValue<Product>("Product", ref fProduct, value); }
		}
		string fDescription;
		public string Description
		{
			get { return fDescription; }
			set { SetPropertyValue<string>("Description", ref fDescription, value); }
		}
		string fUM;
		public string UM
		{
			get { return fUM; }
			set { SetPropertyValue<string>("UM", ref fUM, value); }
		}
		decimal fAmount;
		public decimal Amount
		{
			get { return fAmount; }
			set { SetPropertyValue<decimal>("Amount", ref fAmount, value); }
		}
		string fAmountType;
		public string AmountType
		{
			get { return fAmountType; }
			set { SetPropertyValue<string>("AmountType", ref fAmountType, value); }
		}
		string fDiscountType;
		public string DiscountType
		{
			get { return fDiscountType; }
			set { SetPropertyValue<string>("DiscountType", ref fDiscountType, value); }
		}
		decimal fPrice;
		public decimal Price
		{
			get { return fPrice; }
			set { SetPropertyValue<decimal>("Price", ref fPrice, value); }
		}
		string fAdjustmentSchedule;
		public string AdjustmentSchedule
		{
			get { return fAdjustmentSchedule; }
			set { SetPropertyValue<string>("AdjustmentSchedule", ref fAdjustmentSchedule, value); }
		}
		bool fFreeGoods;
		public bool FreeGoods
		{
			get { return fFreeGoods; }
			set { SetPropertyValue<bool>("FreeGoods", ref fFreeGoods, value); }
		}
		bool fBillBack;
		public bool BillBack
		{
			get { return fBillBack; }
			set { SetPropertyValue<bool>("BillBack", ref fBillBack, value); }
		}
		decimal fBillBackAmount;
		public decimal BillBackAmount
		{
			get { return fBillBackAmount; }
			set { SetPropertyValue<decimal>("BillBackAmount", ref fBillBackAmount, value); }
		}
		string fFreeGoodsProduct;
		public string FreeGoodsProduct
		{
			get { return fFreeGoodsProduct; }
			set { SetPropertyValue<string>("FreeGoodsProduct", ref fFreeGoodsProduct, value); }
		}
		string fFreeGoodsUM;
		public string FreeGoodsUM
		{
			get { return fFreeGoodsUM; }
			set { SetPropertyValue<string>("FreeGoodsUM", ref fFreeGoodsUM, value); }
		}
		long fSequence;
		public long Sequence
		{
			get { return fSequence; }
			set { SetPropertyValue<long>("Sequence", ref fSequence, value); }
		}
		string fOriginalAssortmentCode;
		public string OriginalAssortmentCode
		{
			get { return fOriginalAssortmentCode; }
			set { SetPropertyValue<string>("OriginalAssortmentCode", ref fOriginalAssortmentCode, value); }
		}
		long fOriginalAssortmentTotal;
		public long OriginalAssortmentTotal
		{
			get { return fOriginalAssortmentTotal; }
			set { SetPropertyValue<long>("OriginalAssortmentTotal", ref fOriginalAssortmentTotal, value); }
		}
		string fOriginalAssortmentPromotionCode;
		public string OriginalAssortmentPromotionCode
		{
			get { return fOriginalAssortmentPromotionCode; }
			set { SetPropertyValue<string>("OriginalAssortmentPromotionCode", ref fOriginalAssortmentPromotionCode, value); }
		}
		long fOriginalFreeCasesQty;
		public long OriginalFreeCasesQty
		{
			get { return fOriginalFreeCasesQty; }
			set { SetPropertyValue<long>("OriginalFreeCasesQty", ref fOriginalFreeCasesQty, value); }
		}
		long fOrderedCases;
		public long OrderedCases
		{
			get { return fOrderedCases; }
			set { SetPropertyValue<long>("OrderedCases", ref fOrderedCases, value); }
		}
		long fOrderedUnits;
		public long OrderedUnits
		{
			get { return fOrderedUnits; }
			set { SetPropertyValue<long>("OrderedUnits", ref fOrderedUnits, value); }
		}
		string fPromoType;
		public string PromoType
		{
			get { return fPromoType; }
			set { SetPropertyValue<string>("PromoType", ref fPromoType, value); }
		}
		decimal fCaseListPrice;
		public decimal CaseListPrice
		{
			get { return fCaseListPrice; }
			set { SetPropertyValue<decimal>("CaseListPrice", ref fCaseListPrice, value); }
		}
		decimal fUnitListPrice;
		public decimal UnitListPrice
		{
			get { return fUnitListPrice; }
			set { SetPropertyValue<decimal>("UnitListPrice", ref fUnitListPrice, value); }
		}
		string fContractNumber;
		public string ContractNumber
		{
			get { return fContractNumber; }
			set { SetPropertyValue<string>("ContractNumber", ref fContractNumber, value); }
		}
		string fOriginalPromoCode;
		public string OriginalPromoCode
		{
			get { return fOriginalPromoCode; }
			set { SetPropertyValue<string>("OriginalPromoCode", ref fOriginalPromoCode, value); }
		}
		long fOriginalSequence;
		public long OriginalSequence
		{
			get { return fOriginalSequence; }
			set { SetPropertyValue<long>("OriginalSequence", ref fOriginalSequence, value); }
		}
		decimal fOriginalAmount;
		public decimal OriginalAmount
		{
			get { return fOriginalAmount; }
			set { SetPropertyValue<decimal>("OriginalAmount", ref fOriginalAmount, value); }
		}
		string fOriginalPromoType;
		public string OriginalPromoType
		{
			get { return fOriginalPromoType; }
			set { SetPropertyValue<string>("OriginalPromoType", ref fOriginalPromoType, value); }
		}
		long fFreeCasesQty;
		public long FreeCasesQty
		{
			get { return fFreeCasesQty; }
			set { SetPropertyValue<long>("FreeCasesQty", ref fFreeCasesQty, value); }
		}
		string fAssortmentCode;
		public string AssortmentCode
		{
			get { return fAssortmentCode; }
			set { SetPropertyValue<string>("AssortmentCode", ref fAssortmentCode, value); }
		}
		string fAssortmentPromoCode;
		public string AssortmentPromoCode
		{
			get { return fAssortmentPromoCode; }
			set { SetPropertyValue<string>("AssortmentPromoCode", ref fAssortmentPromoCode, value); }
		}
		long fAssortmentTotal;
		public long AssortmentTotal
		{
			get { return fAssortmentTotal; }
			set { SetPropertyValue<long>("AssortmentTotal", ref fAssortmentTotal, value); }
		}
		DateTime fPromoLastDate;
		public DateTime PromoLastDate
		{
			get { return fPromoLastDate; }
			set { SetPropertyValue<DateTime>("PromoLastDate", ref fPromoLastDate, value); }
		}
		long fPricingOptions;
		public long PricingOptions
		{
			get { return fPricingOptions; }
			set { SetPropertyValue<long>("PricingOptions", ref fPricingOptions, value); }
		}
		long fPricingOptionActions;
		public long PricingOptionActions
		{
			get { return fPricingOptionActions; }
			set { SetPropertyValue<long>("PricingOptionActions", ref fPricingOptionActions, value); }
		}
	}

}
