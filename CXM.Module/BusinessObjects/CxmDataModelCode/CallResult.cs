﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Persistent.Base;

namespace CXM.Module.BusinessObjects.CXM
{

    public partial class CallResult
    {
        public CallResult(Session session) : base(session) { }
        public override void AfterConstruction() { base.AfterConstruction(); }

		[VisibleInDetailView(false), VisibleInListView(false)]
		public string Planned { get { return Call.Planned ? "YES" : "NO"; } }

		[VisibleInDetailView(false), VisibleInListView(false)]
		public string Outgoing { get { return Call.Outgoing ? "OUTGOING" : "INCOMING"; } }
	}
}
