﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Persistent.Base;

namespace CXM.Module.BusinessObjects.CXM
{
    [ImageName("BO_Employee")]
    public partial class Assignees
    {
        public Assignees(Session session) : base(session) { }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}