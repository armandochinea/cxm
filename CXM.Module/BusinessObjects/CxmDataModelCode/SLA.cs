﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Persistent.Base;

namespace CXM.Module.BusinessObjects.CXM
{
    [DefaultProperty("SlaID")]
    public partial class SLA
    {
		[VisibleInListView(false), VisibleInDetailView(false)]
		public string SlaDesc => string.Format("{0} - {1}", SlaID, Description) ?? String.Empty;
		
        public SLA(Session session) : base(session) { }

        public override void AfterConstruction() {
            base.AfterConstruction();
        }
    }
}