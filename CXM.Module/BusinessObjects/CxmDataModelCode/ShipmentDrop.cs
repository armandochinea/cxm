﻿using CXM.Module.BusinessObjects.CXM;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CXM.Module.BusinessObjects.CxmDataModelCode
{
	public enum ShipmentDropStatus
	{
		InRoute = 0,
		Dropped = 3,
		Canceled = 9
	}

	[Persistent("vwShipmentDrop")]
	public class ShipmentDrop : XPLiteObject, IMapsMarker
	{
		public ShipmentDrop(Session session) : base(session) { }

		[Key]
		[VisibleInListView(false), VisibleInDetailView(false)]
		public string ID { get; set; }
        
        public long ShipmentNumber { get; set; }
		public long ShipmentDropId { get; set; }

		public string OrderNumber { get; set; }
		public string OrderErpID { get; set; }
		public bool CashOnDelivery { get; set; }
		public int DeliveryOrder { get; set; }

		[VisibleInListView(false)]
		public string Customer { get; set; }
		public double Amount { get; set; }

		public DateTime? DeliveryETA { get; set; }
		[VisibleInListView(false)]
		public DateTime? DeliveryETD { get; set; }
		[VisibleInListView(false)]
		public DateTime? ArrivedAt { get; set; }
		[VisibleInListView(false)]
		public DateTime? DropStarted { get; set; }
		[VisibleInListView(false)]
		public DateTime? DropEnded { get; set; }

		public ShipmentDropStatus DropStatus { get; set; }
		[VisibleInListView(false)]
		public string InvoiceNumber { get; set; }
		[VisibleInListView(false)]
		public string SignatureName { get; set; }

		public string Name { get; set; }

		[VisibleInListView(false), VisibleInDetailView(false)]
		public string Title => Name;
		[VisibleInListView(false), VisibleInDetailView(false)]
		public double Latitude { get; set; }
		[VisibleInListView(false), VisibleInDetailView(false)]
		public double Longitude { get; set; }

		[VisibleInDetailView(true), VisibleInListView(false), VisibleInLookupListView(false), Delayed]
		public XPCollection<ShipmentLine> Lines
		{
			get
			{
                Session mSession = this.Session;
                CriteriaOperator op = XPQuery<ShipmentLine>.TransformExpression(mSession, r => r.ShipmentDrop == this.ShipmentDropId);
                var mLines = new XPCollection<ShipmentLine>(mSession, op);
                return mLines;
            }
		}
	}
}
