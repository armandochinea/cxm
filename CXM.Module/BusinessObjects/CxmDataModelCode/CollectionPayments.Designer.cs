﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
namespace CXM.Module.BusinessObjects.CXM
{

    public partial class CollectionPayments : XPLiteObject
    {
        Collections fCollection;
        [Association(@"CollectionPaymentsReferencesCollections")]
        public Collections Collection
        {
            get { return fCollection; }
            set { SetPropertyValue<Collections>("Collection", ref fCollection, value); }
        }
        Invoice fInvoice;
        public Invoice Invoice
        {
            get { return fInvoice; }
            set { SetPropertyValue<Invoice>("Invoice", ref fInvoice, value); }
        }
        decimal fAmountPaid;
        public decimal AmountPaid
        {
            get { return fAmountPaid; }
            set { SetPropertyValue<decimal>("AmountPaid", ref fAmountPaid, value); }
        }
        decimal fDiscount;
        public decimal Discount
        {
            get { return fDiscount; }
            set { SetPropertyValue<decimal>("Discount", ref fDiscount, value); }
        }
        decimal fOpenAmount;
        public decimal OpenAmount
        {
            get { return fOpenAmount; }
            set { SetPropertyValue<decimal>("OpenAmount", ref fOpenAmount, value); }
        }
        long fID;
        [Key(true)]
        public long ID
        {
            get { return fID; }
            set { SetPropertyValue<long>("ID", ref fID, value); }
        }
    }

}
