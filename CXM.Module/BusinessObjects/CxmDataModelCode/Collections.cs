﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using static CXM.Module.Enums;
using DevExpress.Persistent.Base;
using System.Linq;

namespace CXM.Module.BusinessObjects.CXM
{
    public partial class Collections
    {
        public Collections(Session session) : base(session) { }
        public override void AfterConstruction()
        {
            base.AfterConstruction();

            this.OrderReference = string.Empty;
        }

        protected override void OnChanged(string propertyName, object oldValue, object newValue)
        {
            base.OnChanged(propertyName, oldValue, newValue);

            if (propertyName == "Payments")
            {
                this.TotalCollected = this.Payments.Sum(l => l.AmountPaid);
            }
        }

        private Trans_Status fTransStatus;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public Trans_Status TransStatus
        {
            get { return fTransStatus; }
            set { SetPropertyValue<Trans_Status>("TransStatus", ref fTransStatus, value); }
        }

        private CollectionStatus fStatus;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public CollectionStatus Status
        {
            get { return fStatus; }
            set { SetPropertyValue<CollectionStatus>("Status", ref fStatus, value); }
        }

        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public decimal PaymentAmount
        {
            get { return this.Details.Sum(l => l.PaymentAmount); }
        }

        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string TotalPaymentAmount
        {
            get { return this.PaymentAmount.ToString("$#,##0.00"); }
        }

        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public decimal AppliedAmount
        {
            get { return this.Payments.Sum(l => l.AmountPaid); }
        }

        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string TotalAppliedAmount
        {
            get { return this.AppliedAmount.ToString("$#,##0.00"); }
        }

        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string BalanceDue
        {
            get { return (this.AppliedAmount - this.PaymentAmount).ToString("$#,##0.00"); }
        }

        private string fContactEmail;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string ContactEmail
        {
            get { return fContactEmail; }
            set { SetPropertyValue<string>("ContactEmail", ref fContactEmail, value); }
        }

        private string fOrderReference;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string OrderReference
        {
            get { return fOrderReference; }
            set { SetPropertyValue<string>("OrderReference", ref fOrderReference, value); }
        }
    }
}
