﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Persistent.Base;

namespace CXM.Module.BusinessObjects.CXM
{

	public partial class OrderPromo
	{
		public OrderPromo(Session session) : base(session) { }
		public override void AfterConstruction()
		{
			base.AfterConstruction();

			CompID = 1;
			PromoCode = string.Empty;
			Description = string.Empty;
			UM = string.Empty;
			Amount = 0;
			AmountType = string.Empty;
			DiscountType = string.Empty;
			Price = 0;
			AdjustmentSchedule = string.Empty;
			FreeGoods = false;
			BillBack = false;
			BillBackAmount = 0;
			FreeGoodsProduct = string.Empty;
			FreeGoodsUM = string.Empty;
			Sequence = 0;
			OriginalAssortmentCode = string.Empty;
			OriginalAssortmentTotal = 0;
			OriginalAssortmentPromotionCode = string.Empty;
			OriginalFreeCasesQty = 0;
			OrderedCases = 0;
			OrderedUnits = 0;
			PromoType = string.Empty;
			CaseListPrice = 0;
			UnitListPrice = 0;
			ContractNumber = string.Empty;
			OriginalPromoCode = string.Empty;
			OriginalSequence = 0;
			OriginalAmount = 0;
			OriginalPromoType = string.Empty;
			FreeCasesQty = 0;
			AssortmentCode = string.Empty;
			AssortmentPromoCode = string.Empty;
			AssortmentTotal = 0;
			PromoLastDate = DateTime.MinValue;
			PricingOptions = 0;
			PricingOptionActions = 0;
		}

		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public string Applied
		{
			get
			{
				return this.PricingOptionActions == (int)PricingOption.PricingOptionActions.Selected ? "YES" : "NO";
			}
		}

		protected override void OnChanged(string propertyName, object oldValue, object newValue)
		{
			base.OnChanged(propertyName, oldValue, newValue);

			if (propertyName == "Amount")
			{
				if (this.PromoType == "DS" && this.DiscountType == "V")
				{
					if (Math.Abs((decimal)newValue) > Math.Abs(this.OriginalAmount))
					{
						Amount = (decimal)oldValue;
					}
					else if ((decimal)newValue > 0)
					{
						Amount = (decimal)newValue * -1;
					}
				}
			}
		}
	}
}
