﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using DevExpress.Persistent.Base;
using CXM.Module.BusinessObjects.CxmDataModelCode;

namespace CXM.Module.BusinessObjects.CXM
{

	public partial class Shipment
	{
		public Shipment(Session session) : base(session) { }
		public override void AfterConstruction() { base.AfterConstruction(); }

		public string ShipmentErpNumber { get; set; }
		public int DeliveryOrder { get; set; }

		[VisibleInDetailView(true), VisibleInListView(false), VisibleInLookupListView(false), Delayed]
		public XPCollection<ShipmentDrop> Drops
		{
			get
			{
                Session mSession = this.Session;
                CriteriaOperator op = XPQuery<ShipmentDrop>.TransformExpression(mSession, r => r.ShipmentNumber == this.ShipmentNumber);
                var mDrops = new XPCollection<ShipmentDrop>(mSession, op);
                return mDrops;
            }
		}

		[VisibleInDetailView(true), VisibleInLookupListView(false), Delayed]
		public int DropCount { get { if (Drops == null) { return 0; } else { return Drops.Count; } } }

		[VisibleInDetailView(true), VisibleInLookupListView(false), Delayed]
		public int CompletedDropsCount { get { if (Drops == null) { return 0; } else { return Drops.Count(d => d.DropStatus == ShipmentDropStatus.Dropped || d.DropStatus == ShipmentDropStatus.Canceled); } } }
	}
}
