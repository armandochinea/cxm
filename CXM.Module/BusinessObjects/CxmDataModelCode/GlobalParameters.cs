﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Persistent.Base;
using System.Linq;

namespace CXM.Module.BusinessObjects.CXM
{
    [DefaultProperty("Name")]
    public partial class GlobalParameters
    {
        public GlobalParameters(Session session) : base(session) { }
        public override void AfterConstruction() { base.AfterConstruction(); }

		public int GetIntValueForParameter(string name)
		{
			int value = 0;
			int.TryParse(Session.Query<GlobalParameters>().Where(r => r.Name == name).FirstOrDefault().Value, out value);
			return value;
		}

		public decimal GetDecimalValueForParameter(string name)
		{
			decimal value;
			decimal.TryParse(Session.Query<GlobalParameters>().Where(r => r.Name == name).FirstOrDefault().Value, out value);
			return value;
		}

		public bool GetBoolValueForParameter(string name)
		{
			bool value;
			bool.TryParse(Session.Query<GlobalParameters>().Where(r => r.Name == name).FirstOrDefault().Value, out value);
			return value;
		}

		public string GetStringValueForParameter(string name)
		{
			return Session.Query<GlobalParameters>().Where(r => r.Name == name).FirstOrDefault().Value;
		}
	}

}
