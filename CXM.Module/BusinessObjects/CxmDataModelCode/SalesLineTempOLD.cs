﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using DevExpress.Persistent.Base;

namespace CXM.Module.BusinessObjects.CXM
{
    [Persistent("SalesLineTemp")]
	public partial class SalesLineTemp
	{
		public SalesLineTemp(Session session) : base(session) { }
		public override void AfterConstruction()
		{
			base.AfterConstruction();

			Qty = 1;
			FreeGoods = 0;
			FreeGoodsVal = -1;
			SalesTax = 0;
			Discount = 0;
			ModelNo = string.Empty;
			SerialNo = string.Empty;
			UnitOfMeasure = string.Empty;
			HasErrorMessages = false;
			HasMessages = false;
		}

		protected override void OnChanged(string propertyName, object oldValue, object newValue)
		{
			base.OnChanged(propertyName, oldValue, newValue);

			if (propertyName == "Product")
			{
				if (newValue != null)
				{
					this.UnitPrice = this.Product.UnitPrice;
				}
			}

			if (propertyName == "Qty")
			{
				if (newValue != oldValue)
				{
					if (Order != null)
					{
						this.Order.ServerPricingStatus = 0;
					}
				}

				this.ExtPrice = this.UnitPrice * this.Qty;
			}
		}

		public string Description
		{
			get
			{
				return this.Product?.Description ?? String.Empty;
			}
		}

		private string fUnitOfMeasure;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public string UnitOfMeasure
		{
			get
			{
				return fUnitOfMeasure;
			}
			set
			{
				SetPropertyValue<string>("UnitOfMeasure", ref fUnitOfMeasure, value);
			}
		}

		private Int16 fFreeGoods;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public Int16 FreeGoods
		{
			get
			{
				return fFreeGoods;
			}
			set
			{
				SetPropertyValue<Int16>("FreeGoods", ref fFreeGoods, value);
			}
		}

		private Int16 fFreeGoodsVal;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public Int16 FreeGoodsVal
		{
			get
			{
				return fFreeGoodsVal;
			}
			set
			{
				SetPropertyValue<Int16>("FreeGoodsVal", ref fFreeGoodsVal, value);
			}
		}

		private Decimal fSalesTax;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public Decimal SalesTax
		{
			get
			{
				return fSalesTax;
			}
			set
			{
				SetPropertyValue<Decimal>("SalesTax", ref fSalesTax, value);
			}
		}

		private Decimal fDiscount;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public Decimal Discount
		{
			get
			{
				return fDiscount;
			}
			set
			{
				SetPropertyValue<Decimal>("Discount", ref fDiscount, value);
			}
		}

		private string fModelNo;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public string ModelNo
		{
			get
			{
				return fModelNo;
			}
			set
			{
				SetPropertyValue<string>("ModelNo", ref fModelNo, value);
			}
		}

		private string fSerialNo;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public string SerialNo
		{
			get
			{
				return fSerialNo;
			}
			set
			{
				SetPropertyValue<string>("SerialNo", ref fSerialNo, value);
			}
		}

		private bool fHasErrorMessages;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public bool HasErrorMessages
		{
			get
			{
				return fHasErrorMessages;
			}
			set
			{
				SetPropertyValue<bool>("HasErrorMessages", ref fHasErrorMessages, value);
			}
		}

		private bool fHasMessages;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public bool HasMessages
		{
			get
			{
				return fHasMessages;
			}
			set
			{
				SetPropertyValue<bool>("HasMessages", ref fHasMessages, value);
			}
		}

		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public string Applied
		{
			get
			{
				return this.FreeGoods == 1 ? "YES" : "NO";
			}
		}

		private long fContractNumber;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public long ContractNumber
		{
			get
			{
				return fContractNumber;
			}
			set
			{
				SetPropertyValue<long>("ContractNumber", ref fContractNumber, value);
			}
		}

		private long fFreeCaseContractNumber;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public long FreeCaseContractNumber
		{
			get
			{
				return fFreeCaseContractNumber;
			}
			set
			{
				SetPropertyValue<long>("FreeCaseContractNumber", ref fFreeCaseContractNumber, value);
			}
		}

		private string fDiscountRecord;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public string DiscountRecord
		{
			get
			{
				return fDiscountRecord;
			}
			set
			{
				SetPropertyValue<string>("DiscountRecord", ref fDiscountRecord, value);
			}
		}
	}
}
