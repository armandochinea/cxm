﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.ConditionalAppearance;
using static CXM.Module.Enums;

namespace CXM.Module.BusinessObjects.CXM
{
    public partial class CallReschedule
	{
        public enum CallTimeFrames
        {
            [XafDisplayName("Before 9:00 AM")]
            BeforeNine = 1,
            [XafDisplayName("Between 9:00 AM and 12:00 PM")]
            BetweenNineAndTwielve = 2,
            [XafDisplayName("Between 12:00 PM and 3:00 PM")]
            BetweenTwelveAndThree = 3,
            [XafDisplayName("After 3:00 PM")]
            AfterThree = 4
        }

        public CallReschedule(Session session) : base(session) { }
		public override void AfterConstruction() { base.AfterConstruction(); }

		CallTimeFrames fCallTimeFrame;
		public CallTimeFrames CallTimeFrame
		{
			get { return fCallTimeFrame; }
			set { SetPropertyValue<CallTimeFrames>("CallTimeFrame", ref fCallTimeFrame, value); }
		}

		CxmUser fAgent;
		[VisibleInDetailView(false), VisibleInListView(false)]
		public CxmUser Agent
		{
			get { return fAgent; }
			set { SetPropertyValue<CxmUser>("Agent", ref fAgent, value); }
		}
		
		bool fUpdateCustomerPlan;
		[VisibleInDetailView(false), VisibleInListView(false), ImmediatePostData]
		public bool UpdateCustomerPlan
		{
			get { return fUpdateCustomerPlan; }
			set { SetPropertyValue<bool>("UpdateCustomerPlan", ref fUpdateCustomerPlan, value); }
		}

		FrequencyType fFrequency;
		[VisibleInDetailView(false), VisibleInListView(false), Appearance("FrequencyEnabled", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide, Criteria = "UpdateCustomerPlan == false", Context = "DetailView"), ImmediatePostData]
		public FrequencyType Frequency
		{
			get { return fFrequency; }
			set { SetPropertyValue<FrequencyType>("Frequency", ref fFrequency, value); }
		}
		bool fMonday;
		[VisibleInDetailView(false), VisibleInListView(false), Appearance("ShowMonday", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide, Criteria = "UpdateCustomerPlan == false", Context = "DetailView")]
		public bool Monday
		{
			get { return fMonday; }
			set { SetPropertyValue<bool>("Monday", ref fMonday, value); }
		}
		bool fTuesday;
		[VisibleInDetailView(false), VisibleInListView(false), Appearance("ShowTuesday", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide, Criteria = "UpdateCustomerPlan == false", Context = "DetailView")]
		public bool Tuesday
		{
			get { return fTuesday; }
			set { SetPropertyValue<bool>("Tuesday", ref fTuesday, value); }
		}
		bool fWednesday;
		[VisibleInDetailView(false), VisibleInListView(false), Appearance("ShowWednesday", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide, Criteria = "UpdateCustomerPlan == false", Context = "DetailView")]
		public bool Wednesday
		{
			get { return fWednesday; }
			set { SetPropertyValue<bool>("Wednesday", ref fWednesday, value); }
		}
		bool fThursday;
		[VisibleInDetailView(false), VisibleInListView(false), Appearance("ShowThursday", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide, Criteria = "UpdateCustomerPlan == false", Context = "DetailView")]
		public bool Thursday
		{
			get { return fThursday; }
			set { SetPropertyValue<bool>("Thursday", ref fThursday, value); }
		}
		bool fFriday;
		[VisibleInDetailView(false), VisibleInListView(false), Appearance("ShowFriday", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide, Criteria = "UpdateCustomerPlan == false", Context = "DetailView")]
		public bool Friday
		{
			get { return fFriday; }
			set { SetPropertyValue<bool>("Friday", ref fFriday, value); }
		}
		bool fSaturday;
		[VisibleInDetailView(false), VisibleInListView(false), Appearance("ShowSaturday", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide, Criteria = "UpdateCustomerPlan == false", Context = "DetailView")]
		public bool Saturday
		{
			get { return fSaturday; }
			set { SetPropertyValue<bool>("Saturday", ref fSaturday, value); }
		}
		bool fEven;
		[VisibleInDetailView(false), VisibleInListView(false), Appearance("EvenEnabled", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide, Criteria = "Frequency <> 1", Context = "DetailView")]
		public bool Even
		{
			get { return fEven; }
			set { SetPropertyValue<bool>("Even", ref fEven, value); }
		}
		bool fOdd;
		[VisibleInDetailView(false), VisibleInListView(false), Appearance("OddEnabled", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide, Criteria = "Frequency <> 1", Context = "DetailView")]
		public bool Odd
		{
			get { return fOdd; }
			set { SetPropertyValue<bool>("Odd", ref fOdd, value); }
		}
		bool fFirst;
		[VisibleInDetailView(false), VisibleInListView(false), DevExpress.Xpo.DisplayName(@"1"), Appearance("FirstEnabled", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide, Criteria = "Frequency <> 2", Context = "DetailView")]
		public bool First
		{
			get { return fFirst; }
			set { SetPropertyValue<bool>("First", ref fFirst, value); }
		}
		bool fSecond;
		[VisibleInDetailView(false), VisibleInListView(false), DevExpress.Xpo.DisplayName(@"2"), Appearance("SecondEnabled", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide, Criteria = "Frequency <> 2", Context = "DetailView")]
		public bool Second
		{
			get { return fSecond; }
			set { SetPropertyValue<bool>("Second", ref fSecond, value); }
		}
		bool fThird;
		[VisibleInDetailView(false), VisibleInListView(false), DevExpress.Xpo.DisplayName(@"3"), Appearance("ThirdEnabled", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide, Criteria = "Frequency <> 2", Context = "DetailView")]
		public bool Third
		{
			get { return fThird; }
			set { SetPropertyValue<bool>("Third", ref fThird, value); }
		}
		bool fFourth;
		[VisibleInDetailView(false), VisibleInListView(false), DevExpress.Xpo.DisplayName(@"4"), Appearance("FourthEnabled", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide, Criteria = "Frequency <> 2", Context = "DetailView")]
		public bool Fourth
		{
			get { return fFourth; }
			set { SetPropertyValue<bool>("Fourth", ref fFourth, value); }
		}
		private bool fBefore9;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false), Appearance("ShowBefore9", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide, Criteria = "UpdateCustomerPlan == false", Context = "DetailView")]
		public bool Before9
		{
			get { return fBefore9; }
			set { SetPropertyValue<bool>("Before9", ref fBefore9, value); }
		}

		private bool fBetween9and12;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false), Appearance("ShowBetween9&12", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide, Criteria = "UpdateCustomerPlan == false", Context = "DetailView")]
		public bool Between9and12
		{
			get { return fBetween9and12; }
			set { SetPropertyValue<bool>("Between9and12", ref fBetween9and12, value); }
		}

		private bool fBetween12and3;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false), Appearance("ShowBetween12&3", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide, Criteria = "UpdateCustomerPlan == false", Context = "DetailView")]
		public bool Between12and3
		{
			get { return fBetween12and3; }
			set { SetPropertyValue<bool>("Between12and3", ref fBetween12and3, value); }
		}

		private bool fAfter3;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false), Appearance("ShowAfter3", Visibility = DevExpress.ExpressApp.Editors.ViewItemVisibility.Hide, Criteria = "UpdateCustomerPlan == false", Context = "DetailView")]
		public bool After3
		{
			get { return fAfter3; }
			set { SetPropertyValue<bool>("After3", ref fAfter3, value); }
		}
	}

}
