﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
namespace CXM.Module.BusinessObjects.CXM
{
	[DefaultProperty("Description")]
	public partial class IssueStatus
	{
		public IssueStatus(Session session) : base(session) { }
		public override void AfterConstruction() { base.AfterConstruction(); }
	}

}
