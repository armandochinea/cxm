﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Persistent.Base;

namespace CXM.Module.BusinessObjects.CXM
{
	[DefaultProperty("FullDescription")]
	public partial class Warehouse
	{
		public Warehouse(Session session) : base(session) { }
		public override void AfterConstruction() { base.AfterConstruction(); }

		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public string FullDescription => string.Format("{0} - {1}", ID, Description);
	}

}
