﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
namespace CXM.Module.BusinessObjects.CXM
{

	public partial class SalesOrder : XPLiteObject
	{
		long fID;
		[Key(true)]
		public long ID
		{
			get { return fID; }
			set { SetPropertyValue<long>("ID", ref fID, value); }
		}
		Customers fCustomer;
		[Association(@"SalesOrderReferencesCustomers")]
		[DevExpress.Persistent.Base.ImmediatePostData]
		public Customers Customer
		{
			get { return fCustomer; }
			set { SetPropertyValue<Customers>("Customer", ref fCustomer, value); }
		}
		DateTime fOrderDate;
		public DateTime OrderDate
		{
			get { return fOrderDate; }
			set { SetPropertyValue<DateTime>("OrderDate", ref fOrderDate, value); }
		}
		string fComments;
		[Size(SizeAttribute.Unlimited)]
		public string Comments
		{
			get { return fComments; }
			set { SetPropertyValue<string>("Comments", ref fComments, value); }
		}
		DateTime fShipDate;
		public DateTime ShipDate
		{
			get { return fShipDate; }
			set { SetPropertyValue<DateTime>("ShipDate", ref fShipDate, value); }
		}
		string fOrderPO;
		public string OrderPO
		{
			get { return fOrderPO; }
			set { SetPropertyValue<string>("OrderPO", ref fOrderPO, value); }
		}
		Warehouse fWhsID;
		[Association(@"SalesOrderReferencesWarehouse")]
		public Warehouse WhsID
		{
			get { return fWhsID; }
			set { SetPropertyValue<Warehouse>("WhsID", ref fWhsID, value); }
		}
		[DevExpress.Xpo.Delayed]
		[Association(@"SalesLineReferencesSalesOrder")]
		public XPCollection<SalesLine> SalesLines { get { return GetCollection<SalesLine>("SalesLines"); } }
		[DevExpress.Xpo.Delayed]
		[Association(@"OrderPromoReferencesSalesOrder")]
		public XPCollection<OrderPromo> OrderPromoes { get { return GetCollection<OrderPromo>("OrderPromoes"); } }
		[DevExpress.Xpo.Delayed]
		[Association(@"OrderMessagesReferencesSalesOrder")]
		public XPCollection<OrderMessages> OrderMessagesCollection { get { return GetCollection<OrderMessages>("OrderMessagesCollection"); } }
		[DevExpress.Persistent.Base.VisibleInDetailView(false)]
		[Association(@"QuickEntryReferencesSalesOrder")]
		public XPCollection<QuickEntry> QuickEntries { get { return GetCollection<QuickEntry>("QuickEntries"); } }
	}

}
