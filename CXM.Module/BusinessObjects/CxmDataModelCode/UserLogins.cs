﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Persistent.Base;

namespace CXM.Module.BusinessObjects.CXM
{

	public partial class UserLogins
	{
		public enum On_Off
		{
			[ImageName("BO_LogOn")]
			LogOn = 1,
			[ImageName("BO_LogOff")]
			LogOff = 0,
		};

		public UserLogins(Session session) : base(session) { }
		public override void AfterConstruction() { base.AfterConstruction(); }

		private On_Off fOnOff;
		public On_Off OnOff
		{
			get { return fOnOff; }
			set { SetPropertyValue("OnOff", ref fOnOff, value); }
		}
	}
}
