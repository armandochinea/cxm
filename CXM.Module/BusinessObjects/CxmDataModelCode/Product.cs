﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Persistent.Base;
using System.Drawing;
using System.IO;

namespace CXM.Module.BusinessObjects.CXM
{
    [ImageName("BO_Product"),DefaultProperty("ProductID")]
    public partial class Product
    {
        public Product(Session session) : base(session) { }
        public override void AfterConstruction() { base.AfterConstruction(); }

		private Decimal fSalesTax;
		[VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
		public Decimal SalesTax
		{
			get
			{
				return fSalesTax;
			}
			set
			{
				SetPropertyValue<Decimal>("SalesTax", ref fSalesTax, value);
			}
		}

        byte[] fProductImage;
        [Size(SizeAttribute.Unlimited)]
        [MemberDesignTimeVisibility(true)]
        [ImageEditor, Delayed]
        public byte[] ProductImage
        {
            get { return fProductImage; }
            set { SetPropertyValue<byte[]>("ProductImage", ref fProductImage, value); }
        }
    }
}
