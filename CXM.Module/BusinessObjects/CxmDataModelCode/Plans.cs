﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.ExpressApp;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using System.Linq;
using DevExpress.Persistent.Base;

namespace CXM.Module.BusinessObjects.CXM
{
    public partial class Plans
    {
		public enum PlanStatus
		{
			New = 0,
			Confirmed = 1,
			InProcess = 2,
			Completed = 9
		}

		public Plans(Session session) : base(session) { }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            PlanDate = DateTime.Today;
            PlanID = Guid.NewGuid().ToString();
			Status = PlanStatus.New;

			PermissionPolicyUser currentUser = Session.Query<PermissionPolicyUser>().Where(r => r.UserName == SecuritySystem.CurrentUserName).FirstOrDefault();
			PermissionPolicyRole supRole = Session.Query<PermissionPolicyRole>().Where(r => r.Name == "Supervisor").FirstOrDefault();
			if (currentUser.Roles.Any(r => r.Oid == supRole.Oid)) {
				Supervisor = currentUser;
			}
        }

		#region Properties
		[Delayed]
		XPCollection<PermissionPolicyUser> Agents
        {
            get
            {
                var role = Session.Query<PermissionPolicyRole>().Where(r => r.Name == "Agent").FirstOrDefault();
                var agents = role?.Users;
                return agents;
            }
        }

		PermissionPolicyUser fAgent;
        [DataSourceProperty("Agents")]
        public PermissionPolicyUser Agent
        {
            get { return fAgent; }
            set { SetPropertyValue<PermissionPolicyUser>("Agent", ref fAgent, value); }
        }

		[Delayed]
        XPCollection<PermissionPolicyUser> Supervisors
        {
            get
            {
                var role = Session.Query<PermissionPolicyRole>().Where(r => r.Name == "Supervisor").FirstOrDefault();
                var supervisors = role?.Users;
                return supervisors;
            }
        }

		PermissionPolicyUser fSupervisor;
        [DataSourceProperty("Supervisors")]
        public PermissionPolicyUser Supervisor
        {
            get { return fSupervisor; }
            set { SetPropertyValue<PermissionPolicyUser>("Supervisor", ref fSupervisor, value); }
        }

		PlanStatus fStatus;
		[VisibleInListView(false), VisibleInDetailView(false)]
		public PlanStatus Status
		{
			get { return fStatus; }
			set { SetPropertyValue<PlanStatus>("Status", ref fStatus, value); }
		}
		#endregion
	}
}