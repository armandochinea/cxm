﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.Persistent.Base;

namespace CXM.Module.BusinessObjects.CXM
{

    public partial class CollectionPayments
    {
        public CollectionPayments(Session session) : base(session) { }
        public override void AfterConstruction()
        {
            base.AfterConstruction();

            this.OrderReference = string.Empty;
        }

        private string fOrderReference;
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        public string OrderReference
        {
            get { return fOrderReference; }
            set { SetPropertyValue<string>("OrderReference", ref fOrderReference, value); }
        }
    }

}
