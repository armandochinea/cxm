﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;

namespace CXM.Module.BusinessObjects.CXM
{
   [DefaultProperty("Description")]
	public partial class CustomerSpecialProducts
	{
		public CustomerSpecialProducts(Session session) : base(session) { }
		public override void AfterConstruction() { base.AfterConstruction(); }
    }
}
