﻿using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
namespace CXM.Module.BusinessObjects.CXM
{

	public partial class OrderMessages
	{
		public OrderMessages(Session session) : base(session) { }
		public override void AfterConstruction()
		{
			base.AfterConstruction();

			ID = Guid.NewGuid().ToString();
			CompID = 1;
			UM = string.Empty;
			MessageType = string.Empty;
			Message = string.Empty;
			MessageNumber = string.Empty;
			MessageID = string.Empty;
			MessageReferences = string.Empty;
			MessageDatetime = DateTime.MinValue;
		}
	}

}
