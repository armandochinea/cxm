﻿using System;
	using DevExpress.Xpo;
	using DevExpress.Data.Filtering;
	using System.Collections.Generic;
	using System.ComponentModel;
using DevExpress.Persistent.Base;

namespace CXM.Module.BusinessObjects.CXM
{
	[DefaultProperty("Description")]
	public partial class Account
	{
		public Account(Session session) : base(session) { }
		public override void AfterConstruction() { base.AfterConstruction(); }

		private string fReference;
		[VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(false)]
		public string Reference
		{
			get { return fReference; }
			set { SetPropertyValue<string>("Reference", ref fReference, value); }
		}
	}

	
}
