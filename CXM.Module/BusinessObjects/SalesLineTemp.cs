﻿using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CXM.Module.BusinessObjects
{
    [NonPersistent]
    public class SalesLineTemp : XPLiteObject
    {

        public SalesLineTemp(Session session) : base(session) { }

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            ID = Guid.NewGuid();
        }


        Guid fID;
        [Key]
        public Guid ID
        {
            get { return fID; }
            set { SetPropertyValue<Guid>("ID", ref fID, value); }
        }
        long fQty;
        public long Qty
        {
            get { return fQty; }
            set { SetPropertyValue<long>("Qty", ref fQty, value); }
        }
        decimal fUnitPrice;
        public decimal UnitPrice
        {
            get { return fUnitPrice; }
            set { SetPropertyValue<decimal>("UnitPrice", ref fUnitPrice, value); }
        }
       
        string fUnitOfMeasure;
        public string UnitOfMeasure
        {
            get { return fUnitOfMeasure; }
            set { SetPropertyValue<string>("UnitOfMeasure", ref fUnitOfMeasure, value); }
        }

        string fProduct;
        public string Product
        {
            get { return fProduct; }
            set { SetPropertyValue<string>("Product", ref fProduct, value); }
        }

        string fDescription;
        public string Description
        {
            get { return fDescription; }
            set { SetPropertyValue<string>("Description", ref fDescription, value); }
        }

        string fLine;
        public string Line
        {
            get { return fLine; }
            set { SetPropertyValue<string>("Line", ref fLine, value); }
        }

        string fSubLine;
        public string SubLine
        {
            get { return fSubLine; }
            set { SetPropertyValue<string>("SubLine", ref fSubLine, value); }
        }


        string fGroup;
        public string Group
        {
            get { return fGroup; }
            set { SetPropertyValue<string>("Group", ref fGroup, value); }
        }


        Guid fUserID;
        public Guid UserID
        {
            get { return fUserID; }
            set { SetPropertyValue<Guid>("UserID", ref fUserID, value); }
        }
    }
}
