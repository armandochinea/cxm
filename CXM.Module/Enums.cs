﻿using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CXM.Module
{
    public class Enums
    {
        public enum TransTypes
        {
            [XafDisplayName("Standard")]
            NormalOrder = 1,
            [XafDisplayName("Pick Up")]
            PickUpOrder = 5
        };

        public enum Trans_Status
        {
            Deleted = -1,
            New = 0,
            ReadyToSend = 100,
            LockedForTransfer = 150,
            SentToMSF = 200,
            Canceled = 400
        }

        public enum UserStatus
        {
            Unset = -1,
            Available = 0,
            BreakTime = 1,
            MealTime = 2,
            AdminMeeting = 3
        }

        public enum CollectionStatus
        {
            Normal = 1,
            Canceled = 2
        }

        public enum FrequencyType
        {
            Weekly = 0,
            BiWeekly = 1,
            Monthly = 2
        }

        public enum PaymentTypes
        {
            ACH = 1,
            CreditCard = 2
        }

        public enum AccountTypes
        {
            Checking = 0,
            [XafDisplayName("VISA")]
            Visa = 6,
            [XafDisplayName("Master Card")]
            MasterCard = 7,
            Discover = 8,
            [XafDisplayName("AMEX")]
            Amex = 9,
            Diners = 10,
            None = 18
        }

        public enum Months
        {
            [XafDisplayName("---")]
            UnSet = 0,
            January = 1,
            February = 2,
            March = 3,
            April = 4,
            May = 5,
            June = 6,
            July = 7,
            August = 8,
            September = 9,
            October = 10,
            November = 11,
            December = 12
        }
    }
}
