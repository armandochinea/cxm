﻿using CXM.Module.BusinessObjects.CXM;
//using CXM.Module.ProfitStars;
using DevExpress.ExpressApp;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CXM.Module
{
	public static class Globals
	{
		#region "Online Payments - Dynamics Payments"
		public static string PaymentValidationMessage(CollectionDetails payment)
		{
			string retval = string.Empty;

			if (payment.SavedAccount != null)
			{
				return retval;
			}

			Int64 result;
			var message = "";
			if (string.IsNullOrWhiteSpace(payment.CardholderName))
			{
				message += System.Environment.NewLine + "- Missing account holder's name";
			}

			if (payment.PaymentType == Enums.PaymentTypes.ACH)
			{
				if (payment.AccountType != Enums.AccountTypes.Checking)
				{
					message += System.Environment.NewLine + "- For ACH payments, choose Checking account type";
				}
				if (string.IsNullOrWhiteSpace(payment.RoutingNumber))
				{
					message += System.Environment.NewLine + "- Missing Routing Number";
				}
				else
				{
					Int64.TryParse(payment.RoutingNumber, out result);
					if (result == 0)
					{
						message += System.Environment.NewLine + "- Only numbers are allowed for routing number.";
					}
				}
				if (string.IsNullOrWhiteSpace(payment.AccountNumber))
				{
					message += System.Environment.NewLine + "- Missing Account Number";
				}
				else
				{
					Int64.TryParse(payment.AccountNumber, out result);
					if (result == 0)
					{
						message += System.Environment.NewLine + "- Only numbers are allowed for account number.";
					}
				}
			}
			else if (payment.PaymentType == Enums.PaymentTypes.CreditCard)
			{
				if (payment.AccountType == Enums.AccountTypes.None || payment.AccountType == Enums.AccountTypes.Checking)
				{
					message += System.Environment.NewLine + "- For Credit Card payments, choose any of the credit card account type options (Visa, Master Card, etc.)";
				}

				if (string.IsNullOrWhiteSpace(payment.CardNumber))
				{
					message += System.Environment.NewLine + "- Missing Credit Card Number";
				}
				else
				{
					Int64.TryParse(payment.CardNumber, out result);
					if (result == 0)
					{
						message += System.Environment.NewLine + "- Only numbers are allowed for credit card number.";
					}
				}
				if (payment.CardExpMonth == Enums.Months.UnSet)
				{
					message += System.Environment.NewLine + "- Select credit card expiration month.";
				}

				if (string.IsNullOrWhiteSpace(payment.CVVNumber))
				{
					message += System.Environment.NewLine + "- Enter CVV number (last 3 digits located at the back of the card).";
				}
				else
				{
					int cvv = -1;
					int.TryParse(payment.CVVNumber, out cvv);
					if (cvv == 0)
					{
						message += System.Environment.NewLine + "- Only numbers are allowed for CVV number.";
					}
					else if (cvv < 0)
					{
						message += System.Environment.NewLine + "- Invalid CVV number. Please review.";
					}
				}
			}

			if (message.Length > 0)
			{
				message = "Please review the following information:" + message;
				retval = message;
			}

			return retval;
		}

		public static void ProcessPaymentWithDynamicsPayments(DetailView detailView, CollectionDetails collectionDetail)
		{
			// Check if the customer has an account locally (local DB is used as a reference for registered customer accounts and payment methods).
			Customers customer = detailView.ObjectSpace.GetObjectsQuery<Customers>().Where(r => r.CustomerID == collectionDetail.Collection.Customer.CustomerID).FirstOrDefault();
			string confirmationNumber = "";
			string[] result;

			if (!customer.HasVaultAccount)
			{
				// Attempt to register customer in PaymentVault.
				//result = PS_Context.RegisterCustomer(customer).Split('|');

				//if (result != null)
				//{
				//	if (result[0] == "RESPONSE_ERROR" || result[0] == "RESPONSE_UNKNOWN" || result[0] == "RESPONSE_EXCEPTION" || result[0] == "EXCEPTION")
				//	{
				//		// If the response message is "The operation failed due to unique constraint.", the customer already has an account in PaymentVault. Execution will continue to payment.
				//		if (result[1].ToString() != "The operation failed due to a unique constraint.")
				//		{
				//			throw new Exception($"Error occurred registering customer to PaymentVault. TYPE: {result[0].ToString()}, MESSAGE: {result[1].ToString()}");
				//		}
				//	}
				//	else
				//	{
				//		// Customer registration was successful.
				//		customer.HasVaultAccount = true;
				//		customer.Save();
				//		detailView.ObjectSpace.CommitChanges();
				//	}
				//}
				//else
				//{
				//	throw new Exception("Error occurred sending RegisterCustomer request.");
				//}
			}

			Account account = null;

			if (collectionDetail.SavedAccount == null && ((collectionDetail.PaymentType == Enums.PaymentTypes.ACH && !string.IsNullOrWhiteSpace(collectionDetail.AccountNumber) && !string.IsNullOrWhiteSpace(collectionDetail.RoutingNumber)) || (collectionDetail.PaymentType == Enums.PaymentTypes.CreditCard && !string.IsNullOrWhiteSpace(collectionDetail.CardNumber))))
			{
				// If any of the fields have value, a new account should be registered for the customer.
				account = detailView.ObjectSpace.CreateObject<Account>();
				account.ID = Guid.NewGuid();
				account.Customer = collectionDetail.Collection.Customer;

				// Attempt to register account.
				//result = PS_Context.RegisterAccount(collectionDetail).Split('|');

				//if (result != null)
				//{
				//	if (result[0] == "RESPONSE_ERROR" || result[0] == "RESPONSE_UNKNOWN" || result[0] == "RESPONSE_EXCEPTION" || result[0] == "EXCEPTION")
				//	{
				//		throw new Exception($"Error occurred registering account to PaymentVault. TYPE: {result[0].ToString()}, MESSAGE: {result[1].ToString()}");
				//	}
				//	else
				//	{
				//		// Account registration was successful.
				//		account.AccountKey = result[0].ToString();
				//		account.Description = result[1].ToString();
				//		account.IsActive = true;
				//		account.Reference = string.Empty;

				//		if (collectionDetail.PaymentType == Enums.PaymentTypes.CreditCard)
				//		{
				//			account.Reference = $"Exp.: {collectionDetail.CardExpDesc}";
				//		}

				//		account.Save();

				//		collectionDetail.SavedAccount = account;
				//		collectionDetail.Save();

				//		detailView.ObjectSpace.CommitChanges();
				//	}
				//}
				//else
				//{
				//	throw new Exception("Error occurred sending RegisterAccount request.");
				//}
			}

			if (collectionDetail.SavedAccount != null)
			{
				if (account == null) { account = collectionDetail.SavedAccount; }

				if (collectionDetail.SavedAccount.AccountKey.Contains("ACH"))
				{
					//result = PS_Context.SalesFromBankAccount(account, collectionDetail).Split('|');

					//if (result != null)
					//{
					//	if (result[0] == "RESPONSE_ERROR" || result[0] == "RESPONSE_UNKNOWN" || result[0] == "RESPONSE_EXCEPTION" || result[0] == "EXCEPTION")
					//	{
					//		throw new Exception($"Error occurred submitting payment. TYPE: {result[0].ToString()}, MESSAGE: {result[1].ToString()}");
					//	}
					//	else if (result[0] == "DECLINED")
					//	{
					//		throw new Exception($"ACH payment was declined. MESSAGE: {result[1].ToString()}");
					//	}
					//	else if (result[0] == "OK")
					//	{
					//		confirmationNumber = result[1].ToString();
					//	}
					//}
					//else
					//{
					//	throw new Exception("Error occurred sending SalesFromBankAccount request.");
					//}
				}
				else
				{
					GlobalParameters paramCreditCardMaximumPaymentAllowed = detailView.ObjectSpace.GetObjectsQuery<GlobalParameters>().Where(r => r.Name == "CREDITCARD_MAXIMUM_PAYMENT_ALLOWED").FirstOrDefault();
					if (paramCreditCardMaximumPaymentAllowed != null)
					{
						decimal maximumPaymentAllowed;
						decimal.TryParse(paramCreditCardMaximumPaymentAllowed.Value, out maximumPaymentAllowed);

						if (maximumPaymentAllowed > 0 && collectionDetail.PaymentAmount > maximumPaymentAllowed)
						{
							
							throw new Exception($"Payment amount exceeds the maximum amount allowed for Credit Card payments. Please adjust the payment amount applied to invoices.");
						}
					}
					
					//result = PS_Context.SalesFromCardAccount(account, collectionDetail).Split('|');

					//if (result != null)
					//{
					//	if (result[0] == "RESPONSE_ERROR" || result[0] == "RESPONSE_UNKNOWN" || result[0] == "RESPONSE_EXCEPTION" || result[0] == "EXCEPTION")
					//	{
					//		throw new Exception($"Error occurred submitting payment. TYPE: {result[0].ToString()}, MESSAGE: {result[1].ToString()}");
					//	}
					//	else if (result[0] == "DECLINED")
					//	{
					//		throw new Exception($"Credit card payment was declined. MESSAGE: {result[1].ToString()}");
					//	}
					//	else if (result[0] == "OK")
					//	{
					//		confirmationNumber = result[1].ToString();
					//	}
					//}
					//else
					//{
					//	throw new Exception("Error occurred sending SalesFromCardAccount request.");
					//}
				}
			}

			collectionDetail.ConfirmationNumber = confirmationNumber;
			collectionDetail.AccountNumber = string.Empty;
			collectionDetail.RoutingNumber = string.Empty;
			collectionDetail.CardNumber = string.Empty;
			collectionDetail.CardExpMonth = Enums.Months.UnSet;
			collectionDetail.CardExpYear = -1;
			collectionDetail.CVVNumber = string.Empty;
			collectionDetail.Save();
			detailView.ObjectSpace.CommitChanges();
		}
		#endregion
	}
}
