﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using CXM.Module.BusinessObjects.CXM;
using System.Data.SqlClient;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;

namespace CXM.Module.Controllers
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class CurrentCallViewController : ViewController
	{
		public CurrentCallViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			CurrentCall.Caption = string.Empty;
			CurrentCall.Enabled["CurrentCall"] = false;

			try
			{
				CallHelper callHelper = View.ObjectSpace.GetObjectsQuery<CallHelper>().Where(r => r.UserID == SecuritySystem.CurrentUserName).FirstOrDefault();
				if (callHelper != null)
				{
					if (callHelper.CurrentCall != null)
					{
						CurrentCall.Caption = callHelper.CurrentCall.CustomerName;
						CurrentCall.Enabled["CurrentCall"] = true;
					}
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();
		}

		void ShowWarning(string warning) => Application.ShowViewStrategy.ShowMessage(warning, InformationType.Warning, 5000, InformationPosition.Top);

		private void CurrentCall_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			try
			{
				PermissionPolicyUser currentUser = View.ObjectSpace.GetObjectsQuery<PermissionPolicyUser>().Where(r => r.UserName == SecuritySystem.CurrentUserName).FirstOrDefault();
				PermissionPolicyRole agentRole = View.ObjectSpace.GetObjectsQuery<PermissionPolicyRole>().Where(r => r.Name == "Agent").FirstOrDefault();
				if (currentUser.Roles.Any(r => r.Oid == agentRole.Oid))
				{
					List<UserActivity> userActivityList = View.ObjectSpace.GetObjectsQuery<UserActivity>().Where(r => r.UserID == currentUser.UserName).ToList();
					UserActivity userActivity = userActivityList.FirstOrDefault(r => r.ActivityDate == DateTime.Today);
					if (userActivity?.CurrentStatus != "Available")
					{
						ShowWarning("You must be in \"Available\" status to be able to continue this call.");
						return;
					}
				}

				IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(Calls));

				CallHelper callHelper = View.ObjectSpace.GetObjectsQuery<CallHelper>().Where(r => r.UserID == SecuritySystem.CurrentUserName).FirstOrDefault();
				if (callHelper != null)
				{
					if (callHelper.CurrentCall != null)
					{
						// If the selected customer number belongs to one of the company's personnel, display the Issues List instead of the Activity window.
						if (callHelper.CurrentCall.CustomerID.ShipTo == "SALESMAN" || callHelper.CurrentCall.CustomerID.ShipTo == "MERCHANDISER" || callHelper.CurrentCall.CustomerID.ShipTo == "DRIVER")
						{
							Type objectType = typeof(Issues);
							IObjectSpace issuesObjectSpace = Application.CreateObjectSpace(objectType);
							string listViewId = "OnCall_Issues_ListView";
							CollectionSourceBase collectionSource = Application.CreateCollectionSource(issuesObjectSpace, objectType, listViewId);
							ListView listView = Application.CreateListView(listViewId, collectionSource, true);
							e.ShowViewParameters.CreatedView = listView;
							return;
						}

						DetailView detailView = Application.CreateDetailView(objectSpace, "Agent_Calls_DetailView", true, objectSpace.GetObject(callHelper.CurrentCall));
						e.ShowViewParameters.CreatedView = detailView;
					}
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}

		}
	}
}
