﻿namespace CXM.Module.Controllers.List_Views
{
	partial class SupervisorUnplannedCallsListViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.SupOutgoingCall = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
			this.SupIncomingCall = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
			// 
			// SupOutgoingCall
			// 
			this.SupOutgoingCall.Caption = "New Outgoing Call";
			this.SupOutgoingCall.Category = "Edit";
			this.SupOutgoingCall.ConfirmationMessage = null;
			this.SupOutgoingCall.Id = "SupOutgoingCall";
			this.SupOutgoingCall.ToolTip = null;
			this.SupOutgoingCall.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SupOutgoingCall_Execute);
			// 
			// SupIncomingCall
			// 
			this.SupIncomingCall.Caption = "New Incoming Call";
			this.SupIncomingCall.Category = "Edit";
			this.SupIncomingCall.ConfirmationMessage = null;
			this.SupIncomingCall.Id = "SupIncomingCall";
			this.SupIncomingCall.ToolTip = null;
			this.SupIncomingCall.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SupIncomingCall_Execute);
			// 
			// SupervisorUnplannedCallsListViewController
			// 
			this.Actions.Add(this.SupOutgoingCall);
			this.Actions.Add(this.SupIncomingCall);
			this.TargetViewId = "Supervisor_UnplannedCalls_ListView";

		}

		#endregion

		private DevExpress.ExpressApp.Actions.SimpleAction SupOutgoingCall;
		private DevExpress.ExpressApp.Actions.SimpleAction SupIncomingCall;
	}
}
