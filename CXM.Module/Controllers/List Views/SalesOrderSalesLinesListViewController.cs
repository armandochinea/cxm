﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using System.Web;
using Newtonsoft.Json;
using CXM.Module.BusinessObjects.CXM;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo.DB;
using DevExpress.Xpo;
using DevExpress.Xpo.Helpers;
using CXM.Module.Controllers.Detail_Views;

namespace CXM.Module.Controllers.List_Views
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SalesOrderSalesLinesListViewController : ViewController
    {
        //CollectionSourceBase collectionSource;

        public SalesOrderSalesLinesListViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
			// Perform various tasks depending on the target View.

			DetailView detailView = (View as ListView).ObjectSpace.Owner as DetailView;
			if (detailView != null)
			{
				AddItems.Active["AddItems"] = false;
				QuickEntry.Active["QuickEntry"] = false;

				if (detailView.ViewEditMode == ViewEditMode.Edit)
				{
					AddItems.Active["AddItems"] = true;
					QuickEntry.Active["QuickEntry"] = true;
				}
			}
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();

            //collectionSource.Dispose();
        }

        private void AddItems_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            Type objectType = typeof(SalesLineTemporary);
            IObjectSpace objSpace = Application.CreateObjectSpace(objectType);

            SalesOrder salesOrder = objSpace.GetObject(((SalesOrder)(View.ObjectSpace.Owner as DetailView).CurrentObject));

            var SalesID = salesOrder.ID;
            //**** STORE SALESLINE ITEMS IN VARIABLE FOR SEARCHING
            List<SalesLine> filteredSalesLineList = salesOrder.SalesLines.ToList();
            string listViewId = Application.FindListViewId(objectType);

            // collectionSource = Application.CreateCollectionSource(objSpace, objectType, "SalesLineTemporary_ListView");
            var sql = "";

            sql += "DELETE FROM SalesLineTemporary WHERE OrderID = " + salesOrder.ID + ";";
            sql += "INSERT INTO SalesLineTemporary SELECT NEWID(), " + salesOrder.ID + ", ProductID, Description, 0, 0, [Group], Line, SubLine, UnitOfMeasure, UnitPrice from Product where Active = 1;";

            //((XPObjectSpace)objSpace).Session.ExecuteNonQuery("DELETE FROM SalesLineTemporary WHERE OrderID = " + salesOrder.ID);
            //((XPObjectSpace)objSpace).Session.ExecuteNonQuery("INSERT INTO SalesLineTemporary SELECT NEWID(), " + salesOrder.ID + ", ProductID, Description, 0, 0, [Group], Line, SubLine, UnitOfMeasure, UnitPrice from Product where Active = 1");

            foreach (var item in filteredSalesLineList)
            {
                sql += "UPDATE SalesLineTemporary SET Qty = " + item.Qty + ", SuggestedOrderQty = " + item.SuggestedQty + " where OrderID = " + salesOrder.ID + " and ProductID = '" + item.Product.ProductID + "';";
                //((XPObjectSpace)objSpace).Session.ExecuteNonQuery("UPDATE SalesLineTemporary SET Qty = " + item.Qty + ", SuggestedOrderQty = " + item.SuggestedQty + " where OrderID = " + salesOrder.ID + " and ProductID = '" + item.Product.ProductID + "'");
            }

            ((XPObjectSpace)objSpace).Session.ExecuteNonQuery(sql);

            CollectionSourceBase collectionSource = Application.CreateCollectionSource(objSpace, typeof(SalesLineTemporary), listViewId);
            collectionSource.Criteria["MainFilter"] = CriteriaOperator.Parse("OrderID == " + salesOrder.ID);
            ListView listView2 = Application.CreateListView(listViewId, collectionSource, false);
            e.View = listView2;

            DialogController dialogController = new DialogController();
            e.DialogController = dialogController;

            dialogController.ViewClosed += SalesLineTemporary_ViewClose;

            salesOrder = null;
            objSpace = null;
            filteredSalesLineList = null;

            GC.WaitForPendingFinalizers();
            GC.Collect();
        }

        private void AddItems_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            ListView sltemp = (e.PopupWindowView) as ListView;
            IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(SalesOrder));

            Session session = ((XPObjectSpace)(objectSpace)).Session;

            SalesOrder salesOrder = objectSpace.GetObject(((SalesOrder)(View.ObjectSpace.Owner as DetailView).CurrentObject));

            session.Delete(salesOrder.SalesLines);
            session.Save(salesOrder.SalesLines);

            sltemp.CollectionSource.Criteria.Clear();

            SalesLineTemporary tLine = null;
            List<SalesLineTemporary> viewList = new List<SalesLineTemporary>();
            if (sltemp.CollectionSource.List.GetType() == typeof(XpoServerCollectionFlagger))
            {
                CriteriaOperator op = XPQuery<SalesLineTemporary>.TransformExpression(salesOrder.Session, r => r.OrderID == salesOrder.ID && r.Qty > 0);
                ((XpoServerCollectionFlagger)sltemp.CollectionSource.List).SetFixedCriteria(op);
                for (int i = 0; i < sltemp.CollectionSource.List.Count - 1; i++)
                {
                    tLine = (SalesLineTemporary)sltemp.CollectionSource.List[i];

                    //if (tLine.Qty > 0 && tLine.OrderID == salesOrder.ID)
                    //{
                    viewList.Add(tLine);
                    //}

                    //tLine = null;
                }
            }
            else
            {
                foreach (var item in sltemp.CollectionSource.List)
                {

                    tLine = (SalesLineTemporary)item;

                    if (tLine.Qty > 0 && tLine.OrderID == salesOrder.ID)
                    {
                        viewList.Add(tLine);
                    }

                    tLine = null;
                }
            }

            foreach (SalesLineTemporary obj in viewList)
            {
                if (obj.Qty > 0)
                {
                    var salesLine = objectSpace.CreateObject<SalesLine>();
                    salesLine.Product = objectSpace.GetObjectsQuery<Product>().Where(r => r.ProductID == obj.ProductID).FirstOrDefault();//objectSpace.GetObject(obj.Product); // row.Values[4].ToString();
                    salesLine.Order = salesOrder;
                    salesLine.Qty = obj.Qty;
                    salesLine.SuggestedQty = obj.SuggestedOrderQty;
                    salesLine.UnitOfMeasure = obj.UnitOfMeasure;
                    salesLine.Save();
                    salesOrder.SalesLines.Add(salesLine);
                    salesOrder.Save();
                    salesLine = null;
                }
            }
            viewList.Clear();

            objectSpace.CommitChanges();
            View.RefreshDataSource();

            salesOrder = null;
            objectSpace = null;
            session = null;
            salesOrder = null;
            sltemp = null;
            viewList = null;
            GC.Collect();
        }

        private void SalesLineTemporary_ViewClose(object sender, EventArgs e)
        {
            SalesOrder salesOrder = View.ObjectSpace.GetObject(((SalesOrder)(View.ObjectSpace.Owner as DetailView).CurrentObject));
            ((XPObjectSpace)View.ObjectSpace).Session.ExecuteNonQuery("DELETE FROM SalesLineTemporary WHERE OrderID = " + salesOrder.ID);
        }

		private void QuickEntry_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			IObjectSpace objSpace = View.ObjectSpace;

			SalesOrder salesOrder = (objSpace.Owner as DetailView).CurrentObject as SalesOrder;

			DetailView detailView = Application.CreateDetailView(objSpace, "SalesOrderQuickEntry_DetailView", false, salesOrder);
			detailView.ViewEditMode = ViewEditMode.Edit;
			e.View = detailView;

			DialogController dialogController = new DialogController();
			dialogController.SaveOnAccept = false;
			e.DialogController = dialogController;

			dialogController.Accepting += QuickEntryDialogController_Accepting;

			salesOrder = null;
			objSpace = null;

			GC.WaitForPendingFinalizers();
			GC.Collect();
		}

		private void QuickEntryDialogController_Accepting(object sender, DialogControllerAcceptingEventArgs e)
		{
			var message = string.Empty;

			DialogController dialogController = (sender as DialogController);
			DetailView detailView = dialogController.Frame.View as DetailView;
			IObjectSpace objSpace = detailView.ObjectSpace;
			SalesOrder salesOrderFromPopup = detailView.CurrentObject as SalesOrder;

			foreach (QuickEntry quickEntry in salesOrderFromPopup.QuickEntries)
			{
				Product product = objSpace.GetObjectsQuery<Product>().Where(p => p.ProductID == quickEntry.ProductNumber).FirstOrDefault();
				if (product == null)
				{
					quickEntry.Invalid = true;
					quickEntry.Save();

					message = $"{Environment.NewLine}  - {quickEntry.ProductNumber}";
				}
				else
				{
					SalesLine salesLine = salesOrderFromPopup.SalesLines.Where(s => s.Product.ProductID == product.ProductID && s.FreeGoodsVal == -1).FirstOrDefault();
					if (salesLine == null)
					{
						salesLine = objSpace.CreateObject<SalesLine>();
						salesLine.Product = objSpace.GetObject<Product>(product);
						salesLine.Order = salesOrderFromPopup;
						salesLine.Qty = quickEntry.Qty;
						salesLine.SuggestedQty = 0;
						salesLine.UnitOfMeasure = product.UnitOfMeasure;
						salesLine.Save();

						salesOrderFromPopup.SalesLines.Add(salesLine);
						salesOrderFromPopup.Save();

						salesLine = null;
					}
					else
					{
						salesLine.Qty = quickEntry.Qty;
						salesLine.Save();
					}
				}
			}

			if (message.Length > 0)
			{
				e.Cancel = true;

				throw new UserFriendlyException($"The following product numbers are invalid: {message}");
			}
			else
			{
				List<QuickEntry> quickEntries = salesOrderFromPopup.QuickEntries.ToList();
				foreach (QuickEntry item in quickEntries)
				{
					salesOrderFromPopup.QuickEntries.Remove(item);
				}
				salesOrderFromPopup.Save();
				quickEntries = null;

				SalesOrderHelper.PartialSave = true;
				objSpace.CommitChanges();
				SalesOrderHelper.PartialSave = false;
			}
		}
	}
}
