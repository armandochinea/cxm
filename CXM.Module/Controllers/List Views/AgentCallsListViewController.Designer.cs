﻿namespace CXM.Module.Controllers
{
	partial class AgentCallsListViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem1 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem2 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem3 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
            this.AgentStartCall = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.AgentCallPlanFilter = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.ChooseContact = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // AgentStartCall
            // 
            this.AgentStartCall.Caption = "Start Call";
            this.AgentStartCall.Category = "Edit";
            this.AgentStartCall.ConfirmationMessage = null;
            this.AgentStartCall.Id = "AgentStartCall";
            this.AgentStartCall.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.AgentStartCall.TargetViewId = "Agent_Calls_ListView";
            this.AgentStartCall.ToolTip = null;
            this.AgentStartCall.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AgentStartCall_Execute);
            // 
            // AgentCallPlanFilter
            // 
            this.AgentCallPlanFilter.Caption = "Plan For";
            this.AgentCallPlanFilter.Category = "Edit";
            this.AgentCallPlanFilter.ConfirmationMessage = null;
            this.AgentCallPlanFilter.Id = "AgentCallPlanFilter";
            choiceActionItem1.Caption = "Previous Day";
            choiceActionItem1.Id = "Previous Day";
            choiceActionItem1.ImageName = null;
            choiceActionItem1.Shortcut = null;
            choiceActionItem1.ToolTip = null;
            choiceActionItem2.Caption = "Today";
            choiceActionItem2.Id = "Today";
            choiceActionItem2.ImageName = null;
            choiceActionItem2.Shortcut = null;
            choiceActionItem2.ToolTip = null;
            choiceActionItem3.Caption = "Tomorrow";
            choiceActionItem3.Id = "Tomorrow";
            choiceActionItem3.ImageName = null;
            choiceActionItem3.Shortcut = null;
            choiceActionItem3.ToolTip = null;
            this.AgentCallPlanFilter.Items.Add(choiceActionItem1);
            this.AgentCallPlanFilter.Items.Add(choiceActionItem2);
            this.AgentCallPlanFilter.Items.Add(choiceActionItem3);
            this.AgentCallPlanFilter.TargetObjectType = typeof(CXM.Module.BusinessObjects.CXM.Calls);
            this.AgentCallPlanFilter.TargetViewId = "Agent_Calls_ListView";
            this.AgentCallPlanFilter.ToolTip = null;
            this.AgentCallPlanFilter.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.AgentCallPlanFilter_Execute);
            // 
            // ChooseContact
            // 
            this.ChooseContact.AcceptButtonCaption = null;
            this.ChooseContact.CancelButtonCaption = null;
            this.ChooseContact.Caption = "Choose Contact";
            this.ChooseContact.Category = "Edit";
            this.ChooseContact.ConfirmationMessage = null;
            this.ChooseContact.Id = "ChooseContact";
            this.ChooseContact.ImageName = "BO_Contact";
            this.ChooseContact.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.CaptionAndImage;
            this.ChooseContact.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.ChooseContact.TargetObjectsCriteria = "CallNumber = \'\'";
            this.ChooseContact.ToolTip = "Choose a contact";
            this.ChooseContact.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.ChooseContact_CustomizePopupWindowParams);
            // 
            // AgentCallsListViewController
            // 
            this.Actions.Add(this.AgentStartCall);
            this.Actions.Add(this.AgentCallPlanFilter);
            this.Actions.Add(this.ChooseContact);
            this.TargetObjectType = typeof(CXM.Module.BusinessObjects.CXM.Calls);
            this.TargetViewId = "Agent_Calls_ListView";

		}

		#endregion

		private DevExpress.ExpressApp.Actions.SimpleAction AgentStartCall;
		private DevExpress.ExpressApp.Actions.SingleChoiceAction AgentCallPlanFilter;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction ChooseContact;
    }
}
