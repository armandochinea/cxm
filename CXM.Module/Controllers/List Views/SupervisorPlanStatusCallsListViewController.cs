﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using System.Data.SqlClient;
using CXM.Module.BusinessObjects.CXM;

namespace CXM.Module.Controllers.List_Views
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class SupervisorPlanStatusCallsListViewController : ViewController
	{
		public SupervisorPlanStatusCallsListViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem += CallsListViewController_CustomProcessSelectedItem;
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();

			Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem += CallsListViewController_CustomProcessSelectedItem;
		}

		void ShowWarning(string warning) => Application.ShowViewStrategy.ShowMessage(warning, InformationType.Warning, 5000, InformationPosition.Top);

		private void CallsListViewController_CustomProcessSelectedItem(object sender, CustomProcessListViewSelectedItemEventArgs e)
		{
			try
			{
				e.Handled = true;

				var selectedCall = (Calls)View.CurrentObject;

				if (selectedCall != null)
				{
					if (selectedCall.Attempted)
					{
						IObjectSpace newObjSpace = Application.CreateObjectSpace(typeof(CallResult));
						CallResult callResult = View.ObjectSpace.GetObjectsQuery<CallResult>().Where(r => r.Call == selectedCall).FirstOrDefault();
						DetailView detailView = Application.CreateDetailView(newObjSpace, "CompletedCallResult_DetailView", false, newObjSpace.GetObject(callResult));
						detailView.ViewEditMode = ViewEditMode.View;
						e.InnerArgs.ShowViewParameters.CreatedView = detailView;
						e.InnerArgs.ShowViewParameters.TargetWindow = TargetWindow.NewWindow;
					}
					else
					{
						Application.ShowViewStrategy.ShowMessage("This call has not been attempted.", InformationType.Info, 5000, InformationPosition.Top);
						return;
					}
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}
	}
}
