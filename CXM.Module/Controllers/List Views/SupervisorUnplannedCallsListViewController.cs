﻿using System;
using System.Linq;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.SystemModule;
using CXM.Module.BusinessObjects.CXM;
using System.Data.SqlClient;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;

namespace CXM.Module.Controllers.List_Views
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class SupervisorUnplannedCallsListViewController : ViewController
	{
		static Calls newCall;
		IObjectSpace newCallObjSpace;

		public SupervisorUnplannedCallsListViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem += UnplannedCallsListViewController_CustomProcessSelectedItem;

			DateTime dtStart = DateTime.Today;
			DateTime dtEnd = dtStart.AddDays(1).AddSeconds(-1);
			((ListView)View).CollectionSource.Criteria["ReceivedCalls"] = CriteriaOperator.Parse("StartDateTime >= ? && StartDateTime <= ? && UserID == ?", dtStart, dtEnd, SecuritySystem.CurrentUserName);
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();

			Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem -= UnplannedCallsListViewController_CustomProcessSelectedItem;
		}

		private void SupOutgoingCall_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			try
			{
				CreateCall();

				if (newCall != null)
				{
					newCall.Outgoing = true;
				}

				e.ShowViewParameters.CreatedView = ContactSearchListView();

				DialogController dialogController = new DialogController();
				dialogController.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(DialogController_Accepting);
				e.ShowViewParameters.Controllers.Add(dialogController);
				e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void SupIncomingCall_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			try
			{
				CreateCall();

				if (newCall != null)
				{
					newCall.Outgoing = false;
				}

				e.ShowViewParameters.CreatedView = ContactSearchListView();

				DialogController dialogController = new DialogController();
				dialogController.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(DialogController_Accepting);
				e.ShowViewParameters.Controllers.Add(dialogController);
				e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		void DialogController_Accepting(object sender, DialogControllerAcceptingEventArgs e)
		{
			try
			{
				var listView = ((sender as DialogController).Frame.View as ListView);

				if (listView.CurrentObject != null)
				{
					var selectedContact = listView.CurrentObject as Contacts;

					newCall.CustomerID = newCallObjSpace.GetObjectsQuery<Customers>().Where(r => r.CustomerID == selectedContact.CustomerID.CustomerID).FirstOrDefault();
					newCall.Contact = newCallObjSpace.GetObjectsQuery<Contacts>().Where(r => r.ContactID == selectedContact.ContactID).FirstOrDefault();
					newCall.CallOrder = 0;

					ShowDetailView();
				}
				else
				{
					var message = "";
					if (listView.SelectedObjects.Count > 1)
					{
						message = "Please select only one contact.";
					}
					else if (listView.SelectedObjects.Count == 0)
					{
						message = "Please select a contact.";
					}
					Application.ShowViewStrategy.ShowMessage(message, InformationType.Warning, 3000, InformationPosition.Top);
					e.Cancel = true;
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		void CreateCall ()
		{
			try
			{
				newCallObjSpace = null;
				newCallObjSpace = Application.CreateObjectSpace(typeof(Calls));
				newCall = newCallObjSpace.CreateObject<Calls>();
				newCall.StartDateTime = DateTime.Now;
				newCall.EndDateTime = DateTime.Now.Date;
				newCall.Planned = false;
				newCall.Attempted = true;
				newCall.CallNumber = string.Empty;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		void ShowDetailView()
		{
			try
			{
				var objSpace = View.ObjectSpace;
				PermissionPolicyUser currentUser = objSpace.GetObjectsQuery<PermissionPolicyUser>().Where(r => r.UserName == SecuritySystem.CurrentUserName).FirstOrDefault();
				PermissionPolicyRole role = objSpace.GetObjectsQuery<PermissionPolicyRole>().Where(r => r.Name == "Supervisor").FirstOrDefault();
				if (currentUser.Roles.Any(r => r.Oid == role.Oid))
				{
					newCall.UserID = currentUser.UserName;
					newCallObjSpace.CommitChanges();

					CallHelper callHelper = objSpace.GetObjectsQuery<CallHelper>().Where(r => r.UserID == currentUser.UserName).FirstOrDefault();

					if (callHelper == null)
					{
						callHelper = objSpace.CreateObject<CallHelper>();
						callHelper.UserID = currentUser.UserName;
					}

					callHelper.CurrentCall = objSpace.GetObject(newCall);

					CallResult callResult = objSpace.GetObjectsQuery<CallResult>().Where(r => r.Call == callHelper.CurrentCall).FirstOrDefault();
					if (callResult == null)
					{
						callResult = objSpace.CreateObject<CallResult>();
						callResult.Call = objSpace.GetObject(newCall);
					}

					objSpace.CommitChanges();

					newCallObjSpace = null;

					//DetailView detailView = Application.CreateDetailView(newCallObjSpace, "Supervisor_Calls_DetailView", false, newCall);
					DetailView detailView = Application.CreateDetailView(newCallObjSpace, "Agent_Calls_DetailView", true, newCall);
					detailView.ViewEditMode = ViewEditMode.View;
					Frame.SetView(detailView);
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		ListView ContactSearchListView()
		{
			ListView listView;

			try
			{
				Type objectType = typeof(Contacts);
				IObjectSpace contactObjectSpace = Application.CreateObjectSpace(objectType);
				string listViewId = Application.FindLookupListViewId(objectType);
				CollectionSourceBase collectionSource = Application.CreateCollectionSource(
					contactObjectSpace, objectType, listViewId);
				listView = Application.CreateListView(listViewId, collectionSource, true);
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
			
			return listView;
		}

		private void UnplannedCallsListViewController_CustomProcessSelectedItem(object sender, CustomProcessListViewSelectedItemEventArgs e)
		{
			try
			{
				e.Handled = true;

				var call = (View as ListView).SelectedObjects[0] as Calls;

				if (call.CallNumber == string.Empty)
				{
					IObjectSpace newObjSpace = Application.CreateObjectSpace(typeof(Calls));
					//DetailView detailView = Application.CreateDetailView(newObjSpace, "Supervisor_Calls_DetailView", true, newObjSpace.GetObject(call));
					DetailView detailView = Application.CreateDetailView(newObjSpace, "Agent_Calls_DetailView", true, newObjSpace.GetObject(call));
					e.InnerArgs.ShowViewParameters.CreatedView = detailView;
					e.InnerArgs.ShowViewParameters.TargetWindow = TargetWindow.Current;
				}
				else
				{
					IObjectSpace newObjSpace = Application.CreateObjectSpace(typeof(CallResult));
					CallResult callResult = View.ObjectSpace.GetObjectsQuery<CallResult>().Where(r => r.Call == call).FirstOrDefault();
					//DetailView detailView = Application.CreateDetailView(newObjSpace, "Supervisor_CompletedCallResult_DetailView", true, newObjSpace.GetObject(callResult));
					DetailView detailView = Application.CreateDetailView(newObjSpace, "CompletedCallResult_DetailView", true, newObjSpace.GetObject(callResult));
					detailView.ViewEditMode = ViewEditMode.View;
					e.InnerArgs.ShowViewParameters.CreatedView = detailView;
					e.InnerArgs.ShowViewParameters.TargetWindow = TargetWindow.Current;
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}
	}
}
