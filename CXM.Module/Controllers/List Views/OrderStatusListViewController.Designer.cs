﻿namespace CXM.Module.Controllers.List_Views
{
	partial class OrderStatusListViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.FetchData = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
			// 
			// FetchData
			// 
			this.FetchData.Caption = "Fetch Data";
			this.FetchData.Category = "Edit";
			this.FetchData.ConfirmationMessage = null;
			this.FetchData.Id = "FetchData";
			this.FetchData.ToolTip = null;
			this.FetchData.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.FetchData_Execute);
			// 
			// OrderStatusListViewController
			// 
			this.Actions.Add(this.FetchData);
			this.TargetViewId = "OrderStatus_ListView";

		}

		#endregion

		private DevExpress.ExpressApp.Actions.SimpleAction FetchData;
	}
}
