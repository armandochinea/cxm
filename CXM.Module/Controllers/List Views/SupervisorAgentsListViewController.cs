﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using CXM.Module.BusinessObjects.CXM;
using System.Data.SqlClient;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;

namespace CXM.Module.Controllers.List_Views
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class SupervisorAgentsListViewController : ViewController
	{
		public SupervisorAgentsListViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			ApplyFilter();
			Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem += SupervisorAgentsListViewController_CustomProcessSelectedItem;

			var role = View.ObjectSpace.GetObjectsQuery<PermissionPolicyRole>().Where(r => r.Name == "Supervisor").FirstOrDefault();

			Supervisors.Items.Clear();
			Supervisors.Items.Add(new ChoiceActionItem("-", null));
			foreach (CxmUser supervisor in role?.Users)
			{
				Supervisors.Items.Add(new ChoiceActionItem(supervisor.FullName, supervisor));
			}
			Supervisors.SelectedIndex = 0;
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();

			Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem -= SupervisorAgentsListViewController_CustomProcessSelectedItem;
		}

		private void Supervisors_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
		{
			try
			{
				var selectedObjects = (View as ListView).SelectedObjects;
				var selectedSupervisor = Supervisors.SelectedItem.Data as CxmUser;

				if (selectedObjects.Count > 0)
				{
					foreach (CxmUser agent in (View as ListView).SelectedObjects)
					{
						CxmUser mAgent = View.ObjectSpace.GetObject(agent);
						mAgent.Supervisor = selectedSupervisor;
					}

					Supervisors.SelectedIndex = 0;
					View.ObjectSpace.CommitChanges();
					View.ObjectSpace.Refresh();
				}				
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void ApplyFilter()
		{
			try
			{
				var objSpace = View.ObjectSpace;
				string filter = "";
				List<object> parameters = new List<object>();

				PermissionPolicyRole role = objSpace.GetObjectsQuery<PermissionPolicyRole>().Where(r => r.Name == "Agent").FirstOrDefault();

				foreach (CxmUser agent in role?.Users)
				{
					if (filter.Length == 0)
					{
						filter = "(Oid == ?)";
					} 
					else
					{
						filter += " || (Oid == ?)";
					}

					parameters.Add(agent.Oid);
				}

				((ListView)View).CollectionSource.Criteria["Agents"] = CriteriaOperator.Parse(filter, parameters.ToArray());
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		void SupervisorAgentsListViewController_CustomProcessSelectedItem(object sender, CustomProcessListViewSelectedItemEventArgs e)
		{
			e.Handled = true;
		}
	}
}
