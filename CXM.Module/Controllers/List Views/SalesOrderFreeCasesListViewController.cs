﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using CXM.Module.BusinessObjects.CXM;
using System.Data.SqlClient;
using CXM.Module.Controllers.Detail_Views;
using DevExpress.Web;
using DevExpress.ExpressApp.Web.Editors.ASPx;
using System.Drawing;

namespace CXM.Module.Controllers.List_Views
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class SalesOrderFreeCasesListViewController : ViewController
	{
		public SalesOrderFreeCasesListViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();
		}

		bool BitwiseValueHasFlagEnabled(int value, int flag)
		{
			return (value & flag) == flag;
		}

		private void ChangeFreeCaseStatus_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			try
			{
				var listView = (ListView)View;

				if ((View as ListView).CurrentObject is SalesLine currentLine)
				{
					var objSpace = View.ObjectSpace;

					SalesLine freeCaseLine = objSpace.GetObject<SalesLine>(currentLine);
					////Single line free case.
					//OrderFreeCaseCell *cell = (OrderFreeCaseCell*)[tableView cellForRowAtIndexPath:indexPath];
					//DADataTableRow *freeCaseRow = (DADataTableRow*)freeCaseEntry;
					//NSArray *rows;
					//DADataTableRow *responsiblePromotion, *row;
					//NSInteger i;
					//PricingOption pricingOptions;

					//if (cell.accessoryType == UITableViewCellAccessoryNone && !cell.appliedLabelHidden) {
					//	//Find the promotion responsible for this free case cell.
					//	//This promotion will also hold the responsible product through fields product_no and um.
					//	rows = [[DataAccess sharedInstance].orderPromotionsTable rowsFilteredUsingPredicate:[NSPredicate predicateWithFormat:@"promouid == %@", [freeCaseRow valueForKey:@"modelno"]]];
					OrderPromo freeCasePromo = objSpace.GetObjectsQuery<OrderPromo>().Where(r => r.PromoID == freeCaseLine.ModelNo).FirstOrDefault();

					if (freeCasePromo != null)
					{
						if (freeCaseLine.FreeGoods == 1)
						{
							freeCaseLine.FreeGoods = 0;
							freeCaseLine.Save();

							freeCasePromo.PricingOptionActions = (int)PricingOption.PricingOptionActions.NotSelected;
							freeCasePromo.Save();
						}
						else
						{
							var paramBfhChangePrice = objSpace.GetObjectsQuery<GlobalParameters>().Where(r => r.Name == "BFH_CHANGE_PRICE").FirstOrDefault()?.Value;

							var optionalDiscounts = objSpace.GetObjectsQuery<OrderPromo>().Where(r => r.Order == freeCaseLine.Order && r.PromoType == "DS" && r.DiscountType == "V").ToList<OrderPromo>();

							int i;
							for (i = 0; i < optionalDiscounts.Count; i++)
							{
								var discount = optionalDiscounts[i];
								int pricingOption = (int)discount.PricingOptions;

								if (paramBfhChangePrice == "0")
								{
									if (BitwiseValueHasFlagEnabled(pricingOption, (int)PricingOption.PricingOptions.UserDecision) && discount.Product == freeCasePromo.Product && discount.UM == freeCasePromo.UM && discount.PricingOptionActions == (int)PricingOption.PricingOptionActions.Selected)
									{
										Application.ShowViewStrategy.ShowMessage("Can't apply free cases because the product has a discount already applied.", InformationType.Warning, 5000, InformationPosition.Top);
										break;
									}
								}
							}

							if (i == optionalDiscounts.Count)
							{
								freeCaseLine.FreeGoods = 1;
								freeCaseLine.Save();

								freeCasePromo.PricingOptionActions = (int)PricingOption.PricingOptionActions.Selected;
								freeCasePromo.Save();
							}
						}

						//objSpace.CommitChanges();
						//objSpace.Refresh();
					}
					else
					{
						Application.ShowViewStrategy.ShowMessage("Can't apply free cases because the corresponding promotion line could not be found.", InformationType.Error, 5000, InformationPosition.Top);
					}
					//	if (rows.count > 0) {
					//		responsiblePromotion = (DADataTableRow*)[rows objectAtIndex:0];

					//		if (cell.isApplied) {
					//			//Just unapply the free case.
					//			cell.isApplied = NO;
					//			[freeCaseRow setValue:[NSNumber numberWithBool:NO] forKey:@"freegoods"];
					//			[responsiblePromotion setValue:[NSNumber numberWithInt:kPricingOptionActionNotSelected] forKey:@"pricingOptionActions"];
					//		} else {
					//			//The user wants to apply a free case.

					//			//For each optional discount, check if it has the responsible product and whether the discount is applied or not.
					//			for (i = 0; i < mDiscountsForSection.count; i++) {
					//				row = (DADataTableRow*)[mDiscountsForSection objectAtIndex:i];
					//				pricingOptions = [(NSNumber*)[row valueForKey:@"pricingOptions"] intValue];

					//				//Check the following to block free case application:
					//				// - The user can decide whether to apply the discount or not; AND
					//				// - The discount refers to the responsible product; AND
					//				// - The discount is applied.
					//				if (![[GlobalSettings settings] isValueEnabled: BFH_CHANGE_PRICE withDefaultValue:@"0"]) {
					//					if ((pricingOptions & kPricingOptionUserDecision) == kPricingOptionUserDecision && [[row valueForKey: @"product_no"] isEqualToString:[responsiblePromotion valueForKey: @"product_no"]] && [[row valueForKey: @"um"] isEqualToString:[responsiblePromotion valueForKey: @"um"]] && [[row valueForKey: @"pricingOptionActions"] intValue] == kPricingOptionActionSelected)
					//					{
					//						//The free case cannot be applied, show a message to the user.
					//						[UIAlertView showSimpleWithTitle:nil andMessage:@"No se pueden aplicar las cajas gratis porque el producto ya tiene un descuento aplicado."];
					//						//Exit the loop.
					//						break;
					//					}
					//				}
					//			}

					//			if (i == mDiscountsForSection.count) {
					//				//All optional discounts were verified and none raised any message.
					//				cell.isApplied = YES;
					//				[freeCaseRow setValue:[NSNumber numberWithBool:YES] forKey:@"freegoods"];
					//				[responsiblePromotion setValue:[NSNumber numberWithInt:kPricingOptionActionSelected] forKey:@"pricingOptionActions"];
					//			}
					//		}
					//	} else
					//		[UIAlertView showSimpleWithTitle:nil andMessage:@"Los productos gratis no se pueden aplicar porque no se encontró la promoción correspondiente."];
					//}
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}
	}
}
