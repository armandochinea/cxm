﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using System.Data.SqlClient;
using CXM.Module.BusinessObjects.CXM;


namespace CXM.Module.Controllers.List_Views
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class PlansListViewController : ViewController
	{
		public PlansListViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			DeleteObjectsViewController controller = Frame.GetController<DeleteObjectsViewController>();
			if (controller != null)
			{
				controller.Deleting += Controller_Deleting;
			}
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			DeleteObjectsViewController controller = Frame.GetController<DeleteObjectsViewController>();
			if (controller != null)
			{
				controller.Deleting -= Controller_Deleting;
			}

			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();
		}

		private void Controller_Deleting(object sender, DeletingEventArgs e)
		{
			try
			{
				var objSpace = View.ObjectSpace;

				foreach (Plans plan in View.SelectedObjects)
				{
					List<Calls> calls = plan.Calls.ToList();
					foreach (Calls call in calls)
					{
						plan.Calls.Remove(call);
					}
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void ConfirmPlan_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			try
			{
				var objSpace = View.ObjectSpace;

				foreach (Plans plan in View.SelectedObjects)
				{
					if (plan.Status == Plans.PlanStatus.New)
					{
						plan.Status = Plans.PlanStatus.Confirmed;

						short callOrder = 1;
						foreach (Calls call in plan.Calls)
						{
							call.UserID = plan.Agent.UserName;
							call.CustomerID = call.Contact.CustomerID;
							call.CallOrder = callOrder;
							call.StartDateTime = plan.PlanDate;
							call.EndDateTime = plan.PlanDate;
							callOrder++;
						}
					}
				}

				objSpace.CommitChanges();
				objSpace.Refresh();
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}
	}
}
