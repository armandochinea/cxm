﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using CXM.Module.BusinessObjects.CXM;
using System.Data.SqlClient;
using CXM.Module.Controllers.Detail_Views;
using DevExpress.ExpressApp.Web.SystemModule;

namespace CXM.Module.Controllers.List_Views
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class SalesOrderOrderPromoesListViewController : ViewController
	{
		public SalesOrderOrderPromoesListViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}

		ListViewController controller = null;

		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			controller = Frame.GetController<ListViewController>();
			if (controller != null)
			{
				controller.InlineEditAction.TargetObjectsCriteria = "True";
			}
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();

			if (controller != null)
			{
				controller.InlineEditAction.TargetObjectsCriteria = "False";
			}
		}

		bool BitwiseValueHasFlagEnabled(int value, int flag)
		{
			return (value & flag) == flag;
		}

		private void ApplyDiscount_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			try
			{
				var listView = (ListView)View;

				if ((View as ListView).CurrentObject is OrderPromo item)
				{
					var objSpace = View.ObjectSpace;
					bool canPerformToggle;

					var paramBfhChangePrice = objSpace.GetObjectsQuery<GlobalParameters>().Where(r => r.Name == "BFH_CHANGE_PRICE").FirstOrDefault()?.Value;

					OrderPromo orderPromo = objSpace.GetObject<OrderPromo>(item);
					if (orderPromo != null)
					{
						decimal discountAmount = orderPromo.Amount;

						SalesLine salesLine = objSpace.GetObjectsQuery<SalesLine>().Where(r => r.Order == orderPromo.Order && r.Product == orderPromo.Product && r.UnitOfMeasure == orderPromo.UM && r.FreeGoodsVal == -1).FirstOrDefault();
						OrderPromo netPricePromo = objSpace.GetObjectsQuery<OrderPromo>().Where(r => r.Order == orderPromo.Order && r.Product == orderPromo.Product && r.UM == orderPromo.UM && r.PromoType == "NP").FirstOrDefault();

						if (salesLine == null)
						{
							Application.ShowViewStrategy.ShowMessage("Discount could not be applied because the corresponding sales line was not found.", InformationType.Warning, 3000, InformationPosition.Top);
						}
						else if (netPricePromo == null)
						{
							Application.ShowViewStrategy.ShowMessage("Discount could not be applied because the corresponding net price was not found.", InformationType.Warning, 3000, InformationPosition.Top);
						}
						else
						{
							int pricingOption = (int)orderPromo.PricingOptions;

							if (BitwiseValueHasFlagEnabled(pricingOption, (int)PricingOption.PricingOptions.UserDecision))
							{
								if (orderPromo.Applied == "YES")
								{
									canPerformToggle = true;
								}
								else
								{
									if (paramBfhChangePrice == "1")
									{
										canPerformToggle = true;
									}
									else
									{
										OrderPromo freeCasePromoLine = objSpace.GetObjectsQuery<OrderPromo>().Where(r => r.Order == orderPromo.Order && r.Product == orderPromo.Product && r.UM == orderPromo.UM && r.PromoType == "FC" && BitwiseValueHasFlagEnabled((int)r.PricingOptions, (int)PricingOption.PricingOptions.UserDecision) && (long)r.PricingOptionActions == (long)PricingOption.PricingOptionActions.Selected).FirstOrDefault();

										if (freeCasePromoLine == null)
										{
											canPerformToggle = true;
										}
										else
										{
											canPerformToggle = false;
											Application.ShowViewStrategy.ShowMessage("Discount cannot be applied because the product has a free case promotion already applied.", InformationType.Warning, 3000, InformationPosition.Top);
										}
									}
								}

								if (canPerformToggle)
								{
									var paramOrdToggleDiscountOption = objSpace.GetObjectsQuery<GlobalParameters>().Where(r => r.Name == "ORD_TOGGLE_DISCOUNT_OPTION").FirstOrDefault()?.Value;
									paramOrdToggleDiscountOption = paramOrdToggleDiscountOption == null ? "0" : paramOrdToggleDiscountOption;

									if (orderPromo.Applied == "YES")
									{
										

										orderPromo.PricingOptionActions = (int)PricingOption.PricingOptionActions.NotSelected;

										/*
										if ([[GlobalSettings settings] intValueForKey: ORD_TOGGLE_DISCOUNT_OPTION withDefault:@"0"] == 0) {
											//Substract discount from the detail discount.
											if ([[orderPromotion valueForKey: @"disctype"] isEqualToString: @"V"]) {
												NSNumber* temp = [NSNumber numberWithDouble: ([(NSNumber*)[detailRow valueForKey: @"Discount"] doubleValue] * -1 - discountAmount * -1)];

												if ([temp isGreaterThanNumber:@0])
												{
													temp = [temp multiplyByNumber:@-1];
												}


												[detailRow setValue:temp forKey:@"Discount"];
											} else {
												[detailRow setValue:[NSNumber numberWithDouble:([(NSNumber *)[detailRow valueForKey:@"Discount"] doubleValue] - discountAmount)] forKey:@"Discount"];
											}

											//Substract discount from the detail unit price.
											[detailRow setValue:[NSNumber numberWithDouble:([(NSNumber *)[detailRow valueForKey:@"unit_price"] doubleValue] - discountAmount)] forKey:@"unit_price"];
                        
											//Substract discount from net price condition.
											[netPriceRow setValue:[NSNumber numberWithDouble:([(NSNumber *)[netPriceRow valueForKey:@"amount"] doubleValue] - discountAmount)] forKey:@"amount"];
										} else {
											//No need to update the detail.
											//Just remove the discount from the discount total.
											if (discountAmount< 0)
												//If discount amount is negative, to remove it from the discount total, add the discount amount.
												[detailRow setValue:[NSNumber numberWithDouble:([(NSNumber*)[detailRow valueForKey: @"Discount"] doubleValue] + discountAmount)] forKey:@"Discount"];
											else
												[detailRow setValue:[NSNumber numberWithDouble:([(NSNumber *)[detailRow valueForKey:@"Discount"] doubleValue] - discountAmount)] forKey:@"Discount"];
                        
										}
										*/
										if (paramOrdToggleDiscountOption == "0")
										{
											if (orderPromo.DiscountType == "V")
											{
												var temp = salesLine.Discount * -1 - discountAmount * -1;

												if (temp > 0)
												{
													temp *= -1;
												}

												salesLine.Discount = temp;

												// Enable Edit button to allow user to make changes to it.
												if (controller != null)
												{
													controller.InlineEditAction.TargetObjectsCriteria = "True";
												}
											}
											else
											{
												salesLine.Discount -= discountAmount;
											}

											salesLine.UnitPrice -= discountAmount;
											netPricePromo.Amount -= discountAmount;
										}
										else
										{
											if (discountAmount < 0)
											{
												salesLine.Discount += discountAmount;
											}
											else
											{
												salesLine.Discount -= discountAmount;
											}
										}
									}
									else
									{
										

										orderPromo.PricingOptionActions = (int)PricingOption.PricingOptionActions.Selected;

										/*
										if ([[GlobalSettings settings] intValueForKey: ORD_TOGGLE_DISCOUNT_OPTION withDefault:@"0"] == 0) {
											//Add discount to the detail discount.
											if ([[orderPromotion valueForKey: @"disctype"] isEqualToString: @"V"]) {
												NSNumber* temp = [NSNumber numberWithDouble: ([(NSNumber*)[detailRow valueForKey: @"Discount"] doubleValue] * -1 + discountAmount * -1)];

												if ([temp isGreaterThanNumber:@0])
												{
													temp = [temp multiplyByNumber:@-1];
												}


												[detailRow setValue:temp forKey:@"Discount"];
											} else {
												[detailRow setValue:[NSNumber numberWithDouble:([(NSNumber *)[detailRow valueForKey:@"Discount"] doubleValue] + discountAmount)] forKey:@"Discount"];
											}

											//Add discount to the detail unit price.
											[detailRow setValue:[NSNumber numberWithDouble:([(NSNumber *)[detailRow valueForKey:@"unit_price"] doubleValue] + discountAmount)] forKey:@"unit_price"];
                        
											//Add discount to net price condition.
											[netPriceRow setValue:[NSNumber numberWithDouble:([(NSNumber *)[netPriceRow valueForKey:@"amount"] doubleValue] + discountAmount)] forKey:@"amount"];
										} else {
											//No need to update the detail.
											//Just add the discount to the discount total.
											if (discountAmount< 0)
												//If discount amount is negative, to add it to the discount total, substract the discount amount.
												[detailRow setValue:[NSNumber numberWithDouble:([(NSNumber*)[detailRow valueForKey: @"Discount"] doubleValue] - discountAmount)] forKey:@"Discount"];
											else
												[detailRow setValue:[NSNumber numberWithDouble:([(NSNumber *)[detailRow valueForKey:@"Discount"] doubleValue] + discountAmount)] forKey:@"Discount"];
                        
										}
										*/
										if (paramOrdToggleDiscountOption == "0")
										{
											if (orderPromo.DiscountType == "V")
											{
												var temp = salesLine.Discount * -1 + discountAmount * -1;

												if (temp > 0)
												{
													temp *= -1;
												}

												salesLine.Discount = temp;

												// Disables Edit button to prevent user from changing the discount after it's been applied.
												if (controller != null)
												{
													controller.InlineEditAction.TargetObjectsCriteria = "False";
												}
											}
											else
											{
												salesLine.Discount += discountAmount;
											}

											salesLine.UnitPrice += discountAmount;
											netPricePromo.Amount += discountAmount;
										}
										else
										{
											if (discountAmount < 0)
											{
												salesLine.Discount -= discountAmount;
											}
											else
											{
												salesLine.Discount += discountAmount;
											}
										}
									}

									/*
									if ([[orderPromotion valueForKey: @"disctype"] isEqualToString: @"V"]) {
										if (cell.isApplied)
										{
											cell.discountChange.enabled = YES;
										}
										else
										{
											cell.discountChange.enabled = NO;
										}
									}
									
									[orderPromotion setValue:[NSNumber numberWithInt:pricingAction] forKey:@"pricingOptionActions"];
                
									[[DataAccess sharedInstance] calculateLineTotalsForRow:detailRow addingTax:(custTax != 0)];
                
									//Find the cell corresponding to the product and mark it for reload.
									[rowsToReload addObject:[NSIndexPath indexPathForRow:[self.orderLines indexOfObject:detailRow] inSection:mDetailsSection]];
                
									[rowsToReload addObject:[NSIndexPath indexPathForRow:0 inSection:mOrderTotalSection]];
									*/

									//objSpace.CommitChanges();
									orderPromo.Save();
									salesLine.Save();
									netPricePromo.Save();
								}
							}
						}
					}

					if (controller != null)
					{
						controller.View.Refresh();
					}

					/*
							if (canPerformToggle) {
								cell.isApplied = !cell.isApplied;
                
								if (cell.isApplied) {
									pricingAction = kPricingOptionActionSelected;
                    
									if ([[GlobalSettings settings] intValueForKey:ORD_TOGGLE_DISCOUNT_OPTION withDefault:@"0"] == 0) {
										//Add discount to the detail discount.
										if ([[orderPromotion valueForKey:@"disctype"] isEqualToString:@"V"]) {
											NSNumber* temp = [NSNumber numberWithDouble: ([(NSNumber*)[detailRow valueForKey: @"Discount"] doubleValue] * -1 + discountAmount * -1)];
                            
											if ([temp isGreaterThanNumber:@0]) {
												temp = [temp multiplyByNumber:@-1];
											}
                            
											[detailRow setValue:temp forKey:@"Discount"];
										} else {
											[detailRow setValue:[NSNumber numberWithDouble:([(NSNumber *)[detailRow valueForKey:@"Discount"] doubleValue] + discountAmount)] forKey:@"Discount"];
										}
                        
										//Add discount to the detail unit price.
										[detailRow setValue:[NSNumber numberWithDouble:([(NSNumber *)[detailRow valueForKey:@"unit_price"] doubleValue] + discountAmount)] forKey:@"unit_price"];
                        
										//Add discount to net price condition.
										[netPriceRow setValue:[NSNumber numberWithDouble:([(NSNumber *)[netPriceRow valueForKey:@"amount"] doubleValue] + discountAmount)] forKey:@"amount"];
									} else {
										//No need to update the detail.
										//Just add the discount to the discount total.
										if (discountAmount< 0)
											//If discount amount is negative, to add it to the discount total, substract the discount amount.
											[detailRow setValue:[NSNumber numberWithDouble:([(NSNumber*)[detailRow valueForKey: @"Discount"] doubleValue] - discountAmount)] forKey:@"Discount"];
										else
											[detailRow setValue:[NSNumber numberWithDouble:([(NSNumber *)[detailRow valueForKey:@"Discount"] doubleValue] + discountAmount)] forKey:@"Discount"];
                        
									}
                    
								}
                
								if ([[orderPromotion valueForKey:@"disctype"] isEqualToString:@"V"]) {
									if (cell.isApplied) {
										cell.discountChange.enabled = YES;
									} else {
										cell.discountChange.enabled = NO;
									}
								}
                
								[orderPromotion setValue:[NSNumber numberWithInt:pricingAction] forKey:@"pricingOptionActions"];
                
								[[DataAccess sharedInstance] calculateLineTotalsForRow:detailRow addingTax:(custTax != 0)];
                
								//Find the cell corresponding to the product and mark it for reload.
								[rowsToReload addObject:[NSIndexPath indexPathForRow:[self.orderLines indexOfObject:detailRow] inSection:mDetailsSection]];
                
								[rowsToReload addObject:[NSIndexPath indexPathForRow:0 inSection:mOrderTotalSection]];
							}
            
						} //End checking what action the user can perform on the discount.
        
					} //End checking responsible records were found.
    
					if (rowsToReload.count > 0)
						[self.tableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationBottom];

					[rowsToReload release];
					*/
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}
	}
}
