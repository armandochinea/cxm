﻿namespace CXM.Module.Controllers.List_Views
{
	partial class SupervisorCallsListViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem1 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
			DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem2 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
			DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem3 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
			this.SupervisorStartCall = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
			this.CallPlanFilter = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
			// 
			// SupervisorStartCall
			// 
			this.SupervisorStartCall.Caption = "Start Call";
			this.SupervisorStartCall.Category = "Edit";
			this.SupervisorStartCall.ConfirmationMessage = null;
			this.SupervisorStartCall.Id = "SupervisorStartCall";
			this.SupervisorStartCall.TargetViewId = "Supervisor_Calls_ListView";
			this.SupervisorStartCall.ToolTip = null;
			this.SupervisorStartCall.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SupervisorStartCall_Execute);
			// 
			// CallPlanFilter
			// 
			this.CallPlanFilter.Caption = "Call Plan Filter";
			this.CallPlanFilter.ConfirmationMessage = null;
			this.CallPlanFilter.Id = "CallPlanFilter";
			choiceActionItem1.Caption = "Previous Day";
			choiceActionItem1.Id = "Previous Day";
			choiceActionItem1.ImageName = null;
			choiceActionItem1.Shortcut = null;
			choiceActionItem1.ToolTip = null;
			choiceActionItem2.Caption = "Today";
			choiceActionItem2.Id = "Today";
			choiceActionItem2.ImageName = null;
			choiceActionItem2.Shortcut = null;
			choiceActionItem2.ToolTip = null;
			choiceActionItem3.Caption = "Tomorrow";
			choiceActionItem3.Id = "Tomorrow";
			choiceActionItem3.ImageName = null;
			choiceActionItem3.Shortcut = null;
			choiceActionItem3.ToolTip = null;
			this.CallPlanFilter.Items.Add(choiceActionItem1);
			this.CallPlanFilter.Items.Add(choiceActionItem2);
			this.CallPlanFilter.Items.Add(choiceActionItem3);
			this.CallPlanFilter.ToolTip = null;
			this.CallPlanFilter.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.CallPlanFilter_Execute);
			// 
			// SupervisorCallsListViewController
			// 
			this.Actions.Add(this.SupervisorStartCall);
			this.Actions.Add(this.CallPlanFilter);
			this.TargetViewId = "Supervisor_Calls_ListView";

		}

		#endregion

		private DevExpress.ExpressApp.Actions.SimpleAction SupervisorStartCall;
		private DevExpress.ExpressApp.Actions.SingleChoiceAction CallPlanFilter;
	}
}
