﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using CXM.Module.BusinessObjects.CXM;
using System.Data.SqlClient;

namespace CXM.Module.Controllers.List_Views
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class SupervisorCallsListViewController : ViewController
	{
		public SupervisorCallsListViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem += SupervisorCallsListViewController_CustomProcessSelectedItem;

			CallPlanFilter.SelectedIndex = 1;
			ApplyFilter();
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();

			Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem -= SupervisorCallsListViewController_CustomProcessSelectedItem;
		}

		void ShowWarning(string warning) => Application.ShowViewStrategy.ShowMessage(warning, InformationType.Warning, 5000, InformationPosition.Top);
		
		void SupervisorCallsListViewController_CustomProcessSelectedItem(object sender, CustomProcessListViewSelectedItemEventArgs e)
		{
			e.Handled = true;

			SupervisorStartCall.DoExecute();
		}
		
		private void SupervisorStartCall_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			try
			{
				/*
				CallHelper callHelper = objSpace.GetObjectsQuery<CallHelper>().Where(r => r.UserID == currentUser.UserName).FirstOrDefault();

				if (callHelper == null)
				{
					callHelper = objSpace.CreateObject<CallHelper>();
					callHelper.UserID = currentUser.UserName;
				}
				else if (callHelper.CurrentCall != null && callHelper.CurrentCall != selectedCall && callHelper.CurrentCall.StartDateTime >= DateTime.Today)
				{
					ShowWarning("This user already has a call in progress and cannot start a new call until the previous one is finished.");
					return;
				}
				*/

				if (View.Id == "Supervisor_Calls_ListView")
				{
					if (View.SelectedObjects.Count == 1)
					{
						var call = (View as ListView).SelectedObjects[0] as Calls;

						if (call.StartDateTime.Date == DateTime.Today)
						{
							if (!call.Attempted)
							{
								CallHelper callHelper = View.ObjectSpace.GetObjectsQuery<CallHelper>().Where(r => r.UserID == SecuritySystem.CurrentUserName).FirstOrDefault();

								if (callHelper == null)
								{
									callHelper = View.ObjectSpace.CreateObject<CallHelper>();
									callHelper.UserID = SecuritySystem.CurrentUserName;
								}
								else if (callHelper.CurrentCall != null && callHelper.CurrentCall != call && callHelper.CurrentCall.StartDateTime >= DateTime.Today)
								{
									ShowWarning("This user already has a call in progress and cannot start a new call until the previous one is finished.");
									return;
								}

								if (callHelper.CurrentCall == null)
								{
									callHelper.CurrentCall = View.ObjectSpace.GetObject<Calls>(call);
								}

								if (call.StartDateTime == DateTime.Today)
								{
									call.StartDateTime = DateTime.Now;
								}

								if (call.PlanID.Status == Plans.PlanStatus.Confirmed)
								{
									call.PlanID.Status = Plans.PlanStatus.InProcess;
								}

								var selectedCall = View.ObjectSpace.GetObject(call);
								CallResult callResult = View.ObjectSpace.GetObjectsQuery<CallResult>().Where(r => r.Call == selectedCall).FirstOrDefault();
								if (callResult == null)
								{
									callResult = View.ObjectSpace.CreateObject<CallResult>();
									callResult.Call = selectedCall;
								}

								View.ObjectSpace.CommitChanges();

								IObjectSpace newObjSpace = Application.CreateObjectSpace(typeof(Calls));
								//DetailView detailView = Application.CreateDetailView(newObjSpace, "Supervisor_Calls_DetailView", true, newObjSpace.GetObject(call));
								DetailView detailView = Application.CreateDetailView(newObjSpace, "Agent_Calls_DetailView", true, newObjSpace.GetObject(call));
								e.ShowViewParameters.CreatedView = detailView;
								e.ShowViewParameters.TargetWindow = TargetWindow.Current;
							}
							else
							{
								IObjectSpace newObjSpace = Application.CreateObjectSpace(typeof(CallResult));
								CallResult callResult = View.ObjectSpace.GetObjectsQuery<CallResult>().Where(r => r.Call == call).FirstOrDefault();
								DetailView detailView = Application.CreateDetailView(newObjSpace, "CompletedCallResult_DetailView", true, newObjSpace.GetObject(callResult));
								detailView.ViewEditMode = ViewEditMode.View;
								e.ShowViewParameters.CreatedView = detailView;
								e.ShowViewParameters.TargetWindow = TargetWindow.Current;
							}
						}
						else
						{
							ShowWarning("The selected call does not belong to today's plan.");
						}
					}
					else
					{
						if (View.SelectedObjects.Count == 0)
						{
							ShowWarning("Please select a call.");
						}
						else
						{
							ShowWarning("Only one call can be processed at a time.");
						}
					}
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void ApplyFilter()
		{
			try
			{
				var objSpace = View.ObjectSpace;
				var selectedChoice = CallPlanFilter.SelectedIndex;
				DateTime dtStart = DateTime.Today;
				DateTime dtEnd = dtStart.AddDays(1).AddSeconds(-1);
				string filter = "1 == 1";
				List<object> parameters = new List<object>();

				PermissionPolicyUser currentUser = objSpace.GetObjectsQuery<PermissionPolicyUser>().Where(r => r.UserName == SecuritySystem.CurrentUserName).FirstOrDefault();
				PermissionPolicyRole role = objSpace.GetObjectsQuery<PermissionPolicyRole>().Where(r => r.Name == "Supervisor").FirstOrDefault();
				if (currentUser.Roles.Any(r => r.Oid == role?.Oid))
				{
					filter += " && PlanID.Supervisor.Oid == ?";
					parameters.Add(currentUser.Oid);
				}

				if (selectedChoice == 0)
				{
					if (dtStart.DayOfWeek == DayOfWeek.Monday)
					{
						dtStart = dtStart.AddDays(-2);
					}
					else
					{
						dtStart = dtStart.AddDays(-1);
					}

					dtEnd = dtStart.AddDays(1).AddSeconds(-1);
				}

				if (selectedChoice == 2)
				{
					dtStart = dtStart.AddDays(1);
					dtEnd = dtEnd.AddDays(1);
				}

				filter += " && StartDateTime >= ? && StartDateTime <= ? && Planned == true";
				parameters.Add(dtStart);
				parameters.Add(dtEnd);

				((ListView)View).CollectionSource.Criteria["MainPlanCallFilter"] = CriteriaOperator.Parse(filter, parameters.ToArray());
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void CallPlanFilter_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
		{
			ApplyFilter();
		}
	}
}
