﻿namespace CXM.Module.Controllers.List_Views
{
	partial class SpeedSalesOrderListViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.CancelSpeedOrder = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
			this.NewSpeedOrder = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
			// 
			// CancelSpeedOrder
			// 
			this.CancelSpeedOrder.Caption = "Cancel";
			this.CancelSpeedOrder.Category = "Edit";
			this.CancelSpeedOrder.ConfirmationMessage = "Do you want to cancel the selected order(s)?";
			this.CancelSpeedOrder.Id = "CancelSpeedOrder";
			this.CancelSpeedOrder.ImageName = "Action_Delete";
			this.CancelSpeedOrder.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.CaptionAndImage;
			this.CancelSpeedOrder.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
			this.CancelSpeedOrder.TargetObjectsCriteria = "TransStatus == 0";
			this.CancelSpeedOrder.TargetObjectsCriteriaMode = DevExpress.ExpressApp.Actions.TargetObjectsCriteriaMode.TrueForAll;
			this.CancelSpeedOrder.TargetViewId = "SpeedSalesOrder_ListView";
			this.CancelSpeedOrder.ToolTip = null;
			this.CancelSpeedOrder.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CancelSpeedOrder_Execute);
			// 
			// NewSpeedOrder
			// 
			this.NewSpeedOrder.Caption = "New";
			this.NewSpeedOrder.Category = "ObjectsCreation";
			this.NewSpeedOrder.ConfirmationMessage = null;
			this.NewSpeedOrder.Id = "NewSpeedOrder";
			this.NewSpeedOrder.ImageName = "MenuBar_New";
			this.NewSpeedOrder.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.CaptionAndImage;
			this.NewSpeedOrder.ToolTip = null;
			this.NewSpeedOrder.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.NewObjectAction_Execute);
			// 
			// SpeedSalesOrderListViewController
			// 
			this.Actions.Add(this.CancelSpeedOrder);
			this.Actions.Add(this.NewSpeedOrder);
			this.TargetViewId = "SpeedSalesOrder_ListView";

		}

		#endregion

		private DevExpress.ExpressApp.Actions.SimpleAction CancelSpeedOrder;
		private DevExpress.ExpressApp.Actions.SimpleAction NewSpeedOrder;
	}
}
