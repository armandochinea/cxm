﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Web.SystemModule;

namespace CXM.Module.Controllers.List_Views
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class IssuesActionsListViewController : ViewController
	{
		private const string UnavailableForNewIssueActiveKey = "UnavailableForNewIssue";
		private NewObjectViewController newController = null;
		private ListViewController webListViewController = null;

		public IssuesActionsListViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			newController = Frame.GetController<NewObjectViewController>();
			if (newController != null)
			{
				newController.NewObjectAction.Active.SetItemValue(UnavailableForNewIssueActiveKey, false);
			}
			webListViewController = Frame.GetController<ListViewController>();
			if (webListViewController != null)
			{
				webListViewController.EditAction.Active.SetItemValue(UnavailableForNewIssueActiveKey, false);
			}
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();

			if (newController != null)
			{
				newController.NewObjectAction.Active.RemoveItem(UnavailableForNewIssueActiveKey);
			}
			if (webListViewController != null)
			{
				webListViewController.EditAction.Active.RemoveItem(UnavailableForNewIssueActiveKey);
			}
		}
	}

	public class MyNewItemRowListViewController : NewItemRowListViewController
	{
		protected override NewItemRowPosition CalculateNewItemRowPosition()
		{
			NewItemRowPosition result = NewItemRowPosition.None;
			if (View.Model != null && View.AllowEdit && View.AllowNew)
			{
				result = ((IModelListViewNewItemRow)View.Model).NewItemRowPosition;
			}
			return result;
		}
	}
}
