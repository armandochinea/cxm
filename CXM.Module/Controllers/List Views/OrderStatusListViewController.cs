﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Xpo;
using CXM.Module.BusinessObjects.CXM;
using Newtonsoft.Json;
using System.Net.Http;
using System.Data.SqlClient;

namespace CXM.Module.Controllers.List_Views
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class OrderStatusListViewController : ViewController
	{
		string paramServiceAddress = string.Empty;
		bool firstTime = true;

		public OrderStatusListViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			//Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem += OrderStatusListViewController_CustomProcessSelectedItem;

			List<GlobalParameters> globalParametersList = View.ObjectSpace.GetObjectsQuery<GlobalParameters>().ToList();
			Dictionary<string, string> globalParameters = new Dictionary<string, string>();

			foreach (var parameter in globalParametersList)
			{
				globalParameters.Add(parameter.Name, parameter.Value);
			}

			paramServiceAddress = globalParameters.ContainsKey("SERVICE_ADDRESS") ? globalParameters["SERVICE_ADDRESS"] : "http://63.131.254.222:10000/MSFRESTServicesBFHCXM/MSFRESTSrv.svc";

			if (firstTime)
			{
				FetchData.DoExecute();
				firstTime = false;
			}
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();
			firstTime = true;

			//Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem -= OrderStatusListViewController_CustomProcessSelectedItem;
		}

		private void FetchData_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			try
			{
				var objSpace = View.ObjectSpace;
				var callHelper = objSpace.GetObjectsQuery<CallHelper>().Where(r => r.UserID == SecuritySystem.CurrentUserName).FirstOrDefault();

				if (callHelper != null)
				{
					((XPObjectSpace)objSpace).Session.ExecuteNonQuery("DELETE FROM OrderStatusDetail WHERE Route = '" + SecuritySystem.CurrentUserName + "' AND Customer = '" + callHelper.CurrentCall.CustomerID.CustomerNo + "'");
					((XPObjectSpace)objSpace).Session.ExecuteNonQuery("DELETE FROM OrderStatus WHERE Route = '" + SecuritySystem.CurrentUserName + "' AND Customer = '" + callHelper.CurrentCall.CustomerID.CustomerNo + "'");

					var endPoint = string.Format("{0}/onlineOrderStatus/{1}/{2}?format=json", paramServiceAddress, SecuritySystem.CurrentUserName, callHelper.CurrentCall.CustomerID.CustomerNo);

					// Create the Client
					var client = new HttpClient();

					// Post the JSON
					var responseMessage = client.GetAsync(endPoint).Result;
					var stringResult = responseMessage.Content.ReadAsStringAsync().Result;

					// Convert JSON back to the Object
					var responseObject = JsonConvert.DeserializeObject<OrderStatusResult>(stringResult);

					if (responseObject.Fault == null)
					{
						if (responseObject.Orders.Count > 0)
						{
							string orderID;

							foreach (var order in responseObject.Orders)
							{
								orderID = ((XPObjectSpace)objSpace).Session.ExecuteScalar("SELECT NEWID();").ToString();

								((XPObjectSpace)objSpace).Session.ExecuteNonQuery(
									"INSERT INTO OrderStatus (ID, Route, OrderNumber, OrderType, Warehouse, Customer, OrderDate, ShipDate, PickingNumber, PickingDate, InvoiceNumber, InvoiceDate, TruckerNumber, TotalAmount, HoldCode) " +
									"VALUES ('" + orderID + "', '" + order.Header.Route + "', " + order.Header.OrderNumber + ", '" + order.Header.OrderType + "', '" + order.Header.Warehouse + "', '" + order.Header.Customer + "', '" + order.Header.OrderDate.ToShortDateString() +
									"', '" + order.Header.ShipDate.ToShortDateString() + "', " + order.Header.PickingNumber + ", '" + order.Header.PickingDate.ToShortDateString() + "', " + order.Header.InvoiceNumber + ", '" + order.Header.InvoiceDate.ToShortDateString() +
									"', " + order.Header.TruckerNumber + ", " + order.Header.TotalAmount + ", '" + order.Header.HoldCode + "');"
								);

								foreach (var detail in order.Details)
								{
									((XPObjectSpace)objSpace).Session.ExecuteNonQuery(
										"INSERT INTO OrderStatusDetail (ID, OrderStatus, Route, OrderNumber, OrderType, Warehouse, Customer, Product, Quantity, UM, UnitPrice, ExtPrice, Status, StatusDesc) " +
										"VALUES (NEWID(), '" + orderID + "', '" + detail.Route + "', " + detail.OrderNumber + ", '" + detail.OrderType + "', '" + detail.Warehouse + "', '" + detail.Customer + "', '" + detail.Product +
										"', " + detail.Quantity + ", '" + detail.UM + "', " + detail.UnitPrice + ", " + detail.ExtPrice + ", '" + detail.Status +
										"', '" + detail.StatusDescription + "');"
									);
								}
							}

							//ListView listView = (ListView)View;
							//listView.CollectionSource.Criteria["MainFilter"] = CriteriaOperator.Parse("Route == ? && Customer == ?", SecuritySystem.CurrentUserName, callHelper.CurrentCall.CustomerID.CustomerNo);
						}
					}					
				}
			}
			catch (ActionExecutionException actEx)
			{
				throw new Exception(actEx.Message);
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void OrderStatusListViewController_CustomProcessSelectedItem(object sender, CustomProcessListViewSelectedItemEventArgs e)
		{
			e.Handled = true;
		}

	}
}
