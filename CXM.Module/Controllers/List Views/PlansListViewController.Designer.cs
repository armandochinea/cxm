﻿namespace CXM.Module.Controllers.List_Views
{
	partial class PlansListViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.ConfirmPlan = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
			// 
			// ConfirmPlan
			// 
			this.ConfirmPlan.Caption = "Confirm Plan";
			this.ConfirmPlan.Category = "Edit";
			this.ConfirmPlan.ConfirmationMessage = "Do you wish to confirm the seleted plan(s)?";
			this.ConfirmPlan.Id = "ConfirmPlan";
			this.ConfirmPlan.ImageName = "Action_Validation_Validate";
			this.ConfirmPlan.ToolTip = null;
			this.ConfirmPlan.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ConfirmPlan_Execute);
			// 
			// PlansListViewController
			// 
			this.Actions.Add(this.ConfirmPlan);
			this.TargetViewId = "Plans_ListView";

		}

		#endregion

		private DevExpress.ExpressApp.Actions.SimpleAction ConfirmPlan;
	}
}
