﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using CXM.Module.BusinessObjects.CXM;
//using CXM.Module.ProfitStars;
using DevExpress.ExpressApp.Web;

namespace CXM.Module.Controllers.List_Views
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class AccountListViewController : ViewController
    {
        public AccountListViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void UnregisterAccount_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            var listView = View as ListView;

            if (listView != null)
            {
                if (listView.SelectedObjects.Count > 0)
                {
                    string message = string.Empty;
                    string[] result;

                    foreach (Account item in listView.SelectedObjects)
                    {
                        //result = PS_Context.UnregisterAccount(item.AccountKey).Split('|');

                        //if (result.Length > 0)
                        //{
                        //    if (result[0].ToString() == "OK")
                        //    {
                        //        message += System.Environment.NewLine + string.Format($"* {item.AccountKey}");

                        //        Account account = View.ObjectSpace.GetObjectByKey<Account>(item.ID);
                        //        if (account != null)
                        //        {
                        //            account.IsActive = false;
                        //            account.Save();
                        //        }
                        //    }
                        //    else
                        //    {
                        //        throw new Exception(string.Format($"Error unregistering account with key {item.AccountKey}. Message: {result[1].ToString()}"));
                        //    }
                        //}
                    }

                    if (message.Length > 0)
                    {
                        WebWindow.CurrentRequestWindow.RegisterClientScript("Success", @"alert('The following accounts were successfully unregistered from PaymentVault:" + message + "');");
                        View.ObjectSpace.CommitChanges();
                    }
                }
            }
        }
    }
}
