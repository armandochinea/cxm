﻿namespace CXM.Module.Controllers.List_Views
{
	partial class OnCallIssuesListViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.IssueEndCall = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
			this.OnCallNewIssue = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
			// 
			// IssueEndCall
			// 
			this.IssueEndCall.Caption = "End Call";
			this.IssueEndCall.Category = "Edit";
			this.IssueEndCall.ConfirmationMessage = null;
			this.IssueEndCall.Id = "IssueEndCall";
			this.IssueEndCall.ToolTip = null;
			this.IssueEndCall.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.IssueEndCall_Execute);
			// 
			// OnCallNewIssue
			// 
			this.OnCallNewIssue.AcceptButtonCaption = null;
			this.OnCallNewIssue.CancelButtonCaption = null;
			this.OnCallNewIssue.Caption = "New";
			this.OnCallNewIssue.Category = "Edit";
			this.OnCallNewIssue.ConfirmationMessage = null;
			this.OnCallNewIssue.Id = "OnCallNewIssue";
			this.OnCallNewIssue.TargetViewId = "Nothing";
			this.OnCallNewIssue.ToolTip = null;
			this.OnCallNewIssue.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.OnCallNewIssue_CustomizePopupWindowParams);
			// 
			// OnCallIssuesListViewController
			// 
			this.Actions.Add(this.IssueEndCall);
			this.Actions.Add(this.OnCallNewIssue);
			this.TargetViewId = "OnCall_Issues_ListView";

		}

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction IssueEndCall;
		private DevExpress.ExpressApp.Actions.PopupWindowShowAction OnCallNewIssue;
	}
}
