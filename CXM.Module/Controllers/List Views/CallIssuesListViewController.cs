﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using CXM.Module.BusinessObjects.CXM;
using System.Data.SqlClient;

namespace CXM.Module.Controllers
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class CallIssuesListViewViewController : ViewController
	{
		public CallIssuesListViewViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem += CallIssuesListViewViewController_CustomProcessSelectedItem;
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();

			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();

			Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem -= CallIssuesListViewViewController_CustomProcessSelectedItem;
		}

		void CallIssuesListViewViewController_CustomProcessSelectedItem(object sender, CustomProcessListViewSelectedItemEventArgs e)
		{
			try
			{
				e.Handled = true;
				EditIssue.DoExecute();
				//if ((View as ListView).CurrentObject is Issues issue)
				//{
				//	IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(Issues));
				//	DetailView detailView = Application.CreateDetailView(objectSpace, "New_Issues_DetailView", false, objectSpace.GetObject(issue));
				//	detailView.ViewEditMode = ViewEditMode.Edit;
				//	e.InnerArgs.ShowViewParameters.CreatedView = detailView;
				//}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void EditIssue_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			try
			{
				if ((View as ListView).CurrentObject is Issues issue)
				{
					IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(Issues));
					DetailView detailView = Application.CreateDetailView(objectSpace, "Issues_DetailView", false, objectSpace.GetObject(issue));
					detailView.ViewEditMode = ViewEditMode.Edit;
					e.ShowViewParameters.CreatedView = detailView;
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}
	}
}
