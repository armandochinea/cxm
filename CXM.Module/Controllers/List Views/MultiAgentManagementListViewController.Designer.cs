﻿namespace CXM.Module.Controllers.List_Views
{
	partial class MultiAgentManagementListViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.MultiAgents = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
			// 
			// MultiAgents
			// 
			this.MultiAgents.Caption = "Multi Agents";
			this.MultiAgents.Category = "Edit";
			this.MultiAgents.ConfirmationMessage = "Do you want to assign the selected agent(s) to this multi-agent account?";
			this.MultiAgents.Id = "MultiAgents";
			this.MultiAgents.ToolTip = null;
			this.MultiAgents.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.MultiAgents_Execute);
			// 
			// MultiAgentManagementListViewController
			// 
			this.Actions.Add(this.MultiAgents);
			this.TargetViewId = "MultiAgentManagement_ListView";

		}

		#endregion

		private DevExpress.ExpressApp.Actions.SingleChoiceAction MultiAgents;
	}
}
