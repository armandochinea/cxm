﻿namespace CXM.Module.Controllers.List_Views
{
	partial class SalesOrderOrderPromoesListViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.ApplyDiscount = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
			// 
			// ApplyDiscount
			// 
			this.ApplyDiscount.Caption = "Apply Discount";
			this.ApplyDiscount.Category = "Edit";
			this.ApplyDiscount.ConfirmationMessage = null;
			this.ApplyDiscount.Id = "ApplyDiscount";
			this.ApplyDiscount.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
			this.ApplyDiscount.ToolTip = null;
			this.ApplyDiscount.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ApplyDiscount_Execute);
			// 
			// SalesOrderOrderPromoesListViewController
			// 
			this.Actions.Add(this.ApplyDiscount);
			this.TargetViewId = "SalesOrder_OrderPromoes_ListView";

		}

		#endregion

		private DevExpress.ExpressApp.Actions.SimpleAction ApplyDiscount;
	}
}
