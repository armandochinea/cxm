﻿namespace CXM.Module.Controllers.List_Views
{
	partial class SalesOrderSalesLinesListViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.AddItems = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
			this.QuickEntry = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
			// 
			// AddItems
			// 
			this.AddItems.AcceptButtonCaption = null;
			this.AddItems.CancelButtonCaption = null;
			this.AddItems.Caption = "Add Items";
			this.AddItems.Category = "Edit";
			this.AddItems.ConfirmationMessage = null;
			this.AddItems.Id = "AddItems";
			this.AddItems.QuickAccess = true;
			this.AddItems.ToolTip = null;
			this.AddItems.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.AddItems_CustomizePopupWindowParams);
			this.AddItems.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.AddItems_Execute);
			// 
			// QuickEntry
			// 
			this.QuickEntry.AcceptButtonCaption = null;
			this.QuickEntry.CancelButtonCaption = null;
			this.QuickEntry.Caption = "Quick Entry";
			this.QuickEntry.Category = "Edit";
			this.QuickEntry.ConfirmationMessage = null;
			this.QuickEntry.Id = "QuickEntry";
			this.QuickEntry.ToolTip = null;
			this.QuickEntry.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.QuickEntry_CustomizePopupWindowParams);
			// 
			// SalesOrderSalesLinesListViewController
			// 
			this.Actions.Add(this.AddItems);
			this.Actions.Add(this.QuickEntry);
			this.TargetViewId = "SalesOrder_SalesLines_ListView;SpeedSalesOrder_SalesLines_ListView;";

		}

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction AddItems;
		private DevExpress.ExpressApp.Actions.PopupWindowShowAction QuickEntry;
	}
}
