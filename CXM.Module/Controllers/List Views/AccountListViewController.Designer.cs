﻿namespace CXM.Module.Controllers.List_Views
{
    partial class AccountListViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.UnregisterAccount = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // UnregisterAccount
            // 
            this.UnregisterAccount.Caption = "Unregister";
            this.UnregisterAccount.Category = "Edit";
            this.UnregisterAccount.ConfirmationMessage = "Are you sure you wish to unregister the selected accounts?";
            this.UnregisterAccount.Id = "UnregisterAccount";
            this.UnregisterAccount.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireMultipleObjects;
            this.UnregisterAccount.ToolTip = null;
            this.UnregisterAccount.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.UnregisterAccount_Execute);
            // 
            // AccountListViewController
            // 
            this.Actions.Add(this.UnregisterAccount);
            this.TargetViewId = "Account_ListView";

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction UnregisterAccount;
    }
}
