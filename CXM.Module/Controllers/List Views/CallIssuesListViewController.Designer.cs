﻿namespace CXM.Module.Controllers
{
	partial class CallIssuesListViewViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.EditIssue = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
			// 
			// EditIssue
			// 
			this.EditIssue.Caption = "Edit";
			this.EditIssue.Category = "Edit";
			this.EditIssue.ConfirmationMessage = null;
			this.EditIssue.Id = "EditIssue";
			this.EditIssue.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
			this.EditIssue.ToolTip = null;
			this.EditIssue.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.EditIssue_Execute);
			// 
			// CallIssuesListViewViewController
			// 
			this.Actions.Add(this.EditIssue);
			this.TargetObjectType = typeof(CXM.Module.BusinessObjects.CXM.Issues);
			this.TargetViewId = "Calls_Issues_ListView";

		}

		#endregion
		private DevExpress.ExpressApp.Actions.SimpleAction EditIssue;
	}
}
