﻿namespace CXM.Module.Controllers.List_Views
{
	partial class SalesOrderCustomerSpecialProductsListViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.RefreshProducts = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // RefreshProducts
            // 
            this.RefreshProducts.Caption = "Sync";
            this.RefreshProducts.Category = "Edit";
            this.RefreshProducts.ConfirmationMessage = null;
            this.RefreshProducts.Id = "RefreshProducts";
            this.RefreshProducts.ImageName = "Action_Refresh";
            this.RefreshProducts.ToolTip = null;
            this.RefreshProducts.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.RefreshProducts_Execute);
            // 
            // SalesOrderCustomerSpecialProductsListViewController
            // 
            this.Actions.Add(this.RefreshProducts);
            this.TargetViewId = "SalesOrder_CustomerSpecialProducts_ListView";
            this.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.ListView);

		}

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction RefreshProducts;
    }
}
