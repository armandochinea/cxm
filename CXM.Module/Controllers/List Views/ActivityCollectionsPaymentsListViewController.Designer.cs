﻿namespace CXM.Module.Controllers.List_Views
{
    partial class ActivityCollectionsPaymentsListViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SelectInvoices = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // SelectInvoices
            // 
            this.SelectInvoices.AcceptButtonCaption = null;
            this.SelectInvoices.CancelButtonCaption = null;
            this.SelectInvoices.Caption = "Select Invoices";
            this.SelectInvoices.Category = "Edit";
            this.SelectInvoices.ConfirmationMessage = null;
            this.SelectInvoices.Id = "SelectInvoices";
            this.SelectInvoices.ToolTip = null;
            this.SelectInvoices.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.SelectInvoices_CustomizePopupWindowParams);
            this.SelectInvoices.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.SelectInvoices_Execute);
            // 
            // ActivityCollectionsPaymentsListViewController
            // 
            this.Actions.Add(this.SelectInvoices);
            this.TargetViewId = "Activity_Collections_Payments_ListView";

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction SelectInvoices;
    }
}
