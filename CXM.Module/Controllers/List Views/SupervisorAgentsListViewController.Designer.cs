﻿namespace CXM.Module.Controllers.List_Views
{
	partial class SupervisorAgentsListViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.Supervisors = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
			// 
			// Supervisors
			// 
			this.Supervisors.Caption = "Supervisors";
			this.Supervisors.Category = "Edit";
			this.Supervisors.ConfirmationMessage = "Do you want to assign the selected agent(s) to this supervisor?";
			this.Supervisors.Id = "Supervisors";
			this.Supervisors.ToolTip = null;
			this.Supervisors.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.Supervisors_Execute);
			// 
			// SupervisorAgentsListViewController
			// 
			this.Actions.Add(this.Supervisors);
			this.TargetViewId = "SupervisorAgents_ListView";

		}

		#endregion

		private DevExpress.ExpressApp.Actions.SingleChoiceAction Supervisors;
	}
}
