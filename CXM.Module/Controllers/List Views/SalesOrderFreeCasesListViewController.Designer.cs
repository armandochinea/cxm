﻿namespace CXM.Module.Controllers.List_Views
{
	partial class SalesOrderFreeCasesListViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.ChangeFreeCaseStatus = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
			// 
			// ChangeFreeCaseStatus
			// 
			this.ChangeFreeCaseStatus.Caption = "Change Free Case Status";
			this.ChangeFreeCaseStatus.Category = "Edit";
			this.ChangeFreeCaseStatus.ConfirmationMessage = null;
			this.ChangeFreeCaseStatus.Id = "ChangeFreeCaseStatus";
			this.ChangeFreeCaseStatus.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
			this.ChangeFreeCaseStatus.TargetViewId = "";
			this.ChangeFreeCaseStatus.ToolTip = null;
			this.ChangeFreeCaseStatus.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ChangeFreeCaseStatus_Execute);
			// 
			// SalesOrderFreeCasesListViewController
			// 
			this.Actions.Add(this.ChangeFreeCaseStatus);
			this.TargetViewId = "SalesOrder_FreeCases_ListView";

		}

		#endregion
		private DevExpress.ExpressApp.Actions.SimpleAction ChangeFreeCaseStatus;
	}
}
