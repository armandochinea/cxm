﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using CXM.Module.BusinessObjects.CXM;
using System.Data.SqlClient;

namespace CXM.Module.Controllers.List_Views
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class CallHistoryListViewController : ViewController
	{
		public CallHistoryListViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem += CallHistoryListViewViewController_CustomProcessSelectedItem;
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();

			Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem -= CallHistoryListViewViewController_CustomProcessSelectedItem;
		}

		void CallHistoryListViewViewController_CustomProcessSelectedItem(object sender, CustomProcessListViewSelectedItemEventArgs e)
		{
			try
			{
				e.Handled = true;
				Calls call = null;

				if (View.Id == "CallHistory_Alt_ListView")
					call = (View.CurrentObject as CallResult).Call;
				else
					call = View.CurrentObject as Calls;

				if (call != null)
				{
					IObjectSpace newObjectSpace = Application.CreateObjectSpace();
					CallResult callResult = newObjectSpace.GetObjectsQuery<CallResult>().Where(r => r.Call.ID == call.ID).FirstOrDefault();
					e.InnerArgs.ShowViewParameters.CreatedView = Application.CreateDetailView(newObjectSpace, "CompletedCallResult_DetailView", true, callResult);
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}
	}
}
