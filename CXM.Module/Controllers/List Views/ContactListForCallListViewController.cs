﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using CXM.Module.BusinessObjects.CXM;

namespace CXM.Module.Controllers.List_Views
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class ContactListForCallListViewController : ViewController
	{
		public ContactListForCallListViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem += ContactListForCallListViewController_CustomProcessSelectedItem;
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem -= ContactListForCallListViewController_CustomProcessSelectedItem;

			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();
		}
		private void ContactListForCallListViewController_CustomProcessSelectedItem(object sender, CustomProcessListViewSelectedItemEventArgs e)
		{
			e.Handled = true;
			
			var objSpace = View.ObjectSpace;
			var selectedContactInList = (ContactListForCall)View.CurrentObject;

			if (selectedContactInList != null)
			{
				var selectedContact = objSpace.GetObjectsQuery<Contacts>().Where(r => r.ContactID == selectedContactInList.Contact.ContactID).FirstOrDefault();
				var currentCall = objSpace.GetObjectsQuery<Calls>().Where(r => r.ID == selectedContactInList.Call.ID).FirstOrDefault();

				// Only change the info for unattempted calls.
				if (currentCall != null && selectedContact != null && !currentCall.Attempted)
				{
					currentCall.Contact = selectedContact;
					currentCall.ContactName = selectedContact.Name;
					currentCall.CalledPhoneNumber = selectedContact.Phone;

					objSpace.CommitChanges();
				}
			}
		}
	}
}
