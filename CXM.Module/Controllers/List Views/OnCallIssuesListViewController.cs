﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using CXM.Module.BusinessObjects.CXM;
using DevExpress.ExpressApp.Xpo;
using System.Data.SqlClient;

namespace CXM.Module.Controllers.List_Views
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class OnCallIssuesListViewController : ViewController
	{
		public OnCallIssuesListViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}

		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();
		}

		private void IssueEndCall_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			try
			{

				IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(CallResult));

				CxmUser user = objectSpace.GetObject<CxmUser>((CxmUser)SecuritySystem.CurrentUser);
				CallHelper callHelper = objectSpace.GetObjectsQuery<CallHelper>().Where(r => r.UserID == SecuritySystem.CurrentUserName).FirstOrDefault();

				if (callHelper != null)
				{
					Calls currentCall = objectSpace.GetObjectsQuery<Calls>().Where(x => x.ID == callHelper.CurrentCall.ID).FirstOrDefault();
					if (currentCall != null)
					{
						currentCall.EndDateTime = DateTime.Now;
						currentCall.Attempted = true;

						// Obtain and assign the CallNumber for current call.
						if (user != null)
						{
							user.NextCallNumber++;
							currentCall.CallNumber = string.Format("{0}-{1}", user.UserName, user.NextCallNumber.ToString("000000"));
						}
					}

					// Clear the current call reference in CallHelper.
					callHelper.CurrentCall = null;

					//****** SUBMIT DB CHANGES FOR VOIP COMMANDS
					DevExpress.Xpo.Session session = ((XPObjectSpace)(objectSpace)).Session;
					string qry = string.Format("Insert Into VOIPCommands (UserID, Command) values ('{0}','{1}')", user.Oid, "HANGUP|");
					session.ExecuteQuery(qry);

					// Get the current call's result info to display, or create one.
					CallResult callResult = objectSpace.GetObjectsQuery<CallResult>().Where(r => r.Call == currentCall).FirstOrDefault();
					if (callResult == null)
					{
						callResult = objectSpace.CreateObject<CallResult>();
						callResult.Call = currentCall;
					}

					// Save changes to the database.
					objectSpace.CommitChanges();

					// Prepare current call's results screen to display to the user.
					DetailView detailView = Application.CreateDetailView(objectSpace, "Agent_CallResult_DetailView", true, callResult);
					detailView.ViewEditMode = ViewEditMode.Edit;
					e.ShowViewParameters.CreatedView = detailView;

					View.Close();
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void OnCallNewIssue_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(Issues));

				CallHelper callHelper = objectSpace.GetObjectsQuery<CallHelper>().Where(n => n.UserID == SecuritySystem.CurrentUserName).FirstOrDefault();
				if (callHelper != null)
				{
					if (callHelper.CurrentCall != null)
					{

						Customers customer = objectSpace.GetObject<Customers>(callHelper.CurrentCall.CustomerID);
						Issues newIssue = objectSpace.CreateObject<Issues>();
						newIssue.CustomerID = customer;

						DetailView detailView = Application.CreateDetailView(objectSpace, "New_Issues_DetailView", true, newIssue);
						detailView.ViewEditMode = ViewEditMode.Edit;
						e.View = detailView;

						detailView.Closed += NewIssueDetailViewController_Closed;
					}
				}
				
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void NewIssueDetailViewController_Closed(object sender, EventArgs e)
		{
			try
			{
				var detailView = (DetailView)sender;

				detailView.Closed -= NewIssueDetailViewController_Closed;

				if (detailView.SelectedObjects.Count == 1)
				{
					var createdIssue = (Issues)detailView.SelectedObjects[0];
					if (createdIssue?.IssueID != 0)
					{
						Application.ShowViewStrategy.ShowMessage(string.Format("Issue #{0} created.", createdIssue.IssueID), InformationType.Success, 3000, InformationPosition.Top);

						try
						{
							// Send email of the issue to the assignee.
							var objSpace = (XPObjectSpace)((ListView)View).ObjectSpace;
							var result = objSpace.Session.ExecuteSproc("SendNewIssueEmail", createdIssue.IssueID);
						}
						catch (SqlException sqlEx)
						{
							throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
						}
						catch (Exception ex)
						{
							throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
						}

						try
						{
							var objSpace = View.ObjectSpace;
							CallHelper callHelper = objSpace.GetObjectsQuery<CallHelper>().Where(n => n.UserID == SecuritySystem.CurrentUserName).FirstOrDefault();

							if (callHelper != null)
							{
								if (callHelper.CurrentCall != null)
								{
									CallResult callResult = objSpace.GetObjectsQuery<CallResult>().Where(r => r.Call == callHelper.CurrentCall).FirstOrDefault();
									if (callResult == null)
									{
										callResult = objSpace.CreateObject<CallResult>();
										callResult.Call = callHelper.CurrentCall;
									}
									callResult.NewIssues = true;

									objSpace.CommitChanges();
								}
							}
						}
						catch (SqlException sqlEx)
						{
							throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
						}
						catch (Exception ex)
						{
							throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
						}

						View.ObjectSpace.Refresh();
					}
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}
	}
}
