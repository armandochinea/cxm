﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Web.Editors.ASPx;
using DevExpress.Web;
using System.ComponentModel;
using CXM.Module.BusinessObjects.CXM;
using DevExpress.Utils.DragDrop;
using DevExpress.ExpressApp.Web;
using DevExpress.ExpressApp.Web.Utils;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CXM.Module.Controllers.List_Views
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class PlansCallsListViewController : ViewController
	{
		short callOrder = 0;

		public PlansCallsListViewController()
		{
			// TargetViewId = "DomainObject1_ListView";
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			//WebWindow.CurrentRequestWindow.PagePreRender += CurrentRequestWindowOnPagePreRender;

			// Perform various tasks depending on the target View.
		}
		//private void CurrentRequestWindowOnPagePreRender(object sender, EventArgs e)
		//{
		//    WebWindow.CurrentRequestWindow.RegisterClientScriptInclude("reorderScript", "GridViewFunctions.js");
		//}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.

			ASPxGridListEditor listEditor = ((ListView)View).Editor as ASPxGridListEditor;
			if (listEditor != null)
			{
				ASPxGridView gridView = (ASPxGridView)listEditor.Grid;
				gridView.ClientInstanceName = View.Id;
				GridViewDataColumn detailsColums = gridView.Columns["CallOrder"] as GridViewDataColumn;
				if (detailsColums != null)
				{
					detailsColums.DataItemTemplate = new UpDownButtonsTemplate();
					gridView.ClearSort();
					gridView.SortBy(detailsColums, DevExpress.Data.ColumnSortOrder.Ascending);
					gridView.CustomCallback += new ASPxGridViewCustomCallbackEventHandler(gridView_CustomCallback);
				}




				//ASPxGlobalEvents globalEvents = new ASPxGlobalEvents();
				//globalEvents.ID = "ge";
				//globalEvents.ClientSideEvents.ControlsInitialized = "function (s,e) {InitalizejQuery();}";
				//globalEvents.ClientSideEvents.EndCallback = "function (s,e) {InitalizejQuery();}";
				//((Control)View.Control).Controls.Add(globalEvents);


				//ClientSideEventsHelper.AssignClientHandlerSafe(gridView, "EndCallback", "function(s,e) { NextAction();}", "grid.NextAction");

				///// gridView.CustomCallback += Grid_CustomCallback;
				////gridView.HtmlRowPrepared += Grid_HtmlRowPrepared;

				//gridView.Styles.Row.CssClass = "draggable";
				//gridView.ClientInstanceName = "gridView";
				//gridView.ClientSideEvents.EndCallback = "function(s,e) { NextAction();}";


				// ClientSideEventsHelper.AssignClientHandlerSafe(gridView, "EndCallback", "function(s,e) { NextAction();}", "grid.NextAction");

				//v3m TESTING
				//' gridView. += new System.Windows.Forms.MouseEventHandler(this.gridView_MouseMove);
				//'gridView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView_MouseDown);

				// }
			}

			//V3M
			//DragDropBehavior gridControlBehavior = behaviorManager1.GetBehavior<DragDropBehavior>(gridView);
			//gridControlBehavior.DragDrop += Behavior_DragDrop;
			//gridControlBehavior.DragOver += Behavior_DragOver;
		}

		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();
		}

		//V3m
		//protected override void OnViewChanged()
		//{
		//    base.OnViewChanged();
		//    if (View != null && View.Id == "Plans_Calls_ListView")
		//    {
		//        WebWindow.CurrentRequestWindow.RegisterClientScript("globalEventScript", "SubscrubeGlobalEvents();");
		//    }
		//    else
		//    {
		//        //WebWindow.CurrentRequestWindow.RegisterClientScript("globalEventScript", "UnsubscrubeGlobalEvents();");
		//    }
		//}


		/********/
		void gridView_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
		{
			string[] parameters = e.Parameters.Split('|');
			ASPxGridView gridView = (ASPxGridView)sender;
			bool foundAZero = false;


			for (short itm = 1; itm <= gridView.VisibleRowCount; itm++)
			{
				Calls curItm = ((Calls)gridView.GetRow(itm - 1));
				if (curItm.CallOrder == 0)
				{
					foundAZero = true;
					break;

				}
			}

			if (foundAZero == true)
			{
				for (short itm = 1; itm <= gridView.VisibleRowCount; itm++)
				{
					Calls curItm = ((Calls)gridView.GetRow(itm - 1));
					curItm.CallOrder = itm;
					curItm.Save();


				}
			}





			if (parameters.Length == 2 && (parameters[0] == "up" || parameters[0] == "down"))
			{
				//ASPxGridView gridView = (ASPxGridView)sender;
				Object key = TypeDescriptor.GetConverter(View.ObjectTypeInfo.KeyMember.MemberType).ConvertFromString(parameters[1]);
				int rowIndex = gridView.FindVisibleIndexByKeyValue(key);
				if (parameters[0] == "up" && rowIndex > 0)
				{
					SwitchIndices((Calls)gridView.GetRow(rowIndex), (Calls)gridView.GetRow(rowIndex - 1), rowIndex, rowIndex - 1);
				}
				if (parameters[0] == "down" && rowIndex < gridView.VisibleRowCount - 1)
				{
					SwitchIndices((Calls)gridView.GetRow(rowIndex), (Calls)gridView.GetRow(rowIndex + 1), rowIndex, rowIndex + 1);
				}
			}
		}
		private void SwitchIndices(Calls currentObject, Calls targetObject, int oIndex, int nIndex)
		{
			try
			{
				var myObj = ObjectSpace.GetObjectsQuery<Calls>().Where(r => r.ID == currentObject.ID).FirstOrDefault();
				var otherObj = ObjectSpace.GetObjectsQuery<Calls>().Where(r => r.ID == targetObject.ID).FirstOrDefault();
				int oldIndex = oIndex;//currentObject.CallOrder;
				myObj.CallOrder = (Int16)(nIndex);//.CallOrder;
				otherObj.CallOrder = (Int16)(oldIndex);
				//View.ObjectSpace.CommitChanges();
				ObjectSpace.CommitChanges();
				//myObj.Save();
				//otherObj.Save();
				View.Refresh();
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}
	}

	public class UpDownButtonsTemplate : ITemplate
	{
		const string ClickEventHandlerFormat = "function(s, e) {{ e.processOnServer = false; {0}.PerformCallback(\"{1}|{2}\"); }}";
		void ITemplate.InstantiateIn(Control container)
		{
			GridViewDataItemTemplateContainer gridContainer = (GridViewDataItemTemplateContainer)container;
			ASPxButton downButton = RenderHelper.CreateASPxButton();
			downButton.Text = "Down";
			downButton.ClientSideEvents.Click = String.Format(ClickEventHandlerFormat, gridContainer.Grid.ClientInstanceName, "down", gridContainer.KeyValue);
			downButton.ID = "downButton_" + container.ID;
			ASPxButton upButton = RenderHelper.CreateASPxButton();
			upButton.Text = "Up";
			upButton.ClientSideEvents.Click = String.Format(ClickEventHandlerFormat, gridContainer.Grid.ClientInstanceName, "up", gridContainer.KeyValue);
			upButton.ID = "upButton_" + container.ID;
			Table table = new Table();
			table.Rows.Add(new TableRow());
			table.Rows[0].Cells.Add(new TableCell());
			table.Rows[0].Cells.Add(new TableCell());
			table.Rows[0].Cells[0].Controls.Add(downButton);
			table.Rows[0].Cells[1].Controls.Add(upButton);
			table.Attributes["onclick"] = RenderHelper.EventCancelBubbleCommand;
			container.Controls.Add(table);
		}
	}
}

