﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using CXM.Module.BusinessObjects.CXM;
//using static CXM.Module.Controllers.ChangeStatusViewController;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using System.Data.SqlClient;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp.Web;
using System.Web.UI.WebControls;
using DevExpress.Xpo;
using DevExpress.Xpo.Helpers;
//using CXM.Module.ProfitStars;

namespace CXM.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class AgentCallsListViewController : ViewController
    {
        bool planCreated = false;

        public AgentCallsListViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.

            if (!planCreated)
            {
                CxmUser user = View.ObjectSpace.GetObjectsQuery<CxmUser>().FirstOrDefault(u => u.UserName == SecuritySystem.CurrentUserName);
				if (user != null)
				{
					var isAgent = user.Roles.Any(r => r.Name.Contains("Agent"));

					if (isAgent)
					{
						string userName = user.UserName;

						if (user.MultiAgent != null)
						{
							CxmUser multiAgent = View.ObjectSpace.GetObjectsQuery<CxmUser>().Where(u => u.Oid == user.MultiAgent.Oid).FirstOrDefault();
							if (multiAgent != null) userName = multiAgent.UserName;
						}

						var objSpace = (XPObjectSpace)((ListView)View).ObjectSpace;
						var sql = $"EXEC CreateUserPlan '{userName}'";
						objSpace.Session.ExecuteNonQuery(sql);
						planCreated = true;
					}
				}
            }

            Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem += AgentCallsListViewController_CustomProcessSelectedItem;

            AgentCallPlanFilter.SelectedIndex = 1;
            ApplyFilter();
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();

            Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem -= AgentCallsListViewController_CustomProcessSelectedItem;
        }

        void ShowWarning(string warning) => Application.ShowViewStrategy.ShowMessage(warning, InformationType.Warning, 5000, InformationPosition.Top);

        void AgentCallsListViewController_CustomProcessSelectedItem(object sender, CustomProcessListViewSelectedItemEventArgs e)
        {
            e.Handled = true;

            var listView = (View as ListView);
            var selectedCall = listView?.CurrentObject as Calls;

            if (selectedCall != null)
            {
                if (listView.SelectedObjects.Count > 1)
                {
                    listView.SelectedObjects.Clear();
                    listView.SelectedObjects.Add(selectedCall);
                }

                if (selectedCall.Attempted)
                {
                    AgentStartCall.DoExecute();
                }
                else
                {
                    DetailView detailView = Application.CreateDetailView(View.ObjectSpace, "Calls_Outgoing_Confirmation_DetailView", false, selectedCall);
                    Application.ShowViewStrategy.ShowViewInPopupWindow(detailView, OkDelegate, null, "Yes", "No");
                }
            }
        }

        public void OkDelegate()
        {
            AgentStartCall.DoExecute();
        }

        private void AgentStartCall_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                var view = (View as ListView);
                var objSpace = view.ObjectSpace;

                if (view.SelectedObjects.Count <= 0)
                {
                    return;
                }

                var selectedCall = (view.SelectedObjects[0] as Calls);
                
                List<Calls> viewList = new List<Calls>();
                if (view.CollectionSource.List.GetType() == typeof(XpoServerCollectionFlagger))
                {
                    for (int i = 0; i < view.CollectionSource.List.Count - 1; i++)
                    {
                        viewList.Add((Calls)view.CollectionSource.List[i]);
                    }
                }
                else
                {
                    foreach (var item in view.CollectionSource.List)
                    {
                        viewList.Add((Calls)item);
                    }
                }
				//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
                foreach (Calls call in viewList)
                {
                    if (!call.Attempted && call.Status == Calls.CallStatus.New)
                    {
                        if (call.CallOrder < selectedCall?.CallOrder)
                        {
                            ShowWarning("The selected call cannot be started because there is a previous unattempted call in the plan.");
                            return;
                        }
                    }
                }

                //if (View.SelectedObjects.Count == 1)
                if (selectedCall != null)
                {
					//PermissionPolicyUser currentUser = objSpace.GetObjectsQuery<PermissionPolicyUser>().Where(r => r.UserName == SecuritySystem.CurrentUserName).FirstOrDefault();
					CxmUser currentUser = objSpace.GetObjectsQuery<CxmUser>().Where(r => r.UserName == SecuritySystem.CurrentUserName).FirstOrDefault();
					Guid? multiAgentId = null;

					if (currentUser.MultiAgent != null) multiAgentId = currentUser.MultiAgent.Oid;

					Plans todayPlan = null;
					if (multiAgentId != null)
						todayPlan = objSpace.GetObjectsQuery<Plans>().Where(r => r.Agent.Oid == multiAgentId && r.PlanDate == DateTime.Today).FirstOrDefault();
					else
						todayPlan = objSpace.GetObjectsQuery<Plans>().Where(r => r.Agent == currentUser && r.PlanDate == DateTime.Today).FirstOrDefault();
					
                    if (todayPlan != null)
                    {
                        if (selectedCall.StartDateTime.Date == DateTime.Today || (todayPlan.Status == Plans.PlanStatus.Completed && selectedCall.StartDateTime.Date >= DateTime.Today))
                        {
                            if (!selectedCall.Attempted)
                            {

                                PermissionPolicyRole role = objSpace.GetObjectsQuery<PermissionPolicyRole>().Where(r => r.Name == "Agent").FirstOrDefault();
                                if (currentUser.Roles.Any(r => r.Oid == role.Oid))
                                {
                                    List<UserActivity> userActivityList = objSpace.GetObjectsQuery<UserActivity>().Where(r => r.UserID == currentUser.UserName).ToList();
									UserActivity userActivity = userActivityList.FirstOrDefault(r => r.ActivityDate == DateTime.Today);
                                    if (userActivity?.CurrentStatus != "Available")
                                    {
                                        ShowWarning("User must be in \"Available\" status to be able to perform calls.");
                                    }
                                    else
                                    {
										if (selectedCall.Status != Calls.CallStatus.New)
										{
											ShowWarning("This call is being processed or was processed by another user. Please selected another call.");
											return;
										}

                                        CallHelper callHelper = objSpace.GetObjectsQuery<CallHelper>().Where(r => r.UserID == currentUser.UserName).FirstOrDefault();

                                        if (callHelper == null)
                                        {
                                            callHelper = objSpace.CreateObject<CallHelper>();
                                            callHelper.UserID = currentUser.UserName;
                                        }
                                        else if (callHelper.CurrentCall != null && callHelper.CurrentCall != selectedCall && callHelper.CurrentCall.StartDateTime >= DateTime.Today)
                                        {
                                            ShowWarning("This user already has a call in progress and cannot start a new call until the previous one is finished.");
                                            return;
                                        }

                                        if (callHelper.CurrentCall == null)
                                        {
                                            callHelper.CurrentCall = selectedCall;
                                        }

                                        if (selectedCall.StartDateTime == DateTime.Today || selectedCall.PlanID.PlanID != todayPlan.PlanID)
                                        {
                                            selectedCall.StartDateTime = DateTime.Now;
                                        }

                                        if (selectedCall.PlanID.Status == Plans.PlanStatus.Confirmed)
                                        {
                                            selectedCall.PlanID.Status = Plans.PlanStatus.InProcess;
                                        }

										selectedCall.Status = Calls.CallStatus.InProgress;

                                        CallResult callResult = objSpace.GetObjectsQuery<CallResult>().Where(r => r.Call == selectedCall).FirstOrDefault();
                                        if (callResult == null)
                                        {
                                            callResult = objSpace.CreateObject<CallResult>();
                                            callResult.Call = selectedCall;
                                        }

                                        //****** SUBMIT DB CHANGES FOR VOIP COMMANDS
                                        DevExpress.Xpo.Session session = ((XPObjectSpace)(objSpace)).Session;
                                        string qry = string.Format("Insert Into VOIPCommands (UserID, Command) values ('{0}','{1}')", currentUser.Oid, string.Concat("DIAL|", callHelper.CurrentCall.CalledPhoneNumber));
                                        session.ExecuteQuery(qry);

                                        objSpace.CommitChanges();

                                        IObjectSpace newObjSpace = Application.CreateObjectSpace(typeof(Calls));
                                        DetailView detailView = Application.CreateDetailView(newObjSpace, "Agent_Calls_DetailView", true, newObjSpace.GetObject(selectedCall));
                                        e.ShowViewParameters.CreatedView = detailView;
                                        e.ShowViewParameters.TargetWindow = TargetWindow.Current;
                                    }
                                }
                            }
                            else
                            {
                                IObjectSpace newObjSpace = Application.CreateObjectSpace(typeof(CallResult));
                                CallResult callResult = View.ObjectSpace.GetObjectsQuery<CallResult>().Where(r => r.Call == selectedCall).FirstOrDefault();
                                DetailView detailView = Application.CreateDetailView(newObjSpace, "CompletedCallResult_DetailView", true, newObjSpace.GetObject(callResult));
                                detailView.ViewEditMode = ViewEditMode.View;
                                e.ShowViewParameters.CreatedView = detailView;
                                e.ShowViewParameters.TargetWindow = TargetWindow.Current;
                            }
                        }
                        else
                        {
                            ShowWarning("The selected call does not belong to today's plan.");
                        }
                    }

                }
                else
                {
                    if (View.SelectedObjects.Count == 0)
                    {
                        ShowWarning("Please select a call.");
                    }
                    else
                    {
                        ShowWarning("Only one call can be processed at a time.");
                    }
                }
            }
            catch (SqlException sqlEx)
            {
                throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
            }
        }

        private void ApplyFilter()
        {
            try
            {
                var objSpace = View.ObjectSpace;
                var selectedChoice = AgentCallPlanFilter.SelectedIndex;
                DateTime dtStart = DateTime.Today;
                DateTime dtEnd = dtStart.AddDays(1).AddSeconds(-1);
                string filter = "1 == 1";
                List<object> parameters = new List<object>();

                PermissionPolicyUser currentUser = objSpace.GetObjectsQuery<PermissionPolicyUser>().Where(r => r.UserName == SecuritySystem.CurrentUserName).FirstOrDefault();
                PermissionPolicyRole role = objSpace.GetObjectsQuery<PermissionPolicyRole>().Where(r => r.Name == "Agent").FirstOrDefault();
                if (currentUser.Roles.Any(r => r.Oid == role?.Oid))
                {
					Guid userGuid = currentUser.Oid;

					CxmUser cxmUser = objSpace.GetObjectByKey<CxmUser>(userGuid);
					if (cxmUser != null && cxmUser.MultiAgent != null) userGuid = cxmUser.MultiAgent.Oid;

                    filter += " && PlanID.Agent.Oid == ?";
                    parameters.Add(userGuid);
                }

                // View previous day's call plan.
                if (selectedChoice == 0)
                {
                    if (dtStart.DayOfWeek == DayOfWeek.Monday)
                    {
                        dtStart = dtStart.AddDays(-2);
                    }
                    else
                    {
                        dtStart = dtStart.AddDays(-1);
                    }

                    dtEnd = dtStart.AddDays(1).AddSeconds(-1);
                }

                // View next day's call plan.
                if (selectedChoice == 2)
                {
                    // If today is a saturday, add 2 days to get monday's plan.
                    if (dtStart.DayOfWeek == DayOfWeek.Saturday)
                    {
                        dtStart = dtStart.AddDays(2);
                    }
                    else if (dtStart.DayOfWeek == DayOfWeek.Friday)
                    {
                        if (HasPlanDefinedForSaturday(currentUser))
                        {
                            dtStart = dtStart.AddDays(1);
                        }
                        else
                        {
                            dtStart = dtStart.AddDays(3);
                        }
                    }
                    else
                    {
                        dtStart = dtStart.AddDays(1);
                    }

                    dtEnd = dtStart.AddDays(1).AddSeconds(-1);
                }

                filter += " && StartDateTime >= ? && StartDateTime <= ? && Planned == true";
                parameters.Add(dtStart);
                parameters.Add(dtEnd);

                ((ListView)View).CollectionSource.Criteria["MainPlanCallFilter"] = CriteriaOperator.Parse(filter, parameters.ToArray());
            }
            catch (SqlException sqlEx)
            {
                throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
            }
        }

        private void AgentCallPlanFilter_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            ApplyFilter();
        }

        private bool HasPlanDefinedForSaturday(PermissionPolicyUser mUser)
        {
            try
            {
                Plans plan = View.ObjectSpace.GetObjectsQuery<Plans>().Where(r => r.Agent == mUser && r.PlanDate == DateTime.Today.AddDays(1)).FirstOrDefault();

                if (plan != null)
                {
                    if (plan.Calls != null && plan.Calls.Count > 0)
                    {
                        return true;
                    }
                }
            }
            catch (SqlException sqlEx)
            {
                throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
            }

            return false;
        }

        private void ChooseContact_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            var call = (View as ListView).CurrentObject as Calls;
            Type objectType = typeof(Contacts);
            IObjectSpace contactObjectSpace = Application.CreateObjectSpace(objectType);
            string listViewId = "SelectContact_LookupListView";
            CollectionSourceBase collectionSource = Application.CreateCollectionSource(contactObjectSpace, objectType, listViewId);
            collectionSource.Criteria["MainFilter"] = CriteriaOperator.Parse("CustomerID == '" + call.CustomerID.CustomerID + "'");
            e.View = Application.CreateListView(listViewId, collectionSource, true);

            DialogController dialogController = new DialogController();
            dialogController.Accepting += ChooseContactDialogController_Accepting;
            e.DialogController = dialogController;
        }

        private void ChooseContactDialogController_Accepting(object sender, DialogControllerAcceptingEventArgs e)
        {
            try
            {
                var contactListView = ((sender as DialogController).Frame.View as ListView);

                if (contactListView.SelectedObjects.Count == 0)
                {
                    Application.ShowViewStrategy.ShowMessage("Please select a contact.", InformationType.Warning, 3000, InformationPosition.Top);
                    e.Cancel = true;
                    return;
                }

                var objSpace = View.ObjectSpace;
                var selectedContact = objSpace.GetObjectByKey<Contacts>(((Contacts)contactListView.SelectedObjects[0]).ContactID);

                var call = (View as ListView).CurrentObject as Calls;
                if (call != null)
                {
                    call.Contact = selectedContact;
                    call.Save();
                    objSpace.CommitChanges();
                }
            }
            catch (SqlException sqlEx)
            {
                throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
            }
        }
    }
}
