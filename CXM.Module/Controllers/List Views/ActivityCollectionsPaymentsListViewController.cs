﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using CXM.Module.BusinessObjects.CXM;
using System.Data.SqlClient;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo.Helpers;
using DevExpress.Xpo;

namespace CXM.Module.Controllers.List_Views
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ActivityCollectionsPaymentsListViewController : ViewController
    {
        public ActivityCollectionsPaymentsListViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void SelectInvoices_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            try
            {
                Type objectType = typeof(TempCollectionDetails);
                IObjectSpace objSpace = Application.CreateObjectSpace(objectType);

                Collections collection = ((Collections)(View.ObjectSpace.Owner as DetailView).CurrentObject);

                var collectionID = collection.ID;
                //**** STORE SALESLINE ITEMS IN VARIABLE FOR SEARCHING
                List<CollectionPayments> filteredDetailsList = collection.Payments.ToList();
                string listViewId = Application.FindListViewId(objectType);

                // collectionSource = Application.CreateCollectionSource(objSpace, objectType, "SalesLineTemporary_ListView");
                var sql = "";
                var openInvoices = "";

                foreach (var invoice in collection.Customer.OpenInvoices)
                {
                    if (openInvoices.Length == 0)
                    {
                        openInvoices += " AND InvoiceID in ('" + invoice.InvoiceID + "'";
                    }
                    else
                    {
                        openInvoices += ",'" + invoice.InvoiceID + "'";
                    }
                }

                if (openInvoices.Length > 0)
                {
                    openInvoices += ")";

                    sql += "DELETE FROM TempCollectionDetails WHERE [User] = '" + SecuritySystem.CurrentUserName + "';";
                    sql += "INSERT INTO TempCollectionDetails SELECT NEWID(), 0, InvoiceID, InvoiceDate, CurrentDays, InvoiceType, Amount, OpenAmount, '" + SecuritySystem.CurrentUserName + "' from Invoice where 1 = 1" + openInvoices + ";";

                    foreach (var item in filteredDetailsList)
                    {
                        sql += "UPDATE TempCollectionDetails SET PayAmount = " + item.AmountPaid + " where [User] = '" + SecuritySystem.CurrentUserName + "' and Invoice = '" + item.Invoice.InvoiceID + "';";
                    }

                    ((XPObjectSpace)objSpace).Session.ExecuteNonQuery(sql);
                }
                
                CollectionSourceBase collectionSource = Application.CreateCollectionSource(objSpace, typeof(TempCollectionDetails), listViewId);
                collectionSource.Criteria["MainFilter"] = CriteriaOperator.Parse("[User] == '" + SecuritySystem.CurrentUserName + "'");
                ListView listView2 = Application.CreateListView(listViewId, collectionSource, false);
                e.View = listView2;

                DialogController dialogController = new DialogController();
                e.DialogController = dialogController;

                dialogController.ViewClosed += TempCollectionDetails_ViewClose;

                collection = null;
                objSpace = null;
                filteredDetailsList = null;

                GC.WaitForPendingFinalizers();
                GC.Collect();
                //Type objectType = typeof(Invoice);
                //IObjectSpace invoiceObjectSpace = Application.CreateObjectSpace(objectType);

                //var customer = View.ObjectSpace.GetObject<Customers>(((Collections)(View.ObjectSpace.Owner as DetailView).CurrentObject).Customer);
                //DetailView detailView = Application.CreateDetailView(invoiceObjectSpace, "Collections_CustomerOpenInvoices_DetailView", false);
                //detailView.CurrentObject = invoiceObjectSpace.GetObjectsQuery<Customers>().Where(r => r.CustomerID == customer.CustomerID).FirstOrDefault();
                //e.View = detailView;
            }
            catch (SqlException sqlEx)
            {
                throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
            }
        }

        private void SelectInvoices_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            ListView popupListView = (e.PopupWindowView) as ListView;
            IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(Collections));

            Session session = ((XPObjectSpace)(objectSpace)).Session;

            Collections collection = objectSpace.GetObject(((Collections)(View.ObjectSpace.Owner as DetailView).CurrentObject));

            session.Delete(collection.Payments);
            session.Save(collection.Payments);

            TempCollectionDetails tLine = null;
            List<TempCollectionDetails> viewList = new List<TempCollectionDetails>();

            foreach (TempCollectionDetails item in popupListView.SelectedObjects)
            {
                if (!viewList.Contains(item))
                {
                    viewList.Add(item);
                }
            }

            popupListView.CollectionSource.Criteria.Clear();

            if (popupListView.CollectionSource.List.GetType() == typeof(XpoServerCollectionFlagger))
            {
                CriteriaOperator op = XPQuery<TempCollectionDetails>.TransformExpression(collection.Session, r => r.User == SecuritySystem.CurrentUserName && r.PayAmount > 0);
                ((XpoServerCollectionFlagger)popupListView.CollectionSource.List).SetFixedCriteria(op);
                for (int i = 0; i < popupListView.CollectionSource.List.Count - 1; i++)
                {
                    tLine = (TempCollectionDetails)popupListView.CollectionSource.List[i];

                    //if (tLine.Qty > 0 && tLine.OrderID == salesOrder.ID)
                    //{
                    if (!viewList.Contains(tLine))
                    {
                        viewList.Add(tLine);
                    }
                    //}

                    //tLine = null;
                }
            }
            else
            {
                foreach (var item in popupListView.CollectionSource.List)
                {
                    tLine = (TempCollectionDetails)item;

                    if (tLine.PayAmount > 0 && tLine.User == SecuritySystem.CurrentUserName && !viewList.Contains(tLine))
                    {
                        viewList.Add(tLine);
                    }

                    tLine = null;
                }
            }

            //short idx = 0;
            foreach (TempCollectionDetails obj in viewList)
            {
                var paymentAmount = obj.PayAmount > obj.OpenAmount || obj.PayAmount == 0 ? obj.OpenAmount : obj.PayAmount;

                //var colDetail = objectSpace.CreateObject<CollectionDetails>();
                //colDetail.Collection = collection;
                //colDetail.Invoice = objectSpace.GetObjectsQuery<Invoice>().Where(r => r.InvoiceID == obj.Invoice.InvoiceID).FirstOrDefault();
                //colDetail.Index = idx++;
                //colDetail.Amount = obj.PayAmount;
                //colDetail.PaymentAmount = obj.PayAmount;
                //colDetail.Save();
                //collection.Details.Add(colDetail);
                //collection.Save();
                //colDetail = null;

                    

                var colPayment = objectSpace.CreateObject<CollectionPayments>();
                colPayment.Collection = collection;
                colPayment.Invoice = objectSpace.GetObjectsQuery<Invoice>().Where(r => r.InvoiceID == obj.Invoice.InvoiceID).FirstOrDefault();
                colPayment.AmountPaid = paymentAmount;
                colPayment.OpenAmount = obj.OpenAmount - paymentAmount;
                colPayment.Discount = 0;
                colPayment.Save();
                collection.Payments.Add(colPayment);
                collection.Save();
                colPayment = null;
            }
            viewList.Clear();

            objectSpace.CommitChanges();
            View.RefreshDataSource();

            collection = null;
            objectSpace = null;
            session = null;
            popupListView = null;
            viewList = null;
            GC.Collect();
        }

        private void TempCollectionDetails_ViewClose(object sender, EventArgs e)
        {
            ((XPObjectSpace)View.ObjectSpace).Session.ExecuteNonQuery("DELETE FROM TempCollectionDetails WHERE [User] = '" + SecuritySystem.CurrentUserName + "';");
        }
    }
}
