﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using CXM.Module.BusinessObjects.CXM;
using System.Data.SqlClient;
using DevExpress.ExpressApp.Web.Editors.ASPx;
using DevExpress.ExpressApp.Web.SystemModule;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo.DB;

namespace CXM.Module.Controllers.List_Views
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class SpeedSalesOrderListViewController : ViewController
	{
		ListViewController listViewController = null;

		public SpeedSalesOrderListViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			var listView = View as ListView;
			if (listView != null && !listView.IsDisposed) listView.ControlsCreated += ListView_ControlsCreated;

			Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem += SpeedSalesOrderListViewController_CustomProcessSelectedItem;

			if (listViewController == null)
			{
				listViewController = Frame.GetController<ListViewController>();
				if (listViewController != null)
				{
					var criteria = "TransStatus == 0";

					CallHelper callHelper = View.ObjectSpace.GetObjectsQuery<CallHelper>().FirstOrDefault(x => x.UserID == SecuritySystem.CurrentUserName);
					if (callHelper != null && callHelper.CurrentCall != null && callHelper.CurrentCall.CustomerID != null)
						criteria += $" And Customer == '{callHelper.CurrentCall.CustomerID.CustomerID}'";

					listViewController.EditAction.TargetObjectsCriteria = criteria;
				}
			}
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();

			Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem -= SpeedSalesOrderListViewController_CustomProcessSelectedItem;

			if (listViewController != null)
			{
				listViewController.EditAction.TargetObjectsCriteria = null;
				listViewController = null;
			}
		}

		void ShowWarning(string warning) => Application.ShowViewStrategy.ShowMessage(warning, InformationType.Warning, 5000, InformationPosition.Top);

		private void SpeedSalesOrderListViewController_CustomProcessSelectedItem(object sender, CustomProcessListViewSelectedItemEventArgs e)
		{
			e.Handled = true;

			CxmUser currentUser = View.ObjectSpace.GetObjectsQuery<CxmUser>().Where(u => u.UserName == SecuritySystem.CurrentUserName).FirstOrDefault();
			if (currentUser != null && currentUser.Roles.Any(r => r.Name == "Agent" || r.Name == "Supervisor"))
			{
				List<UserActivity> userActivityList = View.ObjectSpace.GetObjectsQuery<UserActivity>().Where(r => r.UserID == currentUser.UserName).ToList();
				UserActivity userActivity = userActivityList.FirstOrDefault(r => r.ActivityDate == DateTime.Today);
				if (userActivity?.CurrentStatus != "Available")
				{
					ShowWarning("User must be in \"Available\" status to be able to edit orders.");
					return;
				}
			}

			var listView = (View as ListView);
			var order = listView?.CurrentObject as SalesOrder;

			if (order != null)
			{
				var objSpace = Application.CreateObjectSpace(typeof(SalesOrder));
				SalesOrder salesOrder = objSpace.GetObject<SalesOrder>(order);
				DetailView detailView = Application.CreateDetailView(objSpace, "SpeedSalesOrder_DetailView", true, salesOrder);

				if (salesOrder.TransStatus == Enums.Trans_Status.New)
					detailView.ViewEditMode = ViewEditMode.Edit;

				Frame.SetView(detailView);
			}
		}

		private void ListView_ControlsCreated(object sender, EventArgs e)
		{
			var listView = sender as ListView;
			listView.ControlsCreated -= ListView_ControlsCreated;

			listView.CollectionSource.Criteria["MainFilter"] = CriteriaOperator.Parse($"CreatedBy == '{SecuritySystem.CurrentUserName}'");
		}

		private void NewObjectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			try
			{
				IObjectSpace objSpace = Application.CreateObjectSpace(typeof(SalesOrder));
				SalesOrder salesOrder = objSpace.CreateObject<SalesOrder>();

				CxmUser currentUser = View.ObjectSpace.GetObjectsQuery<CxmUser>().Where(u => u.UserName == SecuritySystem.CurrentUserName).FirstOrDefault();
				if (currentUser != null && currentUser.Roles.Any(r => r.Name == "Agent" || r.Name == "Supervisor"))
				{
					List<UserActivity> userActivityList = View.ObjectSpace.GetObjectsQuery<UserActivity>().Where(r => r.UserID == currentUser.UserName).ToList();
					UserActivity userActivity = userActivityList.FirstOrDefault(r => r.ActivityDate == DateTime.Today);
					if (userActivity?.CurrentStatus != "Available")
					{
						ShowWarning("User must be in \"Available\" status to be able to create orders.");
						salesOrder = null;
						objSpace = null;
						return;
					}

					CallHelper callHelper = View.ObjectSpace.GetObjectsQuery<CallHelper>().FirstOrDefault(x => x.UserID == currentUser.UserName);
					if (callHelper == null)
					{
						callHelper = View.ObjectSpace.CreateObject<CallHelper>();
						callHelper.UserID = SecuritySystem.CurrentUserName;
					}

					if (callHelper != null)
					{
						if (callHelper.CurrentCall == null)
						{
							Calls newCall = View.ObjectSpace.CreateObject<Calls>();
							newCall.StartDateTime = DateTime.Now;
							newCall.UserID = SecuritySystem.CurrentUserName;
							newCall.Category = Calls.CallCategory.OffCallTransactions;
							newCall.Outgoing = false;
							newCall.Planned = false;
							newCall.Attempted = true;
							newCall.Status = Calls.CallStatus.InProgress;

							callHelper.CurrentCall = newCall;
						}
						else if (callHelper.CurrentCall.CustomerID != null)
						{
							salesOrder.Customer = objSpace.GetObject<Customers>(callHelper.CurrentCall.CustomerID);
						}

						View.ObjectSpace.CommitChanges();
					}
				}

				objSpace.CommitChanges();

				DetailView detailView = Application.CreateDetailView(objSpace, "SpeedSalesOrder_DetailView", true, salesOrder);
				detailView.ViewEditMode = ViewEditMode.Edit;
				e.ShowViewParameters.CreatedView = detailView;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception($"An error ocurred while processing the information.{Environment.NewLine}MESSAGE: {sqlEx.Message}");
			}
			catch (Exception ex)
			{
				throw new Exception($"An error ocurred while processing the information.{Environment.NewLine}MESSAGE: {ex.Message}{Environment.NewLine}STACK TRACE: {ex.StackTrace}");
			}
		}

		private void CancelSpeedOrder_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			var lv = View as ListView;
			bool doSave = false;

			if (lv != null)
			{
				foreach (SalesOrder order in lv.SelectedObjects)
				{
					if (order.TransStatus == Enums.Trans_Status.New)
					{
						order.TransStatus = Enums.Trans_Status.Canceled;
						order.Save();
						doSave = true;
					}
				}

				if (doSave) lv.ObjectSpace.CommitChanges();
			}
		}
	}
}
