﻿using DevExpress.ExpressApp.Web;
using DevExpress.Web;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CXM.Module.Controllers
{
	public class UpDownButtonsTemplate : ITemplate
	{
		const string ClickEventHandlerFormat = "function(s, e) {{ e.processOnServer = false; {0}.PerformCallback(\"{1}|{2}\"); }}";
		void ITemplate.InstantiateIn(Control container)
		{
			GridViewDataItemTemplateContainer gridContainer = (GridViewDataItemTemplateContainer)container;
			ASPxButton downButton = RenderHelper.CreateASPxButton();
			downButton.Text = "Down";
			downButton.ClientSideEvents.Click = String.Format(ClickEventHandlerFormat, gridContainer.Grid.ClientInstanceName, "down", gridContainer.KeyValue);
			downButton.ID = "downButton_" + container.ID;
			ASPxButton upButton = RenderHelper.CreateASPxButton();
			upButton.Text = "Up";
			upButton.ClientSideEvents.Click = String.Format(ClickEventHandlerFormat, gridContainer.Grid.ClientInstanceName, "up", gridContainer.KeyValue);
			upButton.ID = "upButton_" + container.ID;
			Table table = new Table();
			table.Rows.Add(new TableRow());
			table.Rows[0].Cells.Add(new TableCell());
			table.Rows[0].Cells.Add(new TableCell());
			table.Rows[0].Cells[0].Controls.Add(downButton);
			table.Rows[0].Cells[1].Controls.Add(upButton);
			table.Attributes["onclick"] = RenderHelper.EventCancelBubbleCommand;
			container.Controls.Add(table);
		}
	}
}