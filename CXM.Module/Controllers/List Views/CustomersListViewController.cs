﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using CXM.Module.BusinessObjects.CXM;
using System.Data.SqlClient;

namespace CXM.Module.Controllers.List_Views
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class CustomersListViewController : ViewController
	{
		public CustomersListViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem += CustomersListViewController_CustomProcessSelectedItem;
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			Frame.GetController<ListViewProcessCurrentObjectController>().CustomProcessSelectedItem -= CustomersListViewController_CustomProcessSelectedItem;
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();
		}

		void CustomersListViewController_CustomProcessSelectedItem(object sender, CustomProcessListViewSelectedItemEventArgs e)
		{
			e.Handled = true;

			var customer = (Customers)View.CurrentObject;

			if (customer != null)
			{
				IObjectSpace newObjSpace = Application.CreateObjectSpace(typeof(Customers));
				DetailView detailView = Application.CreateDetailView(newObjSpace, "Customer_Activity_DetailView", true, newObjSpace.GetObject(customer));
				detailView.ViewEditMode = ViewEditMode.View;
				e.InnerArgs.ShowViewParameters.CreatedView = detailView;
			}
		}
	}
}
