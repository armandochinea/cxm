﻿namespace CXM.Module.Controllers.List_Views
{
	partial class AgentUnplannedCallsListViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.AgentIncomingCall = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
			this.AgentOutgoingCall = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
			// 
			// AgentIncomingCall
			// 
			this.AgentIncomingCall.Caption = "New Incoming Call";
			this.AgentIncomingCall.Category = "Edit";
			this.AgentIncomingCall.ConfirmationMessage = null;
			this.AgentIncomingCall.Id = "AgentIncomingCall";
			this.AgentIncomingCall.ToolTip = null;
			this.AgentIncomingCall.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AgentIncomingCall_Execute);
			// 
			// AgentOutgoingCall
			// 
			this.AgentOutgoingCall.Caption = "New Outgoing Call";
			this.AgentOutgoingCall.Category = "Edit";
			this.AgentOutgoingCall.ConfirmationMessage = null;
			this.AgentOutgoingCall.Id = "AgentOutgoingCall";
			this.AgentOutgoingCall.ToolTip = null;
			this.AgentOutgoingCall.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AgentOutgoingCall_Execute);
			// 
			// AgentUnplannedCallsListViewController
			// 
			this.Actions.Add(this.AgentIncomingCall);
			this.Actions.Add(this.AgentOutgoingCall);
			this.TargetViewId = "Agent_UnplannedCalls_ListView";

		}

		#endregion
		private DevExpress.ExpressApp.Actions.SimpleAction AgentIncomingCall;
		private DevExpress.ExpressApp.Actions.SimpleAction AgentOutgoingCall;
	}
}
