﻿namespace CXM.Module.Controllers
{
	partial class CallActivityIssueFilterViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem1 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
			DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem2 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
			DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem3 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
			DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem4 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
			DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem5 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
			this.issueFilter = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
			// 
			// issueFilter
			// 
			this.issueFilter.Caption = "Issue Filter";
			this.issueFilter.Category = "Edit";
			this.issueFilter.ConfirmationMessage = null;
			this.issueFilter.Id = "IssueFilter";
			choiceActionItem1.Caption = "Most Recent Issue";
			choiceActionItem1.Id = "RecentIssue";
			choiceActionItem1.ImageName = null;
			choiceActionItem1.Shortcut = null;
			choiceActionItem1.ToolTip = null;
			choiceActionItem2.Caption = "This Year";
			choiceActionItem2.Id = "ThisYear";
			choiceActionItem2.ImageName = null;
			choiceActionItem2.Shortcut = null;
			choiceActionItem2.ToolTip = null;
			choiceActionItem3.Caption = "Open";
			choiceActionItem3.Id = "Open";
			choiceActionItem3.ImageName = null;
			choiceActionItem3.Shortcut = null;
			choiceActionItem3.ToolTip = null;
			choiceActionItem4.Caption = "Resolved";
			choiceActionItem4.Id = "Resolved";
			choiceActionItem4.ImageName = null;
			choiceActionItem4.Shortcut = null;
			choiceActionItem4.ToolTip = null;
			choiceActionItem5.Caption = "Canceled";
			choiceActionItem5.Id = "Canceled";
			choiceActionItem5.ImageName = null;
			choiceActionItem5.Shortcut = null;
			choiceActionItem5.ToolTip = null;
			this.issueFilter.Items.Add(choiceActionItem1);
			this.issueFilter.Items.Add(choiceActionItem2);
			this.issueFilter.Items.Add(choiceActionItem3);
			this.issueFilter.Items.Add(choiceActionItem4);
			this.issueFilter.Items.Add(choiceActionItem5);
			this.issueFilter.ToolTip = null;
			this.issueFilter.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.issueFilter_Execute);
			// 
			// CallActivityIssueFilterViewController
			// 
			this.Actions.Add(this.issueFilter);
			this.TargetObjectType = typeof(CXM.Module.BusinessObjects.CXM.Issues);
			this.TargetViewId = "Calls_LastIssues_ListView";
			this.TypeOfView = typeof(DevExpress.ExpressApp.View);

		}

		#endregion

		private DevExpress.ExpressApp.Actions.SingleChoiceAction issueFilter;
	}
}
