﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using CXM.Module.BusinessObjects.CXM;
using System.Data.SqlClient;

namespace CXM.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CallHistoryFilterViewController : ViewController
    {
		public CallHistoryFilterViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            callHistoryFilter.SelectedIndex = 0;
			callTypeFilter.SelectedIndex = 0;
			ApplyFilter();
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

		private void CallHistoryFilterSingleChoiceAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
			ApplyFilter();
		}

		private void callTypeFilter_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
		{
			ApplyFilter();
		}

		private void ApplyFilter ()
		{
			try
			{
				var selectedChoice = callHistoryFilter.SelectedIndex;
				DateTime dtStart = DateTime.Today;
				DateTime dtEnd = dtStart.AddDays(1).AddSeconds(-1);
				string filter = "1 == 1";
				List<object> parameters = new List<object>();

				if (selectedChoice == 1)
				{
					while (dtStart.DayOfWeek != DayOfWeek.Sunday)
					{
						dtStart = dtStart.AddDays(-1);
					}
					dtEnd = dtStart.AddDays(7).AddSeconds(-1);
				}
				else if (selectedChoice == 2)
				{
					dtStart = new DateTime(dtStart.Year, dtStart.Month, 1);
					dtEnd = dtStart.AddMonths(1).AddSeconds(-1);
				}
				else if (selectedChoice == 3)
				{
					dtStart = new DateTime(dtStart.Year, 1, 1);
					dtEnd = dtStart.AddYears(1).AddSeconds(-1);
				}

				filter += " && StartDateTime >= ? && StartDateTime <= ? && Attempted == ?";
				parameters.Add(dtStart);
				parameters.Add(dtEnd);
				parameters.Add(true);

				//##############################################

				selectedChoice = callTypeFilter.SelectedIndex;

				if (selectedChoice > 0)
				{
					filter += " && Outgoing == ?";
					if (selectedChoice == 1)
					{
						parameters.Add(true);
					}
					else
					{
						parameters.Add(false);
					}
				}
			
				((ListView)View).CollectionSource.Criteria["CallHistoryFilter"] = CriteriaOperator.Parse(filter, parameters.ToArray());
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}
	}
}