﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using CXM.Module.BusinessObjects.CXM;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using System.Data.SqlClient;

namespace CXM.Module.Controllers
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class CallActivityIssueFilterViewController : ViewController
	{
		public CallActivityIssueFilterViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			issueFilter.SelectedIndex = 0;
			ApplyFilter();
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();
		}

		private void issueFilter_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
		{
			ApplyFilter();
		}

		private void ApplyFilter()
		{
			try
			{
				var objSpace = View.ObjectSpace;
				var selectedChoice = issueFilter.SelectedIndex;
				DateTime dtStart = DateTime.Today;
				DateTime dtEnd = dtStart.AddDays(1).AddSeconds(-1);
				string filter = "1 == 1";
				List<object> parameters = new List<object>();

				PermissionPolicyUser currentUser = objSpace.GetObjectsQuery<PermissionPolicyUser>().Where(r => r.UserName == SecuritySystem.CurrentUserName).FirstOrDefault();

				if (selectedChoice == 0)
				{
					CallHelper callHelper = objSpace.GetObjectsQuery<CallHelper>().Where(r => r.UserID == currentUser.UserName).FirstOrDefault();

					if (callHelper != null)
					{
						if (callHelper.CurrentCall != null && callHelper.CurrentCall.CustomerID != null)
						{
							Customers customer = objSpace.GetObjectsQuery<Customers>().Where(r => r.CustomerID == callHelper.CurrentCall.CustomerID.CustomerID).FirstOrDefault();

							if (customer.LastIssue != null)
							{
								filter += " && IssueID == ?";
								parameters.Add(customer.LastIssue.IssueID);
							}
						}
					}
				}

				if (selectedChoice >= 1)
				{
					dtStart = new DateTime(dtStart.Year, 1, 1);
					dtEnd = dtStart.AddYears(1).AddSeconds(-1);

					filter += " && IssueDate >= ? && IssueDate <= ?";
					parameters.Add(dtStart);
					parameters.Add(dtEnd);

					if (selectedChoice == 2)
					{
						filter += " && Status.Description == ?";
						parameters.Add("OPEN");
					}
					else if (selectedChoice == 3)
					{
						filter += " && Status.Description == ?";
						parameters.Add("RESOLVED");
					}
					else if (selectedChoice == 4)
					{
						filter += " && Status.Description == ?";
						parameters.Add("CANCELED");
					}
				}

				((ListView)View).CollectionSource.Criteria["CallActivityIssueFilter"] = CriteriaOperator.Parse(filter, parameters.ToArray());
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}
	}
}
