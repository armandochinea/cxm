﻿namespace CXM.Module.Controllers
{
    partial class CallHistoryFilterViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem1 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
			DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem2 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
			DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem3 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
			DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem4 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
			DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem5 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
			DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem6 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
			DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem7 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
			this.callHistoryFilter = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
			this.callTypeFilter = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
			// 
			// callHistoryFilter
			// 
			this.callHistoryFilter.Caption = "Filter Calls";
			this.callHistoryFilter.Category = "Edit";
			this.callHistoryFilter.ConfirmationMessage = null;
			this.callHistoryFilter.Id = "CallHistoryFilter";
			choiceActionItem1.Caption = "For Today";
			choiceActionItem1.Id = "Today";
			choiceActionItem1.ImageName = null;
			choiceActionItem1.Shortcut = null;
			choiceActionItem1.ToolTip = null;
			choiceActionItem2.Caption = "This Week";
			choiceActionItem2.Id = "ThisWeek";
			choiceActionItem2.ImageName = null;
			choiceActionItem2.Shortcut = null;
			choiceActionItem2.ToolTip = null;
			choiceActionItem3.Caption = "This Month";
			choiceActionItem3.Id = "ThisMonth";
			choiceActionItem3.ImageName = null;
			choiceActionItem3.Shortcut = null;
			choiceActionItem3.ToolTip = null;
			choiceActionItem4.Caption = "This Year";
			choiceActionItem4.Id = "ThisYear";
			choiceActionItem4.ImageName = null;
			choiceActionItem4.Shortcut = null;
			choiceActionItem4.ToolTip = null;
			this.callHistoryFilter.Items.Add(choiceActionItem1);
			this.callHistoryFilter.Items.Add(choiceActionItem2);
			this.callHistoryFilter.Items.Add(choiceActionItem3);
			this.callHistoryFilter.Items.Add(choiceActionItem4);
			this.callHistoryFilter.TargetObjectType = typeof(CXM.Module.BusinessObjects.CXM.Calls);
			this.callHistoryFilter.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
			this.callHistoryFilter.ToolTip = null;
			this.callHistoryFilter.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
			this.callHistoryFilter.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.CallHistoryFilterSingleChoiceAction_Execute);
			// 
			// callTypeFilter
			// 
			this.callTypeFilter.Caption = "Call Type";
			this.callTypeFilter.Category = "Edit";
			this.callTypeFilter.ConfirmationMessage = null;
			this.callTypeFilter.Id = "CallTypeFilter";
			choiceActionItem5.Caption = "All";
			choiceActionItem5.Id = "All";
			choiceActionItem5.ImageName = null;
			choiceActionItem5.Shortcut = null;
			choiceActionItem5.ToolTip = null;
			choiceActionItem6.Caption = "Outgoing";
			choiceActionItem6.Id = "Outgoing";
			choiceActionItem6.ImageName = null;
			choiceActionItem6.Shortcut = null;
			choiceActionItem6.ToolTip = null;
			choiceActionItem7.Caption = "Incoming";
			choiceActionItem7.Id = "Incoming";
			choiceActionItem7.ImageName = null;
			choiceActionItem7.Shortcut = null;
			choiceActionItem7.ToolTip = null;
			this.callTypeFilter.Items.Add(choiceActionItem5);
			this.callTypeFilter.Items.Add(choiceActionItem6);
			this.callTypeFilter.Items.Add(choiceActionItem7);
			this.callTypeFilter.TargetObjectType = typeof(CXM.Module.BusinessObjects.CXM.Calls);
			this.callTypeFilter.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
			this.callTypeFilter.ToolTip = null;
			this.callTypeFilter.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
			this.callTypeFilter.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.callTypeFilter_Execute);
			// 
			// CallHistoryFilterViewController
			// 
			this.Actions.Add(this.callHistoryFilter);
			this.Actions.Add(this.callTypeFilter);
			this.TargetObjectType = typeof(CXM.Module.BusinessObjects.CXM.Calls);
			this.TargetViewId = "CallHistory_ListView";
			this.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
			this.TypeOfView = typeof(DevExpress.ExpressApp.ListView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction callHistoryFilter;
		private DevExpress.ExpressApp.Actions.SingleChoiceAction callTypeFilter;
	}
}
