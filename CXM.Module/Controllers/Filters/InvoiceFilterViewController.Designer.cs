﻿namespace CXM.Module.Controllers
{
    partial class InvoiceFilterViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem1 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
			DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem2 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
			DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem3 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
			this.invoiceFilters = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
			// 
			// invoiceFilters
			// 
			this.invoiceFilters.Caption = "Filter Invoices";
			this.invoiceFilters.Category = "Edit";
			this.invoiceFilters.ConfirmationMessage = null;
			this.invoiceFilters.Id = "InvoiceFilters";
			choiceActionItem1.Caption = "All";
			choiceActionItem1.Id = "All";
			choiceActionItem1.ImageName = null;
			choiceActionItem1.Shortcut = null;
			choiceActionItem1.ToolTip = null;
			choiceActionItem2.Caption = "Open";
			choiceActionItem2.Id = "Open";
			choiceActionItem2.ImageName = null;
			choiceActionItem2.Shortcut = null;
			choiceActionItem2.ToolTip = null;
			choiceActionItem3.Caption = "Closed";
			choiceActionItem3.Id = "Closed";
			choiceActionItem3.ImageName = null;
			choiceActionItem3.Shortcut = null;
			choiceActionItem3.ToolTip = null;
			this.invoiceFilters.Items.Add(choiceActionItem1);
			this.invoiceFilters.Items.Add(choiceActionItem2);
			this.invoiceFilters.Items.Add(choiceActionItem3);
			this.invoiceFilters.ToolTip = null;
			this.invoiceFilters.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.InvoiceFilterSingleChoiceAction_Execute);
			// 
			// InvoiceFilterViewController
			// 
			this.Actions.Add(this.invoiceFilters);
			this.TargetViewId = "Invoice_ListView";
			this.TypeOfView = typeof(DevExpress.ExpressApp.View);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction invoiceFilters;
    }
}
