﻿namespace CXM.Module.Controllers
{
	partial class UserNameViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.userName = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
			// 
			// userName
			// 
			this.userName.Caption = "User Name";
			this.userName.Category = "Notifications";
			this.userName.ConfirmationMessage = null;
			this.userName.Id = "UserName";
			this.userName.ToolTip = null;
			// 
			// UserNameViewController
			// 
			this.Actions.Add(this.userName);

		}

		#endregion

		private DevExpress.ExpressApp.Actions.SimpleAction userName;
	}
}
