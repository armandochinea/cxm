﻿using System;
using System.Linq;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using CXM.Module.BusinessObjects.CXM;
using System.Data.SqlClient;
using DevExpress.ExpressApp.Web.Layout;

namespace CXM.Module.Controllers.Detail_Views
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class SupervisorCallsDetailViewController : ViewController
	{
		public SupervisorCallsDetailViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			((View as DetailView).LayoutManager as WebLayoutManager).PageControlCreated += OnPageControlCreated;
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();

			((View as DetailView).LayoutManager as WebLayoutManager).PageControlCreated -= OnPageControlCreated;
		}

		private void OnPageControlCreated(object sender, PageControlCreatedEventArgs e)
		{
			try
			{
				if (e.Model.Id == "Details")
				{
					e.PageControl.ClientSideEvents.Init = "function(s,e){s.SetActiveTabIndex(0);}";
					((View as DetailView).LayoutManager as WebLayoutManager).PageControlCreated -= OnPageControlCreated;
				}
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void SupEndCall_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			try
			{
				IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(CallResult));

				// Set current call's parameters for when it has finished.
				CallResult callResult = objectSpace.CreateObject<CallResult>();
				callResult.Call = objectSpace.GetObject<Calls>(((Calls)View.CurrentObject));
				callResult.Call.EndDateTime = DateTime.Now;
				callResult.Call.Attempted = true;

				// Obtain and assign the CallNumber for current call.
				CxmUser user = objectSpace.GetObject<CxmUser>((CxmUser)SecuritySystem.CurrentUser);
				if (user != null)
				{
					user.NextCallNumber++;
					callResult.Call.CallNumber = string.Format("{0}-{1}", user.UserName, user.NextCallNumber.ToString("000000"));
				}

				// Clear the current call reference in CallHelper.
				CallHelper callHelper = objectSpace.GetObjectsQuery<CallHelper>().Where(r => r.UserID == SecuritySystem.CurrentUserName).FirstOrDefault();
				callHelper.CurrentCall = null;

				// Verify if the current call is part of the plan and determine if today's plan has been completed.
				if (callResult.Call.PlanID != null)
				{
					bool planCompleted = true;
					Plans plan = objectSpace.GetObjectsQuery<Plans>().Where(r => r.PlanID == callResult.Call.PlanID.PlanID).FirstOrDefault();
					foreach (Calls call in plan?.Calls)
					{
						if (call.Attempted == false)
						{
							planCompleted = false;
							break;
						}
					}

					if (planCompleted)
					{
						plan.Status = Plans.PlanStatus.Completed;
					}
				}

				// Save changes to the database.
				objectSpace.CommitChanges();

				// Prepare current call's results screen to display to the user.
				DetailView detailView = Application.CreateDetailView(objectSpace, "Supervisor_CallResult_DetailView", true, callResult);
				detailView.ViewEditMode = ViewEditMode.Edit;
				e.ShowViewParameters.CreatedView = detailView;

				View.Close();
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void SupNewIssue_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(Issues));

				Customers customer = objectSpace.GetObject<Customers>(((Calls)View.CurrentObject).CustomerID);
				Issues newIssue = objectSpace.CreateObject<Issues>();
				newIssue.CustomerID = customer;

				DetailView detailView = Application.CreateDetailView(objectSpace, "New_Issues_DetailView", true, newIssue);
				detailView.ViewEditMode = ViewEditMode.Edit;
				e.View = detailView;

				detailView.Closed += NewIssueDetailViewController_Closed;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void SupNewOrder_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(SalesOrder));

				SalesOrder newOrder = objectSpace.CreateObject<SalesOrder>();
				newOrder.Customer = objectSpace.GetObject<Customers>(((Calls)View.CurrentObject).CustomerID);

				DetailView detailView = Application.CreateDetailView(objectSpace, "SalesOrder_DetailView", true, newOrder);
				detailView.ViewEditMode = ViewEditMode.Edit;
				e.View = detailView;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void SupShowLastInvoices_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				Type objectType = typeof(Invoice);
				IObjectSpace invoiceObjectSpace = Application.CreateObjectSpace(objectType);

				var customer = View.ObjectSpace.GetObject<Customers>(((Calls)View.CurrentObject).CustomerID);
				DetailView detailView = Application.CreateDetailView(invoiceObjectSpace, "CustomerLastInvoices_DetailView", false);
				detailView.CurrentObject = invoiceObjectSpace.GetObjectsQuery<Customers>().Where(r => r.CustomerID == customer.CustomerID).FirstOrDefault();
				e.View = detailView;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void SupShowAging_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				Type objectType = typeof(Invoice);
				IObjectSpace invoiceObjectSpace = Application.CreateObjectSpace(objectType);

				var customer = View.ObjectSpace.GetObject<Customers>(((Calls)View.CurrentObject).CustomerID);
				DetailView detailView = Application.CreateDetailView(invoiceObjectSpace, "CustomerOpenInvoices_DetailView", false);
				detailView.CurrentObject = invoiceObjectSpace.GetObjectsQuery<Customers>().Where(r => r.CustomerID == customer.CustomerID).FirstOrDefault();
				e.View = detailView;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		void NewIssueDetailViewController_Closed(object sender, EventArgs e)
		{
			try
			{
				var detailView = (DetailView)sender;

				detailView.Closed -= NewIssueDetailViewController_Closed;

				if (detailView.SelectedObjects.Count == 1)
				{
					var createdIssue = (Issues)detailView.SelectedObjects[0];
					if (createdIssue?.IssueID != 0)
					{
						Application.ShowViewStrategy.ShowMessage(string.Format("Issue #{0} created.", createdIssue.IssueID), InformationType.Success, 3000, InformationPosition.Top);
						View.ObjectSpace.Refresh();
					}
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}
	}
}
