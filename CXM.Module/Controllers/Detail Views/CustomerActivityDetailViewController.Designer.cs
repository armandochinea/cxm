﻿namespace CXM.Module.Controllers.Detail_Views
{
	partial class CustomerActivityDetailViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.ShowDueInvoices = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.ShowAging = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.ShowLastInvoices = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.NewOrder = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ShowDueInvoices
            // 
            this.ShowDueInvoices.AcceptButtonCaption = null;
            this.ShowDueInvoices.CancelButtonCaption = null;
            this.ShowDueInvoices.Caption = "Customer Show Due Invoices";
            this.ShowDueInvoices.Category = "ShowDueInvoices";
            this.ShowDueInvoices.ConfirmationMessage = null;
            this.ShowDueInvoices.Id = "CustomerShowDueInvoices";
            this.ShowDueInvoices.ImageName = "";
            this.ShowDueInvoices.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.Image;
            this.ShowDueInvoices.ToolTip = null;
            this.ShowDueInvoices.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.ShowDueInvoices_CustomizePopupWindowParams);
            // 
            // ShowAging
            // 
            this.ShowAging.AcceptButtonCaption = null;
            this.ShowAging.CancelButtonCaption = null;
            this.ShowAging.Caption = "Customer Show Aging";
            this.ShowAging.Category = "ShowAging";
            this.ShowAging.ConfirmationMessage = null;
            this.ShowAging.Id = "CustomerShowAging";
            this.ShowAging.ImageName = "";
            this.ShowAging.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.Image;
            this.ShowAging.ToolTip = null;
            this.ShowAging.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.ShowAging_CustomizePopupWindowParams);
            // 
            // ShowLastInvoices
            // 
            this.ShowLastInvoices.AcceptButtonCaption = null;
            this.ShowLastInvoices.CancelButtonCaption = null;
            this.ShowLastInvoices.Caption = "Customer Show Last Invoices";
            this.ShowLastInvoices.Category = "ShowLastInvoices";
            this.ShowLastInvoices.ConfirmationMessage = null;
            this.ShowLastInvoices.Id = "CustomerShowLastInvoices";
            this.ShowLastInvoices.ImageName = "";
            this.ShowLastInvoices.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.Image;
            this.ShowLastInvoices.ToolTip = null;
            this.ShowLastInvoices.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.ShowLastInvoices_CustomizePopupWindowParams);
            // 
            // NewOrder
            // 
            this.NewOrder.Caption = "New Order";
            this.NewOrder.Category = "AgentNewOrder";
            this.NewOrder.ConfirmationMessage = null;
            this.NewOrder.Id = "NewOrder";
            this.NewOrder.ToolTip = null;
            this.NewOrder.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.NewOrder_Execute);
            // 
            // CustomerActivityDetailViewController
            // 
            this.Actions.Add(this.ShowDueInvoices);
            this.Actions.Add(this.ShowAging);
            this.Actions.Add(this.ShowLastInvoices);
            this.Actions.Add(this.NewOrder);
            this.TargetViewId = "Customer_Activity_DetailView";

		}

		#endregion

		private DevExpress.ExpressApp.Actions.PopupWindowShowAction ShowDueInvoices;
		private DevExpress.ExpressApp.Actions.PopupWindowShowAction ShowAging;
		private DevExpress.ExpressApp.Actions.PopupWindowShowAction ShowLastInvoices;
        private DevExpress.ExpressApp.Actions.SimpleAction NewOrder;
    }
}
