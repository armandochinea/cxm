﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using CXM.Module.BusinessObjects.CXM;
using System.Data.SqlClient;
using CXM.Module.Controllers.List_Views;
using DevExpress.ExpressApp.Xpo;
using System.Net.Http;
using Newtonsoft.Json;
using DevExpress.ExpressApp.Web.Layout;
using DevExpress.Xpo.DB;

namespace CXM.Module.Controllers.Detail_Views
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class CustomerActivityDetailViewController : ViewController
	{
		public CustomerActivityDetailViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			((View as DetailView).LayoutManager as WebLayoutManager).PageControlCreated += OnPageControlCreated;

			ListPropertyEditor listPropertyEditor = (View as DetailView).FindItem("OrderStatus") as ListPropertyEditor;
			if (listPropertyEditor != null)
			{
				listPropertyEditor.ControlCreated += new EventHandler<EventArgs>(OrderStatusListPropertyEditor_ControlCreated);
			}

			NewOrder.Enabled.Clear();
			var currentCallHelper = View.ObjectSpace.GetObjectsQuery<CallHelper>().Where(h => h.UserID == SecuritySystem.CurrentUserName).FirstOrDefault();
			if (currentCallHelper != null)
			{
				var currentCustomer = (View as DetailView).CurrentObject as Customers;
				if (currentCallHelper.CurrentCall != null)
					if (currentCustomer.CustomerID != currentCallHelper.CurrentCall.CustomerID?.CustomerID)
						NewOrder.Enabled["NewOrder"] = false;
			}
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();
		}

		void ShowWarning(string warning) => Application.ShowViewStrategy.ShowMessage(warning, InformationType.Warning, 5000, InformationPosition.Top);

		private void OnPageControlCreated(object sender, PageControlCreatedEventArgs e)
		{
			try
			{
				if (e.Model.Id == "Details")
				{
					e.PageControl.ClientSideEvents.Init = "function(s,e){s.SetActiveTabIndex(0);}";
					((View as DetailView).LayoutManager as WebLayoutManager).PageControlCreated -= OnPageControlCreated;
				}
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void ShowLastInvoices_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				Type objectType = typeof(Invoice);
				IObjectSpace invoiceObjectSpace = Application.CreateObjectSpace(objectType);

				var customer = View.ObjectSpace.GetObject<Customers>((Customers)View.CurrentObject);
				DetailView detailView = Application.CreateDetailView(invoiceObjectSpace, "CustomerLastInvoices_DetailView", false);
				detailView.CurrentObject = invoiceObjectSpace.GetObjectsQuery<Customers>().Where(r => r.CustomerID == customer.CustomerID).FirstOrDefault();
				e.View = detailView;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void ShowAging_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				Type objectType = typeof(Invoice);
				IObjectSpace invoiceObjectSpace = Application.CreateObjectSpace(objectType);

				var customer = View.ObjectSpace.GetObject<Customers>((Customers)View.CurrentObject);
				DetailView detailView = Application.CreateDetailView(invoiceObjectSpace, "CustomerOpenInvoices_DetailView", false);
				detailView.CurrentObject = invoiceObjectSpace.GetObjectsQuery<Customers>().Where(r => r.CustomerID == customer.CustomerID).FirstOrDefault();
				e.View = detailView;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void ShowDueInvoices_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				Type objectType = typeof(Invoice);
				IObjectSpace invoiceObjectSpace = Application.CreateObjectSpace(objectType);

				var customer = View.ObjectSpace.GetObject<Customers>((Customers)View.CurrentObject);
				DetailView detailView = Application.CreateDetailView(invoiceObjectSpace, "CustomerDueInvoices_DetailView", false);
				detailView.CurrentObject = invoiceObjectSpace.GetObjectsQuery<Customers>().Where(r => r.CustomerID == customer.CustomerID).FirstOrDefault();
				e.View = detailView;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void OrderStatusListPropertyEditor_ControlCreated(object sender, EventArgs e)
		{
			try
			{
				ListPropertyEditor listPropertyEditor = (ListPropertyEditor)sender;
				if (listPropertyEditor.ListView.Id == "CustomerActivity_OrderStatus_ListView")
				{
					var objSpace = View.ObjectSpace;

					Customers customer = View.CurrentObject as Customers;
					((XPObjectSpace)objSpace).Session.ExecuteNonQuery("DELETE FROM OrderStatusDetail WHERE Route = '" + SecuritySystem.CurrentUserName + "' AND Customer = '" + customer.CustomerNo + "'");
					((XPObjectSpace)objSpace).Session.ExecuteNonQuery("DELETE FROM OrderStatus WHERE Route = '" + SecuritySystem.CurrentUserName + "' AND Customer = '" + customer.CustomerNo + "'");

					List<GlobalParameters> globalParametersList = View.ObjectSpace.GetObjectsQuery<GlobalParameters>().ToList();
					Dictionary<string, string> globalParameters = new Dictionary<string, string>();

					foreach (var parameter in globalParametersList)
					{
						globalParameters.Add(parameter.Name, parameter.Value);
					}

					var paramServiceAddress = globalParameters.ContainsKey("SERVICE_ADDRESS") ? globalParameters["SERVICE_ADDRESS"] : "http://63.131.254.222:10000/MSFRESTServicesBFHCXM/MSFRESTSrv.svc";

					var endPoint = string.Format("{0}/onlineOrderStatus/{1}/{2}?format=json", paramServiceAddress, SecuritySystem.CurrentUserName, customer.CustomerNo);

					// Create the Client
					var client = new HttpClient();

					// Post the JSON
					var responseMessage = client.GetAsync(endPoint).Result;
					var stringResult = responseMessage.Content.ReadAsStringAsync().Result;

					// Convert JSON back to the Object
					var responseObject = JsonConvert.DeserializeObject<OrderStatusResult>(stringResult);

					if (responseObject.Fault == null)
					{
						if (responseObject.Orders.Count > 0)
						{
							string orderID;

							foreach (var order in responseObject.Orders)
							{
								orderID = ((XPObjectSpace)objSpace).Session.ExecuteScalar("SELECT NEWID();").ToString();

								((XPObjectSpace)objSpace).Session.ExecuteNonQuery(
									"INSERT INTO OrderStatus (ID, Route, OrderNumber, OrderType, Warehouse, Customer, OrderDate, ShipDate, PickingNumber, PickingDate, InvoiceNumber, InvoiceDate, TruckerNumber, TotalAmount, HoldCode) " +
									"VALUES ('" + orderID + "', '" + order.Header.Route + "', " + order.Header.OrderNumber + ", '" + order.Header.OrderType + "', '" + order.Header.Warehouse + "', '" + order.Header.Customer + "', '" + order.Header.OrderDate.ToShortDateString() +
									"', '" + order.Header.ShipDate.ToShortDateString() + "', " + order.Header.PickingNumber + ", '" + order.Header.PickingDate.ToShortDateString() + "', " + order.Header.InvoiceNumber + ", '" + order.Header.InvoiceDate.ToShortDateString() +
									"', " + order.Header.TruckerNumber + ", " + order.Header.TotalAmount + ", '" + order.Header.HoldCode + "');"
								);

								foreach (var detail in order.Details)
								{
									((XPObjectSpace)objSpace).Session.ExecuteNonQuery(
										"INSERT INTO OrderStatusDetail (ID, OrderStatus, Route, OrderNumber, OrderType, Warehouse, Customer, Product, Quantity, UM, UnitPrice, ExtPrice, Status, StatusDesc) " +
										"VALUES (NEWID(), '" + orderID + "', '" + detail.Route + "', " + detail.OrderNumber + ", '" + detail.OrderType + "', '" + detail.Warehouse + "', '" + detail.Customer + "', '" + detail.Product +
										"', " + detail.Quantity + ", '" + detail.UM + "', " + detail.UnitPrice + ", " + detail.ExtPrice + ", '" + detail.Status +
										"', '" + detail.StatusDescription + "');"
									);
								}
							}
						}
					}
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void NewOrder_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			try
			{
				Customers customer = View.CurrentObject as Customers;
				//**************
				if (string.IsNullOrEmpty(customer.CreditHold))
				{
					CxmUser currentUser = View.ObjectSpace.GetObjectsQuery<CxmUser>().Where(u => u.UserName == SecuritySystem.CurrentUserName).FirstOrDefault();
					if (currentUser != null && currentUser.Roles.Any(r => r.Name == "Agent" || r.Name == "Supervisor"))
					{
						List<UserActivity> userActivityList = View.ObjectSpace.GetObjectsQuery<UserActivity>().Where(r => r.UserID == currentUser.UserName).ToList();
						UserActivity userActivity = userActivityList.FirstOrDefault(r => r.ActivityDate == DateTime.Today);
						if (userActivity?.CurrentStatus != "Available")
						{
							ShowWarning("User must be in \"Available\" status to be able to create orders.");
							return;
						}

						CallHelper callHelper = View.ObjectSpace.GetObjectsQuery<CallHelper>().FirstOrDefault(x => x.UserID == currentUser.UserName);
						if (callHelper == null)
						{
							callHelper = View.ObjectSpace.CreateObject<CallHelper>();
							callHelper.UserID = SecuritySystem.CurrentUserName;
						}

						if (callHelper != null)
						{
							if (callHelper.CurrentCall == null)
							{
								Calls newCall = View.ObjectSpace.CreateObject<Calls>();
								newCall.StartDateTime = DateTime.Now;
								newCall.UserID = SecuritySystem.CurrentUserName;
								newCall.Category = Calls.CallCategory.OffCallTransactions;
								newCall.Outgoing = false;
								newCall.Planned = false;
								newCall.Attempted = true;
								newCall.CustomerID = customer;
								newCall.Status = Calls.CallStatus.InProgress;

								callHelper.CurrentCall = newCall;
							}

							View.ObjectSpace.CommitChanges();
						}
					}

					IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(SalesOrder));

					SalesOrder newOrder = objectSpace.CreateObject<SalesOrder>();
					newOrder.Customer = objectSpace.GetObject<Customers>(customer);

					Route_Customer route_Customer = objectSpace.GetObjectsQuery<Route_Customer>().Where(r => r.Customer.CustomerID == newOrder.Customer.CustomerID).FirstOrDefault();
					newOrder.Route = objectSpace.GetObjectsQuery<Route>().Where(r => r.RouteNo == route_Customer.Route.RouteNo).FirstOrDefault();
					newOrder.WhsID = objectSpace.GetObjectsQuery<Warehouse>().Where(r => r.ID == "400").FirstOrDefault();

					var result = ((XPObjectSpace)objectSpace).Session.ExecuteSproc("PrepareForNewOrder", newOrder.Route.RouteNo, customer.CustomerID, newOrder.OrderDate.ToShortDateString(), customer.ParamPercProductsForOrder, customer.ParamInvoiceCount);

					if (result.ResultSet.Count() > 0)
					{
						// First ResultSet is the ShipDate.
						newOrder.ShipDate = DateTime.Parse((result.ResultSet.ElementAt(0).Rows.First() as SelectStatementResultRow).Values[0].ToString());

						// Second ResultSet are the last invoiced products that will be added to the order.
						foreach (SelectStatementResultRow row in result.ResultSet[1].Rows)
						{
							foreach (var item in objectSpace.GetObjectsQuery<CustomerProducts>().Where(r => r.CustomerID == customer.CustomerID && r.ProductID.ProductID == row.Values[4].ToString()).ToList())
							{
								var salesLine = objectSpace.CreateObject<SalesLine>();
								salesLine.Product = item.ProductID; // row.Values[4].ToString();
								salesLine.Order = newOrder;
								salesLine.Qty = long.Parse(row.Values[7].ToString());//SuggestedOrderQty
								salesLine.SuggestedQty = long.Parse(row.Values[7].ToString());//SuggestedOrderQty
								salesLine.UnitOfMeasure = row.Values[9].ToString();//item.ProductID.UnitOfMeasure;
								newOrder.SalesLines.Add(salesLine);
							}
						}
					}

					objectSpace.CommitChanges();

					DetailView detailView = Application.CreateDetailView(objectSpace, "SalesOrder_DetailView", true, newOrder);
					detailView.ViewEditMode = ViewEditMode.Edit;
					detailView.Closed += SalesOrderDetailViewController_Closed;
					e.ShowViewParameters.CreatedView = detailView;
				}
				else
				{
					Application.ShowViewStrategy.ShowMessage(string.Format("This customer is currently on Credit Hold: {0}. Cannot create orders at this moment.", customer.CreditHold ?? string.Empty), InformationType.Warning, 5000, InformationPosition.Top);
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void SalesOrderDetailViewController_Closed(object sender, EventArgs e)
		{
			try
			{
				var detailView = sender as DetailView;
				var order = detailView.CurrentObject as SalesOrder;

				detailView.Closed -= SalesOrderDetailViewController_Closed;

				if (order != null && order?.TransStatus == Enums.Trans_Status.ReadyToSend)
				{
					try
					{
						var objSpace = (XPObjectSpace)((DetailView)View).ObjectSpace;

						string message;

						if (order.PrePaid)
						{
							message = $"Invoice #{order.ID} created and transfered to MSF database.";

							Int64 collNumber;
							string confirmationNumber = "";
							Int64.TryParse(order.CollectionReference, out collNumber);
							var collectionDetails = objSpace.GetObjectByKey<Collections>(collNumber)?.Details;
							foreach (var item in collectionDetails)
							{
								confirmationNumber += $"{item.ConfirmationNumber} \\n";
							}

							message += $"\\n\\nConfirmation Number: {confirmationNumber}";
						}
						else
						{
							message = $"Order #{order.ID} created and transfered to MSF database.";
						}

						var result = objSpace.Session.ExecuteSproc("CopyOrderToMsfDatabase", order.ID);

						if (result.ResultSet.Count() > 0)
						{
							if ((result.ResultSet.ElementAt(0).Rows.First() as SelectStatementResultRow).Values[0].ToString() == "0")
							{
								Application.ShowViewStrategy.ShowMessage(string.Format("Order #{0} created and transfered to MSF database.", order.ID), InformationType.Success, 3000, InformationPosition.Top);
							}
							else
							{
								// An error occurred inside the stored procedure [CopyOrderToMsfDatabase]. Details of the error can be viewed in table [errorlog] using query SELECT * FROM errorlog WHERE number = <OrderNumber>
								throw new Exception();
							}
						}

						CxmUser user = detailView.ObjectSpace.GetObjectsQuery<CxmUser>().Where(r => r.UserName == order.Route.RouteNo).FirstOrDefault();

						if (user != null)
						{
							if (user.SendEmail)
							{
								result = objSpace.Session.ExecuteSproc("SendNewOrderEmail", order.ID);
							}

							if (user.SendSMS)
							{
								var recipient = user.PhoneSMSGateWay;
								result = objSpace.Session.ExecuteSproc("SendNewOrderEmail", order.ID, recipient);

							}

							if (user.SendWhatsapp)
							{

								//Send WhatsApp message
							}
						}


					}
					catch (Exception ex)
					{
						Application.ShowViewStrategy.ShowMessage(string.Format("Order #{0} created but could not be transfered to MSF database.", order.ID), InformationType.Warning, 3000, InformationPosition.Top);
						throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
					}
					finally
					{
						try
						{
							var viewObjSpace = View.ObjectSpace;
							var CallHelper = viewObjSpace.GetObjectsQuery<CallHelper>().Where(r => r.UserID == SecuritySystem.CurrentUserName).FirstOrDefault();

							if (CallHelper != null)
							{
								var currentCall = CallHelper.CurrentCall;

								if (currentCall != null)
								{
									CallResult callResult = viewObjSpace.GetObjectsQuery<CallResult>().Where(r => r.Call == currentCall).FirstOrDefault();
									if (callResult == null)
									{
										callResult = viewObjSpace.CreateObject<CallResult>();
										callResult.Call = currentCall;
									}
									callResult.NewOrders = true;
									callResult.Other = true;
									callResult.Notes = $"Order #{order.ID.ToString()} created.";

									viewObjSpace.CommitChanges();
								}
							}
						}
						catch (SqlException sqlEx)
						{
							throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
						}
						catch (Exception ex)
						{
							throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
						}
					}
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}
	}
}
