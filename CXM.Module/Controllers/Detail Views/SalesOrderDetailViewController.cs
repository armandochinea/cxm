﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using CXM.Module.BusinessObjects.CXM;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using DevExpress.ExpressApp.Web.Layout;
using DevExpress.ExpressApp.Web.Editors.ASPx;
using DevExpress.ExpressApp.Xpo;
//using CXM.Module.ProfitStars;
using DevExpress.ExpressApp.Web.SystemModule;
using DevExpress.Xpo.DB;
using DevExpress.ExpressApp.Web;

namespace CXM.Module.Controllers.Detail_Views
{
	public static class SalesOrderHelper
	{
		public static bool PartialSave { get; set; }
	}

	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class SalesOrderDetailViewController : ViewController
	{
		string paramServiceAddress = string.Empty;
		int paramOrderOfflinePromoType = 0, paramOrdSalesTax = 0, paramTaxRoundingOption = 0, paramBfhChangePrice = 0, paramEnableDiscountUserDecision = 0,
			paramUseIvuWithCertDates = 0, paramUseIvuTax = 0, paramEnableProductPercentDiscount = 0, paramOrdEnforceMinOrderAmount = 0;
		double paramIvuMunicipalTax = 0, paramIvuEstatalTax = 0;
		decimal paramIvuAdditionalPercentageValue = 0;
		Collections savedCollection;
		Dictionary<string, string> globalParameters;
		//ListView CustomerSpecialProductsListView, SalesLinesListView;
		public bool closeView = false;

		public SalesOrderDetailViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			var detailView = View as DetailView;

			if (detailView != null && !detailView.IsDisposed)
			{
				var objSpace = detailView.ObjectSpace;

				objSpace.Committing += ObjectSpace_Committing;
				detailView.QueryCanClose += View_QueryCanClose;

				SetGlobalParameters();

				ListPropertyEditor listPropertyEditor = detailView.FindItem("CustomerSpecialProducts") as ListPropertyEditor;
				if (listPropertyEditor != null)
				{
					listPropertyEditor.ControlCreated += SpecialProductsListPropertyEditor_ControlCreated;
				}

				listPropertyEditor = detailView.FindItem("SalesLines") as ListPropertyEditor;
				if (listPropertyEditor != null)
				{
					listPropertyEditor.ControlCreated += SalesLinesListPropertyEditor_ControlCreated;
				}

				(detailView.LayoutManager as WebLayoutManager).PageControlCreated += OnPageControlCreated;

				if (View.Id == "SpeedSalesOrder_DetailView")
				{
					if (SaveOrder != null)
					{
						SaveOrder.Caption = "Save & Send";
						SaveOrder.Active["SaveOrder"] = true;
					}

					var salesOrder = detailView?.CurrentObject as SalesOrder;
					if (salesOrder != null)
					{
						if (salesOrder.TransStatus == Enums.Trans_Status.SentToMSF || salesOrder.TransStatus == Enums.Trans_Status.Canceled || salesOrder.TransStatus == Enums.Trans_Status.Deleted)
							if (SaveOrder != null)
								SaveOrder.Active["SaveOrder"] = false;

						WebModificationsController webModificationsController = Frame.GetController<WebModificationsController>();
						if (webModificationsController != null)
						{
							if (salesOrder.TransStatus != Enums.Trans_Status.New)
								webModificationsController.EditAction.Active["EditAction"] = false;
							else
								webModificationsController.EditAction.Active["EditAction"] = true;
						}
						
					}
				}

				closeView = false;
			}
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();

			var detailView = View as DetailView;

			detailView.ObjectSpace.Committing -= ObjectSpace_Committing;
			detailView.QueryCanClose -= View_QueryCanClose;

			ListPropertyEditor listPropertyEditor = detailView.FindItem("CustomerSpecialProducts") as ListPropertyEditor;
			if (listPropertyEditor != null)
			{
				listPropertyEditor.ControlCreated -= SpecialProductsListPropertyEditor_ControlCreated;
			}

			listPropertyEditor = detailView.FindItem("SalesLines") as ListPropertyEditor;
			if (listPropertyEditor != null)
			{
				listPropertyEditor.ControlCreated -= SalesLinesListPropertyEditor_ControlCreated;

				if (listPropertyEditor.Frame != null)
				{
					Frame listViewFrame = listPropertyEditor.Frame;
					DeleteObjectsViewController deleteController = listViewFrame.GetController<DeleteObjectsViewController>();
					if (deleteController != null)
					{
						deleteController.DeleteAction.Executed -= SalesLinesDeleteAction_Executed;
					}
				}
			}

			//CustomerSpecialProductsListView = null;

			(detailView.LayoutManager as WebLayoutManager).PageControlCreated -= OnPageControlCreated;
		}

		void ShowWarning(string warning) => Application.ShowViewStrategy.ShowMessage(warning, InformationType.Warning, 5000, InformationPosition.Top);

		void SetGlobalParameters()
		{
			List<GlobalParameters> globalParametersList = View.ObjectSpace.GetObjectsQuery<GlobalParameters>().ToList();
			globalParameters = new Dictionary<string, string>();

			foreach (var parameter in globalParametersList)
			{
				globalParameters.Add(parameter.Name, parameter.Value);
			}

			paramServiceAddress = globalParameters.ContainsKey("SERVICE_ADDRESS") ? globalParameters["SERVICE_ADDRESS"] : string.Empty;
			int.TryParse(globalParameters.ContainsKey("BFH_CHANGE_PRICE") ? globalParameters["BFH_CHANGE_PRICE"] : "0", out paramBfhChangePrice);
			int.TryParse(globalParameters.ContainsKey("ENABLE_DISCOUNT_USER_DECISION") ? globalParameters["ENABLE_DISCOUNT_USER_DECISION"] : "0", out paramEnableDiscountUserDecision);
			int.TryParse(globalParameters.ContainsKey("ORD_OFFLINE_PROMO_TYPE") ? globalParameters["ORD_OFFLINE_PROMO_TYPE"] : "0", out paramOrderOfflinePromoType);
			int.TryParse(globalParameters.ContainsKey("ORD_SALES_TAX") ? globalParameters["ORD_SALES_TAX"] : "0", out paramOrdSalesTax);
			int.TryParse(globalParameters.ContainsKey("ORDER_USE_IVU_WITH_CERT_DATES") ? globalParameters["ORDER_USE_IVU_WITH_CERT_DATES"] : "0", out paramUseIvuWithCertDates);
			int.TryParse(globalParameters.ContainsKey("TAX_ROUNDING_OPTION") ? globalParameters["TAX_ROUNDING_OPTION"] : "0", out paramTaxRoundingOption);
			int.TryParse(globalParameters.ContainsKey("USE_IVU_TAX") ? globalParameters["USE_IVU_TAX"] : "0", out paramUseIvuTax);
			int.TryParse(globalParameters.ContainsKey("ENABLE_PRODUCT_PERCENT_DISCOUNT") ? globalParameters["ENABLE_PRODUCT_PERCENT_DISCOUNT"] : "0", out paramEnableProductPercentDiscount);
			int.TryParse(globalParameters.ContainsKey("ORD_ENFORCE_MIN_ORDER_AMOUNT") ? globalParameters["ORD_ENFORCE_MIN_ORDER_AMOUNT"] : "0", out paramOrdEnforceMinOrderAmount);
			double.TryParse(globalParameters.ContainsKey("IVU_MUNICIPAL_TAX") ? globalParameters["IVU_MUNICIPAL_TAX"] : "0", out paramIvuMunicipalTax);
			double.TryParse(globalParameters.ContainsKey("IVU_ESTATAL_TAX") ? globalParameters["IVU_ESTATAL_TAX"] : "0", out paramIvuEstatalTax);
			decimal.TryParse(globalParameters.ContainsKey("IVU_ADDITIONAL_PERCENTAGE_VALUE") ? globalParameters["IVU_ADDITIONAL_PERCENTAGE_VALUE"] : "0", out paramIvuAdditionalPercentageValue);
		}

		private void OnPageControlCreated(object sender, PageControlCreatedEventArgs e)
		{
			try
			{
				if (e.Model.Id == "Tabs")
				{
					e.PageControl.ClientSideEvents.Init = "function(s,e){s.SetActiveTabIndex(0);}";
					((View as DetailView).LayoutManager as WebLayoutManager).PageControlCreated -= OnPageControlCreated;
				}
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void ObjectSpace_Committing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				// isPartialSave will only be false when the Save and Close button is pressed.
				if (!SalesOrderHelper.PartialSave)
				{
					var detailView = ((sender as IObjectSpace).Owner as DetailView);
					var order = detailView.CurrentObject as SalesOrder;

					if (order != null && order.ServerPricingStatus == 0)
					{
						ShowWarning("You must perform price calculation before saving the order.");
						e.Cancel = true;
					}
					else
					{
						if (paramOrdEnforceMinOrderAmount != 0)
						{
							var Rte = (View as DetailView).ObjectSpace.GetObjectsQuery<Route>().Where(x1 => x1.RouteNo == order.Route.RouteNo).FirstOrDefault();
							if (Rte.OrderAmountMinimum > order.OrderTotalAmount)
							{
								ShowWarning(String.Format("The minimum amount for this order is: ${0}. Your current total is: ${1}", Rte.OrderAmountMinimum.ToString("#,##0.00"), order.OrderTotalAmount.ToString("#,##0.00")));
								e.Cancel = true;
								return;
							}
						}

						order.TransStatus = Enums.Trans_Status.ReadyToSend;

						if (order.PrePaid && savedCollection != null)
						{
							int collectionID;
							int.TryParse(((XPObjectSpace)View.ObjectSpace).Session.ExecuteScalar("SELECT MAX(CollectionID) + 1 FROM Collections;").ToString(), out collectionID);
							if (collectionID == 0)
							{
								collectionID = 1;
							}

							Collections refCollection = View.ObjectSpace.GetObjectByKey<Collections>(savedCollection.ID);

							refCollection.CollectionID = collectionID;
							refCollection.TransStatus = Enums.Trans_Status.ReadyToSend;
							refCollection.Save();

							order.CollectionReference = refCollection.CollectionID.ToString();
						}

						order.Save();
						//FreeCasesListView = null;
						closeView = true;

						if (View.Id == "SalesOrder_DetailView" || View.Id == "SpeedSalesOrder_DetailView")
						{
							View.ObjectSpace.Committing -= ObjectSpace_Committing;

							if (View.Id == "SpeedSalesOrder_DetailView")
							{
								View.ObjectSpace.Committed += ObjectSpace_Committed;
							}
						}
					}
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void ObjectSpace_Committed(object sender, EventArgs e)
		{
			if (View.Id == "SpeedSalesOrder_DetailView")
			{
				View.ObjectSpace.Committed -= ObjectSpace_Committed;
				SendOrderToMSF();
			}
		}

		private void View_QueryCanClose(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// The view can only be close if SalesOrder.ServerPricingStatus == 1.
			e.Cancel = !closeView;
		}

		private void SpecialProductsListPropertyEditor_ControlCreated(object sender, EventArgs e)
		{
			try
			{
				ListPropertyEditor listPropertyEditor = sender as ListPropertyEditor;

				if (listPropertyEditor != null)
				{
					listPropertyEditor.ControlCreated -= SpecialProductsListPropertyEditor_ControlCreated;

					Frame listViewFrame = listPropertyEditor.Frame;
					DeleteObjectsViewController deleteController = listViewFrame.GetController<DeleteObjectsViewController>();
					if (deleteController != null)
					{
						deleteController.DeleteAction.Executed += SpecialProductsDeleteAction_Executed;
					}

					SalesOrder order = View.CurrentObject as SalesOrder;
					((XPObjectSpace)View.ObjectSpace).Session.ExecuteSproc("GetCustomerSpecialProducts", order.Customer.CustomerID);
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void SalesLinesListPropertyEditor_ControlCreated(object sender, EventArgs e)
		{
			try
			{
				ListPropertyEditor listPropertyEditor = sender as ListPropertyEditor;

				if (listPropertyEditor != null)
				{
					listPropertyEditor.ControlCreated -= SalesLinesListPropertyEditor_ControlCreated;

					Frame listViewFrame = listPropertyEditor.Frame;
					DeleteObjectsViewController deleteController = listViewFrame.GetController<DeleteObjectsViewController>();
					if (deleteController != null)
					{
						deleteController.DeleteAction.Executed += SalesLinesDeleteAction_Executed;
					}
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void SaveOrder_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			try
			{
				var detailView = (View as DetailView);
				var order = detailView.CurrentObject as SalesOrder;

				if (order != null && order.ServerPricingStatus == 0)
				{
					ShowWarning("You must perform price calculation before saving the order.");
					return;
				}
				else
				{
					if (paramOrdEnforceMinOrderAmount != 0)
					{
						var Rte = (View as DetailView).ObjectSpace.GetObjectsQuery<Route>().Where(x1 => x1.RouteNo == order.Route.RouteNo).FirstOrDefault();
						if (Rte.OrderAmountMinimum > order.OrderTotalAmount)
						{
							ShowWarning(String.Format("The minimum amount for this order is: ${0}. Your current total is: ${1}", Rte.OrderAmountMinimum.ToString("#,##0.00"), order.OrderTotalAmount.ToString("#,##0.00")));
							return;
						}
					}
				}

				if (order.PrePaid)
				{
					IObjectSpace colObjectSpace = Application.CreateObjectSpace(typeof(Collections));
					Collections newCollection = colObjectSpace.CreateObject<Collections>();
					newCollection.Customer = colObjectSpace.GetObject<Customers>(order.Customer);
					newCollection.CollectionDate = DateTime.Now;
					newCollection.SignName = string.Empty;
					newCollection.ReceiptNo = string.Empty;
					newCollection.DSBatchID = string.Empty;
					newCollection.Comments = string.Empty;
					newCollection.OrderReference = order.ID.ToString();
					newCollection.Route = colObjectSpace.GetObjectByKey<Route>(order.Route.RouteNo);

					CallHelper callHelper = colObjectSpace.GetObjectsQuery<CallHelper>().Where(r => r.UserID == SecuritySystem.CurrentUserName).FirstOrDefault();
					newCollection.ContactEmail = order.ContactEmail;
					colObjectSpace.CommitChanges();

					IObjectSpace paymentObjSpace = Application.CreateObjectSpace(typeof(CollectionPayments));
					CollectionPayments payment = paymentObjSpace.CreateObject<CollectionPayments>();
					payment.Collection = paymentObjSpace.GetObjectByKey<Collections>(newCollection.ID);
					payment.AmountPaid = order.OrderTotalAmount;
					payment.OpenAmount = 0;
					payment.OrderReference = order.ID.ToString();
					paymentObjSpace.CommitChanges();

					var detailObjectSpace = Application.CreateObjectSpace(typeof(CollectionDetails));
					var detail = detailObjectSpace.CreateObject<CollectionDetails>();
					detail.Collection = detailObjectSpace.GetObjectByKey<Collections>(newCollection.ID);
					detail.PaymentAmount = order.OrderTotalAmount;
					detail.CardholderName = string.Empty;
					detail.Index = 0;
					detail.OrderReference = order.ID.ToString();
					detailObjectSpace.CommitChanges();

					var collectionDView = Application.CreateDetailView(detailObjectSpace, "Activity_CollectionDetails_DetailView", false, detail);
					collectionDView.ViewEditMode = ViewEditMode.Edit;
					e.ShowViewParameters.CreatedView = collectionDView;

					DialogController dialogController = new DialogController();
					dialogController.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(CheckoutDialogController_Accepting);
					dialogController.Cancelling += CheckoutDialogController_Cancelling;
					e.ShowViewParameters.Controllers.Add(dialogController);
					e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
				}
				else
				{
					View.ObjectSpace.CommitChanges();
					View.Close();
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void SalesLinesDeleteAction_Executed(object sender, ActionBaseEventArgs e)
		{
			try
			{
				SalesOrder order = (SalesOrder)View.CurrentObject;

				if (order != null)
				{
					order.ServerPricingStatus = 0;
				}

				SalesOrderHelper.PartialSave = true;
				View.ObjectSpace.CommitChanges();
				SalesOrderHelper.PartialSave = false;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void SpecialProductsDeleteAction_Executed(object sender, ActionBaseEventArgs e)
		{
			try
			{
				SalesOrderHelper.PartialSave = true;
				View.ObjectSpace.CommitChanges();
				SalesOrderHelper.PartialSave = false;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void CancelOrder_CustomGetFormattedConfirmationMessage(object sender, CustomGetFormattedConfirmationMessageEventArgs e)
		{
			if (View.Id == "SalesOrder_DetailView")
			{
				(sender as SimpleAction).ConfirmationMessage = "Are you sure you want to cancel this transaction?";
			}
		}

		//private void FreeCasesListView_SelectionChanged(object sender, EventArgs e)

		//				//if (cell.accessoryType == UITableViewCellAccessoryNone && !cell.appliedLabelHidden) {
		//				//	//Find the promotion responsible for this free case cell.
		//				//	//This promotion will also hold the responsible product through fields product_no and um.
		//				//	rows = [[DataAccess sharedInstance].orderPromotionsTable rowsFilteredUsingPredicate:[NSPredicate predicateWithFormat:@"promouid == %@", [freeCaseRow valueForKey:@"modelno"]]];
		//				SalesLine freeCaseLine = objSpace.GetObject<SalesLine>(selectedFreeCaseLine);
		//				OrderPromo freeCasePromo = objSpace.GetObjectsQuery<OrderPromo>().Where(r => r.PromoID == freeCaseLine.ModelNo).FirstOrDefault();

		//				if (freeCasePromo != null)
		//				{
		//					if (freeCaseLine.FreeGoods == 1)
		//					{
		//						selectedFreeCaseLine.FreeGoods = 0;
		//						freeCaseLine.FreeGoods = 0;
		//						freeCaseLine.Save();

		//						freeCasePromo.PricingOptionActions = (int)PricingOption.PricingOptionActions.NotSelected;
		//						freeCasePromo.Save();
		//					}
		//					else
		//					{
		//						var paramBfhChangePrice = objSpace.GetObjectsQuery<GlobalParameters>().Where(r => r.Name == "BFH_CHANGE_PRICE").FirstOrDefault()?.Value;

		//						var optionalDiscounts = objSpace.GetObjectsQuery<OrderPromo>().Where(r => r.Order == freeCaseLine.Order && r.PromoType == "DS" && r.DiscountType == "V").ToList<OrderPromo>();

		//						int i;
		//						for (i = 0; i < optionalDiscounts.Count; i++)
		//						{
		//							var discount = optionalDiscounts[i];
		//							int pricingOption = (int)discount.PricingOptions;

		//							if (paramBfhChangePrice == "0")
		//							{
		//								if (BitwiseValueHasFlagEnabled(pricingOption, (int)PricingOption.PricingOptions.UserDecision) && discount.Product == freeCasePromo.Product && discount.UM == freeCasePromo.UM && discount.PricingOptionActions == (int)PricingOption.PricingOptionActions.Selected)
		//								{
		//									Application.ShowViewStrategy.ShowMessage("Can't apply free cases because the product has a discount already applied.", InformationType.Warning, 5000, InformationPosition.Top);
		//									break;
		//								}
		//							}
		//						}

		//						if (i == optionalDiscounts.Count)
		//						{
		//							selectedFreeCaseLine.FreeGoods = 1;
		//							freeCaseLine.FreeGoods = 1;
		//							freeCaseLine.Save();

		//							freeCasePromo.PricingOptionActions = (int)PricingOption.PricingOptionActions.Selected;
		//							freeCasePromo.Save();
		//						}
		//					}

		//					performSave = true;
		//				}
		//				else
		//				{
		//					Application.ShowViewStrategy.ShowMessage("Can't apply free cases because the corresponding promotion line could not be found.", InformationType.Error, 5000, InformationPosition.Top);
		//				}
		//			}

		//			if (performSave)
		//			{
		//				isPartialSave = true;
		//				objSpace.CommitChanges();
		//				isPartialSave = false;

		//				FreeCasesListView.CollectionSource.Reload();
		//			}
		//		}
		//	}
		//	catch (SqlException sqlEx)
		//	{
		//		throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
		//	}
		//	catch (Exception ex)
		//	{
		//		throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
		//	}
		//}

		private void CancelOrder_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			try
			{
				if (View.Id == "SalesOrder_DetailView" || View.Id == "SpeedSalesOrder_DetailView")
				{
					View.ObjectSpace.Committing -= ObjectSpace_Committing;

					//if (View.Id == "SalesOrder_DetailView")
					//{
					SalesOrder order = View.ObjectSpace.GetObject(((View as DetailView).CurrentObject as SalesOrder));

					if (order.TransStatus == Enums.Trans_Status.New)
					{
						order.TransStatus = Enums.Trans_Status.Deleted;

						CxmUser currentUser = View.ObjectSpace.GetObjectsQuery<CxmUser>().Where(u => u.UserName == SecuritySystem.CurrentUserName).FirstOrDefault();
						if (currentUser != null)
						{
							if (currentUser.Roles.Any(r => r.Name == "Agent" || r.Name == "Supervisor"))
							{
								CallHelper callHelper = View.ObjectSpace.GetObjectsQuery<CallHelper>().FirstOrDefault(c => c.UserID == currentUser.UserName);
								if (callHelper != null && callHelper.CurrentCall != null)
								{
									Calls call = View.ObjectSpace.GetObject<Calls>(callHelper.CurrentCall);
									if (call != null && call.Category == Calls.CallCategory.OffCallTransactions)
									{
										call.EndDateTime = DateTime.Now;
										currentUser.NextCallNumber++;
										call.CallNumber = $"{currentUser.UserName}-{currentUser.NextCallNumber.ToString("000000")}";
										call.Status = Calls.CallStatus.Finished;

										if (order.Customer != null) call.CustomerID = order.Customer;

										CallResult callResult = View.ObjectSpace.GetObjectByKey<CallResult>(call.ID);
										if (callResult == null)
										{
											callResult = View.ObjectSpace.CreateObject<CallResult>();
											callResult.Call = call;
										}

										callResult.Other = true;
										callResult.Notes = "New order canceled.";
										callResult.Save();

										call.Save();

										callHelper.CurrentCall = null;
										callHelper.Save();
									}
								}
							}
						}

						View.ObjectSpace.CommitChanges();
					}
					
					//}
				}

				closeView = true;
				View.Close();
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void CalculatePrices_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			try
			{
				//
				//PricingHeader header = new PricingHeader();
				//PricingRequest request = new PricingRequest(header);

				var objSpace = View.ObjectSpace;
				SalesOrderHelper.PartialSave = true;
				objSpace.CommitChanges();
				SalesOrderHelper.PartialSave = false;

				SalesOrder order = ((DetailView)View).CurrentObject as SalesOrder;
				order.Reload();

				if (order.Route == null)
				{
					ShowWarning("A route is required to perform this action. Please select a route.");
					return;
				}
				else if (View.Id == "SpeedSalesOrder_DetailView" && order.Customer == null)
				{
					ShowWarning("A customer is required to perform this action. Please select a customer.");
					return;
				}
				else if (order.SalesLines.Count == 0)
				{
					ShowWarning("There are no items in the order.");
					return;
				}

				PricingRequest request = new PricingRequest();
				request.CustomerNumber = order.Customer.CustomerNo;
				request.ShipToNumber = order.Customer.ShipTo;
				request.ShipDate = order.ShipDate.ToString(PricingConstants.DATE_FORMAT);
				request.TransType = 1; //INCLUDE
				request.Route = order.Route.RouteNo;
				request.Warehouse = order.Customer.WhsID.ID;
				request.CompanyId = 1; //INCLUDE
				request.PoNumber = string.Empty;
				List<PricingProduct> products = new List<PricingProduct>();
				PricingProduct pricingProduct;

				foreach (SalesLine line in order.SalesLines)
				{
					// Lines with FreeGoodsVal == 1 are Free Cases and should not be considered in the pricing request.
					if (line.FreeGoodsVal == -1)
					{
						pricingProduct = new PricingProduct();
						pricingProduct.ProductNumber = line.Product.ProductID;
						pricingProduct.UnitOfMeasure = line.UnitOfMeasure;
						pricingProduct.QuantityOrdered = (int)line.Qty;
						products.Add(pricingProduct);
					}
				}

				request.Products = products;

				var endPoint = string.Format("{0}/onlinePricesSearch?format=json", paramServiceAddress);

				// Convert Object to JSON
				var requestMessage = JsonConvert.SerializeObject(request);
				var content = new StringContent(requestMessage, Encoding.UTF8, "application/json");

				// Create the Client
				var client = new HttpClient();

				// Post the JSON
				var responseMessage = client.PostAsync(endPoint, content).Result;
				var stringResult = responseMessage.Content.ReadAsStringAsync().Result;

				// Convert JSON back to the Object
				var responseObject = JsonConvert.DeserializeObject<PricingResult>(stringResult);

				string errorMessages = string.Empty;
				string pricingErrorTitle = "A pricing error has occurred:";

				if (responseObject.ReturnMessages.ToList().Count == 0)
				{
					ClearData(order);

					foreach (var pricingOption in responseObject.PricingOptions)
					{
						HandlePricingOption(pricingOption, order);
					}

					//Refresh totals for each line.
					foreach (var salesLine in order.SalesLines)
					{
						/*
						//Check if order line has a price contract.
						contract_no = [(DADataTableRow*)[self.orderLines objectAtIndex:j] valueForKey:@"contract_no"];
                    
						if (contract_no != nil && contract_no != [NSNull null] && [(NSNumber*)contract_no intValue] != 0) {
							//Retain contract number's NSNumber to prevent its release when price contract field is cleared during the next instruction.
							[contract_no retain];
                        
							//Clear price contract field.
							[(DADataTableRow*)[self.orderLines objectAtIndex:j] setValue:[NSNull null] forKey:@"contract_no"];
                        
							//Find price contract record.
							productSearch = [[DataAccess sharedInstance].contractsTable rowsFilteredUsingPredicate:[NSPredicate predicateWithFormat:@"contract_no == %@", (NSNumber*)contract_no]];
                        
							//Release contract number.
							[contract_no release];
                        
							if (productSearch.count > 0)
								//Add price contract.
								[DataUtils addPriceContract:(DADataTableRow*)[productSearch objectAtIndex:0] toOrderLine:(DADataTableRow*)[self.orderLines objectAtIndex:j]];
						}
						*/

						/*
						if ([[GlobalSettings settings] isValueEnabled:BFH_CHANGE_PRICE withDefaultValue:@"0"]) {
							DADataTableRow *lineRow = [self.orderLines objectAtIndex:j];

							[lineRow setValue:[[lineRow valueForKey:@"unit_price"] addNumber:[lineRow valueForKey:@"Discount"] ] forKey:@"unit_price"];
						}

						[[DataAccess sharedInstance] calculateOnlyLineTotalsForRow:(DADataTableRow*)[self.orderLines objectAtIndex:j] addTax:(custTax != 0)];
						[[DataAccess sharedInstance] calculateTaxesForOrderLine:(DADataTableRow*)[self.orderLines objectAtIndex:j] inOrder:self.orderRow];
						*/
						if (paramBfhChangePrice == 1)
						{
							salesLine.UnitPrice = salesLine.UnitPrice + salesLine.Discount;
						}

						float customerTax = order.Customer.StateTaxPercent;
						CalculateOnlyLineTotalsForSalesLine(salesLine, customerTax != 0 ? true : false);
						CalculateTaxesForSalesLine(salesLine, order);
					}

					////Refresh order totals.
					//[[DataAccess sharedInstance] calculateOrderTotalsForOrder:[self.orderRow valueForKey:@"order_no"]];
					CalculateOrderTotals(order);

					//[orderRow setValue:[NSNumber numberWithInt:1] forKey:@"serverPricingStatus"];
					if (order.SalesLines.Count > 0)
					{
						order.ServerPricingStatus = 1;
					}
					//##################################################################################################
				}
				else
				{
					/*
					//Pricing errors were returned.
					for (j = 0; j < pricingMessages.count; j++)
						if ([(NSNumber*)[(NSDictionary*)[pricingMessages objectAtIndex: j] valueForKey: @"ShowToUser"] intValue] == 1)

						[UIAlertView showSimpleWithTitle: __(@"ERROR_PRECIOS", @"Error de precios") andMessage:[NSString stringWithFormat: __(@"MSG_ERROR_PRECIOS", @"Ha ocurrido un error de precios:\n%@"), [(NSDictionary*)[pricingMessages objectAtIndex: j] valueForKey: @"Message"]]];
					*/
					foreach (var message in responseObject.ReturnMessages)
					{
						if (message.ShowToUser)
						{
							errorMessages += string.Format("\n{0}", message.Message);
						}
					}

					if (errorMessages != string.Empty)
					{
						throw new ActionExecutionException(CalculatePrices, new Exception(string.Format("{0} {1}", pricingErrorTitle, errorMessages)));
						//Application.ShowViewStrategy.ShowMessage(string.Format("{0}{1}", pricingErrorTitle, errorMessages), InformationType.Error, 10000, InformationPosition.Top);
					}
				}

				if (responseObject.OrderMessages.ToList().Count > 0)
				{
					foreach (var orderMessage in responseObject.OrderMessages)
					{
						OrderMessages newOrderMessage = objSpace.CreateObject<OrderMessages>();
						SalesLine salesLineItem = null;

						newOrderMessage.ID = orderMessage.idx;
						newOrderMessage.CompID = order.Route.CompID;
						newOrderMessage.Route = objSpace.GetObject<Route>(order.Route);
						newOrderMessage.Order = order;
						newOrderMessage.Product = objSpace.GetObjectsQuery<Product>().Where(r => r.ProductID == orderMessage.product_no).FirstOrDefault();
						newOrderMessage.UM = orderMessage.um;
						newOrderMessage.MessageType = orderMessage.messageType;
						newOrderMessage.Message = orderMessage.message;
						newOrderMessage.MessageNumber = orderMessage.messageNumber;
						newOrderMessage.MessageID = orderMessage.messageId;
						newOrderMessage.MessageReferences = orderMessage.messageReferences;

						string strDateTime = "1900-01-01";
						if (!orderMessage.messageDateTime.Contains("-") && !orderMessage.messageDateTime.Contains("/"))
							strDateTime = orderMessage.messageDateTime.Substring(0, 4) + "-" + orderMessage.messageDateTime.Substring(4, 2) + "-" + orderMessage.messageDateTime.Substring(6);

						newOrderMessage.MessageDatetime = DateTime.Parse(strDateTime);

						if (newOrderMessage.Product != null)
						{
							salesLineItem = objSpace.GetObjectsQuery<SalesLine>().Where(r => r.Order == order && r.Product == newOrderMessage.Product && r.UnitOfMeasure == newOrderMessage.UM).FirstOrDefault();

							if (salesLineItem != null)
							{
								if (newOrderMessage.MessageType == "E")
								{
									salesLineItem.HasErrorMessages = true;
								}
								else
								{
									salesLineItem.HasMessages = true;
								}

								salesLineItem.Save();
							}
						}

						if (salesLineItem == null)
						{
							if (newOrderMessage.MessageType == "E")
							{
								order.HasErrorMessages = true;
							}
							else
							{
								order.HasMessages = true;
							}

							order.Save();
						}

						newOrderMessage.Save();
					}

					Application.ShowViewStrategy.ShowMessage("The server sent messages about the pricing procedure. Please check them on the Messages tab before continuing.", InformationType.Warning, 8000, InformationPosition.Top);
				}

				SalesOrderHelper.PartialSave = true;
				objSpace.CommitChanges();
				SalesOrderHelper.PartialSave = false;

				//if (FreeCasesListView != null)
				//{
				//	if (FreeCasesListView.CollectionSource != null)
				//	{
				//		FreeCasesListView.CollectionSource.Reload();
				//	}
				//}
			}
			catch (ActionExecutionException actEx)
			{
				throw new Exception(actEx.Message);
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		void CalculateOnlyLineTotalsForSalesLine(SalesLine salesLine, bool addTax)
		{
			try
			{
				/*
				DAIndex *itemIndex = [DAIndex indexWithDataTable:[[DataAccess sharedInstance] productsTable] indexField:@"ProductID"];
				DADataTableRow *itemRow = [itemIndex rowForIndexValue:[lineRow valueForKey:@"product_no"]];
				float itemTax = [[itemRow valueForKey:@"SalesTax"] floatValue];
				float discount = [[lineRow valueForKey:@"Discount"] floatValue];
				double price = [(NSNumber *)[lineRow valueForKey:@"unit_price"] doubleValue];
   
				double qty = [(NSNumber *)[lineRow valueForKey:@"qty"] doubleValue];

				//double subTotal = price * qty;
				//Se redondea para evitar problemas con los decimales en el cálculo del precio.
				float subTotal = [[[NSNumber numberWithFloat:price] roundToDecimalPlaces:4] floatValue] * qty;

				if ([[GlobalSettings settings] intValueForKey:ORD_OFFLINE_PROMO_TYPE] == 5) {
					subTotal = subTotal - discount;
				}

				double tax = 0;
    
				if ([[DataUtils pricingSettings] integerValueForKey:ORD_SALES_TAX] == 0) {
					if (addTax) {
						if([[DataUtils pricingSettings] isValueEnabled:ORDER_USE_IVU_WITH_CERT_DATES] && itemTax > 0.0f){
							//Se utilizan los porcientos de los certificados de IVU.
							double munTax = [[[DataUtils pricingSettings] numberValueForKey:IVU_MUNICIPAL_TAX] doubleValue];
							double estTax = [[[DataUtils pricingSettings] numberValueForKey:IVU_ESTATAL_TAX] doubleValue];
                
							tax = (subTotal * munTax) + (subTotal * estTax);
						} else {
							//Para el cálculo del tax hay que considerar si hay un descuento aplicado.
							if(discount < 0.0f){
								discount *= -1;
							}
							tax = ( subTotal - discount ) * ( itemTax / 100 );
						}
					}
				}
    
				[lineRow setValue:[NSNumber numberWithDouble:subTotal] forKey:@"ext_price"];
				//Si el parametro es diferente de 0, el cálculo del tax se hace en la función netPriceForOrder.
				if ([[DataUtils pricingSettings] integerValueForKey:ORD_SALES_TAX] == 0) {
					[lineRow setValue:[NSNumber numberWithDouble:tax] forKey:@"salesTax"];
				}
				*/
				var objSpace = View.ObjectSpace;
				var product = objSpace.GetObjectsQuery<Product>().Where(r => r.ProductID == salesLine.Product.ProductID).FirstOrDefault();
				var itemTax = salesLine.SalesTax;
				var discount = salesLine.Discount;
				var price = salesLine.UnitPrice;
				var qty = salesLine.Qty;

				var subTotal = price * qty;

				if (paramOrderOfflinePromoType == 5)
				{
					subTotal = subTotal - discount;
				}

				/*
				double tax = 0;

				if (paramOrdSalesTax == 0)
				{
					if (addTax)
					{
						if (paramUseIvuWithCertDates == 1 && itemTax > 0)
						{
							tax = ((double)subTotal * paramIvuMunicipalTax) + ((double)subTotal * paramIvuEstatalTax);
						}
						else
						{
							if (discount < 0)
							{
								discount *= -1;
							}
							tax = (double)(subTotal - discount) * (double)(itemTax / 100); 
						}
					}
				}
				*/

				decimal tax = 0;

				if (paramOrdSalesTax == 0)
				{
					if (addTax)
					{
						if (paramUseIvuWithCertDates == 1 && itemTax > 0)
						{
							tax = (subTotal * (decimal)paramIvuMunicipalTax) + (subTotal * (decimal)paramIvuEstatalTax);
						}
						else
						{
							if (discount < 0)
							{
								discount *= -1;
							}
							tax = (subTotal - discount) * (itemTax / 100);
						}
					}
				}

				salesLine.ExtPrice = subTotal;
				if (paramOrdSalesTax == 0) salesLine.SalesTax = (decimal)tax;

				salesLine.Save();
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		void CalculateTaxesForSalesLine(SalesLine salesLine, SalesOrder order)
		{
			try
			{
				/*
				NSNumber *retVal = @0, *tempNumber, *basePrice, *netPrice;
				DADataTableRow *taxRow, *customerRow, *orderPromo;
    
				if (orderRow == nil)
					orderRow = [self.orderTable rowsFilteredUsingPredicate:[NSPredicate predicateWithFormat:@"order_no == %@", [lRow valueForKey:@"order_no"]]].lastObject;

    
				customerRow = [[DataUtils pricingSettings] objectForKey:@"CUSTOMER_ROW"];
    
				//Find calculated base price.
				basePrice = [[self.orderPromotionsTable rowsFilteredUsingPredicate:[NSPredicate predicateWithFormat:@"compid == %@ && route == %@ && order_no == %@ && product_no == %@ && um == %@ && promotype == 'BP'", [lRow valueForKey:@"compid"], [lRow valueForKey:@"route"], [lRow valueForKey:@"order_no"], [lRow valueForKey:@"product_no"], [lRow valueForKey:@"um"]]].lastObject valueForKey:@"amount"];

				//Find calculated net price.
				netPrice = [[self.orderPromotionsTable rowsFilteredUsingPredicate:[NSPredicate predicateWithFormat:@"compid == %@ && route == %@ && order_no == %@ && product_no == %@ && um == %@ && promotype == 'NP'", [lRow valueForKey:@"compid"], [lRow valueForKey:@"route"], [lRow valueForKey:@"order_no"], [lRow valueForKey:@"product_no"], [lRow valueForKey:@"um"]]].lastObject valueForKey:@"amount"];


				if ([[DataUtils pricingSettings] integerValueForKey:ORD_SALES_TAX] == 2) {
					//First find taxes specific for the customer.
					NSPredicate *p = [NSPredicate predicateWithFormat:@"status == 'A' && CodImp == %@ && Libro == %@ && Producto == %@", [customerRow valueForKey:@"custtaxcode"], [customerRow valueForKey:@"customer_no"], [lRow valueForKey:@"product_no"]];
					taxRow = [[DataAccess sharedInstance].taxesTable rowsFilteredUsingPredicate:p].lastObject;
        
					if (taxRow == nil) {
						//If not customer specific taxes were found, try to find taxes specific for the customer group.
						p = [NSPredicate predicateWithFormat:@"status == 'A' && CodImp == %@ && Libro == %@ && Producto == %@", [customerRow valueForKey:@"custtaxcode"], [customerRow valueForKey:@"custgroup"], [lRow valueForKey:@"product_no"]];
						taxRow = [[DataAccess sharedInstance].taxesTable rowsFilteredUsingPredicate:p].lastObject;
            
						if (taxRow == nil) {
							//If not customer group specific taxes were found, try to find taxes for all customers.
							p = [NSPredicate predicateWithFormat:@"status == 'A' && CodImp == %@ && Libro == 'ZS' && Producto == %@", [customerRow valueForKey:@"custtaxcode"], [lRow valueForKey:@"product_no"]];
							taxRow = [[DataAccess sharedInstance].taxesTable rowsFilteredUsingPredicate:p].lastObject;
						}
					}
        
        
					if (taxRow != nil) {
						//Tax row was found.
						//[(precioLista)(1 + AdValoren) - descuento + selectivo](1 + ITBIS)
            
            
						//Calculate AdValoren.
						tempNumber = (NSNumber*)[taxRow valueForKey:@"Advaloren"];
						tempNumber = [[basePrice multiplyByNumber:tempNumber] roundToDecimalPlaces:2];
						retVal = [retVal addNumber:tempNumber];
            
						if ([[DataUtils pricingSettings] isValueEnabled:ORD_INSERT_RECORD_ORDER_PROMO]) {
							//Save AdValoren as a price condition.
							orderPromo = [DataUtils createEmptyOrderPromo];
							[orderPromo setValue:[orderRow valueForKey:@"compid"] forKey:@"compid"];
							[orderPromo setValue:[orderRow valueForKey:@"route"] forKey:@"route"];
							[orderPromo setValue:[orderRow valueForKey:@"order_no"] forKey:@"order_no"];
							[orderPromo setValue:@"AdValoren" forKey:@"promocode"];
							[orderPromo setValue:[lRow valueForKey:@"product_no"] forKey:@"product_no"];
							[orderPromo setValue:@"AdValoren" forKey:@"description"];
							[orderPromo setValue:[lRow valueForKey:@"um"] forKey:@"um"];
							[orderPromo setValue:tempNumber forKey:@"amount"];
							[orderPromo setValue:[NSNumber numberWithInt:9000] forKey:@"sequence"];
							[orderPromo setValue:@"TX" forKey:@"promotype"];
						}
            
            
						//Calculate Selectivo.
						tempNumber = [(NSNumber*)[taxRow valueForKey:@"Selectivo"] roundToDecimalPlaces:2];
						retVal = [retVal addNumber:tempNumber];
            
						if ([[DataUtils pricingSettings] isValueEnabled:ORD_INSERT_RECORD_ORDER_PROMO]) {
							//Save selectivo as a price condition.
							orderPromo = [DataUtils createEmptyOrderPromo];
							[orderPromo setValue:[orderRow valueForKey:@"compid"] forKey:@"compid"];
							[orderPromo setValue:[orderRow valueForKey:@"route"] forKey:@"route"];
							[orderPromo setValue:[orderRow valueForKey:@"order_no"] forKey:@"order_no"];
							[orderPromo setValue:@"Selectivo" forKey:@"promocode"];
							[orderPromo setValue:[lRow valueForKey:@"product_no"] forKey:@"product_no"];
							[orderPromo setValue:@"Selectivo" forKey:@"description"];
							[orderPromo setValue:[lRow valueForKey:@"um"] forKey:@"um"];
							[orderPromo setValue:tempNumber forKey:@"amount"];
							[orderPromo setValue:[NSNumber numberWithInt:9100] forKey:@"sequence"];
							[orderPromo setValue:@"TX" forKey:@"promotype"];
						}
            
            
						//Calculate ITBIS.
						tempNumber = (NSNumber*)[taxRow valueForKey:@"ITBIS"];
						tempNumber = [[netPrice multiplyByNumber:tempNumber] roundToDecimalPlaces:2];
						retVal = [retVal addNumber:tempNumber];
            
						if ([[DataUtils pricingSettings] isValueEnabled:ORD_INSERT_RECORD_ORDER_PROMO]) {
							//Save ITBIS as a price condition.
							orderPromo = [DataUtils createEmptyOrderPromo];
							[orderPromo setValue:[orderRow valueForKey:@"compid"] forKey:@"compid"];
							[orderPromo setValue:[orderRow valueForKey:@"route"] forKey:@"route"];
							[orderPromo setValue:[orderRow valueForKey:@"order_no"] forKey:@"order_no"];
							[orderPromo setValue:@"ITBIS" forKey:@"promocode"];
							[orderPromo setValue:[lRow valueForKey:@"product_no"] forKey:@"product_no"];
							[orderPromo setValue:@"ITBIS" forKey:@"description"];
							[orderPromo setValue:[lRow valueForKey:@"um"] forKey:@"um"];
							[orderPromo setValue:tempNumber forKey:@"amount"];
							[orderPromo setValue:[NSNumber numberWithInt:9200] forKey:@"sequence"];
							[orderPromo setValue:@"TX" forKey:@"promotype"];
						}
					}
        
				} else if ([[DataUtils pricingSettings] integerValueForKey:ORD_SALES_TAX] == 3) {
					//First find taxes specific for the customer.
					NSPredicate *p = [NSPredicate predicateWithFormat:@"status == 'A' && CodImp == %@ && Producto == %@", [customerRow valueForKey:@"custtaxcode"], [lRow valueForKey:@"product_no"]];
					taxRow = [[DataAccess sharedInstance].taxesTable rowsFilteredUsingPredicate:p].lastObject;
        
					if (taxRow != nil) {
						//Calculate Selectivo.
						tempNumber = (NSNumber*)[taxRow valueForKey:@"Advaloren"];
						//El redondeo se debe realizar luego de multiplicar por el precio extendido.
						tempNumber = [[[netPrice multiplyByNumber:[lRow valueForKey:@"qty"]] multiplyByNumber:tempNumber] roundToDecimalPlaces:2];
            
						if ([[DataUtils pricingSettings] isValueEnabled:ORD_INSERT_RECORD_ORDER_PROMO]) {
							//Save selectivo as a price condition.
							orderPromo = [DataUtils createEmptyOrderPromo];
							[orderPromo setValue:[orderRow valueForKey:@"compid"] forKey:@"compid"];
							[orderPromo setValue:[orderRow valueForKey:@"route"] forKey:@"route"];
							[orderPromo setValue:[orderRow valueForKey:@"order_no"] forKey:@"order_no"];
							[orderPromo setValue:@"IVU" forKey:@"promocode"];
							[orderPromo setValue:[lRow valueForKey:@"product_no"] forKey:@"product_no"];
							[orderPromo setValue:@"IVU" forKey:@"description"];
							[orderPromo setValue:[lRow valueForKey:@"um"] forKey:@"um"];
							[orderPromo setValue:tempNumber forKey:@"amount"];
							[orderPromo setValue:[NSNumber numberWithInt:100] forKey:@"sequence"];
							[orderPromo setValue:@"TX" forKey:@"promotype"];
						}
            
						//Se cambia el valor del tax en Order_d
						[lRow setValue:tempNumber forKey:@"salesTax"];
						retVal = tempNumber;
					}
				}
				*/
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		void CalculateOrderTotals(SalesOrder order)
		{
			try
			{
				var objSpace = View.ObjectSpace;

				//NSPredicate *p = [NSPredicate predicateWithFormat:@"order_no == %@", orderNumber];
				//DADataTableRow *orderRow = [[self.orderTable rowsFilteredUsingPredicate:p] lastObject];
				//NSArray *rows = nil, *search;
				//NSInteger i;
				//NSInteger taxRoundingOption = [[GlobalSettings settings] intValueForKey:TAX_ROUNDING_OPTION];

				//if([[orderRow valueForKey:@"transtype"] intValue]==19){
				//	rows = [[self.recargaDetailTable rows] filteredArrayUsingPredicate:p];
				//}else{
				//	rows = [[self.orderDetailTable rows] filteredArrayUsingPredicate:p]; 
				//}

				decimal productTax = 0, totalTax = 0, subTotal = 0, totalStateTax = 0, totalCityTax = 0;
				//NSNumber *productTax;
				//NSNumber *totalTax = @0;
				//NSNumber *subTotal = @0;
				//NSNumber *totalStateTax = @0.00;
				//NSNumber *totalCityTax = @0.00;

				foreach (var salesLine in order.SalesLines)
				{
					subTotal += salesLine.ExtPrice;
					totalTax += salesLine.SalesTax;
				}
				//for (DADataTableRow *orow in rows)
				//{
				//	subTotal = [subTotal addNumber:[orow valueForKey:@"ext_price"]];
				//	totalTax = [totalTax addNumber:[orow valueForKey:@"salesTax"]];
				//}

				if (paramUseIvuTax == 1)
				{
					/*
					NSNumber *ivuMunicipal = @0;
					NSNumber *ivuEstatal = @0;
					if([[orderRow valueForKey:@"customer_no"] isEqualToString:@"GENERIC"]){
						NSString *cert = [orderRow valueForKey:@"newcertmunicipal"] ;
						cert = [cert stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
						if([cert length]==0){
							ivuMunicipal = [subTotal multiplyByFloat:[[GlobalSettings settings] stringValueForKey:IVU_MUNICIPAL_TAX].floatValue];
							ivuEstatal = [subTotal multiplyByFloat:[[GlobalSettings settings] stringValueForKey:IVU_ESTATAL_TAX].floatValue];
						}
					}else{
						DADataTableRow *custRow = [[DataAccess sharedInstance] customerRowWithRefRow:orderRow];
            
						if([[custRow valueForKey:@"certmunicipal"] length]==0){
							ivuMunicipal = [subTotal multiplyByFloat:[[GlobalSettings settings] stringValueForKey:IVU_MUNICIPAL_TAX].floatValue];
							ivuEstatal = [subTotal multiplyByFloat:[[GlobalSettings settings] stringValueForKey:IVU_ESTATAL_TAX].floatValue];
						}
					}
        
				   [orderRow setValue:[ivuEstatal addNumber:ivuMunicipal] forKey:@"totalSalesTax"]; 
					*/
				}
				else if (paramOrdSalesTax == 1 /*&& ([[orderRow valueForKey: @"TransactionType"] intValue] == 0 || ([[GlobalSettings settings] isValueEnabled: CALCULATE_TAX_ON_CREDIT] && [[orderRow valueForKey: @"TransactionType"] intValue] == 1))*/)
				{
					/*
					if ([DataUtils canCalculateTaxForCustomer:[orderRow valueForKey:@"customer_no"]]){
						totalTax = @0;
            
						//Se movio antes del for para que no se verifique por cada producto de la orden.
						BOOL noTax = NO;
						DADataTableRow *custRow = [[[[DataAccess sharedInstance] customersTable] rowsFilteredUsingPredicate:[NSPredicate predicateWithFormat:@"customer_no = %@", [orderRow valueForKey:@"customer_no"]]] lastObject];
						if([[NSDate date] isEarlierThanDate:[custRow valueForKey:@"certMunicipalExpiration"]] && [[GlobalSettings settings] isValueEnabled:ORDER_USE_IVU_WITH_CERT_DATES]){
							noTax = YES;
						}
            
						if ([[GlobalSettings settings] isValueEnabled:ORD_USE_IVU_WITH_CERT_NUMBER]) {
							if([[orderRow valueForKey:@"customer_no"] isEqualToString:@"GENERIC"]){
								NSString *cert=[orderRow valueForKey:@"newcertmunicipal"] ;
								cert= [cert stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
								if([cert length] == 0){
									noTax = NO;
								} else {
									noTax = YES;
								}
							}else{
								if ([[orderRow valueForKey:@"TransactionType"] intValue] == 0) {
									if([[custRow valueForKey:@"certmunicipal"] length] == 0) {
										noTax = NO;
									} if ([[orderRow valueForKey:@"newcertmunicipal"] length] == 0) {
										noTax = NO;
									} else {
										noTax = YES;
									}
								}
                    
								if ([[orderRow valueForKey:@"TransactionType"] intValue] == 1) {
									if([[custRow valueForKey:@"certmunicipal"] length] == 0) {
										noTax = NO;
									} else {
										noTax = YES;
									}
								}
							}
						}
            
            
						for (DADataTableRow *orow in rows)
						{
							NSPredicate *pred = [NSPredicate predicateWithFormat:@"ProductCode == %@", [orow valueForKey:@"product_no"]];
							DADataTableRow *prod = [[[[DataAccess sharedInstance] productsTable] rowsFilteredUsingPredicate:pred] lastObject];
							productTax = @0;
                
							if ([(NSNumber*)[prod valueForKey:@"SalesTax"] isGreaterThanFloat:0.0f]){
								//Round product tax now to avoid precision discrepancies when accumulating the tax for the total.
								if (taxRoundingOption == 1)
									//Use ceiling procedure.
									productTax = [[[(NSNumber*)[prod valueForKey:@"SalesTax"] divideByInt:100] multiplyByNumber:[(NSNumber*)[orow valueForKey:@"ext_price"] substractNumber:[(NSNumber*)[orow valueForKey:@"Discount"] multiplyByNumber:[orow valueForKey:@"qty"]]]] ceilingToDecimalPlaces:2];
								else
									//Use rounding procedure.
									if ([[GlobalSettings settings] isValueEnabled:@"PRICING_TAX_USE_NETPRICE"]) {
										productTax = [[[(NSNumber*)[prod valueForKey:@"SalesTax"] divideByInt:100] multiplyByNumber:(NSNumber*)[orow valueForKey:@"ext_price"]] roundToDecimalPlaces:2];
									} else {
										productTax = [[[(NSNumber*)[prod valueForKey:@"SalesTax"] divideByInt:100] multiplyByNumber:[(NSNumber*)[orow valueForKey:@"ext_price"] substractNumber:[(NSNumber*)[orow valueForKey:@"Discount"] multiplyByNumber:[orow valueForKey:@"qty"]]]] roundToDecimalPlaces:2];
									}
							}
                
							//Si el cliente no tiene las fechas de certificación vigentes se desactiva.
							if(noTax){
								productTax = @0;
							}
                
							//if(productTax > 0){
								[orow setValue:productTax forKey:@"salesTax"];
								totalTax = [totalTax addNumber:productTax];
							//}
						}
					}
					[orderRow setValue:totalTax forKey:@"totalSalesTax"];
					*/
				}
				else if (paramOrdSalesTax == 2 /*&& [[orderRow valueForKey:@"TransactionType"] intValue] == 0*/)
				{
					/*
					//Calculate taxes as done with Brugal.
					totalTax = @0;

					for (DADataTableRow *orow in rows) {
						// Verificar y no calcular impuestos a las cajas gratis
						if ([[orow valueForKey:@"unit_price"] floatValue] == 0.00f) {
							continue;
						}
						productTax = @0;
						search = [self.orderPromotionsTable rowsFilteredUsingPredicate:[NSPredicate predicateWithFormat:@"order_no == %@ && product_no == %@ && um == %@ && promotype == 'TX'", orderNumber, [orow valueForKey:@"product_no"], [orow valueForKey:@"um"]]];
            
						for (i = 0; i < search.count; i++) {
							productTax = [productTax addNumber:[(NSNumber*)[(DADataTableRow*)[search objectAtIndex:i] valueForKey:@"amount"] multiplyByNumber:[orow valueForKey:@"qty"]]];
						}
            
						if([productTax isGreaterThanInt:0]){
							[orow setValue:productTax forKey:@"salesTax"];
							totalTax = [totalTax addNumber:productTax];
						}
					}

					[orderRow setValue:totalTax forKey:@"totalSalesTax"];
					*/
				}
				else if (paramOrdSalesTax == 4 /*&& [[orderRow valueForKey:@"TransactionType"] intValue] == 0*/)
				{
					if (order.Customer.UseSalesTax)
					{
						totalTax = 0;

						bool addIvuPercentageValue = false;

						// If the certification has expired, the tax will be added.
						if (DateTime.Now.Date > order.Customer.CertMunicipalExpDate)
						{
							addIvuPercentageValue = true;
						}

						foreach (var salesLine in order.SalesLines)
						{
							var product = objSpace.GetObjectsQuery<Product>().Where(r => r.ProductID == salesLine.Product.ProductID).FirstOrDefault();
							productTax = product.SalesTax;

							if (addIvuPercentageValue)
							{
								productTax += paramIvuAdditionalPercentageValue;
							}

							if (paramTaxRoundingOption == 1)
							{
								//Use ceiling procedure.
								//productTax = [[[productTax divideByInt: 100] multiplyByNumber: (NSNumber*)[orow valueForKey: @"ext_price"]] ceilingToDecimalPlaces: 2];
							}
							else
							{
								productTax = decimal.Round((productTax / 100) * salesLine.ExtPrice, 2);
							}

							salesLine.SalesTax = productTax;
						}
					}
					/*
					if ([DataUtils canCalculateTaxForCustomer:[orderRow valueForKey:@"customer_no"]]) {
						totalTax = @0;
            
						//Se movio antes del for para que no se verifique por cada producto de la orden.
						BOOL addIvuPercentageValue = NO;
						DADataTableRow *custRow = [[[[DataAccess sharedInstance] customersTable] rowsFilteredUsingPredicate:[NSPredicate predicateWithFormat:@"customer_no = %@", [orderRow valueForKey:@"customer_no"]]] lastObject];
            
						if(![[NSDate date] isEarlierThanDate:[custRow valueForKey:@"certMunicipalExpiration"]]) {
							addIvuPercentageValue = YES;
						}
            
						for (DADataTableRow *orow in rows)
						{
							NSPredicate *pred = [NSPredicate predicateWithFormat:@"ProductCode == %@", [orow valueForKey:@"product_no"]];
							DADataTableRow *prod = [[[[DataAccess sharedInstance] productsTable] rowsFilteredUsingPredicate:pred] lastObject];
							productTax = [prod valueForKey:@"SalesTax"];
                
							if (addIvuPercentageValue) {
								productTax = [productTax addNumber:[[GlobalSettings settings] numberValueForKey:IVU_ADDITIONAL_PERCENTAGE_VALUE]];
							}
                
							//Round product tax now to avoid precision discrepancies when accumulating the tax for the total.
							if (taxRoundingOption == 1)
								//Use ceiling procedure.
								productTax = [[[productTax divideByInt:100] multiplyByNumber:(NSNumber*)[orow valueForKey:@"ext_price"]] ceilingToDecimalPlaces:2];
								//productTax = [[[productTax divideByInt:100] multiplyByNumber:[(NSNumber*)[orow valueForKey:@"ext_price"] substractNumber:[(NSNumber*)[orow valueForKey:@"Discount"] multiplyByNumber:[orow valueForKey:@"qty"]]]] ceilingToDecimalPlaces:2];
							else
								//Use rounding procedure.
								productTax = [[[productTax divideByInt:100] multiplyByNumber:(NSNumber*)[orow valueForKey:@"ext_price"]] roundToDecimalPlaces:2];
								//productTax = [[[productTax divideByInt:100] multiplyByNumber:[(NSNumber*)[orow valueForKey:@"ext_price"] substractNumber:[(NSNumber*)[orow valueForKey:@"Discount"] multiplyByNumber:[orow valueForKey:@"qty"]]]] roundToDecimalPlaces:2];
                
							[orow setValue:productTax forKey:@"salesTax"];
							totalTax = [totalTax addNumber:productTax];
						}
					}
					[orderRow setValue:totalTax forKey:@"totalSalesTax"];
					*/
				}
				else if (paramOrdSalesTax == 5 /*&& [[orderRow valueForKey:@"TransactionType"] intValue] == 0) || ([[GlobalSettings settings] isValueEnabled:CALCULATE_TAX_ON_CREDIT] && [[orderRow valueForKey:@"TransactionType"] intValue] == 1)*/)
				{
					/*
					if ([DataUtils canCalculateTaxForCustomer:[orderRow valueForKey:@"customer_no"]]) {
						totalTax = @0;
            
						//Se movio antes del for para que no se verifique por cada producto de la orden.
						BOOL addIvuPercentageValue = NO;
						DADataTableRow *custRow = [[[[DataAccess sharedInstance] customersTable] rowsFilteredUsingPredicate:[NSPredicate predicateWithFormat:@"customer_no = %@", [orderRow valueForKey:@"customer_no"]]] lastObject];
            
						if([[orderRow valueForKey:@"customer_no"] isEqualToString:@"GENERIC"]){
							NSString *cert=[orderRow valueForKey:@"newcertmunicipal"] ;
							cert= [cert stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
							if([cert length] == 0){
								addIvuPercentageValue = YES;
							} else {
								addIvuPercentageValue = NO;
							}
						}else{
							if([[custRow valueForKey:@"certmunicipal"] length] == 0) {
								addIvuPercentageValue = YES;
							} else if ([[orderRow valueForKey:@"TransactionType"] intValue] == 0 && [[orderRow valueForKey:@"newcertmunicipal"] length] == 0) {
								addIvuPercentageValue = YES;
							} else {
								addIvuPercentageValue = NO;
							}
						}
            
						for (DADataTableRow *orow in rows)
						{
							NSPredicate *pred = [NSPredicate predicateWithFormat:@"ProductCode == %@", [orow valueForKey:@"product_no"]];
							DADataTableRow *prod = [[[[DataAccess sharedInstance] productsTable] rowsFilteredUsingPredicate:pred] lastObject];
							productTax = [prod valueForKey:@"SalesTax"];
                
							if ([productTax floatValue] > 0 && addIvuPercentageValue) {
								productTax = [productTax addNumber:[[GlobalSettings settings] numberValueForKey:IVU_ADDITIONAL_PERCENTAGE_VALUE]];
							}
                
							//Round product tax now to avoid precision discrepancies when accumulating the tax for the total.
							if (taxRoundingOption == 1)
								//Use ceiling procedure.
								productTax = [[[productTax divideByInt:100] multiplyByNumber:[(NSNumber*)[orow valueForKey:@"ext_price"] substractNumber:[(NSNumber*)[orow valueForKey:@"Discount"] multiplyByNumber:[orow valueForKey:@"qty"]]]] ceilingToDecimalPlaces:2];
							else
								//Use rounding procedure.
								productTax = [[[productTax divideByInt:100] multiplyByNumber:[(NSNumber*)[orow valueForKey:@"ext_price"] substractNumber:[(NSNumber*)[orow valueForKey:@"Discount"] multiplyByNumber:[orow valueForKey:@"qty"]]]] roundToDecimalPlaces:2];
                
							[orow setValue:productTax forKey:@"salesTax"];
							totalTax = [totalTax addNumber:productTax];
						}
					}
					[orderRow setValue:totalTax forKey:@"totalSalesTax"];
					*/
				}
				else if (paramOrdSalesTax == 6 /*&& [[orderRow valueForKey:@"TransactionType"] intValue] == 0*/)
				{
					/*
					// Lógica para cálculo de tax para Cruzval.
					NSNumber *ivuMunicipal = [[GlobalSettings settings] numberValueForKey:IVU_MUNICIPAL_TAX];
					NSNumber *ivuEstatal = [[GlobalSettings settings] numberValueForKey:IVU_ESTATAL_TAX];
					DADataTableRow *custRow = [[DataAccess sharedInstance] customerRowWithRefRow:orderRow];
					totalTax = @0;
        
					if (custRow) {
						BOOL addCityTax = [[custRow valueForKey:@"certmunicipal"] length] == 0;
						BOOL customerIsTaxExempt = [[custRow valueForKey:@"usesalestax"] intValue] == 0;
						NSNumber *productTax;
            
						for (DADataTableRow *orow in rows) {
							productTax = [NSNumber numberWithInt:0];
							NSPredicate *pred = [NSPredicate predicateWithFormat:@"ProductCode == %@", [orow valueForKey:@"product_no"]];
							DADataTableRow *prod = [[[[DataAccess sharedInstance] productsTable] rowsFilteredUsingPredicate:pred] lastObject];
                
							if (!customerIsTaxExempt) {
								if ([[prod valueForKey:@"SalesTax"] floatValue] > 0) {
									productTax = [productTax addNumber:ivuEstatal];
                        
									if (addCityTax) {
										productTax = [productTax addNumber:ivuMunicipal];
									}
								}
                    
								//Round product tax now to avoid precision discrepancies when accumulating the tax for the total.
								if (taxRoundingOption == 1)
									//Use ceiling procedure.
									productTax = [[productTax multiplyByNumber:[(NSNumber*)[orow valueForKey:@"ext_price"] substractNumber:[(NSNumber*)[orow valueForKey:@"Discount"] multiplyByNumber:[orow valueForKey:@"qty"]]]] ceilingToDecimalPlaces:2];
								else
									//Use rounding procedure.
									//productTax = [[productTax multiplyByNumber:[(NSNumber*)[orow valueForKey:@"ext_price"] substractNumber:[(NSNumber*)[orow valueForKey:@"Discount"] multiplyByNumber:[orow valueForKey:@"qty"]]]] roundToDecimalPlaces:2];
									productTax = [[productTax multiplyByNumber:[orow valueForKey:@"ext_price"]] ceilingToDecimalPlaces:2];

							}
                
							[orow setValue:productTax forKey:@"salesTax"];
							totalTax = [totalTax addNumber:productTax];
						}
            
						[orderRow setValue:totalTax forKey:@"totalSalesTax"];
					}
					*/
				}
				/*
				else if([[GlobalSettings settings] isValueEnabled:DS_SALES_TAX] && [[orderRow valueForKey:@"TransactionType"] intValue] == 3) {
					if ([DataUtils canCalculateTaxForCustomer:[orderRow valueForKey:@"customer_no"]]){
						//Si el parámetro es igual a 3 ya el cálculo del tax se realizó en el método netPriceForOrder.
						if ([[GlobalSettings settings] intValueForKey:ORD_SALES_TAX] != 3) {
							totalTax = @0;
                
							//Se movio antes del for para que no se verifique por cada producto de la orden.
							BOOL noTax = NO;
							DADataTableRow *custRow = [[[[DataAccess sharedInstance] customersTable] rowsFilteredUsingPredicate:[NSPredicate predicateWithFormat:@"customer_no = %@", [orderRow valueForKey:@"customer_no"]]] lastObject];
							if([[NSDate date] isEarlierThanDate:[custRow valueForKey:@"certMunicipalExpiration"]] && [[GlobalSettings settings] isValueEnabled:FAC_USE_IVU_WITH_CERT_DATES]){
								noTax = YES;
							}
                
							if ([[GlobalSettings settings] isValueEnabled:FAC_USE_IVU_WITH_CERT_NUMBER]) {
								if([[orderRow valueForKey:@"customer_no"] isEqualToString:@"GENERIC"]){
									NSString *cert=[orderRow valueForKey:@"newcertmunicipal"] ;
									cert= [cert stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
									if([cert length] == 0){
										noTax = NO;
									} else {
										noTax = YES;
									}
								}else{
									if([[custRow valueForKey:@"certmunicipal"] length] == 0) {
										noTax = NO;
									} if ([[orderRow valueForKey:@"newcertmunicipal"] length] == 0) {
										noTax = NO;
									} else {
										noTax = YES;
									}
								}
							}

							for (DADataTableRow *orow in rows)
							{
								NSPredicate *pred = [NSPredicate predicateWithFormat:@"ProductCode == %@", [orow valueForKey:@"product_no"]];
								DADataTableRow *prod = [[[[DataAccess sharedInstance] productsTable] rowsFilteredUsingPredicate:pred] lastObject];
								productTax = @0;
                    
								if ([(NSNumber*)[prod valueForKey:@"SalesTax"] isGreaterThanFloat:0.0f]){
									if ([[GlobalSettings settings] isValueEnabled:PRINT_TOTAL_DISCOUNT]) {
										productTax = [[(NSNumber*)[prod valueForKey:@"SalesTax"] divideByInt:100] multiplyByNumber:[orow valueForKey:@"ext_price"]];
									} else {
										//Se realizó ajuste para considerar el descuento al momento de calcular el tax.
										//productTax = ([[prod valueForKey:@"SalesTax"] floatValue] / 100) * ([[orow valueForKey:@"ext_price"] floatValue] - ([[orow valueForKey:@"Discount"] floatValue] * [[orow valueForKey:@"qty"] floatValue]));
                            
										productTax = [[(NSNumber*)[prod valueForKey:@"SalesTax"] divideByInt:100] multiplyByNumber:[(NSNumber*)[orow valueForKey:@"ext_price"] substractNumber:[(NSNumber*)[orow valueForKey:@"Discount"] multiplyByNumber:[orow valueForKey:@"qty"]]]];
                            
										if (taxRoundingOption == 0) {
											//Standard rounding.
											productTax = [productTax roundToDecimalPlaces:2];
										} else if (taxRoundingOption == 1) {
											//Ceiling rounding.
											productTax = [productTax ceilingToDecimalPlaces:2];
										}
									}
									//Se realizó ajuste para considerar el descuento al momento de calcular el tax.
			//                        productTax = ([[prod valueForKey:@"SalesTax"] floatValue] / 100) * ([[orow valueForKey:@"ext_price"] floatValue] - ([[orow valueForKey:@"Discount"] floatValue] * [[orow valueForKey:@"qty"] floatValue]));
								}

								if(noTax){
									productTax = 0;
								}

								//Se eliminó el IF para que se le asigne TAX 0 a los productos que no llevan tax.
			//                if(productTax > 0){
									[orow setValue:productTax forKey:@"salesTax"];
									totalTax = [totalTax addNumber:productTax];
			//                }
							}
						}
					}

				   [orderRow setValue:totalTax forKey:@"totalSalesTax"];

				}
				// Si el valor de DS_SALES_TAX es igual a 2 y el tipo de transaccion es Factura (TransactionType = 3) se calcula el tax de esta forma
				// si el certificado es vigente se le cobra el salesTax que aparece en la tabla de product y si no esta vigente se le cobra el valor
				// que esta en la tabla de product + 1%
				else if ([[GlobalSettings settings] intValueForKey:DS_SALES_TAX] == 2 && [[orderRow valueForKey:@"TransactionType"] intValue] == 3) {
					if ([DataUtils canCalculateTaxForCustomer:[orderRow valueForKey:@"customer_no"]]) {
						totalTax = @0;
            
						//Se movio antes del for para que no se verifique por cada producto de la orden.
						BOOL addIvuPercentageValue = NO;
						DADataTableRow *custRow = [[[[DataAccess sharedInstance] customersTable] rowsFilteredUsingPredicate:[NSPredicate predicateWithFormat:@"customer_no = %@", [orderRow valueForKey:@"customer_no"]]] lastObject];
            
						if(![[NSDate date] isEarlierThanDate:[custRow valueForKey:@"certMunicipalExpiration"]]) {
							addIvuPercentageValue = YES;
						}
            
						for (DADataTableRow *orow in rows)
						{
							NSPredicate *pred = [NSPredicate predicateWithFormat:@"ProductCode == %@", [orow valueForKey:@"product_no"]];
							DADataTableRow *prod = [[[[DataAccess sharedInstance] productsTable] rowsFilteredUsingPredicate:pred] lastObject];
							productTax = [prod valueForKey:@"SalesTax"];
                
							if (addIvuPercentageValue) {
								productTax = [productTax addNumber:[[GlobalSettings settings] numberValueForKey:IVU_ADDITIONAL_PERCENTAGE_VALUE]];
							}
                
							//Round product tax now to avoid precision discrepancies when accumulating the tax for the total.
							if (taxRoundingOption == 1)
								//Use ceiling procedure.
								productTax = [[[productTax divideByInt:100] multiplyByNumber:(NSNumber*)[orow valueForKey:@"ext_price"]] ceilingToDecimalPlaces:2];
								//productTax = [[[productTax divideByInt:100] multiplyByNumber:[(NSNumber*)[orow valueForKey:@"ext_price"] substractNumber:[(NSNumber*)[orow valueForKey:@"Discount"] multiplyByNumber:[orow valueForKey:@"qty"]]]] ceilingToDecimalPlaces:2];
							else
								//Use rounding procedure.
								productTax = [[[productTax divideByInt:100] multiplyByNumber:(NSNumber*)[orow valueForKey:@"ext_price"]] roundToDecimalPlaces:2];
								//productTax = [[[productTax divideByInt:100] multiplyByNumber:[(NSNumber*)[orow valueForKey:@"ext_price"] substractNumber:[(NSNumber*)[orow valueForKey:@"Discount"] multiplyByNumber:[orow valueForKey:@"qty"]]]] roundToDecimalPlaces:2];
                
							[orow setValue:productTax forKey:@"salesTax"];
							totalTax = [totalTax addNumber:productTax];
						}
					}
					[orderRow setValue:totalTax forKey:@"totalSalesTax"];

				}
				// Si el valor de DS_SALES_TAX es igual a 3 y el tipo de transaccion es Factura (TransactionType = 3) se calcula el tax de esta forma
				// si el certificado es vigente usando el campo certmunicipal se le cobra el salesTax que aparece en la tabla de product y si no esta vigente se le cobra el valor que esta en la tabla de product + 1%
				else if ([[GlobalSettings settings] intValueForKey:DS_SALES_TAX] == 3 && [[orderRow valueForKey:@"TransactionType"] intValue] == 3) {
					if ([DataUtils canCalculateTaxForCustomer:[orderRow valueForKey:@"customer_no"]]) {
						totalTax = @0;
            
						//Se movio antes del for para que no se verifique por cada producto de la orden.
						BOOL addIvuPercentageValue = NO;
						DADataTableRow *custRow = [[[[DataAccess sharedInstance] customersTable] rowsFilteredUsingPredicate:[NSPredicate predicateWithFormat:@"customer_no = %@", [orderRow valueForKey:@"customer_no"]]] lastObject];
        
						if([[orderRow valueForKey:@"customer_no"] isEqualToString:@"GENERIC"]){
							NSString *cert=[orderRow valueForKey:@"newcertmunicipal"] ;
							cert= [cert stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
							if([cert length] == 0){
								addIvuPercentageValue = YES;
							} else {
								addIvuPercentageValue = NO;
							}
						}else{
							if([[custRow valueForKey:@"certmunicipal"] length] == 0) {
								addIvuPercentageValue = YES;
							} if ([[orderRow valueForKey:@"newcertmunicipal"] length] == 0) {
								addIvuPercentageValue = YES;
							} else {
								addIvuPercentageValue = NO;
							}
						}
            
						for (DADataTableRow *orow in rows)
						{
							if ([[GlobalSettings settings] isValueEnabled:INV_PRINT_MUNICIPAL_ESTATAL_TAX withDefaultValue:@"0"]) {
								NSPredicate *pred = [NSPredicate predicateWithFormat:@"ProductCode == %@", [orow valueForKey:@"product_no"]];
								DADataTableRow *prod = [[[[DataAccess sharedInstance] productsTable] rowsFilteredUsingPredicate:pred] lastObject];
								productTax = [prod valueForKey:@"SalesTax"];
								NSNumber *stateTax = [[prod valueForKey:@"SalesTax"] divideByInt:100];
								NSNumber *cityTax = @0;
                    
								if ([stateTax isGreaterThanNumber:@0]) {
									if (addIvuPercentageValue) {
										cityTax = [[[GlobalSettings settings] numberValueForKey:IVU_ADDITIONAL_PERCENTAGE_VALUE] divideByInt:100];
									}
                        
									//Round product tax now to avoid precision discrepancies when accumulating the tax for the total.
									if (taxRoundingOption == 1) {
										//Use ceiling procedure.
										stateTax = [[stateTax multiplyByNumber:[(NSNumber*)[orow valueForKey:@"ext_price"] substractNumber:[(NSNumber*)[orow valueForKey:@"Discount"] multiplyByNumber:[orow valueForKey:@"qty"]]]] ceilingToDecimalPlaces:4];
										cityTax = [[cityTax multiplyByNumber:[(NSNumber*)[orow valueForKey:@"ext_price"] substractNumber:[(NSNumber*)[orow valueForKey:@"Discount"] multiplyByNumber:[orow valueForKey:@"qty"]]]] ceilingToDecimalPlaces:4];
									}
									else {
										//Use rounding procedure.
										stateTax = [[stateTax multiplyByNumber:[(NSNumber*)[orow valueForKey:@"ext_price"] substractNumber:[(NSNumber*)[orow valueForKey:@"Discount"] multiplyByNumber:[orow valueForKey:@"qty"]]]] roundToDecimalPlaces:4];
                            
										cityTax = [[cityTax multiplyByNumber:[(NSNumber*)[orow valueForKey:@"ext_price"] substractNumber:[(NSNumber*)[orow valueForKey:@"Discount"] multiplyByNumber:[orow valueForKey:@"qty"]]]] roundToDecimalPlaces:4];
									}
								}
                    
								[orow setValue:[[stateTax addNumber:cityTax] roundToDecimalPlaces:2] forKey:@"salesTax"];
								totalStateTax = [[totalStateTax addNumber:stateTax] roundToDecimalPlaces:4];
								totalCityTax = [[totalCityTax addNumber:cityTax] roundToDecimalPlaces:4];
							} else {
								NSPredicate *pred = [NSPredicate predicateWithFormat:@"ProductCode == %@", [orow valueForKey:@"product_no"]];
								DADataTableRow *prod = [[[[DataAccess sharedInstance] productsTable] rowsFilteredUsingPredicate:pred] lastObject];
								productTax = [prod valueForKey:@"SalesTax"];
                    
								if ([productTax floatValue] > 0 && addIvuPercentageValue) {
									productTax = [productTax addNumber:[[GlobalSettings settings] numberValueForKey:IVU_ADDITIONAL_PERCENTAGE_VALUE]];
								}
                    
								//Round product tax now to avoid precision discrepancies when accumulating the tax for the total.
								if (taxRoundingOption == 1)
									//Use ceiling procedure.
									productTax = [[[productTax divideByInt:100] multiplyByNumber:[(NSNumber*)[orow valueForKey:@"ext_price"] substractNumber:[(NSNumber*)[orow valueForKey:@"Discount"] multiplyByNumber:[orow valueForKey:@"qty"]]]] ceilingToDecimalPlaces:2];
								else
									//Use rounding procedure.
									productTax = [[[productTax divideByInt:100] multiplyByNumber:[(NSNumber*)[orow valueForKey:@"ext_price"] substractNumber:[(NSNumber*)[orow valueForKey:@"Discount"] multiplyByNumber:[orow valueForKey:@"qty"]]]] roundToDecimalPlaces:2];
                    
								[orow setValue:productTax forKey:@"salesTax"];
								totalTax = [totalTax addNumber:productTax];
							}
						}
					}
        
					if ([[GlobalSettings settings] isValueEnabled:INV_PRINT_MUNICIPAL_ESTATAL_TAX withDefaultValue:@"0"]) {
						[orderRow setValue:[totalStateTax roundToDecimalPlaces:2] forKey:@"stateTax"];
						[orderRow setValue:[totalCityTax roundToDecimalPlaces:2] forKey:@"cityTax"];
						[orderRow setValue:[[totalStateTax addNumber:totalCityTax] roundToDecimalPlaces:2] forKey:@"totalSalesTax"];
					} else {
						[orderRow setValue:totalTax forKey:@"totalSalesTax"];
					}
        
				}
				else if([[GlobalSettings settings] isValueEnabled:CALCULATE_TAX_ON_CREDIT] && [[orderRow valueForKey:@"TransactionType"] intValue] == 1) {
					if ([DataUtils canCalculateTaxForCustomer:[orderRow valueForKey:@"customer_no"]]){
						if ([[GlobalSettings settings] intValueForKey:ORD_SALES_TAX] != 3) {
							totalTax = @0;
							for (DADataTableRow *orow in rows)
							{
								NSPredicate *pred = [NSPredicate predicateWithFormat:@"ProductCode == %@", [orow valueForKey:@"product_no"]];
								DADataTableRow *prod = [[[[DataAccess sharedInstance] productsTable] rowsFilteredUsingPredicate:pred] lastObject];
								productTax = @0;
								if ([(NSNumber*)[prod valueForKey:@"SalesTax"] isGreaterThanFloat:0.0f]){
									//Se realizó ajuste para considerar el descuento al momento de calcular el tax.
									productTax = [[(NSNumber*)[prod valueForKey:@"SalesTax"] divideByInt:100] multiplyByNumber:[orow valueForKey:@"ext_price"]];
								}
                    
								if([productTax isGreaterThanInt:0]){
									[orow setValue:productTax forKey:@"salesTax"];
									totalTax = [totalTax addNumber:productTax];
								}
							}
						}
					}
        
					[orderRow setValue:totalTax forKey:@"totalSalesTax"];
        
				}
				*/
				else
				{
					//[orderRow setValue:totalTax forKey:@"totalSalesTax"];
					//order.TotalSalesTax = totalTax;
				}

				//order.Amount = subTotal;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		bool BitwiseValueHasFlagEnabled(int value, int flag)
		{
			return (value & flag) == flag;
		}

		void ClearData(SalesOrder order)
		{
			try
			{
				var objSpace = View.ObjectSpace;
				/*
				//Clear current price conditions and delete them from the table.
				for (j = 0; j < [mOrderPromotions count]; j++) {
					if ([[[DataAccess sharedInstance].orderPromotionsTable  rows] containsObject:(DADataTableRow*)[mOrderPromotions objectAtIndex:j]]) {
						[[DataAccess sharedInstance].orderPromotionsTable removeRow:(DADataTableRow*)[mOrderPromotions objectAtIndex:j]];
					}
				}
            
				[mOrderPromotions removeAllObjects];
				[mDiscountsForSection removeAllObjects];
				*/

				foreach (var promo in objSpace.GetObjectsQuery<OrderPromo>().Where(r => r.Order == order))
				{
					objSpace.Delete(promo);
					promo.Save();
				}
				//foreach (var orderPromo in order.OrderPromoes)
				//{
				//	objSpace.Delete(orderPromo);
				//	orderPromo.Save();
				//}
				//#################################################################

				//#################################################################
				/*
				//Clear current header messages and delete them from the table.
				for (j = 0; j < mHeaderMessages.count; j++) {
					if ([[[DataAccess sharedInstance].orderMessageTable  rows]containsObject:(DADataTableRow*)[mHeaderMessages objectAtIndex:j]]) {
						[[DataAccess sharedInstance].orderMessageTable removeRow:(DADataTableRow*)[mHeaderMessages objectAtIndex:j]];
					}
				}

				[mHeaderMessages removeAllObjects];

				//Clear current detail messages and delete them from the table.
				for (j = 0; j < mDetailMessages.count; j++) {
					if ([[[DataAccess sharedInstance].orderMessageTable  rows] containsObject:(DADataTableRow*)[mDetailMessages objectAtIndex:j]]) {
						[[DataAccess sharedInstance].orderMessageTable removeRow:(DADataTableRow*)[mDetailMessages objectAtIndex:j]];
					}
				}

				[mDetailMessages removeAllObjects];
				*/

				foreach (var orderMessage in objSpace.GetObjectsQuery<OrderMessages>().Where(r => r.Order == order))
				{
					objSpace.Delete(orderMessage);
					orderMessage.Save();
				}
				//foreach (var orderMessage in order.OrderMessagesCollection)
				//{
				//	objSpace.Delete(orderMessage);
				//	orderMessage.Save();
				//}
				//#################################################################

				//#################################################################
				/*
				//Clear current free cases and delete them from the table.
				if ([[orderRow valueForKey:@"transtype"] intValue] == 19)
					table = [DataAccess sharedInstance].recargaDetailTable;
				else
					table = [DataAccess sharedInstance].orderDetailTable;


				while (mFreeCasesLines2.count > 0) {
					freeCaseEntry = [mFreeCasesLines2 objectAtIndex:0];

					if ([freeCaseEntry class] == [DADataTableRow class])
						//The entry is a free case row.
						[table removeRow:(DADataTableRow*)freeCaseEntry];

					else {
						//The entry has an array of free case rows.
						for (j = 0; j < ((NSArray*)freeCaseEntry).count; j++)
							[table removeRow:(DADataTableRow*)[(NSArray*)freeCaseEntry objectAtIndex:j]];
					}

					[mFreeCasesLines2 removeObjectAtIndex:0];
				}
				*/

				foreach (var salesLine in objSpace.GetObjectsQuery<SalesLine>().Where(r => r.Order == order))
				{
					if (salesLine.FreeGoodsVal == 1 && salesLine.SerialNo != string.Empty && salesLine.ModelNo != string.Empty)
					{
						objSpace.Delete(salesLine);
					}
					else
					{
						salesLine.HasErrorMessages = false;
						salesLine.HasMessages = false;

						if (paramEnableProductPercentDiscount == 0)
						{
							salesLine.Discount = 0;
						}
					}

					salesLine.Save();
				}

				//#################################################################

				//#################################################################
				/*
				//Clear messages flags at header level.
				pricingOptionNumber = [NSNumber numberWithInt:0];
				[orderRow setValue:pricingOptionNumber forKey:@"hasErrorMessages"];
				[orderRow setValue:pricingOptionNumber forKey:@"hasMessages"];
				*/

				/*
				//Clear messages flags and discount at detail level.
				for (j = 0; j < orderLines.count; j++) {
					orderLineRow = (DADataTableRow*)[orderLines objectAtIndex:j];

					[orderLineRow setValue:pricingOptionNumber forKey:@"hasErrorMessages"];
					[orderLineRow setValue:pricingOptionNumber forKey:@"hasMessages"];

					if (![[GlobalSettings settings] isValueEnabled:ENABLE_PRODUCT_PERCENT_DISCOUNT]) {
						[orderLineRow setValue:pricingOptionNumber forKey:@"Discount"];
					}
				}
				*/
				order.HasErrorMessages = false;
				order.HasMessages = false;

				//foreach (var line in order.SalesLines)
				//{
				//	line.HasErrorMessages = false;
				//	line.HasMessages = false;
				//	line.Save();
				//}

				order.Save();

				SalesOrderHelper.PartialSave = true;
				objSpace.CommitChanges();
				SalesOrderHelper.PartialSave = false;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		void HandlePricingOption(PricingOption pricingOption, SalesOrder order)
		{
			try
			{
				var objSpace = View.ObjectSpace;
				//IObjectSpace objSpace = Application.CreateNestedObjectSpace(View.ObjectSpace);

				string promoID, promoType, variableDiscount, freeGoodsProduct, freeGoodsUM;
				int prcOption, prcOptionType, prcOptionAction;

				promoID = Guid.NewGuid().ToString();
				promoType = string.Empty;
				variableDiscount = string.Empty;
				freeGoodsProduct = string.Empty;
				freeGoodsUM = string.Empty;
				prcOptionAction = (int)PricingOption.PricingOptionActions.None;

				SalesLine salesLine = null;

				foreach (var item in order.SalesLines)
				{
					if (item.Product.ProductID == pricingOption.ProductNumber)
					{
						salesLine = objSpace.GetObjectsQuery<SalesLine>().Where(r => r.ID == item.ID).FirstOrDefault();
						break;
					}
				}

				if (salesLine != null)
				{
					if (BitwiseValueHasFlagEnabled(pricingOption.PricingOptionType, (int)PricingOption.PricingOptionTypes.NetPrice))
					{
						promoType = "NP";

						if (paramBfhChangePrice == 0)
						{
							salesLine.UnitPrice = pricingOption.Amount;
						}
					}
					else if (BitwiseValueHasFlagEnabled(pricingOption.PricingOptionType, (int)PricingOption.PricingOptionTypes.Adjustment))
					{
						promoType = "AD";
					}
					else if (BitwiseValueHasFlagEnabled(pricingOption.PricingOptionType, (int)PricingOption.PricingOptionTypes.Discount))
					{
						promoType = "DS";

						if (BitwiseValueHasFlagEnabled(pricingOption.Options, (int)PricingOption.PricingOptions.AutoApply))
						{
							salesLine.Discount += pricingOption.Amount;
							prcOptionAction = (int)PricingOption.PricingOptionActions.Selected;
						}

						//if (enableDiscountUserDecision && BitwiseValueHasFlagEnabled(pricingOption.Options, (int)PricingOption.PricingOptions.UserDecision))
						//{
						//	//Add promo to Discounts List
						//}

						if (paramBfhChangePrice == 1)
						{
							//if (BitwiseValueHasFlagEnabled(pricingOption.Options, (int)PricingOption.PricingOptions.UserDecision))
							//{
							//	//Add promo to Discounts List
							//}

							if (BitwiseValueHasFlagEnabled(pricingOption.Options, (int)PricingOption.PricingOptions.UserCanChange))
							{
								variableDiscount = "V";

								//if (!BitwiseValueHasFlagEnabled(pricingOption.Options, (int)PricingOption.PricingOptions.UserDecision))
								//{
								//	//Add promo to Discounts List
								//}
							}
						}
					}
					else if (BitwiseValueHasFlagEnabled(pricingOption.PricingOptionType, (int)PricingOption.PricingOptionTypes.FreeCases))
					{
						freeGoodsProduct = pricingOption.FreeGoodProductNumber;
						freeGoodsUM = pricingOption.FreeGoodUnitOfMeasure;
						//IObjectSpace objSpace = Application.CreateObjectSpace(typeof(SalesLine));

						SalesLine freeCaseLine = objSpace.CreateObject<SalesLine>();
						freeCaseLine.Order = objSpace.GetObject<SalesOrder>(order);
						Product product = objSpace.GetObjectsQuery<Product>().Where(r => r.ProductID == pricingOption.FreeGoodProductNumber).FirstOrDefault();
						freeCaseLine.Product = objSpace.GetObject<Product>(product);
						freeCaseLine.UnitPrice = 0;
						freeCaseLine.ExtPrice = 0;
						freeCaseLine.SalesTax = 0;
						freeCaseLine.UnitOfMeasure = pricingOption.FreeGoodUnitOfMeasure;
						freeCaseLine.Discount = 0;
						freeCaseLine.FreeGoodsVal = (short)pricingOption.Options;
						freeCaseLine.ModelNo = promoID;
						freeCaseLine.SerialNo = pricingOption.ConditionType;

						if (pricingOption.Options == (int)PricingOption.PricingOptions.None || BitwiseValueHasFlagEnabled(pricingOption.Options, (int)PricingOption.PricingOptions.AutoApply))
						{
							freeCaseLine.FreeGoods = 1;
							prcOptionAction = (int)PricingOption.PricingOptionActions.Selected;
						}
						else
						{
							freeCaseLine.FreeGoods = 0;
						}

						if (BitwiseValueHasFlagEnabled(pricingOption.PricingOptionType, (int)PricingOption.PricingOptionTypes.Assortment))
						{
							promoType = "FA";

							/*
							NSInteger index = 0;
							NSString *key = @"";

							index = 0;
							for (int x = 0; x < mFreeCasesIndex.count; x++) {
								key = [mFreeCasesIndex objectAtIndex:x];
								if ([key isEqualToString:[pricingOption objectForKey:@"ProductNumber"]]) {
									index = x;
									break;
								}
							}
							*/

							freeCaseLine.Qty = 0;

							/*
							if (index == 0) {
								[mFreeCasesLines2 addObject:[NSMutableArray arrayWithObject:newFreeCaseLine]];
								[mFreeCasesIndex addObject:[pricingOption objectForKey:@"ProductNumber"]];
							} else {
								[[mFreeCasesLines2 objectAtIndex:index] addObjectsFromArray:[NSMutableArray arrayWithObject:newFreeCaseLine]];
							}
							*/
						}
						else
						{
							promoType = "FC";
							freeCaseLine.Qty = (long)pricingOption.Amount;

							/*
							//Save the new free case detail.
							[mFreeCasesLines2 addObject:newFreeCaseLine];
							[mFreeCasesIndex addObject:[pricingOption objectForKey:@"ProductNumber"]]; 
							*/
						}

						//freeCaseObjSpace.CommitChanges();
					}
					else if (BitwiseValueHasFlagEnabled(pricingOption.PricingOptionType, (int)PricingOption.PricingOptionTypes.BasePrice))
					{
						promoType = "BP";

						if (paramBfhChangePrice == 1)
						{
							salesLine.UnitPrice = pricingOption.Amount;
						}
					}
					else if (BitwiseValueHasFlagEnabled(pricingOption.PricingOptionType, (int)PricingOption.PricingOptionTypes.Informative))
					{
						promoType = "IN";
					}

					salesLine.Save();
				}

				if (promoType != string.Empty)
				{
					//IObjectSpace promoObjSpace = Application.CreateObjectSpace(typeof(OrderPromo));
					OrderPromo orderPromo = objSpace.CreateObject<OrderPromo>();
					//SalesOrder order = objSpace.GetObjectsQuery<SalesOrder>().Where(r => r.ID == order.ID).FirstOrDefault();

					orderPromo.PromoID = promoID;
					orderPromo.CompID = order.Route.CompID;
					orderPromo.Route = objSpace.GetObject<Route>(order.Route);
					orderPromo.Order = order;
					orderPromo.PromoCode = pricingOption.ConditionType;
					orderPromo.Product = objSpace.GetObjectsQuery<Product>().Where(r => r.ProductID == pricingOption.ProductNumber).FirstOrDefault();
					orderPromo.UM = pricingOption.UnitOfMeasure;
					orderPromo.Amount = pricingOption.Amount;
					orderPromo.OriginalAmount = pricingOption.Amount;
					orderPromo.OriginalPromoCode = pricingOption.ConditionNumber;
					orderPromo.Sequence = pricingOption.Sequence;
					orderPromo.Description = pricingOption.Description;
					orderPromo.PromoType = promoType;
					orderPromo.PricingOptions = (int)pricingOption.Options;

					if (paramBfhChangePrice == 1 && pricingOption.Description != "NET PRICE")
					{
						orderPromo.OriginalAssortmentPromotionCode = pricingOption.BasisCode != null ? pricingOption.BasisCode : string.Empty;
						orderPromo.OriginalAssortmentCode = pricingOption.PromoCode != null ? pricingOption.PromoCode : string.Empty;
						orderPromo.ContractNumber = pricingOption.Discretional != null ? pricingOption.Discretional : string.Empty;
						orderPromo.DiscountType = pricingOption.FixedVariable != null ? pricingOption.FixedVariable : string.Empty;
					}

					orderPromo.PricingOptionActions = prcOptionAction;

					if (variableDiscount != string.Empty)
					{
						orderPromo.DiscountType = variableDiscount;
					}

					orderPromo.Save();
					SalesOrderHelper.PartialSave = true;
					objSpace.CommitChanges();
					SalesOrderHelper.PartialSave = false;
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		void CheckoutDialogController_Accepting(object sender, DialogControllerAcceptingEventArgs e)
		{
			var detailView = ((sender as DialogController).Frame.View as DetailView);
			CollectionDetails collectionDetail = detailView.CurrentObject as CollectionDetails;

			if (collectionDetail != null)
			{
				var paymentValidation = Globals.PaymentValidationMessage(collectionDetail);
				if (paymentValidation.Length > 0)
				{
					e.Cancel = true;
					throw new Exception(paymentValidation);
				}

				//PS_Context.SetCredentials(globalParameters);

				Globals.ProcessPaymentWithDynamicsPayments(detailView, collectionDetail);

				savedCollection = collectionDetail.Collection;
				(sender as DialogController).Accepting -= CheckoutDialogController_Accepting;

				View.ObjectSpace.CommitChanges();
				View.Close();
			}
		}

		void CheckoutDialogController_Cancelling(object sender, EventArgs e)
		{
			var detailView = ((sender as DialogController).Frame.View as DetailView);
			var collection = detailView.ObjectSpace.GetObjectByKey<Collections>((detailView.CurrentObject as CollectionDetails).Collection.ID);

			List<CollectionDetails> colDetails = collection.Details.ToList();
			collection.Details.DeleteObjectOnRemove = true;
			foreach (var item in colDetails)
			{
				collection.Details.Remove(item);
			}

			List<CollectionPayments> colPayments = collection.Payments.ToList();
			collection.Payments.DeleteObjectOnRemove = true;
			foreach (var item in colPayments)
			{
				collection.Payments.Remove(item);
			}

			collection.Delete();

			detailView.ObjectSpace.CommitChanges();

			(sender as DialogController).Cancelling -= CheckoutDialogController_Cancelling;
		}

		void SendOrderToMSF()
		{
			try
			{
				var detailView = View as DetailView;
				var order = (SalesOrder)detailView.CurrentObject;
				SelectedData result;
				var objSpace = (XPObjectSpace)((DetailView)View).ObjectSpace;

				if (order != null && order?.TransStatus == Enums.Trans_Status.ReadyToSend)
				{
					try
					{
						string message;

						if (order.PrePaid)
						{
							message = $"Invoice #{order.ID} created and transfered to MSF database.";

							Int64 collNumber;
							string confirmationNumber = "";
							Int64.TryParse(order.CollectionReference, out collNumber);
							var collectionDetails = objSpace.GetObjectByKey<Collections>(collNumber)?.Details;
							foreach (var item in collectionDetails)
							{
								confirmationNumber += $"{item.ConfirmationNumber} \\n";
							}

							message += $"\\n\\nConfirmation Number: {confirmationNumber}";
						}
						else
						{
							message = $"Order #{order.ID} created and transfered to MSF database.";
						}

						result = objSpace.Session.ExecuteSproc("CopyOrderToMsfDatabase", order.ID);
						if (result.ResultSet.Count() > 0)
						{
							if ((result.ResultSet.ElementAt(0).Rows.First() as SelectStatementResultRow).Values[0].ToString() == "0")
							{
								if (order.PrePaid)
								{
									WebWindow.CurrentRequestWindow.RegisterClientScript("Transaction completed", @"alert('" + message + "');");
								}
								else
								{
									Application.ShowViewStrategy.ShowMessage(message, InformationType.Success, 3000, InformationPosition.Top);
								}
							}
							else
							{
								// An error occurred inside the stored procedure [CopyOrderToMsfDatabase]. Details of the error can be viewed in table [errorlog] using query SELECT * FROM errorlog WHERE number = <OrderNumber>
								throw new Exception();
							}
						}
					}
					catch (Exception)
					{
						Application.ShowViewStrategy.ShowMessage(string.Format("Order #{0} created but could not be transfered to MSF database.", order.ID), InformationType.Warning, 3000, InformationPosition.Top);
					}
					finally
					{
						try
						{
							var viewObjSpace = View.ObjectSpace;
							var CallHelper = viewObjSpace.GetObjectsQuery<CallHelper>().Where(r => r.UserID == SecuritySystem.CurrentUserName).FirstOrDefault();

							if (CallHelper != null)
							{
								var currentCall = CallHelper.CurrentCall;

								if (currentCall != null)
								{
									CxmUser currentUser = viewObjSpace.GetObjectsQuery<CxmUser>().Where(u => u.UserName == SecuritySystem.CurrentUserName).FirstOrDefault();
									if (currentUser != null)
									{
										//Calls call = View.ObjectSpace.GetObject<Calls>(currentCall);
										if (currentCall.Category == Calls.CallCategory.OffCallTransactions)
										{
											currentCall.EndDateTime = DateTime.Now;
											currentUser.NextCallNumber++;
											currentCall.CallNumber = $"{currentUser.UserName}-{currentUser.NextCallNumber.ToString("000000")}";
											currentCall.CustomerID = viewObjSpace.GetObject<Customers>(order.Customer);
											currentCall.Status = Calls.CallStatus.Finished;
											currentCall.Save();
										}

										CallResult callResult = viewObjSpace.GetObjectsQuery<CallResult>().Where(r => r.Call.ID == currentCall.ID).FirstOrDefault();
										if (callResult == null)
										{
											callResult = viewObjSpace.CreateObject<CallResult>();
											callResult.Call = currentCall;
										}
										callResult.NewOrders = true;
										callResult.Other = true;
										callResult.Notes = $"Order {order.ID.ToString()} created.";
										callResult.Save();

										CallHelper.CurrentCall = null;
										CallHelper.Save();

										viewObjSpace.CommitChanges();
									}
								}
							}
						}
						catch (SqlException sqlEx)
						{
							throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
						}
						catch (Exception ex)
						{
							throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
						}
					}

					try
					{
						CxmUser user = detailView.ObjectSpace.GetObjectsQuery<CxmUser>().Where(r => r.UserName == order.Route.RouteNo).FirstOrDefault();

						if (user != null)
						{
							if (user.SendEmail)
							{
								if (order.PrePaid)
								{
									Int64 collectionNumber;
									Int64.TryParse(order.CollectionReference, out collectionNumber);
									var recipient = order.ContactEmail;
									result = objSpace.Session.ExecuteSproc("SendNewInvoiceEmail", order.ID, recipient);
								}
								else
								{
									result = objSpace.Session.ExecuteSproc("SendNewOrderEmail", order.ID);
								}

							}

							if (user.SendSMS)
							{
								var recipient = user.PhoneSMSGateWay;
								result = objSpace.Session.ExecuteSproc("SendNewOrderEmail", order.ID, recipient);

							}

							if (user.SendWhatsapp)
							{

								//Send WhatsApp message
							}
						}
					}
					catch (Exception ex)
					{
						Application.ShowViewStrategy.ShowMessage($"A problem ocurred while trying to send the email confirmation. Message: {ex.Message}", InformationType.Warning, 3000, InformationPosition.Top);
					}
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}
	}
}
