﻿using System;
using System.Linq;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using CXM.Module.BusinessObjects.CXM;
using System.Data.SqlClient;
using DevExpress.ExpressApp.Web.Layout;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo.DB;

namespace CXM.Module.Controllers.Detail_Views
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class B2BCustomerActivityDetailViewController : ViewController
	{
		public B2BCustomerActivityDetailViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();
		}

		private void OnPageControlCreated(object sender, PageControlCreatedEventArgs e)
		{
			try
			{
				if (e.Model.Id == "Details")
				{
					e.PageControl.ClientSideEvents.Init = "function(s,e){s.SetActiveTabIndex(0);}";
					((View as DetailView).LayoutManager as WebLayoutManager).PageControlCreated -= OnPageControlCreated;
				}
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void B2BShowAging_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				Type objectType = typeof(Invoice);
				IObjectSpace invoiceObjectSpace = Application.CreateObjectSpace(objectType);

				var customer = View.ObjectSpace.GetObject<Customers>((Customers)View.CurrentObject);
				DetailView detailView = Application.CreateDetailView(invoiceObjectSpace, "CustomerOpenInvoices_DetailView", false);
				detailView.CurrentObject = invoiceObjectSpace.GetObjectsQuery<Customers>().Where(r => r.CustomerID == customer.CustomerID).FirstOrDefault();
				e.View = detailView;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void B2BShowLastInvoices_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				Type objectType = typeof(Invoice);
				IObjectSpace invoiceObjectSpace = Application.CreateObjectSpace(objectType);

				var customer = View.ObjectSpace.GetObject<Customers>((Customers)View.CurrentObject);
				DetailView detailView = Application.CreateDetailView(invoiceObjectSpace, "CustomerLastInvoices_DetailView", false);
				detailView.CurrentObject = invoiceObjectSpace.GetObjectsQuery<Customers>().Where(r => r.CustomerID == customer.CustomerID).FirstOrDefault();
				e.View = detailView;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void B2BNewOrder_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(SalesOrder));

				SalesOrder newOrder = objectSpace.CreateObject<SalesOrder>();
				newOrder.Customer = objectSpace.GetObject<Customers>((Customers)View.CurrentObject);

				DetailView detailView = Application.CreateDetailView(objectSpace, "SalesOrder_DetailView", true, newOrder);
				detailView.ViewEditMode = ViewEditMode.Edit;
				e.View = detailView;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void B2BNewIssue_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(Issues));

				Customers customer = objectSpace.GetObject<Customers>((Customers)View.CurrentObject);
				Issues newIssue = objectSpace.CreateObject<Issues>();
				newIssue.CustomerID = customer;

				DetailView detailView = Application.CreateDetailView(objectSpace, "New_Issues_DetailView", true, newIssue);
				detailView.ViewEditMode = ViewEditMode.Edit;
				e.View = detailView;

				detailView.Closed += NewIssueDetailViewController_Closed;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		void NewIssueDetailViewController_Closed(object sender, EventArgs e)
		{
			try
			{
				var detailView = (DetailView)sender;

				detailView.Closed -= NewIssueDetailViewController_Closed;

				if (detailView.SelectedObjects.Count == 1)
				{
					var createdIssue = (Issues)detailView.SelectedObjects[0];
					if (createdIssue?.IssueID != 0)
					{
						Application.ShowViewStrategy.ShowMessage(string.Format("Issue #{0} created.", createdIssue.IssueID), InformationType.Success, 3000, InformationPosition.Top);
						View.ObjectSpace.Refresh();
					}
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void B2BShowDueInvoices_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				Type objectType = typeof(Invoice);
				IObjectSpace invoiceObjectSpace = Application.CreateObjectSpace(objectType);

				var customer = (Customers)View.CurrentObject;
				DetailView detailView = Application.CreateDetailView(invoiceObjectSpace, "CustomerDueInvoices_DetailView", false);
				detailView.CurrentObject = invoiceObjectSpace.GetObjectsQuery<Customers>().Where(r => r.CustomerID == customer.CustomerID).FirstOrDefault();
				e.View = detailView;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void B2BNewOrder_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			try
			{
				Customers customer = (Customers)View.CurrentObject;

				if (string.IsNullOrEmpty(customer.CreditHold))
				{
					IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(SalesOrder));

					SalesOrder newOrder = objectSpace.CreateObject<SalesOrder>();
					newOrder.Customer = objectSpace.GetObject<Customers>(customer);

					foreach (var item in objectSpace.GetObjectsQuery<CustomerProducts>().Where(r => r.CustomerID == customer.CustomerID).ToList())
					{
						if (item.ProductID.Active)
						{
							var salesLine = objectSpace.CreateObject<SalesLine>();
							salesLine.Product = item.ProductID;
							salesLine.Order = newOrder;
							salesLine.Qty = item.SuggestedOrderQty;
							salesLine.SuggestedQty = item.SuggestedOrderQty;
							salesLine.UnitOfMeasure = item.ProductID.UnitOfMeasure;

							newOrder.SalesLines.Add(salesLine);
						}
					}

					objectSpace.CommitChanges();

					DetailView detailView = Application.CreateDetailView(objectSpace, "SalesOrder_DetailView", true, newOrder);
					detailView.ViewEditMode = ViewEditMode.Edit;
					detailView.Closed += SalesOrderDetailViewController_Closed;
					e.ShowViewParameters.CreatedView = detailView;
				}
				else
				{
					Application.ShowViewStrategy.ShowMessage(string.Format("On Credit Hold: {0}. Cannot create orders at this moment.", customer.CreditHold ?? string.Empty), InformationType.Warning, 5000, InformationPosition.Top);
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void SalesOrderDetailViewController_Closed(object sender, EventArgs e)
		{
			try
			{
				var detailView = (DetailView)sender;
				var order = (SalesOrder)detailView.CurrentObject;

				detailView.Closed -= SalesOrderDetailViewController_Closed;

				if (order != null && order?.TransStatus == Enums.Trans_Status.ReadyToSend)
				{
					try
					{
						var objSpace = (XPObjectSpace)((DetailView)View).ObjectSpace;
						var sql = string.Format("EXEC CopyOrderToMsfDatabase {0}", order.ID);
						//var result = objSpace.Session.ExecuteSproc(sql);
						var result = objSpace.Session.ExecuteSproc("CopyOrderToMsfDatabase", order.ID);

						if (result.ResultSet.Count() > 0)
						{
							if ((result.ResultSet.ElementAt(0).Rows.First() as SelectStatementResultRow).Values[0].ToString() == "0")
							{
								Application.ShowViewStrategy.ShowMessage(string.Format("Order #{0} created and transfered to MSF database.", order.ID), InformationType.Success, 3000, InformationPosition.Top);
							}
							else
							{
								// An error occurred inside the stored procedure [CopyOrderToMsfDatabase]. Details of the error can be viewed in table [errorlog] using query SELECT * FROM errorlog WHERE number = <OrderNumber>
								throw new Exception();
							}
						}
					}
					catch (Exception)
					{
						Application.ShowViewStrategy.ShowMessage(string.Format("Order #{0} created but could not be transfered to MSF database.", order.ID), InformationType.Warning, 3000, InformationPosition.Top);
					}
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}
	}
}
