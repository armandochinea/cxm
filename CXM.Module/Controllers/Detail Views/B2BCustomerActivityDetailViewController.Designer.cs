﻿namespace CXM.Module.Controllers.Detail_Views
{
	partial class B2BCustomerActivityDetailViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.B2BShowAging = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
			this.B2BShowLastInvoices = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
			this.B2BNewIssue = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
			this.B2BShowDueInvoices = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
			this.B2BNewOrder = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
			// 
			// B2BShowAging
			// 
			this.B2BShowAging.AcceptButtonCaption = null;
			this.B2BShowAging.CancelButtonCaption = null;
			this.B2BShowAging.Caption = "Show Aging";
			this.B2BShowAging.Category = "ShowAging";
			this.B2BShowAging.ConfirmationMessage = null;
			this.B2BShowAging.Id = "B2BShowAging";
			this.B2BShowAging.ImageName = "Action_Printing_Preview";
			this.B2BShowAging.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.Image;
			this.B2BShowAging.ToolTip = null;
			this.B2BShowAging.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.B2BShowAging_CustomizePopupWindowParams);
			// 
			// B2BShowLastInvoices
			// 
			this.B2BShowLastInvoices.AcceptButtonCaption = null;
			this.B2BShowLastInvoices.CancelButtonCaption = null;
			this.B2BShowLastInvoices.Caption = "Show Last Invoices";
			this.B2BShowLastInvoices.Category = "ShowLastInvoices";
			this.B2BShowLastInvoices.ConfirmationMessage = null;
			this.B2BShowLastInvoices.Id = "B2BShowLastInvoices";
			this.B2BShowLastInvoices.ImageName = "Action_Printing_Preview";
			this.B2BShowLastInvoices.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.Image;
			this.B2BShowLastInvoices.ToolTip = null;
			this.B2BShowLastInvoices.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.B2BShowLastInvoices_CustomizePopupWindowParams);
			// 
			// B2BNewIssue
			// 
			this.B2BNewIssue.AcceptButtonCaption = "Save";
			this.B2BNewIssue.CancelButtonCaption = null;
			this.B2BNewIssue.Caption = "New Issue";
			this.B2BNewIssue.Category = "AgentNewIssue";
			this.B2BNewIssue.ConfirmationMessage = null;
			this.B2BNewIssue.Id = "B2BNewIssue";
			this.B2BNewIssue.ToolTip = null;
			this.B2BNewIssue.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.B2BNewIssue_CustomizePopupWindowParams);
			// 
			// B2BShowDueInvoices
			// 
			this.B2BShowDueInvoices.AcceptButtonCaption = null;
			this.B2BShowDueInvoices.CancelButtonCaption = null;
			this.B2BShowDueInvoices.Caption = "Show Due Invoices";
			this.B2BShowDueInvoices.Category = "ShowDueInvoices";
			this.B2BShowDueInvoices.ConfirmationMessage = null;
			this.B2BShowDueInvoices.Id = "B2BShowDueInvoices";
			this.B2BShowDueInvoices.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.Image;
			this.B2BShowDueInvoices.ToolTip = null;
			this.B2BShowDueInvoices.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.B2BShowDueInvoices_CustomizePopupWindowParams);
			// 
			// B2BNewOrder
			// 
			this.B2BNewOrder.Caption = "New Order";
			this.B2BNewOrder.Category = "AgentNewOrder";
			this.B2BNewOrder.ConfirmationMessage = null;
			this.B2BNewOrder.Id = "B2BNewOrder";
			this.B2BNewOrder.ToolTip = null;
			this.B2BNewOrder.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.B2BNewOrder_Execute);
			// 
			// B2BCustomerActivityDetailViewController
			// 
			this.Actions.Add(this.B2BShowAging);
			this.Actions.Add(this.B2BShowLastInvoices);
			this.Actions.Add(this.B2BNewIssue);
			this.Actions.Add(this.B2BShowDueInvoices);
			this.Actions.Add(this.B2BNewOrder);
			this.TargetViewId = "B2B_CustomerActivity_DetailView";

		}

		#endregion

		private DevExpress.ExpressApp.Actions.PopupWindowShowAction B2BShowAging;
		private DevExpress.ExpressApp.Actions.PopupWindowShowAction B2BShowLastInvoices;
		private DevExpress.ExpressApp.Actions.PopupWindowShowAction B2BNewIssue;
		private DevExpress.ExpressApp.Actions.PopupWindowShowAction B2BShowDueInvoices;
		private DevExpress.ExpressApp.Actions.SimpleAction B2BNewOrder;
	}
}
