﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using CXM.Module.BusinessObjects.CXM;

namespace CXM.Module.Controllers.Detail_Views
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class AgentCallResultDetailViewController : ViewController
	{
		public AgentCallResultDetailViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.
			//View.Closed += View_Closed;
			View.QueryCanClose += View_QueryCanClose;
		}

		private void View_QueryCanClose(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!View.IsDisposed && View.Id == "Agent_CallResult_DetailView")
			{
				var detailView = (DetailView)View;
				var callResult = (CallResult)detailView.CurrentObject;

				if (!callResult.Call.Outgoing && callResult.Call.Attempted)
				{
					e.Cancel = true;
					View.QueryCanClose -= View_QueryCanClose;

					//if (detailView.ViewEditMode == ViewEditMode.Edit)
					//{
					//	detailView.ViewEditMode = ViewEditMode.View;
					//}
				}
			}
		}

		private void View_Closed(object sender, EventArgs e)
		{
			if (View.Id == "Agent_CallResult_DetailView")
			{
				if ((View as DetailView).ViewEditMode == ViewEditMode.Edit)
				{
					(View as DetailView).ViewEditMode = ViewEditMode.View;
				}
			}
			//var callResult = (CallResult)View.CurrentObject;

			//if (callResult != null)
			//{
			//	if (!callResult.Call.Outgoing && callResult.Call.Attempted)
			//	{
			//		var listViewId = "Agent_Calls_ListView";
			//		IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(Calls));
			//		CollectionSourceBase collectionSource = Application.CreateCollectionSource(objectSpace, typeof(Calls), listViewId);
			//		var view = Application.CreateListView(listViewId, collectionSource, true);
			//		Application.MainWindow.SetView(view, Frame);
			//		View.Closed -= View_Closed;
			//	}
			//}
		}

		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			View.Closed -= View_Closed;
			View.QueryCanClose -= View_QueryCanClose;
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();
		}

	}
}
