﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using CXM.Module.BusinessObjects.CXM;
using System.Data.SqlClient;
using System.Net.Http;
using Newtonsoft.Json;
using DevExpress.ExpressApp.Xpo;

namespace CXM.Module.Controllers.Detail_Views
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class ProductDetailViewController : ViewController
	{
		string paramServiceAddress = string.Empty;
		List<GlobalParameters> globalParametersList = null;
		Dictionary<string, string> globalParameters;

		public ProductDetailViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			Frame.GetController<RecordsNavigationController>().NextObjectAction.ExecuteCompleted += NextObjectAction_ExecuteCompleted;
			Frame.GetController<RecordsNavigationController>().PreviousObjectAction.ExecuteCompleted += PreviousObjectAction_ExecuteCompleted;

			globalParametersList = View.ObjectSpace.GetObjectsQuery<GlobalParameters>().ToList();
			if (globalParameters == null)
			{
				globalParameters = new Dictionary<string, string>();
			}

			foreach (var parameter in globalParametersList)
			{
				if (!globalParameters.ContainsKey(parameter.Name))
				{
					globalParameters.Add(parameter.Name, parameter.Value);
				}
				else
				{
					globalParameters[parameter.Name] = parameter.Value;
				}
			}

			paramServiceAddress = globalParameters.ContainsKey("SERVICE_ADDRESS") ? globalParameters["SERVICE_ADDRESS"] : "http://63.131.254.222:10000/MSFRESTServicesBFHCXM/MSFRESTSrv.svc";

			LoadQtyOnHand();
		}

		private void PreviousObjectAction_ExecuteCompleted(object sender, ActionBaseEventArgs e)
		{
			LoadQtyOnHand();
		}

		private void NextObjectAction_ExecuteCompleted(object sender, ActionBaseEventArgs e)
		{
			LoadQtyOnHand();
		}

		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();

			globalParametersList = null;
			globalParameters.Clear();
			globalParameters = null;
		}

		private void LoadQtyOnHand()
		{
			try
			{
				var detailView = View as DetailView;
				var product = detailView.CurrentObject as Product;

				var endPoint = string.Format("{0}/qtyOnHand/{1}/{2}?format=json", paramServiceAddress, SecuritySystem.CurrentUserName, product.ProductID);

				// Create the Client
				var client = new HttpClient();

				// Post the JSON
				var responseMessage = client.GetAsync(endPoint).Result;
				var stringResult = responseMessage.Content.ReadAsStringAsync().Result;

				// Convert JSON back to the Object
				var responseObject = JsonConvert.DeserializeObject<QtyOnHandResult>(stringResult);

				if (responseObject.Fault == null)
				{
					product.QtyOnHand = (Int64)responseObject.QtyOnHand;
					detailView.ObjectSpace.CommitChanges();
				}

				GC.Collect();
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}
	}
}
