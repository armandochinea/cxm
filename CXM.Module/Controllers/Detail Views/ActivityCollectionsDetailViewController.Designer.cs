﻿namespace CXM.Module.Controllers.Detail_Views
{
    partial class ActivityCollectionsDetailViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CancelCollection = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.Checkout = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // CancelCollection
            // 
            this.CancelCollection.Caption = "Cancel";
            this.CancelCollection.Category = "UndoRedo";
            this.CancelCollection.ConfirmationMessage = "Are you sure you want to cancel this transaction?";
            this.CancelCollection.Id = "CancelCollection";
            this.CancelCollection.ToolTip = null;
            this.CancelCollection.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CancelCollection_Execute);
            // 
            // Checkout
            // 
            this.Checkout.Caption = "Checkout";
            this.Checkout.Category = "Save";
            this.Checkout.ConfirmationMessage = null;
            this.Checkout.Id = "Checkout";
            this.Checkout.ImageName = "";
            this.Checkout.ToolTip = null;
            this.Checkout.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.Checkout_Execute);
            // 
            // ActivityCollectionsDetailViewController
            // 
            this.Actions.Add(this.CancelCollection);
            this.Actions.Add(this.Checkout);
            this.TargetViewId = "Activity_Collections_DetailView";

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CancelCollection;
        private DevExpress.ExpressApp.Actions.SimpleAction Checkout;
    }
}
