﻿using System;
using System.Linq;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using CXM.Module.BusinessObjects.CXM;
using System.Data.SqlClient;
using DevExpress.ExpressApp.Web.Layout;
using DevExpress.ExpressApp.Web;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo.DB;
using DevExpress.ExpressApp.SystemModule;
using System.Collections.Generic;

namespace CXM.Module.Controllers
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class AgentCallsDetailViewController : ViewController
	{
		bool callRescheduled = false;

		public object Webwindow { get; private set; }

		public AgentCallsDetailViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			((View as DetailView).LayoutManager as WebLayoutManager).PageControlCreated += OnPageControlCreated;
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();

			((View as DetailView).LayoutManager as WebLayoutManager).PageControlCreated -= OnPageControlCreated;
		}

		private void OnPageControlCreated(object sender, PageControlCreatedEventArgs e)
		{
			try
			{
				if (e.Model.Id == "Details")
				{
					e.PageControl.ClientSideEvents.Init = "function(s,e){s.SetActiveTabIndex(0);}";
					((View as DetailView).LayoutManager as WebLayoutManager).PageControlCreated -= OnPageControlCreated;
				}
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void AgentEndCall_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			try
			{
				IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(CallResult));
				Calls currentCall = objectSpace.GetObject<Calls>(((Calls)View.CurrentObject));

				currentCall.EndDateTime = DateTime.Now;
				currentCall.Attempted = true;
				currentCall.Status = Calls.CallStatus.Finished;

				// Obtain and assign the CallNumber for current call.
				CxmUser user = objectSpace.GetObject<CxmUser>((CxmUser)SecuritySystem.CurrentUser);
				if (user != null)
				{
					user.NextCallNumber++;
					currentCall.CallNumber = string.Format("{0}-{1}", user.UserName, user.NextCallNumber.ToString("000000"));
				}

				// Clear the current call reference in CallHelper.
				CallHelper callHelper = objectSpace.GetObjectsQuery<CallHelper>().Where(r => r.UserID == SecuritySystem.CurrentUserName).FirstOrDefault();
				callHelper.CurrentCall = null;

				// Verify if the current call is part of the plan and determine if today's plan has been completed.
				if (currentCall.PlanID != null)
				{
					bool planCompleted = true;
					Plans plan = objectSpace.GetObjectsQuery<Plans>().Where(r => r.PlanID == currentCall.PlanID.PlanID).FirstOrDefault();
					foreach (Calls call in plan?.Calls)
					{
						if (call.Attempted == false)
						{
							planCompleted = false;
							break;
						}
					}

					if (planCompleted)
					{
						plan.Status = Plans.PlanStatus.Completed;
					}
				}

				//****** SUBMIT DB CHANGES FOR VOIP COMMANDS
				DevExpress.Xpo.Session session = ((XPObjectSpace)(objectSpace)).Session;
				string qry = string.Format("Insert Into VOIPCommands (UserID, Command) values ('{0}','{1}')", user.Oid, "HANGUP|");
				session.ExecuteQuery(qry);


				// Get the current call's result info to display, or create one.
				CallResult callResult = objectSpace.GetObjectsQuery<CallResult>().Where(r => r.Call == currentCall).FirstOrDefault();
				if (callResult == null)
				{
					callResult = objectSpace.CreateObject<CallResult>();
					callResult.Call = currentCall;
				}

				// Save changes to the database.
				objectSpace.CommitChanges();

				// Prepare current call's results screen to display to the user.
				DetailView detailView = Application.CreateDetailView(objectSpace, "Agent_CallResult_DetailView", true, callResult);
				detailView.ViewEditMode = ViewEditMode.Edit;
				e.ShowViewParameters.CreatedView = detailView;

				View.Close();
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void AgentNewIssue_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(Issues));

				Customers customer = objectSpace.GetObject<Customers>(((Calls)View.CurrentObject).CustomerID);
				Issues newIssue = objectSpace.CreateObject<Issues>();
				newIssue.CustomerID = customer;

				DetailView detailView = Application.CreateDetailView(objectSpace, "New_Issues_DetailView", true, newIssue);
				detailView.ViewEditMode = ViewEditMode.Edit;
				e.View = detailView;

				detailView.Closed += NewIssueDetailViewController_Closed;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void AgentShowLastInvoices_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				Type objectType = typeof(Invoice);
				IObjectSpace invoiceObjectSpace = Application.CreateObjectSpace(objectType);

				var customer = View.ObjectSpace.GetObject<Customers>(((Calls)View.CurrentObject).CustomerID);
				DetailView detailView = Application.CreateDetailView(invoiceObjectSpace, "CustomerLastInvoices_DetailView", false);
				detailView.CurrentObject = invoiceObjectSpace.GetObjectsQuery<Customers>().Where(r => r.CustomerID == customer.CustomerID).FirstOrDefault();
				e.View = detailView;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void AgentShowAging_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				Type objectType = typeof(Invoice);
				IObjectSpace invoiceObjectSpace = Application.CreateObjectSpace(objectType);

				var customer = View.ObjectSpace.GetObject<Customers>(((Calls)View.CurrentObject).CustomerID);
				DetailView detailView = Application.CreateDetailView(invoiceObjectSpace, "CustomerOpenInvoices_DetailView", false);
				detailView.CurrentObject = invoiceObjectSpace.GetObjectsQuery<Customers>().Where(r => r.CustomerID == customer.CustomerID).FirstOrDefault();
				e.View = detailView;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void NewIssueDetailViewController_Closed(object sender, EventArgs e)
		{
			try
			{
				var detailView = (DetailView)sender;

				detailView.Closed -= NewIssueDetailViewController_Closed;

				if (detailView.SelectedObjects.Count == 1)
				{
					var createdIssue = (Issues)detailView.SelectedObjects[0];
					if (createdIssue?.IssueID != 0)
					{
						Application.ShowViewStrategy.ShowMessage(string.Format("Issue #{0} created.", createdIssue.IssueID), InformationType.Success, 3000, InformationPosition.Top);

						try
						{
							// Send email of the issue to the assignee.
							var objSpace = (XPObjectSpace)((DetailView)View).ObjectSpace;
							var result = objSpace.Session.ExecuteSproc("SendNewIssueEmail", createdIssue.IssueID);
						}
						catch (SqlException sqlEx)
						{
							throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
						}
						catch (Exception ex)
						{
							throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
						}

						try
						{
							var objSpace = View.ObjectSpace;
							Calls currentCall = (View as DetailView).CurrentObject as Calls;

							CallResult callResult = objSpace.GetObjectsQuery<CallResult>().Where(r => r.Call == currentCall).FirstOrDefault();
							if (callResult == null)
							{
								callResult = objSpace.CreateObject<CallResult>();
								callResult.Call = currentCall;
							}
							callResult.NewIssues = true;

							objSpace.CommitChanges();
						}
						catch (SqlException sqlEx)
						{
							throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
						}
						catch (Exception ex)
						{
							throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
						}

						View.ObjectSpace.Refresh();
					}
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void AgentNewOrder_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			try
			{
				Customers customer = ((Calls)View.CurrentObject).CustomerID;

				if (string.IsNullOrEmpty(customer.CreditHold))
				{
					IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(SalesOrder));

					SalesOrder newOrder = objectSpace.CreateObject<SalesOrder>();
					newOrder.Customer = objectSpace.GetObject<Customers>(customer);

					Route_Customer route_Customer = objectSpace.GetObjectsQuery<Route_Customer>().Where(r => r.Customer.CustomerID == newOrder.Customer.CustomerID).FirstOrDefault();
					newOrder.Route = objectSpace.GetObjectsQuery<Route>().Where(r => r.RouteNo == route_Customer.Route.RouteNo).FirstOrDefault();
					newOrder.WhsID = objectSpace.GetObjectsQuery<Warehouse>().Where(r => r.ID == "400").FirstOrDefault();
					newOrder.ContactEmail = ((Calls)View.CurrentObject).Contact?.Email;

					var result = ((XPObjectSpace)objectSpace).Session.ExecuteSproc("PrepareForNewOrder", newOrder.Route.RouteNo, customer.CustomerID, newOrder.OrderDate.ToShortDateString(), customer.ParamPercProductsForOrder, customer.ParamInvoiceCount);

					if (result.ResultSet.Count() > 0)
					{
						// First ResultSet is the ShipDate.
						newOrder.ShipDate = DateTime.Parse((result.ResultSet.ElementAt(0).Rows.First() as SelectStatementResultRow).Values[0].ToString());

						// Second ResultSet are the last invoiced products that will be added to the order.
						foreach (SelectStatementResultRow row in result.ResultSet[1].Rows)
						{
							foreach (var item in objectSpace.GetObjectsQuery<CustomerProducts>().Where(r => r.CustomerID == customer.CustomerID && r.ProductID.ProductID == row.Values[4].ToString()).ToList())
							{

								var salesLine = objectSpace.CreateObject<SalesLine>();
								salesLine.Product = item.ProductID; // row.Values[4].ToString();
								salesLine.Order = newOrder;
								salesLine.Qty = long.Parse(row.Values[7].ToString());//SuggestedOrderQty
								salesLine.SuggestedQty = long.Parse(row.Values[7].ToString());//SuggestedOrderQty
								salesLine.UnitOfMeasure = row.Values[9].ToString();//item.ProductID.UnitOfMeasure;
								newOrder.SalesLines.Add(salesLine);
							}
						}
					}
					/*
					var result = ((XPObjectSpace)objectSpace).Session.ExecuteSproc("GetShipDate", newOrder.Route.RouteNo, customer.CustomerNo, customer.ShipTo, newOrder.OrderDate.ToShortDateString());
					if (result.ResultSet.Count() > 0)
					{
						newOrder.ShipDate = DateTime.Parse((result.ResultSet.ElementAt(0).Rows.First() as SelectStatementResultRow).Values[0].ToString());
					}

					// Fill Customer's Special products from MSF database.
					((XPObjectSpace)objectSpace).Session.ExecuteSproc("GetCustomerSpecialProducts", newOrder.Customer.CustomerID);
					
					DevExpress.Xpo.Session session = ((XPObjectSpace)(objectSpace)).Session;

					string[] parameters = new string[] { "@CustomerID", "@PercentageToVerify", "@LastInvoicesAmount" };
                    object[] values = new object[] { newOrder.Customer.CustomerID, customer.ParamPercProductsForOrder,  customer.CustomerInvoiceCount };
                    
                    //SelectedData
                    SelectedData selectedData = session.ExecuteSprocParametrized("GetProductsForCustomerByInvoicePercentage",
                        new SprocParameter("@CustomerID", newOrder.Customer.CustomerID),
                        new SprocParameter("@PercentageToVerify", customer.ParamPercProductsForOrder),
                        new SprocParameter("@LastInvoicesAmount", customer.ParamInvoiceCount));
						
                    foreach (SelectStatementResultRow row in selectedData.ResultSet[0].Rows)
                    {
                        //Console.WriteLine(string.Format("{0}\t{1}", row.Values[0], row.Values[1]));
                      
                        //Product prod = new Product(session);

                        //prod.ProductID = row.Values[4];
                        //salesLine.Product = new Product(session);
                        foreach (var item in objectSpace.GetObjectsQuery<CustomerProducts>().Where(r => r.CustomerID == customer.CustomerID && r.ProductID.ProductID == row.Values[4].ToString()).ToList())
                        {

                            var salesLine = objectSpace.CreateObject<SalesLine>();
                            salesLine.Product = item.ProductID; // row.Values[4].ToString();
                            salesLine.Order = newOrder;
                            salesLine.Qty = long.Parse(row.Values[7].ToString());//SuggestedOrderQty
                            salesLine.SuggestedQty = long.Parse(row.Values[7].ToString());//SuggestedOrderQty
                            salesLine.UnitOfMeasure = row.Values[9].ToString();//item.ProductID.UnitOfMeasure;
                            newOrder.SalesLines.Add(salesLine);
                        }
                    }
					*/

					objectSpace.CommitChanges();

					DetailView detailView = Application.CreateDetailView(objectSpace, "SalesOrder_DetailView", true, newOrder);
					detailView.ViewEditMode = ViewEditMode.Edit;
					detailView.Closed += SalesOrderDetailViewController_Closed;
					e.ShowViewParameters.CreatedView = detailView;
				}
				else
				{
					Application.ShowViewStrategy.ShowMessage(string.Format("This customer is currently on Credit Hold: {0}. Cannot create orders at this moment.", customer.CreditHold ?? string.Empty), InformationType.Warning, 5000, InformationPosition.Top);
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void SalesOrderDetailViewController_Closed(object sender, EventArgs e)
		{
			try
			{
				var detailView = (DetailView)sender;
				var order = (SalesOrder)detailView.CurrentObject;
				SelectedData result;
				var objSpace = (XPObjectSpace)((DetailView)View).ObjectSpace;

				detailView.Closed -= SalesOrderDetailViewController_Closed;

				if (order != null && order?.TransStatus == Enums.Trans_Status.ReadyToSend)
				{
					try
					{
						string message;

						if (order.PrePaid)
						{
							message = $"Invoice #{order.ID} created and transfered to MSF database.";

							Int64 collNumber;
							string confirmationNumber = "";
							Int64.TryParse(order.CollectionReference, out collNumber);
							var collectionDetails = objSpace.GetObjectByKey<Collections>(collNumber)?.Details;
							foreach (var item in collectionDetails)
							{
								confirmationNumber += $"{item.ConfirmationNumber} \\n";
							}

							message += $"\\n\\nConfirmation Number: {confirmationNumber}";
						}
						else
						{
							message = $"Order #{order.ID} created and transfered to MSF database.";
						}

						result = objSpace.Session.ExecuteSproc("CopyOrderToMsfDatabase", order.ID);
						if (result.ResultSet.Count() > 0)
						{
							if ((result.ResultSet.ElementAt(0).Rows.First() as SelectStatementResultRow).Values[0].ToString() == "0")
							{
								if (order.PrePaid)
								{
									WebWindow.CurrentRequestWindow.RegisterClientScript("Transaction completed", @"alert('" + message + "');");
								}
								else
								{
									Application.ShowViewStrategy.ShowMessage(message, InformationType.Success, 3000, InformationPosition.Top);
								}
							}
							else
							{
								// An error occurred inside the stored procedure [CopyOrderToMsfDatabase]. Details of the error can be viewed in table [errorlog] using query SELECT * FROM errorlog WHERE number = <OrderNumber>
								throw new Exception();
							}
						}
					}
					catch (Exception ex)
					{
						Application.ShowViewStrategy.ShowMessage(string.Format("Order #{0} created but could not be transfered to MSF database.", order.ID), InformationType.Warning, 3000, InformationPosition.Top);
					}
					finally
					{
						try
						{
							var viewObjSpace = View.ObjectSpace;
							var CallHelper = viewObjSpace.GetObjectsQuery<CallHelper>().Where(r => r.UserID == SecuritySystem.CurrentUserName).FirstOrDefault();

							if (CallHelper != null)
							{
								var currentCall = CallHelper.CurrentCall;

								if (currentCall != null)
								{
									CallResult callResult = viewObjSpace.GetObjectsQuery<CallResult>().Where(r => r.Call == currentCall).FirstOrDefault();
									if (callResult == null)
									{
										callResult = viewObjSpace.CreateObject<CallResult>();
										callResult.Call = currentCall;
									}
									callResult.NewOrders = true;
									callResult.Other = true;
									callResult.Notes = $"Order #{order.ID.ToString()} created.";

									viewObjSpace.CommitChanges();
								}
							}
						}
						catch (SqlException sqlEx)
						{
							throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
						}
						catch (Exception ex)
						{
							throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
						}
					}

					try
					{
						CxmUser user = detailView.ObjectSpace.GetObjectsQuery<CxmUser>().Where(r => r.UserName == order.Route.RouteNo).FirstOrDefault();

						if (user != null)
						{
							if (user.SendEmail)
							{
								if (order.PrePaid)
								{
									Int64 collectionNumber;
									Int64.TryParse(order.CollectionReference, out collectionNumber);
									var recipient = order.ContactEmail;
									result = objSpace.Session.ExecuteSproc("SendNewInvoiceEmail", order.ID, recipient);
								}
								else
								{
									result = objSpace.Session.ExecuteSproc("SendNewOrderEmail", order.ID);
								}

							}

							if (user.SendSMS)
							{
								var recipient = user.PhoneSMSGateWay;
								result = objSpace.Session.ExecuteSproc("SendNewOrderEmail", order.ID, recipient);

							}

							if (user.SendWhatsapp)
							{

								//Send WhatsApp message
							}
						}
					}
					catch (Exception ex)
					{
						Application.ShowViewStrategy.ShowMessage($"A problem ocurred while trying to send the email confirmation. Message: {ex.Message}", InformationType.Warning, 3000, InformationPosition.Top);
					}
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void EditNotes_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(Customers));
				var customer = objectSpace.GetObject<Customers>(((Calls)View.CurrentObject).CustomerID);
				DetailView detailView = Application.CreateDetailView(objectSpace, "EditNotes_DetailView", false, customer);
				detailView.ViewEditMode = ViewEditMode.Edit;
				detailView.Closed += EditNotesView_Closed;
				e.View = detailView;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void EditNotesView_Closed(object sender, EventArgs e)
		{
			try
			{
				DetailView detailView = (DetailView)sender;
				detailView.Closed -= EditNotesView_Closed;
				View.ObjectSpace.Refresh();
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void ShowDueInvoices_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				Type objectType = typeof(Invoice);
				IObjectSpace invoiceObjectSpace = Application.CreateObjectSpace(objectType);

				var customer = View.ObjectSpace.GetObject<Customers>(((Calls)View.CurrentObject).CustomerID);
				DetailView detailView = Application.CreateDetailView(invoiceObjectSpace, "CustomerDueInvoices_DetailView", false);
				detailView.CurrentObject = invoiceObjectSpace.GetObjectsQuery<Customers>().Where(r => r.CustomerID == customer.CustomerID).FirstOrDefault();
				e.View = detailView;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void RescheduleCall_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
		{
			try
			{
				callRescheduled = false;
				Type objectType = typeof(CallReschedule);
				IObjectSpace rescheduleObjectSpace = Application.CreateObjectSpace(objectType);

				var call = rescheduleObjectSpace.GetObject(((Calls)View.CurrentObject));
				var callReschedule = rescheduleObjectSpace.GetObjectsQuery<CallReschedule>().Where(r => r.Call == call).FirstOrDefault();
				if (callReschedule == null)
				{
					callReschedule = rescheduleObjectSpace.CreateObject<CallReschedule>();
					callReschedule.Call = call;
					callReschedule.ScheduledDate = DateTime.Today;
					callReschedule.CallTimeFrame = CallReschedule.CallTimeFrames.BeforeNine;
					callReschedule.Agent = rescheduleObjectSpace.GetObjectsQuery<CxmUser>().Where(r => r.UserName == SecuritySystem.CurrentUserName).FirstOrDefault();

					var customerPlan = rescheduleObjectSpace.GetObjectsQuery<CustomerPlan>().Where(r => r.Customer == call.CustomerID).FirstOrDefault();
					if (customerPlan != null)
					{
						callReschedule.Frequency = customerPlan.Frequency;
						callReschedule.Monday = customerPlan.Monday;
						callReschedule.Tuesday = customerPlan.Tuesday;
						callReschedule.Wednesday = customerPlan.Wednesday;
						callReschedule.Thursday = customerPlan.Thursday;
						callReschedule.Friday = customerPlan.Friday;
						callReschedule.Saturday = customerPlan.Saturday;
						callReschedule.Even = customerPlan.Even;
						callReschedule.Odd = customerPlan.Odd;
						callReschedule.First = customerPlan.First;
						callReschedule.Second = customerPlan.Second;
						callReschedule.Third = customerPlan.Third;
						callReschedule.Fourth = customerPlan.Fourth;
						callReschedule.Before9 = customerPlan.Before9;
						callReschedule.Between9and12 = customerPlan.Between9and12;
						callReschedule.Between12and3 = customerPlan.Between12and3;
						callReschedule.After3 = customerPlan.After3;
					}
				}

				DetailView detailView = Application.CreateDetailView(rescheduleObjectSpace, "CallReschedule_DetailView", false);
				detailView.CurrentObject = rescheduleObjectSpace.GetObject(callReschedule);
				e.View = detailView;

				DialogController dialogController = new DialogController();
				dialogController.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(CallRescheduleDialogController_Accepting);
				dialogController.ViewClosed += CallRescheduleDialogController_ViewClosed;
				dialogController.ViewQueryCanClose += DialogController_ViewQueryCanClose;
				e.DialogController = dialogController;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void DialogController_ViewQueryCanClose(object sender, System.ComponentModel.CancelEventArgs e)
		{
			e.Cancel = true;
		}

		private void CallRescheduleDialogController_ViewClosed(object sender, EventArgs e)
		{
			if (callRescheduled)
			{
				try
				{
					// Send email of the call reschedule to the supervisor
					var objSpace = (XPObjectSpace)((DetailView)View).ObjectSpace;
					var reschedule = (CallReschedule)objSpace.GetObject(((DetailView)sender).CurrentObject);
					var result = objSpace.Session.ExecuteSproc("SendCallRescheduleEmail", reschedule.Call.ID);
				}
				catch (SqlException sqlEx)
				{
					throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
				}
				catch (Exception ex)
				{
					throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
				}

				AgentEndCall.DoExecute();
			}
		}

		void CallRescheduleDialogController_Accepting(object sender, DialogControllerAcceptingEventArgs e)
		{
			try
			{
				var detailView = ((sender as DialogController).Frame.View as DetailView);

				if (detailView.CurrentObject != null)
				{
					var callReschedule = detailView.CurrentObject as CallReschedule;

					if (callReschedule.ScheduledDate.Date <= DateTime.Today.Date)
					{
						e.Cancel = true;
						Application.ShowViewStrategy.ShowMessage("Cannot choose an earlier date for the reschedule.", InformationType.Warning, 3000, InformationPosition.Top);
						return;
					}

					callReschedule.Call.Category = Calls.CallCategory.Rescheduled;
					var callResult = detailView.ObjectSpace.GetObjectsQuery<CallResult>().Where(r => r.Call == callReschedule.Call).FirstOrDefault();
					if (callResult != null)
					{
						callResult.Other = true;
						callResult.Notes = string.Format("Call was rescheduled for {0}.", callReschedule.ScheduledDate.ToShortDateString());
					}

					if (callReschedule.UpdateCustomerPlan)
					{
						var customerPlan = detailView.ObjectSpace.GetObjectsQuery<CustomerPlan>().Where(r => r.Customer == callReschedule.Call.CustomerID).FirstOrDefault();
						if (customerPlan == null)
						{
							customerPlan = detailView.ObjectSpace.CreateObject<CustomerPlan>();
							customerPlan.Customer = detailView.ObjectSpace.GetObject<Customers>(callReschedule.Call.CustomerID);
							customerPlan.Agent = detailView.ObjectSpace.GetObject<CxmUser>((CxmUser)SecuritySystem.CurrentUser);
						}

						customerPlan.Frequency = callReschedule.Frequency;
						customerPlan.Monday = callReschedule.Monday;
						customerPlan.Tuesday = callReschedule.Tuesday;
						customerPlan.Wednesday = callReschedule.Wednesday;
						customerPlan.Thursday = callReschedule.Thursday;
						customerPlan.Friday = callReschedule.Friday;
						customerPlan.Saturday = callReschedule.Saturday;
						customerPlan.Even = callReschedule.Even;
						customerPlan.Odd = callReschedule.Odd;
						customerPlan.First = callReschedule.First;
						customerPlan.Second = callReschedule.Second;
						customerPlan.Third = callReschedule.Third;
						customerPlan.Fourth = callReschedule.Fourth;
						customerPlan.Before9 = callReschedule.Before9;
						customerPlan.Between9and12 = callReschedule.Between9and12;
						customerPlan.Between12and3 = callReschedule.Between12and3;
						customerPlan.After3 = callReschedule.After3;
						customerPlan.LastUpdated = DateTime.Now;
					}

					detailView.ObjectSpace.CommitChanges();
					callRescheduled = true;
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void AgentNewCollection_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			try
			{
				Customers customer = ((Calls)View.CurrentObject).CustomerID;
				string contactEmail = ((Calls)View.CurrentObject).Contact.Email;

				IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(Collections));

				Collections newCollection = objectSpace.CreateObject<Collections>();
				newCollection.Customer = objectSpace.GetObject<Customers>(customer);
				newCollection.CollectionDate = DateTime.Now;
				newCollection.SignName = string.Empty;
				newCollection.ReceiptNo = string.Empty;
				newCollection.DSBatchID = string.Empty;
				newCollection.Comments = string.Empty;
				newCollection.ContactEmail = contactEmail;

				Route_Customer route_Customer = objectSpace.GetObjectsQuery<Route_Customer>().Where(r => r.Customer.CustomerID == newCollection.Customer.CustomerID).FirstOrDefault();
				newCollection.Route = objectSpace.GetObjectsQuery<Route>().Where(r => r.RouteNo == route_Customer.Route.RouteNo).FirstOrDefault();

				objectSpace.CommitChanges();

				DetailView detailView = Application.CreateDetailView(objectSpace, "Activity_Collections_DetailView", true, newCollection);
				detailView.ViewEditMode = ViewEditMode.Edit;
				detailView.Closed += ActivityCollectionsDetailViewController_Closed;
				e.ShowViewParameters.CreatedView = detailView;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void ActivityCollectionsDetailViewController_Closed(object sender, EventArgs e)
		{
			try
			{
				var detailView = (DetailView)sender;
				var collection = (Collections)detailView.CurrentObject;
				SelectedData result;
				var objSpace = (XPObjectSpace)((DetailView)View).ObjectSpace;

				detailView.Closed -= ActivityCollectionsDetailViewController_Closed;

				if (collection != null)
				{
					if (collection?.TransStatus == Enums.Trans_Status.ReadyToSend)
					{
						try
						{
							result = objSpace.Session.ExecuteSproc("CopyCollectionToMsfDatabase", collection.ID);

							if (result.ResultSet.Count() > 0)
							{
								if ((result.ResultSet.ElementAt(0).Rows.First() as SelectStatementResultRow).Values[0].ToString() == "0")
								{
									string confirmationNumber = "";
									foreach (var item in collection.Details)
									{
										confirmationNumber += $"{item.ConfirmationNumber} \\n";
									}
									string message = string.Format($"Collection #{collection.CollectionID} created and transfered to MSF database.\\n\\nConfirmation Number: {confirmationNumber}");
									WebWindow.CurrentRequestWindow.RegisterClientScript("Transaction completed", @"alert('" + message + "');");
								}
								else
								{
									// An error occurred inside the stored procedure [CopyCollectionToMsfDatabase]. Details of the error can be viewed in table [errorlog] using query SELECT * FROM errorlog WHERE number = <ID>
									throw new Exception();
								}
							}
						}
						catch (Exception)
						{
							Application.ShowViewStrategy.ShowMessage(string.Format("Collection #{0} created but could not be transfered to MSF database.", collection.CollectionID), InformationType.Warning, 3000, InformationPosition.Top);
						}
						finally
						{
							try
							{
								var viewObjSpace = View.ObjectSpace;
								var CallHelper = viewObjSpace.GetObjectsQuery<CallHelper>().Where(r => r.UserID == SecuritySystem.CurrentUserName).FirstOrDefault();

								if (CallHelper != null)
								{
									var currentCall = CallHelper.CurrentCall;

									if (currentCall != null)
									{
										CallResult callResult = viewObjSpace.GetObjectsQuery<CallResult>().Where(r => r.Call == currentCall).FirstOrDefault();
										if (callResult == null)
										{
											callResult = viewObjSpace.CreateObject<CallResult>();
											callResult.Call = currentCall;
										}
										callResult.NewCollections = true;

										viewObjSpace.CommitChanges();
									}
								}
							}
							catch (SqlException sqlEx)
							{
								throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
							}
							catch (Exception ex)
							{
								throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
							}
						}

						try
						{
							// Send email of the collection's receipt to contact.
							var recipient = collection.ContactEmail;
							result = objSpace.Session.ExecuteSproc("SendNewCollectionEmail", collection.ID, recipient);

							CxmUser user = detailView.ObjectSpace.GetObjectByKey<CxmUser>(SecuritySystem.CurrentUserId);

							if (user != null)
							{
								if (user.SendEmail)
								{
									recipient = user.Email == "N/A" ? null : user.Email;
									result = objSpace.Session.ExecuteSproc("SendNewCollectionEmail", collection.ID, recipient);
								}

								if (user.SendSMS)
								{
									recipient = user.PhoneSMSGateWay;
									result = objSpace.Session.ExecuteSproc("SendNewCollectionEmail", collection.ID, recipient);
								}

								if (user.SendWhatsapp)
								{
									//Send WhatsApp message
								}
							}
						}
						catch (Exception ex)
						{
							Application.ShowViewStrategy.ShowMessage($"A problem ocurred while trying to send the email confirmation. Message: {ex.Message}", InformationType.Warning, 3000, InformationPosition.Top);
						}
					}
					else if (collection?.TransStatus == Enums.Trans_Status.Deleted)
					{
						List<CollectionDetails> colDetails = collection.Details.ToList();
						collection.Details.DeleteObjectOnRemove = true;
						foreach (var item in colDetails)
						{
							collection.Details.Remove(item);
						}

						List<CollectionPayments> colPayments = collection.Payments.ToList();
						collection.Payments.DeleteObjectOnRemove = true;
						foreach (var item in colPayments)
						{
							collection.Payments.Remove(item);
						}

						collection.Delete();
						View.ObjectSpace.CommitChanges();
						Application.ShowViewStrategy.ShowMessage("Transaction was canceled.", InformationType.Warning, 3000, InformationPosition.Top);
					}
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}
	}
}