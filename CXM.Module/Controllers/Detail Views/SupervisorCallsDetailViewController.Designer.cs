﻿namespace CXM.Module.Controllers.Detail_Views
{
	partial class SupervisorCallsDetailViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.SupEndCall = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
			this.SupNewIssue = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
			this.SupNewOrder = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
			this.SupShowLastInvoices = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
			this.SupShowAging = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
			// 
			// SupEndCall
			// 
			this.SupEndCall.Caption = "End Call";
			this.SupEndCall.ConfirmationMessage = null;
			this.SupEndCall.Id = "SupEndCall";
			this.SupEndCall.TargetViewId = "";
			this.SupEndCall.ToolTip = null;
			this.SupEndCall.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SupEndCall_Execute);
			// 
			// SupNewIssue
			// 
			this.SupNewIssue.AcceptButtonCaption = "Save";
			this.SupNewIssue.CancelButtonCaption = "";
			this.SupNewIssue.Caption = "New Issue";
			this.SupNewIssue.Category = "AgentNewIssue";
			this.SupNewIssue.ConfirmationMessage = null;
			this.SupNewIssue.Id = "SupNewIssue";
			this.SupNewIssue.ToolTip = null;
			this.SupNewIssue.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.SupNewIssue_CustomizePopupWindowParams);
			// 
			// SupNewOrder
			// 
			this.SupNewOrder.AcceptButtonCaption = "Save";
			this.SupNewOrder.CancelButtonCaption = null;
			this.SupNewOrder.Caption = "New Order";
			this.SupNewOrder.Category = "AgentNewOrder";
			this.SupNewOrder.ConfirmationMessage = null;
			this.SupNewOrder.Id = "SupNewOrder";
			this.SupNewOrder.ToolTip = null;
			this.SupNewOrder.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.SupNewOrder_CustomizePopupWindowParams);
			// 
			// SupShowLastInvoices
			// 
			this.SupShowLastInvoices.AcceptButtonCaption = null;
			this.SupShowLastInvoices.CancelButtonCaption = null;
			this.SupShowLastInvoices.Caption = "Show Last Invoices";
			this.SupShowLastInvoices.Category = "ShowLastInvoices";
			this.SupShowLastInvoices.ConfirmationMessage = null;
			this.SupShowLastInvoices.Id = "SupShowLastInvoices";
			this.SupShowLastInvoices.ImageName = "Action_Printing_Preview";
			this.SupShowLastInvoices.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.Image;
			this.SupShowLastInvoices.ToolTip = null;
			this.SupShowLastInvoices.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.SupShowLastInvoices_CustomizePopupWindowParams);
			// 
			// SupShowAging
			// 
			this.SupShowAging.AcceptButtonCaption = null;
			this.SupShowAging.CancelButtonCaption = null;
			this.SupShowAging.Caption = "Show Aging";
			this.SupShowAging.Category = "ShowAging";
			this.SupShowAging.ConfirmationMessage = null;
			this.SupShowAging.Id = "SupShowAging";
			this.SupShowAging.ImageName = "Action_Printing_Preview";
			this.SupShowAging.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.Image;
			this.SupShowAging.ToolTip = null;
			this.SupShowAging.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.SupShowAging_CustomizePopupWindowParams);
			// 
			// SupervisorCallsDetailViewController
			// 
			this.Actions.Add(this.SupEndCall);
			this.Actions.Add(this.SupNewIssue);
			this.Actions.Add(this.SupNewOrder);
			this.Actions.Add(this.SupShowLastInvoices);
			this.Actions.Add(this.SupShowAging);
			this.TargetViewId = "Nothing";

		}

		#endregion

		private DevExpress.ExpressApp.Actions.SimpleAction SupEndCall;
		private DevExpress.ExpressApp.Actions.PopupWindowShowAction SupNewIssue;
		private DevExpress.ExpressApp.Actions.PopupWindowShowAction SupNewOrder;
		private DevExpress.ExpressApp.Actions.PopupWindowShowAction SupShowLastInvoices;
		private DevExpress.ExpressApp.Actions.PopupWindowShowAction SupShowAging;
	}
}
