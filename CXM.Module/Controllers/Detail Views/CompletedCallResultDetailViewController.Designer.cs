﻿namespace CXM.Module.Controllers.Detail_Views
{
	partial class CompletedCallResultDetailViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.Close = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
			// 
			// Close
			// 
			this.Close.Caption = "Close";
			this.Close.Category = "UndoRedo";
			this.Close.ConfirmationMessage = null;
			this.Close.Id = "Close";
			this.Close.TargetViewId = "";
			this.Close.ToolTip = null;
			this.Close.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.Close_Execute);
			// 
			// CompletedCallResultDetailViewController
			// 
			this.Actions.Add(this.Close);
			this.TargetViewId = "CompletedCallResult_DetailView";

		}

		#endregion

		private DevExpress.ExpressApp.Actions.SimpleAction Close;
	}
}
