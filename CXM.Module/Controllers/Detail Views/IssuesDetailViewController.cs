﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using CXM.Module.BusinessObjects.CXM;
using System.Data.SqlClient;

namespace CXM.Module.Controllers.Detail_Views
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class IssuesDetailViewController : ViewController
	{
		public IssuesDetailViewController()
		{
			InitializeComponent();
			// Target required Views (via the TargetXXX properties) and create their Actions.
		}
		protected override void OnActivated()
		{
			base.OnActivated();
			// Perform various tasks depending on the target View.

			View.Closing += View_Closing;
		}
		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();
			// Access and customize the target View control.
		}
		protected override void OnDeactivated()
		{
			// Unsubscribe from previously subscribed events and release other references and resources.
			base.OnDeactivated();
			
			View.Closing -= View_Closing;
		}

		private void View_Closing(object sender, EventArgs e)
		{
			try
			{
				var detailView = (View as DetailView);
				var issue = detailView.CurrentObject as Issues;

				if (issue.ObjectChanged)
				{
					if (detailView.SelectedObjects.Count == 1)
					{
						var selectedObj = (Issues)detailView.SelectedObjects[0];
						if (selectedObj != null && selectedObj.ObjectChanged)
						{
							issue.LastUpdate = DateTime.Now;

							Application.ShowViewStrategy.ShowMessage(string.Format("Issue #{0} updated.", issue.IssueID), InformationType.Success, 3000, InformationPosition.Top);

							CallHelper callHelper = View.ObjectSpace.GetObjectsQuery<CallHelper>().Where(r => r.UserID == SecuritySystem.CurrentUserName).FirstOrDefault();

							if (callHelper != null)
							{
								if (callHelper.CurrentCall != null)
								{
									Calls currentCall = callHelper.CurrentCall;
									if (currentCall.CustomerID?.CustomerID == issue.CustomerID.CustomerID)
									{
										CallResult callResult = View.ObjectSpace.GetObjectsQuery<CallResult>().Where(r => r.Call == currentCall).FirstOrDefault();
										if (callResult == null)
										{
											callResult = View.ObjectSpace.CreateObject<CallResult>();
											callResult.Call = currentCall;
										}
										callResult.UpdateIssues = true;
									}
								}
							}

							View.ObjectSpace.CommitChanges();
						}
					}
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void NewAction_Execute(object sender, SimpleActionExecuteEventArgs e)
		{
			try
			{
				IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(Actions));

				Actions newAction = objectSpace.CreateObject<Actions>();
				newAction.ActionID.IssueID = objectSpace.GetObject<Issues>(((Issues)View.CurrentObject));

				DetailView detailView = Application.CreateDetailView(objectSpace, "Actions_DetailView", true, newAction);
				detailView.ViewEditMode = ViewEditMode.Edit;
				e.ShowViewParameters.CreatedView = detailView;

				DialogController dialogController = new DialogController();
				dialogController.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(DialogController_Accepting);
				e.ShowViewParameters.Controllers.Add(dialogController);

				detailView.Closed += NewActionDetailView_Closed;
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void NewActionDetailView_Closed(object sender, EventArgs e)
		{
			try
			{
				(sender as DetailView).Closed -= NewActionDetailView_Closed;
				View.ObjectSpace.Refresh();
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		void DialogController_Accepting(object sender, DialogControllerAcceptingEventArgs e)
		{
			try
			{
				var objSpace = View.ObjectSpace;
				var detailView = (DetailView)View;

				if (detailView != null)
				{
					var issue = (Issues)detailView.CurrentObject;

					if (issue != null)
					{
						issue.ObjectChanged = true;
						issue.Save();
					}
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}
	}
}
