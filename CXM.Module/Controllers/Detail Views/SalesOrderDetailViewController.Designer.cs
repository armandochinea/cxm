﻿namespace CXM.Module.Controllers.Detail_Views
{
	partial class SalesOrderDetailViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.CancelOrder = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
			this.CalculatePrices = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
			this.SaveOrder = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
			// 
			// CancelOrder
			// 
			this.CancelOrder.Caption = "Cancel";
			this.CancelOrder.Category = "UndoRedo";
			this.CancelOrder.ConfirmationMessage = null;
			this.CancelOrder.Id = "CancelOrder";
			this.CancelOrder.TargetViewId = "";
			this.CancelOrder.ToolTip = null;
			this.CancelOrder.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CancelOrder_Execute);
			this.CancelOrder.CustomGetFormattedConfirmationMessage += new System.EventHandler<DevExpress.ExpressApp.Actions.CustomGetFormattedConfirmationMessageEventArgs>(this.CancelOrder_CustomGetFormattedConfirmationMessage);
			// 
			// CalculatePrices
			// 
			this.CalculatePrices.Caption = "Calculate Prices";
			this.CalculatePrices.Category = "CalculatePrices";
			this.CalculatePrices.ConfirmationMessage = null;
			this.CalculatePrices.Id = "CalculatePrices";
			this.CalculatePrices.TargetViewId = "";
			this.CalculatePrices.ToolTip = null;
			this.CalculatePrices.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CalculatePrices_Execute);
			// 
			// SaveOrder
			// 
			this.SaveOrder.Caption = "Save Order";
			this.SaveOrder.Category = "Save";
			this.SaveOrder.ConfirmationMessage = null;
			this.SaveOrder.Id = "SaveOrder";
			this.SaveOrder.ToolTip = null;
			this.SaveOrder.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SaveOrder_Execute);
			// 
			// SalesOrderDetailViewController
			// 
			this.Actions.Add(this.CancelOrder);
			this.Actions.Add(this.CalculatePrices);
			this.Actions.Add(this.SaveOrder);
			this.TargetViewId = "SalesOrder_DetailView;SpeedSalesOrder_DetailView;";

		}

		#endregion

		private DevExpress.ExpressApp.Actions.SimpleAction CancelOrder;
		private DevExpress.ExpressApp.Actions.SimpleAction CalculatePrices;
        private DevExpress.ExpressApp.Actions.SimpleAction SaveOrder;
    }
}
