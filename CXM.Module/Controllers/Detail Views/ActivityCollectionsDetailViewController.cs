﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using CXM.Module.BusinessObjects.CXM;
using System.Data.SqlClient;
using DevExpress.ExpressApp.Xpo;
//using CXM.Module.ProfitStars;

namespace CXM.Module.Controllers.Detail_Views
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ActivityCollectionsDetailViewController : ViewController
    {
        public bool isPartialSave = false, closeView = false;
        //static CollectionDetails payment;
        //IObjectSpace paymentObjSpace;

        public ActivityCollectionsDetailViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.

            var detailView = View as DetailView;

            if (detailView != null && !detailView.IsDisposed)
            {
                var objSpace = detailView.ObjectSpace;

                objSpace.Committing += ObjectSpace_Committing;
                detailView.QueryCanClose += View_QueryCanClose;

                ListPropertyEditor listPropertyEditor = detailView.FindItem("Payments") as ListPropertyEditor;
                if (listPropertyEditor != null)
                {
                    listPropertyEditor.ControlCreated += InvoicesListPropertyEditor_ControlCreated;
                }

                closeView = false;
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();

            var detailView = View as DetailView;

            detailView.ObjectSpace.Committing -= ObjectSpace_Committing;
            detailView.QueryCanClose -= View_QueryCanClose;

            ListPropertyEditor listPropertyEditor = detailView.FindItem("Payments") as ListPropertyEditor;
            if (listPropertyEditor != null)
            {
                listPropertyEditor.ControlCreated -= InvoicesListPropertyEditor_ControlCreated;

                if (listPropertyEditor.Frame != null)
                {
                    Frame listViewFrame = listPropertyEditor.Frame;
                    DeleteObjectsViewController deleteController = listViewFrame.GetController<DeleteObjectsViewController>();
                    if (deleteController != null)
                    {
                        deleteController.DeleteAction.Executed -= InvoicesDeleteAction_Executed;
                    }
                }
            }
        }

        void ShowWarning(string warning) => Application.ShowViewStrategy.ShowMessage(warning, InformationType.Warning, 5000, InformationPosition.Top);

        private void ObjectSpace_Committing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                // isPartialSave will only be false when the Save and Close button is pressed.
                if (!isPartialSave)
                {
                    var detailView = ((sender as IObjectSpace).Owner as DetailView);
                    var collection = detailView.CurrentObject as Collections;

                    if (collection != null) 
                    {
                        int collectionID;
                        int.TryParse(((XPObjectSpace)View.ObjectSpace).Session.ExecuteScalar("SELECT MAX(CollectionID) + 1 FROM Collections;").ToString(), out collectionID);
                        if (collectionID == 0)
                        {
                            collectionID = 1;
                        }
                        collection.CollectionID = collectionID;

                        collection.TransStatus = Enums.Trans_Status.ReadyToSend;
                        collection.Save();

                        closeView = true;

                        if (View.Id == "Activity_Collections_DetailView")
                        {
                            View.ObjectSpace.Committing -= ObjectSpace_Committing;
                        }
                    }
                }
            }
            catch (SqlException sqlEx)
            {
                throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
            }
        }

        private void View_QueryCanClose(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // The view can only be close if SalesOrder.ServerPricingStatus == 1.
            e.Cancel = !closeView;
        }

        private void InvoicesListPropertyEditor_ControlCreated(object sender, EventArgs e)
        {
            try
            {
                ListPropertyEditor listPropertyEditor = sender as ListPropertyEditor;

                if (listPropertyEditor != null)
                {
                    listPropertyEditor.ControlCreated -= InvoicesListPropertyEditor_ControlCreated;

                    Frame listViewFrame = listPropertyEditor.Frame;
                    DeleteObjectsViewController deleteController = listViewFrame.GetController<DeleteObjectsViewController>();
                    if (deleteController != null)
                    {
                        deleteController.DeleteAction.Executed += InvoicesDeleteAction_Executed;
                    }
                }
            }
            catch (SqlException sqlEx)
            {
                throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
            }
        }

        private void CancelCollection_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                if (View.Id == "Activity_Collections_DetailView")
                {
                    View.ObjectSpace.Committing -= ObjectSpace_Committing;
                }

                Collections collection = View.ObjectSpace.GetObject(((View as DetailView).CurrentObject as Collections));
                collection.TransStatus = Enums.Trans_Status.Deleted;
                
                View.ObjectSpace.CommitChanges();

                closeView = true;
                View.Close();
            }
            catch (SqlException sqlEx)
            {
                throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
            }
        }

        private void InvoicesDeleteAction_Executed(object sender, ActionBaseEventArgs e)
        {
            try
            {
                Collections collection = (Collections)View.CurrentObject;

                if (collection != null)
                {
                    collection.TransStatus = Enums.Trans_Status.New;
                }

                isPartialSave = true;
                View.ObjectSpace.CommitChanges();
                isPartialSave = false;
            }
            catch (SqlException sqlEx)
            {
                throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
            }
        }

        private void Checkout_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                Collections collection = (View as DetailView).CurrentObject as Collections;
                if (collection != null)
                {
                    if (collection.Payments.Count == 0)
                    {
                        ShowWarning("There are no invoices in the collection.");
                        return;
                    }
                    if (collection.AppliedAmount == 0)
                    {
                        ShowWarning("The balance to pay is zero (0). Please review the transaction.");
                        return;
                    }

                    var paymentObjSpace = Application.CreateObjectSpace(typeof(CollectionDetails));
                    var payment = paymentObjSpace.CreateObject<CollectionDetails>();
                    payment.Collection = paymentObjSpace.GetObject<Collections>((View as DetailView).CurrentObject as Collections);
                    payment.PaymentAmount = payment.Collection.AppliedAmount;
                    payment.CardholderName = payment.Collection.Customer.Name;
                    payment.Index = 0;

                    e.ShowViewParameters.CreatedView = Application.CreateDetailView(paymentObjSpace, "Activity_CollectionDetails_DetailView", false, payment);

                    DialogController dialogController = new DialogController();
                    dialogController.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(CheckoutDialogController_Accepting);
                    e.ShowViewParameters.Controllers.Add(dialogController);
                    e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
                }
            }
            catch (SqlException sqlEx)
            {
                throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
            }
        }

        void CheckoutDialogController_Accepting(object sender, DialogControllerAcceptingEventArgs e)
        {
            var detailView = ((sender as DialogController).Frame.View as DetailView);
            CollectionDetails collectionDetail = detailView.CurrentObject as CollectionDetails;

            if (collectionDetail != null)
            {
                var paymentValidation = Globals.PaymentValidationMessage(collectionDetail);
                if (paymentValidation.Length > 0)
                {
                    e.Cancel = true;
                    throw new Exception(paymentValidation);
                }

                List<GlobalParameters> globalParametersList = View.ObjectSpace.GetObjectsQuery<GlobalParameters>().ToList();
                Dictionary<string, string> globalParameters = new Dictionary<string, string>();

                foreach (var parameter in globalParametersList)
                {
                    globalParameters.Add(parameter.Name, parameter.Value);
                }

                //PS_Context.SetCredentials(globalParameters);

                Globals.ProcessPaymentWithDynamicsPayments(detailView, collectionDetail);

                View.ObjectSpace.CommitChanges();
                View.Close();
            }
        }
    }
}
