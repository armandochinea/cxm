﻿namespace CXM.Module.Controllers
{
	partial class AgentCallsDetailViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.AgentEndCall = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.AgentNewIssue = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.AgentShowLastInvoices = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.AgentShowAging = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.AgentNewOrder = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.EditNotes = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.ShowDueInvoices = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.RescheduleCall = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.AgentNewCollection = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // AgentEndCall
            // 
            this.AgentEndCall.Caption = "End Call";
            this.AgentEndCall.Category = "Edit";
            this.AgentEndCall.ConfirmationMessage = null;
            this.AgentEndCall.Id = "AgentEndCall";
            this.AgentEndCall.TargetViewId = "";
            this.AgentEndCall.ToolTip = null;
            this.AgentEndCall.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AgentEndCall_Execute);
            // 
            // AgentNewIssue
            // 
            this.AgentNewIssue.AcceptButtonCaption = "Save";
            this.AgentNewIssue.CancelButtonCaption = "";
            this.AgentNewIssue.Caption = "New Issue";
            this.AgentNewIssue.Category = "AgentNewIssue";
            this.AgentNewIssue.ConfirmationMessage = null;
            this.AgentNewIssue.Id = "AgentNewIssue";
            this.AgentNewIssue.ToolTip = null;
            this.AgentNewIssue.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.AgentNewIssue_CustomizePopupWindowParams);
            // 
            // AgentShowLastInvoices
            // 
            this.AgentShowLastInvoices.AcceptButtonCaption = null;
            this.AgentShowLastInvoices.CancelButtonCaption = null;
            this.AgentShowLastInvoices.Caption = "Show Last Invoices";
            this.AgentShowLastInvoices.Category = "ShowLastInvoices";
            this.AgentShowLastInvoices.ConfirmationMessage = null;
            this.AgentShowLastInvoices.Id = "AgentShowLastInvoices";
            this.AgentShowLastInvoices.ImageName = "";
            this.AgentShowLastInvoices.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.Image;
            this.AgentShowLastInvoices.ToolTip = null;
            this.AgentShowLastInvoices.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.AgentShowLastInvoices_CustomizePopupWindowParams);
            // 
            // AgentShowAging
            // 
            this.AgentShowAging.AcceptButtonCaption = null;
            this.AgentShowAging.CancelButtonCaption = null;
            this.AgentShowAging.Caption = "Show Aging";
            this.AgentShowAging.Category = "ShowAging";
            this.AgentShowAging.ConfirmationMessage = null;
            this.AgentShowAging.Id = "AgentShowAging";
            this.AgentShowAging.ImageName = "";
            this.AgentShowAging.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.Image;
            this.AgentShowAging.ToolTip = null;
            this.AgentShowAging.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.AgentShowAging_CustomizePopupWindowParams);
            // 
            // AgentNewOrder
            // 
            this.AgentNewOrder.Caption = "New Order";
            this.AgentNewOrder.Category = "AgentNewOrder";
            this.AgentNewOrder.ConfirmationMessage = null;
            this.AgentNewOrder.Id = "AgentNewOrder";
            this.AgentNewOrder.ToolTip = null;
            this.AgentNewOrder.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AgentNewOrder_Execute);
            // 
            // EditNotes
            // 
            this.EditNotes.AcceptButtonCaption = null;
            this.EditNotes.CancelButtonCaption = null;
            this.EditNotes.Caption = "Edit Notes";
            this.EditNotes.Category = "EditNotes";
            this.EditNotes.ConfirmationMessage = null;
            this.EditNotes.Id = "EditNotes";
            this.EditNotes.ToolTip = null;
            this.EditNotes.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.EditNotes_CustomizePopupWindowParams);
            // 
            // ShowDueInvoices
            // 
            this.ShowDueInvoices.AcceptButtonCaption = null;
            this.ShowDueInvoices.CancelButtonCaption = null;
            this.ShowDueInvoices.Caption = "Show Due Invoices";
            this.ShowDueInvoices.Category = "ShowDueInvoices";
            this.ShowDueInvoices.ConfirmationMessage = null;
            this.ShowDueInvoices.Id = "ShowDueInvoices";
            this.ShowDueInvoices.ImageName = "";
            this.ShowDueInvoices.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.Image;
            this.ShowDueInvoices.ToolTip = null;
            this.ShowDueInvoices.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.ShowDueInvoices_CustomizePopupWindowParams);
            // 
            // RescheduleCall
            // 
            this.RescheduleCall.AcceptButtonCaption = null;
            this.RescheduleCall.CancelButtonCaption = null;
            this.RescheduleCall.Caption = "Reschedule";
            this.RescheduleCall.Category = "Edit";
            this.RescheduleCall.ConfirmationMessage = null;
            this.RescheduleCall.Id = "RescheduleCall";
            this.RescheduleCall.ToolTip = null;
            this.RescheduleCall.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.RescheduleCall_CustomizePopupWindowParams);
            // 
            // AgentNewCollection
            // 
            this.AgentNewCollection.Caption = "New Collection";
            this.AgentNewCollection.Category = "AgentNewCollection";
            this.AgentNewCollection.ConfirmationMessage = null;
            this.AgentNewCollection.Id = "AgentNewCollection";
            this.AgentNewCollection.ToolTip = null;
            this.AgentNewCollection.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AgentNewCollection_Execute);
            // 
            // AgentCallsDetailViewController
            // 
            this.Actions.Add(this.AgentEndCall);
            this.Actions.Add(this.AgentNewIssue);
            this.Actions.Add(this.AgentShowLastInvoices);
            this.Actions.Add(this.AgentShowAging);
            this.Actions.Add(this.AgentNewOrder);
            this.Actions.Add(this.EditNotes);
            this.Actions.Add(this.ShowDueInvoices);
            this.Actions.Add(this.RescheduleCall);
            this.Actions.Add(this.AgentNewCollection);
            this.TargetViewId = "Agent_Calls_DetailView";

		}

		#endregion
		private DevExpress.ExpressApp.Actions.SimpleAction AgentEndCall;
		private DevExpress.ExpressApp.Actions.PopupWindowShowAction AgentNewIssue;
		private DevExpress.ExpressApp.Actions.PopupWindowShowAction AgentShowLastInvoices;
		private DevExpress.ExpressApp.Actions.PopupWindowShowAction AgentShowAging;
		private DevExpress.ExpressApp.Actions.SimpleAction AgentNewOrder;
		private DevExpress.ExpressApp.Actions.PopupWindowShowAction EditNotes;
		private DevExpress.ExpressApp.Actions.PopupWindowShowAction ShowDueInvoices;
		private DevExpress.ExpressApp.Actions.PopupWindowShowAction RescheduleCall;
        private DevExpress.ExpressApp.Actions.SimpleAction AgentNewCollection;
    }
}
