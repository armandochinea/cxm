﻿namespace CXM.Module.Controllers
{
	partial class CurrentCallViewController
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.CurrentCall = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
			// 
			// CurrentCall
			// 
			this.CurrentCall.Caption = "Current Call";
			this.CurrentCall.Category = "Notifications";
			this.CurrentCall.ConfirmationMessage = null;
			this.CurrentCall.Id = "CurrentCall";
			this.CurrentCall.ToolTip = null;
			this.CurrentCall.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CurrentCall_Execute);
			// 
			// CurrentCallViewController
			// 
			this.Actions.Add(this.CurrentCall);

		}

		#endregion

		private DevExpress.ExpressApp.Actions.SimpleAction CurrentCall;
	}
}
