﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using System.Timers;
using CXM.Module.BusinessObjects.CXM;
using System.Data.SqlClient;
using DevExpress.ExpressApp.Web.Templates.ActionContainers;
using DevExpress.ExpressApp.Web.Templates.ActionContainers.Menu;
using DevExpress.Web;
using System.Web.UI;
using System.Web.Services;
using DevExpress.ExpressApp.Web;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp.Web.Templates;
using static CXM.Module.Enums;

namespace CXM.Module.Controllers
{
	// For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
	public partial class ChangeStatusViewController : ViewController, IXafCallbackHandler
	{

		ASPxTimer _timer;
		XafCallbackManager _callbackManager;

		static double timeAvailable = 0;
		string format = @"hh\:mm\:ss";
		static DateTime availStartTimeStamp;
		static UserStatus currentStatus = UserStatus.Unset;
		static UserStatus previousStatus = UserStatus.Unset;
		private Page page;
		public static Dictionary<int, string> UserStatusDesc = new Dictionary<int, string>() { { -1, null }, { 0, "Available" }, { 1, "Break Time" }, { 2, "Meal Time" }, { 3, "Admin/Meeting" } };

		public ChangeStatusViewController() : base()
		{
			InitializeComponent();
			TargetViewNesting = Nesting.Root;
		}

		protected override void OnViewControlsCreated()
		{
			base.OnViewControlsCreated();

			try
			{
				var user = (IPermissionPolicyUser)SecuritySystem.CurrentUser;
				var isAgent = user.Roles.Any(r => r.Name.Contains("Agent"));
				this.StatusList.Active["OnlyAgents"] = isAgent;
				if (!isAgent) { return; }

				Application.LoggingOff += Application_LoggingOff;

				var objectSpace = View.ObjectSpace;
				UserAvailability userAvailability = objectSpace.FindObject<UserAvailability>(CriteriaOperator.Parse("UserID == ? && AvailableDate >= ?", SecuritySystem.CurrentUserName, DateTime.Now.Date));
				if (userAvailability != null)
				{
					if (timeAvailable == 0)
					{
						timeAvailable = userAvailability.TimeAvailable;
					}
				}

				UserActivity userActivity = objectSpace.FindObject<UserActivity>(CriteriaOperator.Parse("UserID == ? && ActivityDate == ?", SecuritySystem.CurrentUserName, DateTime.Now.Date));
				if (userActivity != null)
				{
					foreach (var key in UserStatusDesc.Keys)
					{
						if (UserStatusDesc[key] == userActivity.PreviousStatus)
						{
							previousStatus = (UserStatus)key;
						}

						if (UserStatusDesc[key] == userActivity.CurrentStatus)
						{
							currentStatus = (UserStatus)key;
						}
					}
				}

				if (currentStatus >= UserStatus.Available)
				{
					StatusList.SelectedIndex = (int)currentStatus;
				}

				// Access and customize the target View control.
				if (_timer != null)
					return;

				

				if (View is ListView && View.IsRoot)
				{
					_callbackManager = GetCallbackManager();
					if (_callbackManager != null)
					{
						_callbackManager.RegisterHandler("callbackHandler", this);
						IMyCustomUpdatePanelHolder holder = Frame.Template as IMyCustomUpdatePanelHolder;
						if (holder != null)
						{
							string strAvailStartTimeStamp = availStartTimeStamp.ToString();

							if (strAvailStartTimeStamp != "1/1/0001 12:00:00 AM")
							{
								if (userAvailability != null)
								{
									if (currentStatus.ToString() == "Available")
									{
										timeAvailable = (DateTime.Now - userAvailability.AvailableDate).TotalSeconds;
									}
									else
									{
										timeAvailable = userAvailability.TimeAvailable;
									
									}
								}
							}
							StatusList.Caption = string.Format("Total Available Time: {0}", TimeSpan.FromSeconds(timeAvailable).ToString(format));
							var function = "var counter='" + timeAvailable.ToString() + "'; " +
								"function jsCallServerSideMethod(e)  { " +
									"var statusListLabelId = \"Vertical_SAC_Menu_ITCNT2_xaf_a0_L\";\n" +
									"counter++;\n" +
									"var date = new Date(null);\n" +
									"date.setSeconds(counter);\n" + // specify value for SECONDS here
									"var result = date.toISOString().substr(11, 8);\n" +
									"var domm;\n" +
									"if( document.getElementById ) { " +
										"domm = document.getElementById(statusListLabelId);" +
									"} else if (document.all) { " +
										"domm = document.all[statusListLabelId]; " +
									"} else if (document.layers) {" +
										"domm = document.layers[statusListLabelId]; " +
									"}" +
									"domm.innerHTML= \"Total Available Time: \" + result;" +
								"}";

							((WebWindow)this.Frame).RegisterStartupScript("ReturnData", function);
							_timer = new ASPxTimer();
							_timer.Interval = 1000;
							_timer.ID = "timer";
							if (currentStatus.ToString() == "Available")
							{
								_timer.Enabled = true;
							}
							else
							{
								_timer.Enabled = false;
							}
							_timer.ClientSideEvents.Tick = "function(f, e) { jsCallServerSideMethod(' " + DateTime.Now.ToString("hh:mm:ss tt") + "'); }";

							holder.UpdataPanel.Controls.Add(_timer);
						}
					}
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		protected override void OnActivated()
		{
			base.OnActivated();
		}

		protected override void OnDeactivated()
		{
			base.OnDeactivated();
			if (_timer != null)
			{
				_timer.Enabled = false;
			}

			previousStatus = UserStatus.Unset;
			currentStatus = UserStatus.Unset;

			Application.LoggingOff -= Application_LoggingOff;
		}

		private XafCallbackManager GetCallbackManager()
		{
			XafCallbackManager manager = null;
			if (Frame != null && Frame.Template != null)
			{
				ICallbackManagerHolder holder = Frame.Template as ICallbackManagerHolder;
				if (holder != null)
				{
					manager = holder.CallbackManager;
				}
			}
			return manager;
		}

		private void Application_LoggingOff(object sender, LoggingOffEventArgs e)
		{
			try
			{
				if (View != null)
				{
					// Codigo para responder al cambio de estatus
					var objectSpace = View.ObjectSpace; ;

					if (currentStatus == UserStatus.Available)
					{
						var curDateTime = DateTime.Now;
						var currentAvailableSessionTime = curDateTime.Subtract(availStartTimeStamp).TotalSeconds;

						UserAvailability userAvailability = objectSpace.FindObject<UserAvailability>(CriteriaOperator.Parse("UserID == ? && AvailableDate >= ?", SecuritySystem.CurrentUserName, DateTime.Now.Date));
						if (userAvailability == null)
						{
							userAvailability = objectSpace.CreateObject<UserAvailability>();
							userAvailability.UserID = SecuritySystem.CurrentUserName;
							userAvailability.AvailableDate = DateTime.Now;
						}
						userAvailability.TimeAvailable += currentAvailableSessionTime;
					}

					previousStatus = currentStatus;
					currentStatus = UserStatus.Unset;

					UserActivity userActivity = objectSpace.FindObject<UserActivity>(CriteriaOperator.Parse("UserID == ? && ActivityDate == ?", SecuritySystem.CurrentUserName, DateTime.Now.Date));
					if (userActivity == null)
					{
						userActivity = objectSpace.CreateObject<UserActivity>();
						userActivity.UserID = SecuritySystem.CurrentUserName;
						userActivity.ActivityDate = DateTime.Now.Date;
					}

					userActivity.PreviousStatus = UserStatusDesc[(int)previousStatus];
					userActivity.CurrentStatus = UserStatusDesc[(int)currentStatus];

					objectSpace.CommitChanges();

					//****** SUBMIT DB CHANGES FOR VOIP COMMANDS - USER AVAILABILITY
					DevExpress.Xpo.Session session = ((XPObjectSpace)(objectSpace)).Session;
					string qry = string.Format("Insert Into VOIPCommands (UserID, Command) values ('{0}','{1}')", SecuritySystem.CurrentUserId.ToString(), string.Concat("STATUS CHANGE|", currentStatus));
					session.ExecuteQuery(qry);

					timeAvailable = 0;
				}
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		private void statusList_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
		{
			try
			{
				// Codigo para responder al cambio de estatus
				var objectSpace = View.ObjectSpace;

				previousStatus = currentStatus;
				currentStatus = (UserStatus)StatusList.SelectedIndex;

				if (currentStatus == UserStatus.Available)
				{
					availStartTimeStamp = DateTime.Now;
					_timer.Enabled = true;

					UserAvailability userAvailability = objectSpace.FindObject<UserAvailability>(CriteriaOperator.Parse("UserID == ? && AvailableDate >= ?", SecuritySystem.CurrentUserName, DateTime.Now.Date));
					if (userAvailability == null)
					{
						userAvailability = objectSpace.CreateObject<UserAvailability>();
						userAvailability.UserID = SecuritySystem.CurrentUserName;
						userAvailability.AvailableDate = DateTime.Now;
						userAvailability.TimeAvailable = 0;
					}

					WebWindow.CurrentRequestWindow.RegisterClientScript("DisableTimer", "counter='" + userAvailability.TimeAvailable.ToString() + "'; var editor = ASPxClientControl.GetControlCollection().GetByName('" + _timer.ClientID + "'); editor.SetEnabled(true);" +
						"timer.SetEnabled(true);");
				}
				else if (currentStatus > UserStatus.Available && previousStatus == UserStatus.Available)
				{
					((WebWindow)this.Frame).RegisterStartupScript("ReturnData", "");

					//_timer.ClientSideEvents.Tick = "timer.SetEnabled(false);";

					_timer.Enabled = false;

					var curDateTime = DateTime.Now;
					var currentAvailableSessionTime = curDateTime.Subtract(availStartTimeStamp).TotalSeconds;
					timeAvailable = currentAvailableSessionTime;

					WebWindow.CurrentRequestWindow.RegisterClientScript("DisableTimer", "counter='" + currentAvailableSessionTime.ToString() + "'; var editor = ASPxClientControl.GetControlCollection().GetByName('" + _timer.ClientID + "'); editor.SetEnabled(false);" +
						"timer.SetEnabled(false);");

					UserAvailability userAvailability = objectSpace.FindObject<UserAvailability>(CriteriaOperator.Parse("UserID == ? && AvailableDate >= ?", SecuritySystem.CurrentUserName, DateTime.Now.Date));
					if (userAvailability == null)
					{
						userAvailability = objectSpace.CreateObject<UserAvailability>();
						userAvailability.UserID = SecuritySystem.CurrentUserName;
						userAvailability.AvailableDate = DateTime.Now;
					}
					userAvailability.TimeAvailable += currentAvailableSessionTime;
				}

				UserActivity userActivity = objectSpace.FindObject<UserActivity>(CriteriaOperator.Parse("UserID == ? && ActivityDate == ?", SecuritySystem.CurrentUserName, DateTime.Now.Date));
				if (userActivity == null)
				{
					userActivity = objectSpace.CreateObject<UserActivity>();
					userActivity.UserID = SecuritySystem.CurrentUserName;
					userActivity.ActivityDate = DateTime.Now.Date;
				}
				//if (previousStatus != UserStatus.Unset) { userActivity.PreviousStatus = UserStatusDesc[(int)previousStatus]; }
				//if (currentStatus != UserStatus.Unset) { userActivity.CurrentStatus = UserStatusDesc[(int)currentStatus]; }

				userActivity.PreviousStatus = UserStatusDesc[(int)previousStatus];
				userActivity.CurrentStatus = UserStatusDesc[(int)currentStatus];

				objectSpace.CommitChanges();

				//****** SUBMIT DB CHANGES FOR VOIP COMMANDS - USER AVAILABILITY
				DevExpress.Xpo.Session session = ((XPObjectSpace)(objectSpace)).Session;
				string qry = string.Format("Insert Into VOIPCommands (UserID, Command) values ('{0}','{1}')", SecuritySystem.CurrentUserId.ToString(), string.Concat("STATUS CHANGE|", currentStatus));
				session.ExecuteQuery(qry);
			}
			catch (SqlException sqlEx)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}", sqlEx.Message));
			}
			catch (Exception ex)
			{
				throw new Exception(String.Format("An error ocurred while processing the information. MESSAGE: {0}; STACK TRACE: {1}", ex.Message, ex.StackTrace));
			}
		}

		public void ProcessAction(string parameter)
		{
			return;
		}
	}

	public interface IMyCustomUpdatePanelHolder
	{
		XafUpdatePanel UpdataPanel { get; }
	}
}
