﻿namespace CXM.Module.Controllers
{
    partial class ChangeStatusViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem1 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
			DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem2 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
			DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem3 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
			DevExpress.ExpressApp.Actions.ChoiceActionItem choiceActionItem4 = new DevExpress.ExpressApp.Actions.ChoiceActionItem();
			this.StatusList = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
			// 
			// StatusList
			// 
			this.StatusList.Caption = "Status List";
			this.StatusList.Category = "Notifications";
			this.StatusList.ConfirmationMessage = null;
			this.StatusList.Id = "StatusList";
			choiceActionItem1.Caption = "Available";
			choiceActionItem1.Id = "Available";
			choiceActionItem1.ImageName = null;
			choiceActionItem1.Shortcut = null;
			choiceActionItem1.ToolTip = null;
			choiceActionItem2.Caption = "Break Time";
			choiceActionItem2.Id = "BreakTime";
			choiceActionItem2.ImageName = null;
			choiceActionItem2.Shortcut = null;
			choiceActionItem2.ToolTip = null;
			choiceActionItem3.Caption = "Meal Time";
			choiceActionItem3.Id = "Meal Time";
			choiceActionItem3.ImageName = null;
			choiceActionItem3.Shortcut = null;
			choiceActionItem3.ToolTip = null;
			choiceActionItem4.Caption = "Admin/Meeting";
			choiceActionItem4.Id = "AdminMeeting";
			choiceActionItem4.ImageName = null;
			choiceActionItem4.Shortcut = null;
			choiceActionItem4.ToolTip = null;
			this.StatusList.Items.Add(choiceActionItem1);
			this.StatusList.Items.Add(choiceActionItem2);
			this.StatusList.Items.Add(choiceActionItem3);
			this.StatusList.Items.Add(choiceActionItem4);
			this.StatusList.PaintStyle = DevExpress.ExpressApp.Templates.ActionItemPaintStyle.Caption;
			this.StatusList.TargetObjectType = typeof(DevExpress.Xpo.XPBaseObject);
			this.StatusList.ToolTip = null;
			this.StatusList.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.statusList_Execute);
			// 
			// ChangeStatusViewController
			// 
			this.Actions.Add(this.StatusList);

        }

		#endregion

		private DevExpress.ExpressApp.Actions.SingleChoiceAction StatusList;
	}
}
