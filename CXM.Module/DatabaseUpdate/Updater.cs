﻿using System;
using System.Linq;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Updating;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using CXM.Module.BusinessObjects.CXM;

namespace CXM.Module.DatabaseUpdate {
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppUpdatingModuleUpdatertopic.aspx
    public class Updater : ModuleUpdater {
        public Updater(IObjectSpace objectSpace, Version currentDBVersion) :
            base(objectSpace, currentDBVersion) {
        }
        public override void UpdateDatabaseAfterUpdateSchema() {
            base.UpdateDatabaseAfterUpdateSchema();

            /**********************************************************************************************************************************/
            // Creates role with name "Agent" if it doesn't exist and assigns it to user "Agent1".
            PermissionPolicyRole agentRole = CreateDefaultUserRole("Agent");

            // Creates user "Agent1" if it doesn't exists.
            CxmUser agent = ObjectSpace.FindObject<CxmUser>(new BinaryOperator("UserName", "Agent1"));
            if(agent == null) {
                agent = ObjectSpace.CreateObject<CxmUser>();
                agent.UserName = "Agent1";
                agent.SetPassword("");
                agent.Roles.Add(agentRole);
				agent.NextCallNumber = 1;
            }

            /**********************************************************************************************************************************/
            // Creates role with name "Supervisor" if it doesn't exist and assigns it to user "Sup1". Supervisor and Agent roles have the same permissions.
            PermissionPolicyRole supervisorRole = CreateDefaultUserRole("Supervisor");
            
            // Creates user "Sup1" if it doesn't exists.
            CxmUser supervisor = ObjectSpace.FindObject<CxmUser>(new BinaryOperator("UserName", "Sup1"));
            if (supervisor == null)
            {
                supervisor = ObjectSpace.CreateObject<CxmUser>();
                supervisor.UserName = "Sup1";
                supervisor.SetPassword("");
                supervisor.Roles.Add(supervisorRole);
				supervisor.NextCallNumber = 1;
			}

            /**********************************************************************************************************************************/
            // If a role with the Administrators name doesn't exist in the database, create this role
            PermissionPolicyRole adminRole = ObjectSpace.FindObject<PermissionPolicyRole>(new BinaryOperator("Name", "Administrators"));
            if (adminRole == null)
            {
                adminRole = ObjectSpace.CreateObject<PermissionPolicyRole>();
                adminRole.Name = "Administrators";
            }

            CxmUser admin = ObjectSpace.FindObject<CxmUser>(new BinaryOperator("UserName", "Admin"));
            if(admin == null) {
                admin = ObjectSpace.CreateObject<CxmUser>();
                admin.UserName = "Admin";
                // Set a password if the standard authentication type is used
                admin.SetPassword("");
                admin.Roles.Add(adminRole);
            }

            /**********************************************************************************************************************************/
            // If a role with the SysAdmin name doesn't exist in the database, create this role
            PermissionPolicyRole sysAdminRole = ObjectSpace.FindObject<PermissionPolicyRole>(new BinaryOperator("Name", "SysAdmin"));
            if (sysAdminRole == null)
            {
                sysAdminRole = ObjectSpace.CreateObject<PermissionPolicyRole>();
                sysAdminRole.Name = "SysAdmin";
                sysAdminRole.IsAdministrative = true;
                sysAdminRole.CanEditModel = true;
            }

            CxmUser sysAdmin = ObjectSpace.FindObject<CxmUser>(new BinaryOperator("UserName", "ITG"));
            if (sysAdmin == null)
            {
                sysAdmin = ObjectSpace.CreateObject<CxmUser>();
                sysAdmin.UserName = "ITG";
                // Set a password if the standard authentication type is used
                sysAdmin.SetPassword("");
                sysAdmin.Roles.Add(sysAdminRole);
            }

            /**********************************************************************************************************************************/
            SlaTypes responseSlaType = ObjectSpace.FindObject<SlaTypes>(new BinaryOperator("Type", "RESPONSE"));
            if (responseSlaType == null)
            {
                responseSlaType = ObjectSpace.CreateObject<SlaTypes>();
                responseSlaType.Type = "RESPONSE";
            }

            SlaTypes resolutionSlaType = ObjectSpace.FindObject<SlaTypes>(new BinaryOperator("Type", "RESOLUTION"));
            if (resolutionSlaType == null)
            {
                resolutionSlaType = ObjectSpace.CreateObject<SlaTypes>();
                resolutionSlaType.Type = "RESOLUTION";
            }

			/**********************************************************************************************************************************/
			IssueStatus openStatus = ObjectSpace.FindObject<IssueStatus>(new BinaryOperator("Description", "OPEN"));
			if (openStatus == null)
			{
				openStatus = ObjectSpace.CreateObject<IssueStatus>();
				openStatus.Description = "OPEN";
			}

			IssueStatus resolvedStatus = ObjectSpace.FindObject<IssueStatus>(new BinaryOperator("Description", "RESOLVED"));
			if (resolvedStatus == null)
			{
				resolvedStatus = ObjectSpace.CreateObject<IssueStatus>();
				resolvedStatus.Description = "RESOLVED";
			}

			IssueStatus canceledStatus = ObjectSpace.FindObject<IssueStatus>(new BinaryOperator("Description", "CANCELED"));
			if (canceledStatus == null)
			{
				canceledStatus = ObjectSpace.CreateObject<IssueStatus>();
				canceledStatus.Description = "CANCELED";
			}

			/**********************************************************************************************************************************/
			ObjectSpace.CommitChanges(); //This line persists created object(s).
        }

        public override void UpdateDatabaseBeforeUpdateSchema() {
            base.UpdateDatabaseBeforeUpdateSchema();
            //if(CurrentDBVersion < new Version("1.1.0.0") && CurrentDBVersion > new Version("0.0.0.0")) {
            //    RenameColumn("DomainObject1Table", "OldColumnName", "NewColumnName");
            //}
        }

        private PermissionPolicyRole CreateDefaultUserRole(String roleName) {
            PermissionPolicyRole defaultRole = ObjectSpace.FindObject<PermissionPolicyRole>(new BinaryOperator("Name", roleName));
            if(defaultRole == null) {
                defaultRole = ObjectSpace.CreateObject<PermissionPolicyRole>();

                defaultRole.Name = roleName;

				defaultRole.AddObjectPermission<CxmUser>(SecurityOperations.Read, "[Oid] = CurrentUserId()", SecurityPermissionState.Allow);
                defaultRole.AddNavigationPermission(@"Application/NavigationItems/Items/Default/Items/MyDetails", SecurityPermissionState.Allow);
				defaultRole.AddMemberPermission<CxmUser>(SecurityOperations.Write, "ChangePasswordOnFirstLogon", "[Oid] = CurrentUserId()", SecurityPermissionState.Allow);
				defaultRole.AddMemberPermission<CxmUser>(SecurityOperations.Write, "StoredPassword", "[Oid] = CurrentUserId()", SecurityPermissionState.Allow);
                defaultRole.AddTypePermissionsRecursively<PermissionPolicyRole>(SecurityOperations.Read, SecurityPermissionState.Deny);
                defaultRole.AddTypePermissionsRecursively<ModelDifference>(SecurityOperations.ReadWriteAccess, SecurityPermissionState.Allow);
                defaultRole.AddTypePermissionsRecursively<ModelDifferenceAspect>(SecurityOperations.ReadWriteAccess, SecurityPermissionState.Allow);
            }
            return defaultRole;
        }
    }
}
