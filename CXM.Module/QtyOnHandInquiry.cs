﻿using CXM.Module.BusinessObjects.CXM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CXM.Module
{
	#region RESPONSE
	public class QtyOnHandResult
	{
		public Decimal QtyOnHand { get; set; }
		public Fault Fault { get; set; }
	}
	#endregion
}