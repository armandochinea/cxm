﻿using CXM.Module.BusinessObjects.CXM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CXM.Module
{
	#region RESPONSE
	public class OrderStatusResult
	{
		public List<OrderStatusResponse> Orders { get; set; }
		public Fault Fault { get; set; }
	}

	public class OrderStatusResponse
	{
		public OrderStatusHeaderResult Header { get; set; }
		public List<OrderStatusDetailResult> Details { get; set; }
	}

	public class OrderStatusHeaderResult
	{
		public string Route { get; set; }
		public Decimal OrderNumber { get; set; }
		public string OrderType { get; set; }
		public string Warehouse { get; set; }
		public string Customer { get; set; }
		public DateTime OrderDate { get; set; }
		public DateTime ShipDate { get; set; }
		public Decimal PickingNumber { get; set; }
		public DateTime PickingDate { get; set; }
		public Decimal InvoiceNumber { get; set; }
		public DateTime InvoiceDate { get; set; }
		public string TruckerNumber { get; set; }
		public Decimal TotalAmount { get; set; }
		public string HoldCode { get; set; }
	}

	public class OrderStatusDetailResult
	{
		public string Route { get; set; }
		public Decimal OrderNumber { get; set; }
		public string OrderType { get; set; }
		public string Warehouse { get; set; }
		public string Customer { get; set; }
		public string Product { get; set; }
		public Decimal Quantity { get; set; }
		public string UM { get; set; }
		public Decimal UnitPrice { get; set; }
		public Decimal ExtPrice { get; set; }
		public string Status { get; set; }
		public string StatusDescription { get; set; }
	}
	#endregion
}
