﻿//using CXM.Module.BusinessObjects.CXM;
//using ProfitStarsCommon.Entities;
//using System;
//using System.Collections.Generic;

//namespace CXM.Module.ProfitStars
//{
//    public class PS_Context
//    {
//        private static Credentials credentials = new Credentials();
//        private static int timeout = 30;

//        public static void SetCredentials (Dictionary<string, string> globalParameters)
//        {
//            // Default Credentials (Test Environment)
//            //credentials.StoreId = 1446809;
//            //credentials.StoreKey = "b1UCPQH3Mhum81lYd5G5cRxkx+zc";
//            //credentials.EntityId = 222889;
//            //credentials.LocationId = 990133;

//            if (globalParameters.ContainsKey("DP_STORE_ID"))
//            {
//                int storeId;
//                int.TryParse(globalParameters["DP_STORE_ID"].ToString(), out storeId);
//                credentials.StoreId = storeId;
//            }

//            if (globalParameters.ContainsKey("DP_STORE_KEY"))
//            {
//                credentials.StoreKey = globalParameters["DP_STORE_KEY"].ToString();
//            }

//            if (globalParameters.ContainsKey("DP_ENTITY_ID"))
//            {
//                int entityId;
//                int.TryParse(globalParameters["DP_ENTITY_ID"].ToString(), out entityId);
//                credentials.EntityId = entityId;
//            }

//            if (globalParameters.ContainsKey("DP_LOCATION_ID"))
//            {
//                int locationId;
//                int.TryParse(globalParameters["DP_LOCATION_ID"].ToString(), out locationId);
//                credentials.LocationId = locationId;
//            }
//        }

//        public static string RegisterCustomer(CXM.Module.BusinessObjects.CXM.Customers customer)
//        {
//            ProfitStarsCommon.Entities.Customer DPCustomer = new ProfitStarsCommon.Entities.Customer()
//            {
//                CustomerNumber = customer.CustomerID.ToString(),
//                Field1 = string.Empty,
//                Field2 = string.Empty,
//                Field3 = string.Empty,
//                FirstName = customer.Name,
//                LastName = string.Empty,
//                Email = string.Empty
//            };

//            System.Net.SecurityProtocolType activeProtocol = System.Net.ServicePointManager.SecurityProtocol;

//            try
//            {
//                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
//                Response response = PaymentVault.RegisterCustomer(credentials, DPCustomer, timeout);

//				string errorMessage = string.Empty;
//				switch (response.Result)
//				{
//					case ProfitStarsCommon.ResponseResultEnum.Error:
//						errorMessage = $"RESPONSE_ERROR|ErrorMessage: {response.ErrorMessage ?? "N/A"}";
//						break;
//					case ProfitStarsCommon.ResponseResultEnum.Unknown:
//						errorMessage = $"RESPONSE_UNKNOWN|ErrorMessage: {response.ErrorMessage ?? "N/A"}";
//						break;
//					case ProfitStarsCommon.ResponseResultEnum.Exception:
//						errorMessage = $"RESPONSE_EXCEPTION|ErrorMessage: {response.ErrorMessage ?? "N/A"}";
//						break;
//					default:
//						break;
//				}

//				if (response.Result != ProfitStarsCommon.ResponseResultEnum.Success)
//				{
//					//THERE WAS AN ERROR IN THE REQUEST
//					return errorMessage;
//				}
//				else
//                {
//                    return $"OK|Customer registered successfully.";
//                }
//            }
//            catch (Exception ex)
//            {
//                return $"EXCEPTION|{ex.Message}";
//            }
//            finally
//            {
//                System.Net.ServicePointManager.SecurityProtocol = activeProtocol;
//            }
//        }

//        //private string UpdateCustomer(CXM.Module.BusinessObjects.CXM.Customers customer)
//        //{
//        //    ProfitStarsCommon.Entities.Customer DPCustomer = new ProfitStarsCommon.Entities.Customer()
//        //    {
//        //        CustomerNumber = customer.CustomerID.ToString(),
//        //        Field1 = string.Empty,
//        //        Field2 = string.Empty,
//        //        Field3 = string.Empty,
//        //        FirstName = customer.Name,
//        //        LastName = string.Empty,
//        //        Email = string.Empty,
//        //    };

//        //    try
//        //    {
//        //        Response response = PaymentVault.UpdateCustomer(this.credentials, DPCustomer, this.timeout);

//        //        if (response.Result != ProfitStarsCommon.ResponseResultEnum.Success)
//        //        {
//        //            return string.Format("RESPONSEERROR|{0}", response.ErrorMessage);
//        //        } else
//        //        {
//        //            return string.Format("OK|{0}", "Customer updated successfully.");
//        //        }
//        //    }
//        //    catch (Exception ex)
//        //    {
//        //        return string.Format("EXCEPTION|{0}", ex.Message);
//        //    }
//        //}

//        public static string RegisterAccount(CollectionDetails details)
//        {
//            System.Net.SecurityProtocolType activeProtocol = System.Net.ServicePointManager.SecurityProtocol;
//            try
//            {
//                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

//                ProfitStarsCommon.Entities.Account mAccount = new ProfitStarsCommon.Entities.Account()
//                {
//                    CustomerNumber = details.Collection.Customer.CustomerID,
//                    NameOnAccount = details.CardholderName,
//                };

//                string accountNumber = details.PaymentType == Enums.PaymentTypes.ACH ? details.AccountNumber.ToString() : details.CardNumber.ToString();
//                string lastFour = accountNumber.Length < 4 ? accountNumber.Substring(0, 4) : accountNumber.Substring(accountNumber.Length - 4, 4);
//                int accountKeySuffix = details.Collection.Customer.Accounts.Count;

//                string accType = "NONE";
//                mAccount.AccountType = AccountTypeEnum.None;
//                switch (details.AccountType)
//                {
//                    case Enums.AccountTypes.Checking:
//                        accType = "ACH";
//                        mAccount.AccountType = AccountTypeEnum.Checking;
//                        break;
//                    case Enums.AccountTypes.Visa:
//                        accType = "VISA";
//                        mAccount.AccountType = AccountTypeEnum.Visa;
//                        break;
//                    case Enums.AccountTypes.MasterCard:
//                        accType = "MASC";
//                        mAccount.AccountType = AccountTypeEnum.MasterCard;
//                        break;
//                    case Enums.AccountTypes.Discover:
//                        accType = "DISC";
//                        mAccount.AccountType = AccountTypeEnum.Discover;
//                        break;
//                    case Enums.AccountTypes.Amex:
//                        accType = "AMEX";
//                        mAccount.AccountType = AccountTypeEnum.AMEX;
//                        break;
//                    case Enums.AccountTypes.Diners:
//                        accType = "DINE";
//                        mAccount.AccountType = AccountTypeEnum.Diners;
//                        break;
//                    default:
//                        break;
//                }
                
//                mAccount.AccountReferenceID = string.Format($"{details.Collection.Customer.CustomerNo}{accType}{lastFour}{accountKeySuffix}");

//                if (details.PaymentType == Enums.PaymentTypes.ACH)
//                {
//                    mAccount.AccountName = $"Checking account ending in {lastFour}";
//                    mAccount.AccountNumber = details.AccountNumber;
//                    mAccount.RoutingNumber = int.Parse(details.RoutingNumber);
//                }
//                else if (details.PaymentType == Enums.PaymentTypes.CreditCard)
//                {
//                    mAccount.AccountName = $"{details.AccountType.ToString()} ending in {lastFour}";
//                    mAccount.AccountNumber = details.CardNumber;
//                    mAccount.ExpirationMonth = (byte)((int)details.CardExpMonth);

//                    string strYear = details.CardExpYear.ToString();
//                    mAccount.ExpirationYear = (byte)(int.Parse(strYear.Substring(2, 2)));
//                }

//                Response response = PaymentVault.RegisterAccount(credentials, mAccount, timeout);

//				string errorMessage = string.Empty;
//				switch (response.Result)
//				{
//					case ProfitStarsCommon.ResponseResultEnum.Error:
//						errorMessage = $"RESPONSE_ERROR|ErrorMessage: {response.ErrorMessage ?? "N/A"}";
//						break;
//					case ProfitStarsCommon.ResponseResultEnum.Unknown:
//						errorMessage = $"RESPONSE_UNKNOWN|ErrorMessage: {response.ErrorMessage ?? "N/A"}";
//						break;
//					case ProfitStarsCommon.ResponseResultEnum.Exception:
//						errorMessage = $"RESPONSE_EXCEPTION|ErrorMessage: {response.ErrorMessage ?? "N/A"}";
//						break;
//					default:
//						break;
//				}

//				if (response.Result != ProfitStarsCommon.ResponseResultEnum.Success)
//				{
//					//THERE WAS AN ERROR IN THE REQUEST
//					return errorMessage;
//				}
//				else
//                {
//                    return $"{mAccount.AccountReferenceID}|{mAccount.AccountName}";
//                }
//            }
//            catch (Exception ex)
//            {
//                return $"EXCEPTION|{ex.Message}";
//            }
//            finally
//            {
//                System.Net.ServicePointManager.SecurityProtocol = activeProtocol;
//            }
//        }

//        //private void UpdateAccount(CXM.Module.BusinessObjects.CXM.Account account)
//        //{
//        //    try
//        //    {

//        //        ProfitStarsCommon.Entities.Account DPAccount = new ProfitStarsCommon.Entities.Account()
//        //        {
//        //            CustomerNumber = account.Customer.CustomerID.ToString(),                    //CUSTOMER NUMBER USED IN REGISTERCUSTOMER
//        //            AccountType = AccountTypeEnum.None,     //Checking = 0, Visa = 6, MasterCard = 7, Discover = 8, AMEX = 9, Diners = 10
//        //            NameOnAccount = "",                     //NAME ON CARD OR BANK ACCOUNT
//        //            AccountName = "",                       //ACCOUNT NICK NAME (TO BE USED IN PROFIT STARS
//        //            AccountNumber = "",                     //CARD NUMBER OR BANK ACCOUNT NUMBER
//        //            RoutingNumber = 0,                      //ROUTING NUMBER FOR BANK ACCOUNT
//        //            ExpirationMonth = 0,                    //EXPIRATION MONTH FOR CREDIT CARD
//        //            ExpirationYear = 0,                     //EXPIRATION YEAR FOR CREDIT CARD
//        //            AccountReferenceID = "",                //UNIQUE KEY FOR THIS ACCOUNT, GENERATED BY YOUR SYSTEM
//        //        };

//        //        Response response = PaymentVault.UpdateAccount(this.credentials, DPAccount, this.timeout);

//        //        if (response.Result != ProfitStarsCommon.ResponseResultEnum.Success)
//        //        {
//        //            Console.WriteLine(response.ErrorMessage);
//        //        }
//        //    }
//        //    catch (Exception ex)
//        //    {
//        //        Console.WriteLine(ex.Message);
//        //    }
//        //}

//        public static string UnregisterAccount(string accountKey)
//        {
//            System.Net.SecurityProtocolType activeProtocol = System.Net.ServicePointManager.SecurityProtocol;
//            try
//            {
//                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
//                string accountReferenceId = accountKey;
//                string reason = "Demo registry";

//                Response response = PaymentVault.UnregisterAccount(credentials, accountReferenceId, reason, timeout);

//				string errorMessage = string.Empty;
//				switch (response.Result)
//				{
//					case ProfitStarsCommon.ResponseResultEnum.Error:
//						errorMessage = $"RESPONSE_ERROR|ErrorMessage: {response.ErrorMessage ?? "N/A"}";
//						break;
//					case ProfitStarsCommon.ResponseResultEnum.Unknown:
//						errorMessage = $"RESPONSE_UNKNOWN|ErrorMessage: {response.ErrorMessage ?? "N/A"}";
//						break;
//					case ProfitStarsCommon.ResponseResultEnum.Exception:
//						errorMessage = $"RESPONSE_EXCEPTION|ErrorMessage: {response.ErrorMessage ?? "N/A"}";
//						break;
//					default:
//						break;
//				}

//				if (response.Result != ProfitStarsCommon.ResponseResultEnum.Success)
//                {
//					//THERE WAS AN ERROR IN THE REQUEST
//					return errorMessage;
//                }

//                return "OK|Account successfully unregistered.";
//            }
//            catch (Exception ex)
//            {
//                return $"EXCEPTION|{ex.Message}";
//            }
//            finally
//            {
//                System.Net.ServicePointManager.SecurityProtocol = activeProtocol;
//            }
//        }

//        public static string SalesFromBankAccount(CXM.Module.BusinessObjects.CXM.Account account, CollectionDetails details)
//        {
//            System.Net.SecurityProtocolType activeProtocol = System.Net.ServicePointManager.SecurityProtocol;
//            try
//            {
//                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
//				ProfitStarsCommon.Entities.Payment DPPayment = new ProfitStarsCommon.Entities.Payment()
//				{
//					AccountReferenceID = account.AccountKey,
//					Amount = details.PaymentAmount,
//					TerminalNumber = "__WebService",
//					//TransactionNumber = details.Collection.ID.ToString(),
//					Description = account.Description,
//                };

//                SaleResponse response = PaymentVault.SaleFromBankAccount(credentials, DPPayment, timeout);

//				string errorMessage = string.Empty;
//				switch (response.Result)
//				{
//					case ProfitStarsCommon.ResponseResultEnum.Error:
//						errorMessage = $"RESPONSE_ERROR|ErrorMessage: {response.ErrorMessage ?? "N/A"}, ResponseMessage: {response.ResponseMessage ?? "N/A"}";
//						break;
//					case ProfitStarsCommon.ResponseResultEnum.Unknown:
//						errorMessage = $"RESPONSE_UNKNOWN|ErrorMessage: {response.ErrorMessage ?? "N/A"}, ResponseMessage: {response.ResponseMessage ?? "N/A"}";
//						break;
//					case ProfitStarsCommon.ResponseResultEnum.Exception:
//						errorMessage = $"RESPONSE_EXCEPTION|ErrorMessage: {response.ErrorMessage ?? "N/A"}, ResponseMessage: {response.ResponseMessage ?? "N/A"}";
//						break;
//					default:
//						break;
//				}

//				if (response.Result != ProfitStarsCommon.ResponseResultEnum.Success)
//                {
//					//THERE WAS AN ERROR IN THE REQUEST
//					return errorMessage;
//                }

//                if (response.IsDeclined)
//                {
//                    //REQUEST IS SUCCESS BUT PAYMENT WAS DECLINED
//                    return $"DECLINED|{response.ErrorMessage}";
//                }

//                //response.ReferenceNumber = Numero de referencia o AuthNumber
//                return $"OK|{response.ReferenceNumber}";
//            }
//            catch (Exception ex)
//            {
//                return $"EXCEPTION|{ex.Message}";
//            }
//            finally
//            {
//                System.Net.ServicePointManager.SecurityProtocol = activeProtocol;
//            }
//        }

//        public static string SalesFromCardAccount(CXM.Module.BusinessObjects.CXM.Account account, CollectionDetails details)
//        {
//            System.Net.SecurityProtocolType activeProtocol = System.Net.ServicePointManager.SecurityProtocol;
//            try
//            {
//                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
//                ProfitStarsCommon.Entities.Payment DPPayment = new ProfitStarsCommon.Entities.Payment()
//                {
//                    AccountReferenceID = account.AccountKey,
//                    Amount = details.PaymentAmount,
//                    TerminalNumber = "__WebService",
//                    Description = account.Description,
//                };

//				if (details.CVVNumber != string.Empty)
//				{
//					int cvv = 0;
//					int.TryParse(details.CVVNumber, out cvv);
//					DPPayment.CCVS = cvv;
//				}

//				SaleResponse response = PaymentVault.SaleFromCardAccount(credentials, DPPayment, timeout);

//				string errorMessage = string.Empty;
//				switch (response.Result)
//				{
//					case ProfitStarsCommon.ResponseResultEnum.Error:
//						errorMessage = $"RESPONSE_ERROR|ErrorMessage: {response.ErrorMessage ?? "N/A"}, ResponseMessage: {response.ResponseMessage ?? "N/A"}";
//						break;
//					case ProfitStarsCommon.ResponseResultEnum.Unknown:
//						errorMessage = $"RESPONSE_UNKNOWN|ErrorMessage: {response.ErrorMessage ?? "N/A"}, ResponseMessage: {response.ResponseMessage ?? "N/A"}";
//						break;
//					case ProfitStarsCommon.ResponseResultEnum.Exception:
//						errorMessage = $"RESPONSE_EXCEPTION|ErrorMessage: {response.ErrorMessage ?? "N/A"}, ResponseMessage: {response.ResponseMessage ?? "N/A"}";
//						break;
//					default:
//						break;
//				}

//				if (response.Result != ProfitStarsCommon.ResponseResultEnum.Success)
//				{
//					//THERE WAS AN ERROR IN THE REQUEST
//					return errorMessage;
//				}

//                if (response.IsDeclined)
//                {
//                    //REQUEST IS SUCCESS BUT PAYMENT WAS DECLINED
//                    return $"DECLINED|{response.ErrorMessage}";
//                }

//                //response.ReferenceNumber = Numero de referencia o AuthNumber
//                return $"OK|{response.ReferenceNumber}";                
//            }
//            catch (Exception ex)
//            {
//                return $"EXCEPTION|{ex.Message}";
//            }
//            finally
//            {
//                System.Net.ServicePointManager.SecurityProtocol = activeProtocol;
//            }
//        }
//    }
//}
