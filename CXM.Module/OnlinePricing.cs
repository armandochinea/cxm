﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CXM.Module
{
	public static class PricingConstants
	{
		public const string DATE_FORMAT = "yyyyMMdd HH:mm:ss";
		public const string XML_DATE_FORMAT = "yyyy-MM-dd";
		public const string XML_TIME_FORMAT = "HH:mm:ss";
		public const string SAP_DATE_FORMAT = "yyyyMMdd";
		public const string SAP_TIME_FORMAT = "HHmmss";
		public const string UNAPPLIED_INVOICE = "000000";
	}

	#region REQUEST
	public class PricingRequest : PricingHeader
	{
		public PricingRequest()
		{

		}
		//public PricingRequest(PricingHeader header) : base(header)
		//{

		//}
		public IEnumerable<PricingProduct> Products { get; set; }
	}

	public class PricingHeader
	{
		public PricingHeader()
		{

		}
		//public PricingHeader(PricingHeader pricingHeader)
		//{
		//    this.CustomerNumber = pricingHeader.CustomerNumber;
		//    this.ShipToNumber = pricingHeader.ShipToNumber;
		//}

		public string PoNumber { get; set; }
		public Int32 CompanyId { get; set; }
		public string CustomerNumber { get; set; }
		public string ShipToNumber { get; set; }
		public Int32 TransType { get; set; }

		public String ShipDate { get; set; }

		public string Route { get; set; }
		public string Warehouse { get; set; }

	}

	public class PricingProduct
	{
		public PricingProduct()
		{
		}

		public Int32 StockInventory { get; set; }
		public Int32 QuantityOrdered { get; set; }
		public String ProductNumber { get; set; }
		public String UnitOfMeasure { get; set; }

		public String GetMaterialNumberForMobile(String materialNumber)
		{
			while (materialNumber.Length > 0 && materialNumber[0] == '0')
			{
				materialNumber = materialNumber.Substring(1, materialNumber.Length - 1);
			}
			return materialNumber;
		}
	}
	#endregion

	#region RESPONSE
	public class PricingResult
	{
		public long CompanyId { get; set; }
		public string CustomerNumber { get; set; }
		public object PoNumber { get; set; }
		public object Route { get; set; }
		public object ShipDate { get; set; }
		public string ShipToNumber { get; set; }
		public long TransType { get; set; }
		public object Warehouse { get; set; }
		public IEnumerable<PricingOption> PricingOptions { get; set; }
		public IEnumerable<ReturnMessage> ReturnMessages { get; set; }
		public IEnumerable<PricingOrderMessage> OrderMessages { get; set; }
		public Fault Fault { get; set; }
	}

	public class Fault
	{
		public Fault(Exception ex, String message = null)
		{
			TypeName = ex?.GetType().Name;
			TypeFullName = ex?.GetType().FullName;
			if (message == null)
			{
				Message = ex.Message;
			}
			else
			{
				Message = message;
			}
			if (ex?.InnerException != null)
			{
				InnerFault = new Fault(ex.InnerException);
			}
		}

		public Fault InnerFault { get; set; }
		public string Message { get; set; }
		public string TypeFullName { get; set; }
		public string TypeName { get; set; }
	}

	public class ReturnMessage
	{
		public Boolean ShowToUser { get; set; }
		public string ErpReference1 { get; set; }
		public string MessageType { get; set; }
		public string Id { get; set; }
		public string Number { get; set; }
		public string Message { get; set; }
		public string Message1 { get; set; }
		public string Message2 { get; set; }
		public string Message3 { get; set; }
		public string Message4 { get; set; }
	}

	public class PricingOrderMessage
	{
		public Int32 compid { get; set; }
		public string route { get; set; }
		public string order_no { get; set; }
		public string product_no { get; set; }
		public string um { get; set; }
		public string idx { get; set; }
		public string messageType { get; set; }
		public string message { get; set; }
		public string messageNumber { get; set; }
		public string messageId { get; set; }
		public string messageReferences { get; set; }
		//public DateTime sqlmessageDateTime { get; set; }
		public string messageDateTime { get; set; }
		//{
		//    get { return sqlmessageDateTime.ToString(PricingConstants.DATE_FORMAT); }
		//    set
		//    {
		//        sqlmessageDateTime = DateTime.Parse(value); // .ParseExact(value, PricingConstants.DATE_FORMAT, null);
		//    }
		//}

	}

	public class PricingOption
	{
		public enum PricingOptionTypes
		{
			None = 0,
			BasePrice = 1,
			Discount = 2,
			PriceContract = 4,
			NetPrice = 8,
			FreeCases = 16,
			Adjustment = 32,
			Tax = 64,
			Informative = 128,
			Assortment = 256, 
			FreeCasesAssortment = FreeCases | Assortment
		}

		public enum PricingOptions
		{
			None = 0,
			UserDecision = 1,
			ContractRestrictions = 2,
			UserCanChange = 4,
			AutoApply = 8,
			OrderedQty = 16,
			Mandatory = 32,
			Show = 64,
			UserDecisionAndCanChange = UserDecision | UserCanChange,
			UserDecisionAndAutoApply = UserDecision | AutoApply,
			CanChangeOrderedQty = UserCanChange | OrderedQty,
			UserMandatoryChangeOrderedQty = UserCanChange | OrderedQty | Mandatory
		}

		public enum PricingOptionActions
		{
			None = 0,
			Selected = 1, 
			NotSelected = 2
		}

		public string Description { get; set; }
		public Int32 Sequence { get; set; }
		public Int32 PricingOptionType { get; set; }
		public Int32 Options { get; set; }
		public Decimal Amount { get; set; }
		public string ConditionType { get; set; }
		public string ConditionNumber { get; set; }

		public Int32 QuantityOrdered { get; set; }
		public String ProductNumber { get; set; }
		public String UnitOfMeasure { get; set; }

		public string BasisCode { get; set; }
		public string PromoCode { get; set; }
		public string Discretional { get; set; }
		public string FixedVariable { get; set; }
		public string FreeGoodProductNumber { get; set; }
		public string FreeGoodUnitOfMeasure { get; set; }

		private void Initialize(PricingProduct product)
		{
			this.QuantityOrdered = product.QuantityOrdered;
			this.UnitOfMeasure = product.UnitOfMeasure;
			this.ProductNumber = product.ProductNumber;
		}
	}
	#endregion
}
