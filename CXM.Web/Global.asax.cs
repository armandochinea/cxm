﻿using System;
using System.Configuration;
using System.Web.Configuration;
using System.Web;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Web;
using DevExpress.Web;
using System.Linq;
using CXM.Module.BusinessObjects.CXM;

namespace CXM.Web {
    public class Global : System.Web.HttpApplication {
		static string mUserName;

        public Global() {
            InitializeComponent();
        }
        protected void Application_Start(Object sender, EventArgs e) {
            DevExpress.ExpressApp.FrameworkSettings.DefaultSettingsCompatibilityMode = DevExpress.ExpressApp.FrameworkSettingsCompatibilityMode.v20_1;
			SecurityAdapterHelper.Enable();
            ASPxWebControl.CallbackError += new EventHandler(Application_Error);
			WebApplication.EnableMultipleBrowserTabsSupport = true;
#if EASYTEST
            DevExpress.ExpressApp.Web.TestScripts.TestScriptsManager.EasyTestEnabled = true;
#endif
        }

		static void WebApplication_LoggedOn(object sender, LogonEventArgs e)
		{
			mUserName = SecuritySystem.CurrentUserName;
			IObjectSpace os = ((XafApplication)sender).CreateObjectSpace();
			UserLogins LogRecord = os.CreateObject<UserLogins>();
			LogRecord.OnOff = UserLogins.On_Off.LogOn;
			LogRecord.UserName = mUserName;
			LogRecord.TimeStamp = DateTime.Now;
			os.CommitChanges();
		}

		static void WebApplication_LoggedOff(object sender, EventArgs e)
		{
			IObjectSpace os = ((XafApplication)sender).CreateObjectSpace();
			UserLogins LogRecord = os.CreateObject<UserLogins>();
			LogRecord.OnOff = UserLogins.On_Off.LogOff;
			LogRecord.UserName = mUserName;
			LogRecord.TimeStamp = DateTime.Now;
			os.CommitChanges();
		}
		
		protected void Session_Start(Object sender, EventArgs e) {
		    Tracing.Initialize();
            WebApplication.SetInstance(Session, new CXMAspNetApplication());
			DevExpress.ExpressApp.Web.Templates.DefaultVerticalTemplateContentNew.ClearSizeLimit();

			// CODIGO PARA HACER SWITCH DINAMICO DEL FORMATO DE LA PANTALLA.
			/*
			if (HttpContext.Current.Request != null && HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"] != null)
			{
				var u = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"].ToString();

				if (u.Length >= 4)
					if (MobileCheck.IsMatch(u) || MobileVersionCheck.IsMatch(u.Substring(0, 4)))
						WebApplication.Instance.SwitchToNewStyle();
			}
			*/

			if (ConfigurationManager.AppSettings.HasKeys())
			{
				var keys = ConfigurationManager.AppSettings.AllKeys.ToList();
				if (keys.Contains("EnableResponsiveUI"))
				{
					var values = ConfigurationManager.AppSettings.GetValues("EnableResponsiveUI");
					if (values.Last() == "True")
					{
						WebApplication.Instance.SwitchToNewStyle();
					}
                }
			}

			if (ConfigurationManager.ConnectionStrings["ConnectionString"] != null) {
                WebApplication.Instance.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }

			WebApplication.Instance.LoggedOn += WebApplication_LoggedOn;
			WebApplication.Instance.LoggedOff += WebApplication_LoggedOff;
			
#if EASYTEST
            if(ConfigurationManager.ConnectionStrings["EasyTestConnectionString"] != null) {
                WebApplication.Instance.ConnectionString = ConfigurationManager.ConnectionStrings["EasyTestConnectionString"].ConnectionString;
            }
#endif
			if (System.Diagnostics.Debugger.IsAttached && WebApplication.Instance.CheckCompatibilityType == CheckCompatibilityType.DatabaseSchema) {
                WebApplication.Instance.DatabaseUpdateMode = DatabaseUpdateMode.UpdateDatabaseAlways;
            }
            WebApplication.Instance.Setup();
            WebApplication.Instance.Start();
        }
        protected void Application_BeginRequest(Object sender, EventArgs e) {
        }
        protected void Application_EndRequest(Object sender, EventArgs e) {
        }
        protected void Application_AuthenticateRequest(Object sender, EventArgs e) {
        }
        protected void Application_Error(Object sender, EventArgs e) {
            ErrorHandling.Instance.ProcessApplicationError();
        }
        protected void Session_End(Object sender, EventArgs e) {
			WebApplication.LogOff(Session);
			WebApplication.DisposeInstance(Session);
        }
        protected void Application_End(Object sender, EventArgs e) {
		}
        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
        }
        #endregion
    }
}
