﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Web;
using DevExpress.ExpressApp.Web.Templates;
using DevExpress.ExpressApp.Web.Templates.ActionContainers;
using CXM.Module.Controllers;

public partial class Default : BaseXafPage, IMyCustomUpdatePanelHolder
{
    protected override ContextActionsMenu CreateContextActionsMenu()
    {
        return new ContextActionsMenu(this, "Edit", "RecordEdit", "ObjectsCreation", "ListView", "Reports");
    }
    public override Control InnerContentPlaceHolder
    {
        get
        {
            return Content;
        }
    }
	protected void Page_Init()
	{
		CustomizeTemplateContent += delegate (object sender, CustomizeTemplateContentEventArgs e) {
			DefaultVerticalTemplateContent content = TemplateContent as DefaultVerticalTemplateContent;
			if (content == null) return;
			content.HeaderImageControl.DefaultThemeImageLocation = "";
			content.HeaderImageControl.ImageName = "Logo.png";
		};
	}



    public XafUpdatePanel UpdataPanel
    {
        get { return MyCustomUpdatePanel; }
    }
    //protected void Page_Load(object sender, EventArgs e)
    //{
    //    if (!IsPostBack)
    //    {
    //        lblTime.Text = DateTime.Now.ToString("hh:mm:ss tt");
    //        // Session["time"] = DateTime.Now.AddSeconds(40);
    //    }
    //}
    //protected void Timer1_Tick(object sender, EventArgs e)
    //{
    //    lblTime.Text = DateTime.Now.ToString("hh:mm:ss tt");
    //    //TimeSpan time1 = new TimeSpan();
    //    //time1 = (DateTime)Session["time"] - DateTime.Now;
    //    //if (time1.Seconds <= 0)
    //    //{
    //    //    lblTime.Text = "TimeOut!";
    //    //}
    //    //else
    //    //{
    //    //    lblTime.Text = time1.Seconds.ToString();
    //    //}

    //}
}
