﻿function InitalizejQuery() {
    $('.draggable').draggable({
        helper: 'clone',
        start: function (ev, ui) {
            var $draggingElement = $(ui.helper);
            $draggingElement.width(gridView.GetWidth());
        }
    });
    $('.draggable').droppable({
        activeClass: 'hover',
        tolerance: 'intersect',
        hoverClass: 'activeHover',
        drop: function (event, ui) {
            var draggingSortIndex = ui.draggable.attr('sortOrder');
            var targetSortIndex = $(this).attr('sortOrder');
            MakeAction('DRAGROW| ' + draggingSortIndex + '|' + targetSortIndex);
        }
    });
}

function MakeAction(action) {
    if (gridView.InCallback())
        states.push(action)
    else
        gridView.PerformCallback(action)
}

function gridView_EndCallback(s, e) {
    NextAction();
}

function NextAction() {
    if (states.length != 0) {
        var currentState = states.shift();
        if (currentState == 'MOVEUP' && gridView.cpbtMoveUp_Enabled)
        gridView.PerformCallback(currentState);
            else if (currentState =='MOVEDOWN' && gridView.cpbtMoveDown_Enabled)
        gridView.PerformCallback(currentState);
        else if (currentState.indexOf('DRAGROW') != -1)
            gridView.PerformCallback(currentState);
        else
            NextAction();
    }
}
