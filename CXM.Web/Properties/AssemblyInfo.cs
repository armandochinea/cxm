﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("CXM.Web")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("-")]
[assembly: AssemblyProduct("CXM.Web")]
[assembly: AssemblyCopyright("Copyright © - 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("3d5900ae-111a-45be-96b3-d9e4606ca793")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.2.7.0")]
[assembly: AssemblyFileVersion("1.2.7.0")]

//##################################################################################################################################
// 1.2.7.0 - 2019-12-04 - AJCO
// Corrección para filtrar lista de órdenes para rol B2B, para que sólo muestre las órdenes del usuario.
//##################################################################################################################################
// 1.2.6.0 - 2019-10-25 - AJCO
// Se cambió valor default para el campo de Año de Expiración de tarjetas de crédito al año siguiente al actual.
//##################################################################################################################################
// 1.2.5.0 - 2019-10-16 - AJCO
// Se incluyó campo para entrada de número CVV de tarjetas de crédito.
//##################################################################################################################################
// 1.2.4.0 - 2019-10-09 - AJCO
// Ajustes en manejo de errores en proceso de pagos electrónicos.
//##################################################################################################################################
// 1.2.3.0 - 2019-09-25 - AJCO
// Ajustes para manejo de plan por MultiAgent.
// Se reactivaron controles para el procesamiento de pagos electrónicos.
//##################################################################################################################################
// 1.2.2.0 - 2019-09-04 - AJCO
// Correcciones en pantalla de listado de órdenes con botón de New. Se incluyeron colores para las órdenes de acuerdo a su TransStatus.
// Se incluyó opción de Speed Sales en menú de los Agentes y Supervisores.
//##################################################################################################################################
// 1.2.1.0 - 2019-09-03 - AJCO
// Se cambió formato de búsqueda de Clientes y Productos en la entrada de órdenes.
//##################################################################################################################################
// 1.2.0.0 - 2019-09-02 - AJCO
// Se incluyó rol y funcionalidades para entrada de órdenes rápida (Speed Sales).
//##################################################################################################################################
// 1.1.3.0 - 2019-05-15 - AJCO
// Se apagaron controles para manejo de cobros y pre-pagos en la orden.
//##################################################################################################################################
// 1.1.2.0 - 2019-05-01 - AJCO
// Se incluyeron consultas de Cobros y de Ordenes.
//##################################################################################################################################
// 1.1.1.0 - 2019-04-26 - AJCO
// Ajustes en proceso de toma de ordenes para separar el renglon de Pre-Pago de los TransTypes.
//##################################################################################################################################
// 1.1.0.0 - 2019-04-23 - AJCO
// Ajustes para incluir módulo de cobros e interface de pagos electrónicos con Dynamics Payments.
//##################################################################################################################################
// 1.0.6.0 - 2018-12-14 - AJCO
// Ajustes para incluir funcionalidad para escoger contacto antes de comenzar la llamada y marcar las llamadas completadas 
// con color verde. Se anadió columna de la ruta del cliente en la pantalla del plan.
//##################################################################################################################################
// 1.0.5.0 - 2018-12-13 - AJCO
// Correcciones en el proceso de entrada de órdenes.
//##################################################################################################################################
// 1.0.4.0 - 2018-11-21 - AJCO
// Ajustes y correcciones.
//##################################################################################################################################
// 1.0.3.0 - 2018-11-07 - AJCO
// Ajustes y mejoras.
//##################################################################################################################################
// 1.0.2.0 - 2018-11-07 - AJCO
// Ajustes y mejoras.
//##################################################################################################################################
// 1.0.1.0 - 2018-11-06 - AJCO
// Ajustes y mejoras.
